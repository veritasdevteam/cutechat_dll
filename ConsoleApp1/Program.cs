﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var DLL = Assembly.LoadFile(@"C:\VeritasRepos\CuteChat_DLL\CuteChatReferences\CuteChat.dll");
            var list = GetLoadableTypes(DLL);
            foreach (var item in list)
            {
                Console.WriteLine(">" + item.Name);
                foreach (var prop in item.GetProperties())
                {
                    Console.WriteLine("---->" + prop.Name);
                    if (prop.Attributes.ToString().ToUpper() != "NONE")
                    {
                        Console.WriteLine("-------->" + prop.Attributes);
                        
                    }                    
                }
            }
        }

        public static IEnumerable<Type> GetLoadableTypes(Assembly assembly)
        {
            if (assembly == null) throw new ArgumentNullException(nameof(assembly));
            try
            {
                return assembly.GetTypes();
            }
            catch (ReflectionTypeLoadException e)
            {
                return e.Types.Where(t => t != null);
            }
        }

    }
}
