﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;

[assembly: AllowPartiallyTrustedCallers]
[assembly: AssemblyCompany("CuteSoft")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCopyright("Copyright (c) 2003-2013 CuteSoft")]
[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyDescription("CuteChat from CuteSoft")]
[assembly: AssemblyKeyFile("../../CuteChat.snk")]
[assembly: AssemblyKeyName("")]
[assembly: AssemblyProduct("CuteChat")]
[assembly: AssemblyTitle("CuteChat 5.1 Release 2013-04-21")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("5.1.0.0")]
[assembly: CLSCompliant(true)]
[assembly: CompilationRelaxations(8)]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows=true)]
