using System;

namespace CuteChat
{
	[Serializable]
	public class AppLobby : IChatLobby
	{
		private static string DefaultAvatarChatImageUrl;

		private int _lobbyid = -1;

		private string _title = "";

		private string _topic = "";

		private string _announcement = "";

		private string _pwd = "";

		private int _maxonline = 100;

		private bool _locked = false;

		private bool _allowanonymous = true;

		private string _description = "";

		private string _avatarchatURL = "";

		private string _integration = "";

		private string _adminlist = "";

		private int _historyday = 7;

		private int _maxidleminute = 30;

		private int _historycount = 20;

		private int _autoawayminute = 30;

		private int _sortindex = 0;

		private bool _isavatarchat = false;

		public bool AllowAnonymous
		{
			get
			{
				return this._allowanonymous;
			}
			set
			{
				this._allowanonymous = value;
			}
		}

		public string Announcement
		{
			get
			{
				return this._announcement;
			}
			set
			{
				this._announcement = value;
				if (this._announcement == null)
				{
					this._announcement = "";
				}
			}
		}

		public int AutoAwayMinute
		{
			get
			{
				return this._autoawayminute;
			}
			set
			{
				this._autoawayminute = value;
			}
		}

		public string AvatarChatURL
		{
			get
			{
				if (this._avatarchatURL == "")
				{
					this._avatarchatURL = AppLobby.DefaultAvatarChatImageUrl;
				}
				return this._avatarchatURL;
			}
			set
			{
				this._avatarchatURL = value;
				if (this._avatarchatURL == null)
				{
					this._avatarchatURL = "";
				}
			}
		}

		int CuteChat.IChatLobby.LobbyId
		{
			get
			{
				return this._lobbyid;
			}
		}

		public string Description
		{
			get
			{
				return this._description;
			}
			set
			{
				this._description = value;
				if (this._description == null)
				{
					this._description = "";
				}
			}
		}

		public int HistoryCount
		{
			get
			{
				return this._historycount;
			}
			set
			{
				this._historycount = value;
			}
		}

		public int HistoryDay
		{
			get
			{
				return this._historyday;
			}
			set
			{
				this._historyday = value;
			}
		}

		public string Integration
		{
			get
			{
				return this._integration;
			}
			set
			{
				this._integration = value;
				if (this._integration == null)
				{
					this._integration = "";
				}
			}
		}

		public bool IsAvatarChat
		{
			get
			{
				return this._isavatarchat;
			}
			set
			{
				this._isavatarchat = value;
			}
		}

		public int LobbyId
		{
			get
			{
				return this._lobbyid;
			}
			set
			{
				this._lobbyid = value;
			}
		}

		public bool Locked
		{
			get
			{
				return this._locked;
			}
			set
			{
				this._locked = value;
			}
		}

		public string ManagerList
		{
			get
			{
				return this._adminlist;
			}
			set
			{
				this._adminlist = value;
				if (this._adminlist == null)
				{
					this._adminlist = "";
				}
			}
		}

		public int MaxIdleMinute
		{
			get
			{
				return this._maxidleminute;
			}
			set
			{
				this._maxidleminute = value;
			}
		}

		public int MaxOnlineCount
		{
			get
			{
				return this._maxonline;
			}
			set
			{
				this._maxonline = value;
			}
		}

		public string Password
		{
			get
			{
				return this._pwd;
			}
			set
			{
				this._pwd = value;
				if (this._pwd == null)
				{
					this._pwd = "";
				}
			}
		}

		public int SortIndex
		{
			get
			{
				return this._sortindex;
			}
			set
			{
				this._sortindex = value;
			}
		}

		public string Title
		{
			get
			{
				return this._title;
			}
			set
			{
				this._title = value;
				if (this._title == null)
				{
					this._title = "";
				}
			}
		}

		public string Topic
		{
			get
			{
				return this._topic;
			}
			set
			{
				this._topic = value;
				if (this._topic == null)
				{
					this._topic = "";
				}
			}
		}

		static AppLobby()
		{
			AppLobby.DefaultAvatarChatImageUrl = "~/CuteSoft_Client/CuteChat/AvatarChat/CuteChat4.gif";
		}

		public AppLobby()
		{
		}
	}
}