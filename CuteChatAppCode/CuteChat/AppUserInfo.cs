using System;
using System.Collections.Specialized;

namespace CuteChat
{
	[Serializable]
	public class AppUserInfo : IChatUserInfo
	{
		private string _server;

		private string _public;

		private string _private;

		private string _contacts;

		private string _ignores;

		private string _username;

		private string _dispname;

		private string _desc;

		private string _userid;

		private bool _stored;

		private bool _serverchanged = false;

		private NameValueCollection _servernvc;

		private bool _publicchanged = false;

		private NameValueCollection _publicnvc;

		private bool _privatechanged = false;

		private NameValueCollection _privatenvc;

		public string BuildinContacts
		{
			get
			{
				return this._contacts;
			}
			set
			{
				this._contacts = value;
			}
		}

		public string BuildinIgnores
		{
			get
			{
				return this._ignores;
			}
			set
			{
				this._ignores = value;
			}
		}

		public string Description
		{
			get
			{
				return this._desc;
			}
			set
			{
				this._desc = value;
			}
		}

		public string DisplayName
		{
			get
			{
				return this._dispname;
			}
			set
			{
				this._dispname = value;
			}
		}

		public bool IsStored
		{
			get
			{
				return JustDecompileGenerated_get_IsStored();
			}
			set
			{
				JustDecompileGenerated_set_IsStored(value);
			}
		}

		public bool JustDecompileGenerated_get_IsStored()
		{
			return this._stored;
		}

		public void JustDecompileGenerated_set_IsStored(bool value)
		{
			this._stored = value;
		}

		public string PrivateProperties
		{
			get
			{
				if (this._privatechanged)
				{
					if (this._privatenvc.Count != 0)
					{
						this._private = ChatUtility.GetProperties(this._privatenvc);
					}
					else
					{
						this._private = null;
					}
					this._privatechanged = false;
				}
				return this._private;
			}
			set
			{
				this._private = value;
				this._privatenvc = null;
				this._privatechanged = false;
			}
		}

		public string PublicProperties
		{
			get
			{
				if (this._publicchanged)
				{
					if (this._publicnvc.Count != 0)
					{
						this._public = ChatUtility.GetProperties(this._publicnvc);
					}
					else
					{
						this._public = null;
					}
					this._publicchanged = false;
				}
				return this._public;
			}
			set
			{
				this._public = value;
				this._publicnvc = null;
				this._publicchanged = false;
			}
		}

		public string ServerProperties
		{
			get
			{
				if (this._serverchanged)
				{
					if (this._servernvc.Count != 0)
					{
						this._server = ChatUtility.GetProperties(this._servernvc);
					}
					else
					{
						this._server = null;
					}
					this._serverchanged = false;
				}
				return this._server;
			}
			set
			{
				this._server = value;
				this._servernvc = null;
				this._serverchanged = false;
			}
		}

		public string UserId
		{
			get
			{
				return this._userid;
			}
		}

		public string UserName
		{
			get
			{
				return this._username;
			}
			set
			{
				this._username = value;
			}
		}

		public AppUserInfo(string userid)
		{
			this._userid = userid;
		}

		public string GetPrivateProperty(string name)
		{
			string item;
			if ((this._private == null ? false : this._private.Length != 0))
			{
				if (this._privatenvc == null)
				{
					this._privatenvc = ChatUtility.GetProperties(this._private);
				}
				item = this._privatenvc[name];
			}
			else
			{
				item = null;
			}
			return item;
		}

		public string GetPublicProperty(string name)
		{
			string item;
			if ((this._public == null ? false : this._public.Length != 0))
			{
				if (this._publicnvc == null)
				{
					this._publicnvc = ChatUtility.GetProperties(this._public);
				}
				item = this._publicnvc[name];
			}
			else
			{
				item = null;
			}
			return item;
		}

		public string GetServerProperty(string name)
		{
			string item;
			if ((this._server == null ? false : this._server.Length != 0))
			{
				if (this._servernvc == null)
				{
					this._servernvc = ChatUtility.GetProperties(this._server);
				}
				item = this._servernvc[name];
			}
			else
			{
				item = null;
			}
			return item;
		}

		public void SetPrivateProperty(string name, string value)
		{
			if (this._privatenvc == null)
			{
				if ((this._private == null ? false : this._private.Length != 0))
				{
					this._privatenvc = ChatUtility.GetProperties(this._private);
				}
				else
				{
					this._privatenvc = new NameValueCollection();
				}
			}
			if (this._privatenvc[name] != value)
			{
				if (value != null)
				{
					this._privatenvc[name] = value;
				}
				else
				{
					this._privatenvc.Remove(name);
				}
				this._privatechanged = true;
			}
		}

		public void SetPublicProperty(string name, string value)
		{
			if (this._publicnvc == null)
			{
				if ((this._public == null ? false : this._public.Length != 0))
				{
					this._publicnvc = ChatUtility.GetProperties(this._public);
				}
				else
				{
					this._publicnvc = new NameValueCollection();
				}
			}
			if (this._publicnvc[name] != value)
			{
				if (value != null)
				{
					this._publicnvc[name] = value;
				}
				else
				{
					this._publicnvc.Remove(name);
				}
				this._publicchanged = true;
			}
		}

		public void SetServerProperty(string name, string value)
		{
			if (this._servernvc == null)
			{
				if ((this._server == null ? false : this._server.Length != 0))
				{
					this._servernvc = ChatUtility.GetProperties(this._server);
				}
				else
				{
					this._servernvc = new NameValueCollection();
				}
			}
			if (this._servernvc[name] != value)
			{
				if (value != null)
				{
					this._servernvc[name] = value;
				}
				else
				{
					this._servernvc.Remove(name);
				}
				this._serverchanged = true;
			}
		}
	}
}