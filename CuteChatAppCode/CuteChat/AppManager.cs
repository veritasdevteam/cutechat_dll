using System;
using System.Collections.Specialized;

namespace CuteChat
{
	public class AppManager : ChatManager
	{
		public new AppPortal Portal
		{
			get
			{
				return (AppPortal)base.Portal;
			}
		}

		public AppManager(ChatPlace place) : base(place)
		{
		}

		public bool HandleCTSAppCommand(ChatConnection conn, NameValueCollection nvc, string[] args)
		{
			bool flag;
			if (args[0] != "ADDCONTACTBYNAME")
			{
				flag = false;
			}
			else
			{
				string str = args[1].Trim();
				if (str.Length != 0)
				{
					string registeredUserId = ChatProvider.GetInstance(this.Portal).GetRegisteredUserId(str);
					if (registeredUserId == null)
					{
						base.PushSTCMessage(conn, "SYS_ALERT_MESSAGE", null, new string[] { "ADDCONTACT_INVALIDNAME" });
					}
					else
					{
						if (string.Compare(conn.Identity.UniqueId, registeredUserId, true) == 0)
						{
							base.PushSTCMessage(conn, "SYS_ALERT_MESSAGE", null, new string[] { "ADDCONTACT_CANTADDSELF" });
							flag = true;
							return flag;
						}
						IChatUserInfo userInfo = this.Portal.DataManager.GetUserInfo(registeredUserId);
						if ((userInfo.DisplayName == null ? true : userInfo.DisplayName == ""))
						{
							userInfo.DisplayName = str;
							this.Portal.DataManager.UpdateUserInfo(userInfo);
						}
						this.Portal.DataManager.AddContact(conn.Identity, registeredUserId);
					}
				}
				flag = true;
			}
			return flag;
		}

		public override bool HandleCTSCustom(ChatConnection conn, NameValueCollection nvc, params string[] args)
		{
			return base.HandleCTSCustom(conn, nvc, args);
		}

		public override bool HandleCTSMessage(ChatConnection conn, string msgid, NameValueCollection nvc, string[] args)
		{
			bool flag;
			flag = (msgid != "USER_APPCOMMAND" ? base.HandleCTSMessage(conn, msgid, nvc, args) : this.HandleCTSAppCommand(conn, nvc, args));
			return flag;
		}
	}
}