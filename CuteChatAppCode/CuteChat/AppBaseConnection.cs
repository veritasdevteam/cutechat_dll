using System;

namespace CuteChat
{
	public class AppBaseConnection : ChatConnection
	{
		public AppBaseConnection(ChatPortal portal, ChatIdentity identity) : base(portal, identity)
		{
		}
	}
}