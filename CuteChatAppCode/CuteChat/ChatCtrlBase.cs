using System;
using System.Web.UI;

namespace CuteChat
{
	public class ChatCtrlBase : UserControl
	{
		public ChatCtrlBase()
		{
		}

		protected override void Render(HtmlTextWriter writer)
		{
			using (ResourceContext resourceContext = new ResourceContext(writer))
			{
				base.Render(writer);
			}
		}
	}
}