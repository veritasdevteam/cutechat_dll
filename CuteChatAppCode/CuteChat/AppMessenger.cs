using System;

namespace CuteChat
{
	public class AppMessenger : ChatMessenger
	{
		public new AppManager Manager
		{
			get
			{
				return (AppManager)base.Manager;
			}
		}

		public new AppPortal Portal
		{
			get
			{
				return (AppPortal)base.Portal;
			}
		}

		public AppMessenger(AppPortal portal) : base(portal)
		{
		}

		protected override ChatManager CreateManagerInstance()
		{
			return new AppManager(this);
		}
	}
}