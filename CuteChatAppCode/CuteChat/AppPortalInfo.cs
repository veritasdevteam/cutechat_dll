using System;
using System.Collections;
using System.Collections.Specialized;

namespace CuteChat
{
	[Serializable]
	public class AppPortalInfo : IChatPortalInfo
	{
		private string _name;

		private string _properties;

		private bool _propchanged = false;

		private NameValueCollection _propnvc;

		public string PortalName
		{
			get
			{
				return this._name;
			}
		}

		public string Properties
		{
			get
			{
				if (this._propchanged)
				{
					if (this._propnvc.Count != 0)
					{
						this._properties = ChatUtility.GetProperties(this._propnvc);
					}
					else
					{
						this._properties = null;
					}
					this._propchanged = false;
				}
				return this._properties;
			}
			set
			{
				this._properties = value;
				this._propnvc = null;
				this._propchanged = false;
			}
		}

		public AppPortalInfo(string name)
		{
			this._name = name;
		}

		public string GetProperty(string name)
		{
			string item;
			if (this._propnvc == null)
			{
				if ((this._properties == null ? true : this._properties.Length == 0))
				{
					item = null;
					return item;
				}
				this._propnvc = ChatUtility.GetProperties(this._properties);
			}
			item = this._propnvc[name];
			return item;
		}

		public Hashtable LoadProperties()
		{
			if (this._propnvc == null)
			{
				if ((this._properties == null ? false : this._properties.Length != 0))
				{
					this._propnvc = ChatUtility.GetProperties(this._properties);
				}
				else
				{
					this._propnvc = new NameValueCollection();
				}
			}
			Hashtable hashtables = new Hashtable();
			foreach (string key in this._propnvc.Keys)
			{
				hashtables[key] = this._propnvc[key];
			}
			return hashtables;
		}

		public void SetProperty(string name, string value)
		{
			if (this._propnvc == null)
			{
				if ((this._properties == null ? false : this._properties.Length != 0))
				{
					this._propnvc = ChatUtility.GetProperties(this._properties);
				}
				else
				{
					this._propnvc = new NameValueCollection();
				}
			}
			if (this._propnvc[name] != value)
			{
				if (value != null)
				{
					this._propnvc[name] = value;
				}
				else
				{
					this._propnvc.Remove(name);
				}
				this._propchanged = true;
			}
		}
	}
}