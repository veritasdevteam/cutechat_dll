using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Threading;
using System.Web;
using System.Xml;

namespace CuteChat
{
	public class ChatAjaxHandler : IHttpAsyncHandler, IHttpHandler
	{
		private HttpContext context;

		private string placename;

		private string method;

		private ChatCookie cookie = new ChatCookie();

		private string[] args = new string[0];

		private NameValueCollection nvc = new NameValueCollection();

		private ChatResponse res;

		private ChatPlace place;

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}

		public ChatAjaxHandler()
		{
		}

        public IAsyncResult BeginProcessRequest(HttpContext context, AsyncCallback cb, object extraData)
        {
            IAsyncResult completedAsyncResult;
            string guestName;
            ChatPortal chatPortal = null;
            this.context = context;
            this.method = context.Request.Form["METHOD"];
            if (this.method == "PARTIALMESSENGER")
            {
                this.ExecutePartialMessenger();
                completedAsyncResult = new ChatAjaxHandler.CompletedAsyncResult(cb, extraData);
            }
            else if (this.method != "OPENCONTACT")
            {
                try
                {
                    this.Prepair();
                }
                catch (Exception exception1)
                {
                    Exception exception = exception1;
                    context.Response.StatusCode = 500;
                    context.Response.Write(string.Concat("error ", exception.Message));
                    completedAsyncResult = new ChatAjaxHandler.CompletedAsyncResult(cb, extraData);
                    return completedAsyncResult;
                }
                AppChatIdentity logonIdentity = ChatProvider.Instance.GetLogonIdentity();
                if (!this.IsValidIdentity(logonIdentity))
                {
                    guestName = this.cookie.GuestName;
                    if (guestName != null)
                    {
                        guestName = guestName.Trim();
                    }
                    if ((guestName == null ? true : guestName.Length == 0))
                    {
                        if (ChatWebUtility.GetConfig("AutoGenerateAnonymousName") != "False")
                        {
                            goto Label1;
                        }
                        this.res = new ChatResponse()
                        {
                            Cookie = this.cookie
                        };
                        this.placename = context.Request.Form["PLACE"];
                        if (string.Compare(this.placename, "messenger", true) != 0)
                        {
                            this.res.ReturnCode = "NEEDNAME";
                        }
                        else
                        {
                            this.res.ReturnCode = "NEEDLOGIN";
                        }
                        completedAsyncResult = new ChatAjaxHandler.CompletedAsyncResult(cb, extraData);
                        return completedAsyncResult;
                    }
                    // TJF Label2:
                    string str = ChatWebUtility.InitUniqueId();
                    logonIdentity = new AppChatIdentity(guestName, true, str, ChatWebUtility.GetIP(context));
                }
                if (!ClusterSupport.IsClusterClient)
                {
                    lock (ChatSystem.Instance.GetCurrentPortal())
                    {
                        this.place = chatPortal.GetPlace(this.placename);
                        if (this.place == null)
                        {
                            this.res = new ChatResponse()
                            {
                                Cookie = this.cookie,
                                ReturnCode = "NOPLACE"
                            };
                            completedAsyncResult = new ChatAjaxHandler.CompletedAsyncResult(cb, extraData);
                            return completedAsyncResult;
                        }
                        else if (this.method != "SYNCDATA")
                        {
                            try
                            {
                                if (this.method != "CONNECT")
                                {
                                    if (this.method != "DISCONNECT")
                                    {
                                        throw new Exception(string.Concat("INVALID METHOD:", this.method));
                                    }
                                    this.res = this.place.Manager.Disconnect(logonIdentity, this.cookie);
                                }
                                else
                                {
                                    this.res = this.place.Manager.Connect(logonIdentity, this.cookie, this.nvc);
                                }
                            }
                            catch (Exception exception2)
                            {
                                ChatSystem.Instance.LogException(exception2);
                                throw;
                            }
                        }
                        else
                        {
                            IAsyncResult asyncResult = this.place.Manager.BeginSyncData(logonIdentity, this.cookie, this.args, this.nvc, cb, extraData);
                            if (!asyncResult.CompletedSynchronously)
                            {
                                completedAsyncResult = asyncResult;
                                return completedAsyncResult;
                            }
                            else
                            {
                                this.res = this.place.Manager.EndSyncData(asyncResult);
                                completedAsyncResult = new ChatAjaxHandler.CompletedAsyncResult(cb, extraData);
                                return completedAsyncResult;
                            }
                        }
                    }
                }
                else
                {
                    this.res = ClusterSupport.Client_Request(this.method, this.placename, logonIdentity, this.cookie, this.args, this.nvc);
                }
                completedAsyncResult = new ChatAjaxHandler.CompletedAsyncResult(cb, extraData);
            }
            else
            {
                this.ExecuteOpenContact();
                completedAsyncResult = new ChatAjaxHandler.CompletedAsyncResult(cb, extraData);
            }
            return completedAsyncResult;

            Label1:
            ChatCookie chatCookie = this.cookie;
            Guid guid = Guid.NewGuid();
            string str1 = string.Concat("Guest_", guid.ToString().Substring(0, 4));
            guestName = str1;
            chatCookie.GuestName = str1;
            // TJF goto Label2;
        }

        public void EndProcessRequest(IAsyncResult result)
		{
			if ((this.method == "PARTIALMESSENGER" ? false : this.method != "OPENCONTACT"))
			{
				this.context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
				this.context.Response.Cache.SetAllowResponseInBrowserHistory(false);
				this.context.Response.Expires = 0;
				this.context.Response.ContentType = "text/xml";
				try
				{
					if (this.res == null)
					{
						if ((this.method != "SYNCDATA" ? false : this.place != null))
						{
							ChatPortal portal = this.place.Portal;
							Monitor.Enter(portal);
							try
							{
								this.res = this.place.Manager.EndSyncData(result);
							}
							finally
							{
								Monitor.Exit(portal);
							}
						}
					}
					if (this.res == null)
					{
						throw new Exception("Request not handled");
					}
					this.cookie = this.res.Cookie;
					XmlWriter xmlTextWriter = new XmlTextWriter(this.context.Response.OutputStream, Encoding.UTF8);
					xmlTextWriter.WriteStartElement("response");
					xmlTextWriter.WriteAttributeString("QueuePosition", this.res.QueuePosition.ToString());
					xmlTextWriter.WriteAttributeString("ReturnCode", this.res.ReturnCode);
					if (this.res.ServerMessage != null)
					{
						xmlTextWriter.WriteAttributeString("ServerMessage", this.res.ServerMessage);
					}
					xmlTextWriter.WriteStartElement("cookie");
					xmlTextWriter.WriteAttributeString("ResponseId", this.cookie.ResponseId.ToString());
					if (this.cookie.GuestName != null)
					{
						xmlTextWriter.WriteAttributeString("GuestName", this.cookie.GuestName);
					}
					if (this.cookie.Password != null)
					{
						xmlTextWriter.WriteAttributeString("Password", this.cookie.Password);
					}
					if (this.cookie.ConnectionId != null)
					{
						xmlTextWriter.WriteAttributeString("ConnectionId", this.cookie.ConnectionId.ToString());
					}
					xmlTextWriter.WriteAttributeString("ConnectionKey", this.cookie.ConnectionKey.ToString());
					xmlTextWriter.WriteEndElement();
					if (this.res.Messages != null)
					{
						foreach (string message in this.res.Messages)
						{
							xmlTextWriter.WriteStartElement("message");
							xmlTextWriter.WriteAttributeString("value", message);
							xmlTextWriter.WriteEndElement();
						}
					}
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.Flush();
				}
				catch (Exception exception1)
				{
					Exception exception = exception1;
					this.context.Response.Clear();
					XmlWriter xmlWriter = new XmlTextWriter(this.context.Response.OutputStream, Encoding.UTF8);
					xmlWriter.WriteStartElement("exception");
					xmlWriter.WriteAttributeString("t", exception.GetType().FullName);
					xmlWriter.WriteAttributeString("message", exception.Message);
					xmlWriter.WriteAttributeString("stacktrace", exception.StackTrace);
					xmlWriter.WriteString(exception.ToString());
					xmlWriter.WriteEndElement();
					xmlWriter.Flush();
				}
			}
		}

		private void ExecuteOpenContact()
		{
			AppChatIdentity logonIdentity = ChatProvider.Instance.GetLogonIdentity();
			if (this.IsValidIdentity(logonIdentity))
			{
				string item = this.context.Request.Form["CONTACT"];
				ChatApi.ExecuteOpenContact(logonIdentity, ChatProvider.Instance.ToUserId(item));
			}
			else
			{
				this.context.Response.Write("NEEDLOGIN");
			}
		}

		private void ExecutePartialMessenger()
		{
			AppChatIdentity logonIdentity = ChatProvider.Instance.GetLogonIdentity();
			if (this.IsValidIdentity(logonIdentity))
			{
				this.context.Response.Write(ChatApi.ExecutePartialMessenger(logonIdentity));
			}
			else
			{
				this.context.Response.Write("NEEDLOGIN");
			}
		}

		private bool IsValidIdentity(AppChatIdentity identity)
		{
			return (identity != null ? true : false);
		}

		private void Prepair()
		{
			ChatUtility.CheckQueryString(this.context);
			this.placename = this.context.Request.Form["PLACE"];
			if ((this.placename == null ? true : this.method == null))
			{
				throw new Exception("Invalid Request!");
			}
			int num = int.Parse(this.context.Request.Form["ARGCOUNT"]);
			this.args = new string[num];
			for (int i = 0; i < num; i++)
			{
				this.args[i] = this.context.Request.Form[string.Concat("ARG", i)];
			}
			int num1 = int.Parse(this.context.Request.Form["NVCCOUNT"]);
			for (int j = 0; j < num1; j++)
			{
				string item = this.context.Request.Form[string.Concat("KEY", j)];
				string str = this.context.Request.Form[string.Concat("VAL", j)];
				this.nvc.Add(item, str);
			}
			string item1 = this.context.Request.Form["COOKIE_ResponseId"];
			if (item1 != null)
			{
				this.cookie.ResponseId = int.Parse(item1);
			}
			item1 = this.context.Request.Form["COOKIE_ConnectionId"];
			if (item1 != null)
			{
				this.cookie.ConnectionId = new ChatGuid(item1);
			}
			item1 = this.context.Request.Form["COOKIE_ConnectionKey"];
			if (item1 != null)
			{
				this.cookie.ConnectionKey = new Guid(item1);
			}
			item1 = this.context.Request.Form["COOKIE_GuestName"];
			if (item1 != null)
			{
				this.cookie.GuestName = item1;
			}
			item1 = this.context.Request.Form["COOKIE_Password"];
			if (item1 != null)
			{
				this.cookie.Password = item1;
			}
		}

		public void ProcessRequest(HttpContext context)
		{
			throw new NotImplementedException();
		}

		private class CompletedAsyncResult : IAsyncResult
		{
			public object _state;

			private ManualResetEvent mre;

			public object AsyncState
			{
				get
				{
					return this._state;
				}
			}

			public WaitHandle AsyncWaitHandle
			{
				get
				{
					WaitHandle waitHandle;
					ChatAjaxHandler.CompletedAsyncResult completedAsyncResult = this;
					Monitor.Enter(completedAsyncResult);
					try
					{
						if (this.mre != null)
						{
							this.mre = new ManualResetEvent(true);
						}
						waitHandle = this.mre;
					}
					finally
					{
						Monitor.Exit(completedAsyncResult);
					}
					return waitHandle;
				}
			}

			public bool CompletedSynchronously
			{
				get
				{
					return true;
				}
			}

			public bool IsCompleted
			{
				get
				{
					return true;
				}
			}

			public CompletedAsyncResult(AsyncCallback cb, object state)
			{
				this._state = state;
				if (cb != null)
				{
					cb(this);
				}
			}
		}
	}
}