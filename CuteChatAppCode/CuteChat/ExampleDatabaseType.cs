using System;

namespace CuteChat
{
	internal enum ExampleDatabaseType
	{
		SqlServer,
		OracleOleDb,
		MSAccess
	}
}