using System;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;

namespace CuteChat
{
	public class ResourceContext : IDisposable
	{
		private StringBuilder sb = new StringBuilder();

		private HttpContext context;

		private Strings strings;

		private HttpResponse resp;

		private HtmlTextWriter htw;

		private TextWriter old_writer;

		private TextWriter new_writer;

		private Stream old_stream;

		private ResourceContext.FilterStream new_stream;

		private bool isrelease = false;

		public ResourceContext(HtmlTextWriter htmlwriter) : this(htmlwriter, ResourceContext.LoadResources(null))
		{
		}

		public ResourceContext(HtmlTextWriter htmlwriter, CultureInfo ci) : this(htmlwriter, ResourceContext.LoadResources(ci))
		{
		}

		public ResourceContext(HtmlTextWriter htmlwriter, Strings strs)
		{
			this.strings = strs;
			this.context = HttpContext.Current;
			this.resp = this.context.Response;
			this.htw = htmlwriter;
			this.old_writer = htmlwriter.InnerWriter;
			this.new_writer = new ResourceContext.MyStringWriter(this);
			htmlwriter.InnerWriter = this.new_writer;
			this.old_stream = this.resp.Filter;
			this.new_stream = new ResourceContext.FilterStream(this);
			this.resp.Filter = this.new_stream;
		}

		public void Dispose()
		{
			if (!this.isrelease)
			{
				this.Release();
			}
		}

		private void Flush()
		{
			this.old_writer.Write(this.ReplaseWithResource(this.sb.ToString()));
			this.sb.Length = 0;
		}

		public static Strings LoadResources(CultureInfo ci)
		{
			Strings @string;
			@string = (ci != null ? ChatApi.GetStrings(ci.ToString()) : ChatApi.Strings);
			return @string;
		}

		private void Release()
		{
			if (this.isrelease)
			{
				throw new Exception("Release should not be called twice");
			}
			this.isrelease = true;
			this.Flush();
			this.resp.Filter = this.old_stream;
			this.htw.InnerWriter = this.old_writer;
		}

		private string ReplaceItem(Match m)
		{
			string str;
			string value = m.Groups[1].Value;
			string str1 = this.strings.GetString(value);
			str = (str1 != null ? str1 : string.Concat("{", value, "}"));
			return str;
		}

		public string ReplaseWithResource(string text)
		{
			Regex regex = new Regex("\\[\\[([\\w-_ ]+)\\]\\]", RegexOptions.IgnoreCase | RegexOptions.Compiled);
			text = regex.Replace(text, new MatchEvaluator(this.ReplaceItem));
			return text;
		}

		private class FilterStream : Stream
		{
			private ResourceContext c;

			public override bool CanRead
			{
				get
				{
					return false;
				}
			}

			public override bool CanSeek
			{
				get
				{
					return false;
				}
			}

			public override bool CanWrite
			{
				get
				{
					return true;
				}
			}

			public override long Length
			{
				get
				{
					throw new NotSupportedException();
				}
			}

			public override long Position
			{
				get
				{
					throw new NotSupportedException();
				}
				set
				{
					throw new NotSupportedException();
				}
			}

			public FilterStream(ResourceContext context)
			{
				this.c = context;
			}

			public override void Flush()
			{
				this.c.Flush();
			}

			public override int Read(byte[] buffer, int offset, int count)
			{
				throw new NotSupportedException();
			}

			public override long Seek(long offset, SeekOrigin origin)
			{
				throw new NotSupportedException();
			}

			public override void SetLength(long value)
			{
				throw new NotSupportedException();
			}

			public override void Write(byte[] buffer, int offset, int count)
			{
				this.c.sb.Append(this.c.resp.ContentEncoding.GetString(buffer, offset, count));
			}
		}

		private class MyStringWriter : StringWriter
		{
			private ResourceContext c;

			public MyStringWriter(ResourceContext context) : base(context.sb)
			{
				this.c = context;
			}

			public override void Flush()
			{
				this.c.Flush();
			}
		}
	}
}