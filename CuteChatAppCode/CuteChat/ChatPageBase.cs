using System;
using System.Web.UI;

namespace CuteChat
{
	public class ChatPageBase : System.Web.UI.Page
	{
		public ChatPageBase()
		{
		}

		protected override void OnInit(EventArgs e)
		{
			ChatUtility.CheckQueryString(this.Context);
			base.OnInit(e);
		}

		protected override void Render(HtmlTextWriter writer)
		{
			using (ResourceContext resourceContext = new ResourceContext(writer))
			{
				base.Render(writer);
			}
		}
	}
}