using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;

namespace CuteChat
{
	public class ChatAdminPage : ChatPageBase
	{
		public ChatAdminPage()
		{
		}

		protected override void OnInit(EventArgs e)
		{
			if ((base.Site == null ? true : !base.Site.DesignMode))
			{
				if (ChatWebUtility.GetLogonIdentity() == null)
				{
					base.Response.Redirect(ChatWebUtility.ResolveResource(this.Context, ChatWebUtility.LoginUrl));
				}
				if (!ChatWebUtility.CurrentIdentityIsAdministrator)
				{
					throw new HttpException(403, "Forbidden: You don't have permission to access Admin Console. Please login as admin.");
				}
				base.OnInit(e);
			}
			else
			{
				base.OnInit(e);
			}
		}

		protected override void Render(HtmlTextWriter writer)
		{
			using (ResourceContext resourceContext = new ResourceContext(writer))
			{
				base.Render(writer);
			}
		}
	}
}