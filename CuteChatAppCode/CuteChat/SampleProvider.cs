using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.Caching;
using System.Web.Security;

namespace CuteChat
{
	public class SampleProvider : ChatProvider
	{
		public SampleProvider()
		{
		}

		public static void CreateUser(string loginName, string nickname, string password)
		{
			using (IDbConnection dbConnection = ChatProvider.Instance.CreateConnection())
			{
				dbConnection.Open();
				using (IDbCommand dbCommand = dbConnection.CreateCommand())
				{
					dbCommand.CommandText = string.Concat(new string[] { "INSERT INTO SampleUsers (LoginName,NickName,Password,Email,IsAdmin) VALUES (", SampleProvider.SqlEncode(loginName), ",", SampleProvider.SqlEncode(nickname), ",", SampleProvider.SqlEncode(password), ",'',0)" });
					dbCommand.ExecuteNonQuery();
				}
			}
			loginName = loginName.ToLower();
			HttpRuntime.Cache.Remove(string.Concat("UserInfo:", loginName));
		}

		public override string FindUserLoginName(string nickName)
		{
			string str;
			if (nickName != null)
			{
				using (IDbConnection dbConnection = this.CreateConnection())
				{
					dbConnection.Open();
					using (IDbCommand dbCommand = dbConnection.CreateCommand())
					{
						dbCommand.CommandText = string.Concat("SELECT LoginName FROM SampleUsers WHERE LOWER(NickName)=", SampleProvider.SqlEncode(nickName.ToLower()));
						using (IDataReader dataReader = dbCommand.ExecuteReader())
						{
							if (dataReader.Read())
							{
								str = dataReader.GetString(0);
								return str;
							}
						}
					}
				}
				str = null;
			}
			else
			{
				str = null;
			}
			return str;
		}

		public override AppChatIdentity GetLogonIdentity()
		{
			return base.GetLogonIdentity();
		}

		public override bool GetUserInfo(string loginName, ref string nickName, ref bool isAdmin)
		{
			string str = null;
			return SampleProvider.GetUserInfo(loginName, ref nickName, ref str, ref isAdmin);
		}

		public static bool GetUserInfo(string loginName, ref string nickName, ref string password, ref bool isAdmin)
		{
			bool flag;
			loginName = loginName.ToLower();
			Hashtable item = (Hashtable)HttpRuntime.Cache[string.Concat("UserInfo:", loginName)];
			if (item == null)
			{
				using (IDbConnection dbConnection = ChatProvider.Instance.CreateConnection())
				{
					dbConnection.Open();
					using (IDbCommand dbCommand = dbConnection.CreateCommand())
					{
						dbCommand.CommandText = string.Concat("SELECT * FROM SampleUsers WHERE LOWER(LoginName)=", SampleProvider.SqlEncode(loginName));
						using (IDataReader dataReader = dbCommand.ExecuteReader())
						{
							if (dataReader.Read())
							{
								item = new Hashtable(new CaseInsensitiveHashCodeProvider(), new CaseInsensitiveComparer());
								for (int i = 0; i < dataReader.FieldCount; i++)
								{
									item[dataReader.GetName(i)] = (dataReader.IsDBNull(i) ? null : dataReader.GetValue(i));
								}
							}
							else
							{
								flag = false;
								return flag;
							}
						}
					}
				}
				HttpRuntime.Cache[string.Concat("UserInfo:", loginName)] = item;
			}
			nickName = item["NickName"].ToString();
			password = item["Password"].ToString();
			isAdmin = 1 == Convert.ToInt32(item["IsAdmin"]);
			flag = true;
			return flag;
		}

		private static string SqlEncode(string value)
		{
			string str;
			str = (value != null ? string.Concat("'", value.ToString().Replace("'", "''"), "'") : "NULL");
			return str;
		}

		public static void UpdateNickname(string loginName, string nickName)
		{
			loginName = loginName.ToLower();
			using (IDbConnection dbConnection = ChatProvider.Instance.CreateConnection())
			{
				dbConnection.Open();
				using (IDbCommand dbCommand = dbConnection.CreateCommand())
				{
					dbCommand.CommandText = string.Concat("UPDATE SampleUsers SET NickName=", SampleProvider.SqlEncode(nickName), " WHERE LOWER(LoginName)", SampleProvider.SqlEncode(loginName));
					dbCommand.ExecuteNonQuery();
				}
			}
			HttpRuntime.Cache.Remove(string.Concat("UserInfo:", loginName));
		}

		public static void UpdatePassword(string loginName, string password)
		{
			loginName = loginName.ToLower();
			using (IDbConnection dbConnection = ChatProvider.Instance.CreateConnection())
			{
				dbConnection.Open();
				using (IDbCommand dbCommand = dbConnection.CreateCommand())
				{
					dbCommand.CommandText = string.Concat("UPDATE SampleUsers SET Password=", SampleProvider.SqlEncode(password), " WHERE LOWER(LoginName)=", SampleProvider.SqlEncode(loginName));
					dbCommand.ExecuteNonQuery();
				}
			}
			HttpRuntime.Cache.Remove(string.Concat("UserInfo:", loginName));
		}

		public override bool ValidateUser(string loginName, string pwd)
		{
			bool flag;
			string str = null;
			string str1 = null;
			bool flag1 = false;
			if (!SampleProvider.GetUserInfo(loginName, ref str, ref str1, ref flag1))
			{
				flag = false;
			}
			else if (str1 == pwd)
			{
				FormsAuthentication.SetAuthCookie(loginName, false, "/");
				flag = true;
			}
			else
			{
				flag = false;
			}
			return flag;
		}
	}
}