using System;

namespace CuteChat
{
#pragma warning disable CS0436 // Type conflicts with imported type
	public interface IAppDataProvider : IChatDataProvider, IDisposable
#pragma warning restore CS0436 // Type conflicts with imported type
	{

	}
}