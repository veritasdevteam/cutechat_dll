using System;
using System.Collections.Specialized;
using System.Web;

namespace CuteChat
{
	[ChatServerType]
	public class SupportCustomerHandler : IHttpHandler
	{
		private bool isnewid;

		private static char[] invalidchars;

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}

		static SupportCustomerHandler()
		{
			SupportCustomerHandler.invalidchars = "<>'\"".ToCharArray();
		}

		public SupportCustomerHandler()
		{
		}

		public static string ExecuteRequest(string requestType, ChatIdentity identity, string customerid, string name, string ipaddr, string culture, string platform, string browser, string url, string referrer, string email, string department, string question, string customdata)
		{
			ChatPortal chatPortal = null;
			string str;
			string str1;
			string str2;
			if (!ClusterSupport.IsClusterClient)
			{
				lock (ChatSystem.Instance.GetCurrentPortal())
				{
					SupportAgentChannel place = (SupportAgentChannel)chatPortal.GetPlace("SupportAgent");
					if (requestType == "WAIT")
					{
						str1 = place.HandleCustomerWait(identity, name, ipaddr, culture, platform, browser, url, referrer, email, department, question, customdata, out str2);
						if (str1 != "WAITING")
						{
							str = string.Concat(str1, ":", str2);
							return str;
						}
						else
						{
							str = string.Concat(new string[] { str1, ":", str2, ":", customerid });
							return str;
						}
					}
					else if (requestType == "VISIT")
					{
						str1 = place.HandleCustomerVisit(identity, name, ipaddr, culture, platform, browser, url, referrer, out str2);
						if (str1 == "READY")
						{
							str = string.Concat("READY:", customerid);
							return str;
						}
						else if (str1 == "INVITE")
						{
							str = string.Concat("INVITE:", str2);
							return str;
						}
						else if (str1 != "ERROR")
						{
							str = str1;
							return str;
						}
						else
						{
							str = string.Concat("ERROR:", str2);
							return str;
						}
					}
					else if (requestType == "ACCEPT")
					{
						str1 = place.HandleCustomerAcceptInvite(customerid, out str2);
						if (str1 == "READY")
						{
							str = string.Concat("READY:SupportSession:", customerid);
							return str;
						}
						else if (str1 == "ERROR")
						{
							str = string.Concat("ERROR:", str2);
							return str;
						}
						else if (str1 != "EXPIRED")
						{
							str = str1;
							return str;
						}
						else
						{
							str = "EXPIRED";
							return str;
						}
					}
					else if (requestType == "REJECT")
					{
						str1 = place.HandleCustomerRejectInvite(customerid, out str2);
						str = "READY";
						return str;
					}
				}
				str = string.Concat("ERROR:no handled-", requestType);
			}
			else
			{
				str = (string)ClusterSupport.ExecuteCurrentMethod(new object[] { requestType, identity, customerid, name, ipaddr, culture, platform, browser, url, referrer, email, department, question, customdata });
			}
			return str;
		}

		public void ProcessRequest(HttpContext context)
		{
			context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
			context.Response.Cache.SetAllowResponseInBrowserHistory(false);
			context.Response.Expires = 0;
			context.Response.ContentType = "text/plain";
			string str = ChatWebUtility.InitUniqueId(out this.isnewid);
			string displayName = this.ProtectQueryString(context.Request.QueryString["Name"]);
			string str1 = this.ProtectQueryString(context.Request.QueryString["Email"]);
			string str2 = this.ProtectQueryString(context.Request.QueryString["Department"]);
			string item = context.Request.QueryString["Question"];
			string item1 = context.Request.QueryString["CustomData"];
			string p = ChatWebUtility.GetIP(context);
			string browser = ChatWebUtility.GetBrowser();
			string platform = ChatWebUtility.GetPlatform();
			AppChatIdentity logonIdentity = ChatWebUtility.GetLogonIdentity();
			if ((displayName == null ? true : displayName == ""))
			{
				if (logonIdentity != null)
				{
					displayName = logonIdentity.DisplayName;
				}
				if ((displayName == null ? true : displayName == ""))
				{
					HttpCookie httpCookie = context.Request.Cookies["CCLSGN"];
					if ((httpCookie == null ? false : httpCookie.Value.Length < 20))
					{
						displayName = httpCookie.Value.Trim();
					}
				}
				if ((displayName == null ? true : displayName == ""))
				{
					displayName = str;
					int num = displayName.IndexOf('-');
					if (num != -1)
					{
						displayName = displayName.Substring(0, num);
					}
					displayName = displayName.Replace(':', '\u005F');
				}
			}
			HttpCookie httpCookie1 = new HttpCookie("CCLSGN", displayName);
			if (ChatWebUtility.GetConfig("EnableLongTimeCookies") != "False")
			{
				httpCookie1.Expires = DateTime.Now.AddYears(1);
			}
			context.Response.Cookies.Add(httpCookie1);
			if (logonIdentity == null)
			{
				logonIdentity = new AppChatIdentity(displayName, true, str, ChatWebUtility.GetIP(context));
			}
			string userLanguages = "";
			try
			{
				userLanguages = context.Request.UserLanguages[0] ?? "";
			}
			catch
			{
			}
			string item2 = context.Request.QueryString["Url"];
			if ((item2 == null ? true : item2 == ""))
			{
				Uri urlReferrer = context.Request.UrlReferrer;
				item2 = (urlReferrer == null ? "" : urlReferrer.AbsoluteUri);
			}
			string str3 = this.ProtectQueryString(context.Request.QueryString["Referrer"]) ?? "";
			string str4 = this.ProtectQueryString(context.Request.QueryString["Type"]);
			if ((!this.isnewid ? false : str.StartsWith("Guest:")))
			{
				if (str4 == "WAIT")
				{
					string str5 = str.Substring(6);
					context.Response.Write(string.Concat("WAITING:1:Guest:", str5.ToLower(), "&"));
					return;
				}
				else if (str4 == "VISIT")
				{
					string str6 = str.Substring(6);
					context.Response.Write(string.Concat("READY:Guest:", str6.ToLower(), "&"));
					return;
				}
			}
			context.Response.Write(SupportCustomerHandler.ExecuteRequest(str4, logonIdentity, str, displayName, p, userLanguages, platform, browser, item2, str3, str1, str2, item, item1));
		}

		protected string ProtectQueryString(string val)
		{
			string str;
			if (val != null)
			{
				str = (val.IndexOfAny(SupportCustomerHandler.invalidchars) == -1 ? val : "");
			}
			else
			{
				str = null;
			}
			return str;
		}
	}
}