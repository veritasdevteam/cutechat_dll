using System;
using System.Diagnostics;
using System.Web.Mail;

namespace CuteChat
{
	public class AppSystem : ChatSystem
	{
		public override string DefaultMailAddress
		{
			get
			{
				return ChatProvider.GetInstance(null).DefaultMailAddress;
			}
		}

		public AppSystem()
		{
		}

		protected override ChatPortal CreatePortalInstance(string name)
		{
			return new AppPortal(name);
		}

		public override void DebugMessage(string message, string category)
		{
			Debug.WriteLine(message, category);
		}

		public override ChatIdentity GetCurrentIdentity()
		{
			return ChatProvider.Instance.GetLogonIdentity();
		}

		public override string GetCurrentPortalName()
		{
			return ChatProvider.GetInstance(null).GetCurrentPortalName();
		}

		public override void LogException(Exception error)
		{
			try
			{
				object obj = this.EnterIdentity();
				try
				{
					ChatProvider.GetInstance(null).LogException(error);
				}
				finally
				{
					this.LeaveIdentity(obj);
				}
			}
			catch (Exception exception)
			{
			}
		}

		public override void SendMail(MailMessage msg)
		{
			ChatProvider.GetInstance(null).SendMail(msg);
		}
	}
}