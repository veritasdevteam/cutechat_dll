using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Caching;
using System.Web.UI;

namespace CuteChat
{
	public class ChatFramePage : ChatPageBase
	{
		private Guid _clientid;

		private string _place;

		private ChatFramePage.ClientData _clientdata;

		private string _guestname;

		private string _password;

		private string _reloadurl;

		private string _connectstatus;

		private string _connectstatusmessage;

		public string ChannelTitle
		{
			get
			{
				string str;
				str = (this.chatvars.place == null ? "Title" : this.chatvars.place.PlaceTitle);
				return str;
			}
		}

		private ChatFramePage.ClientData chatvars
		{
			get
			{
				ChatFramePage.ClientData clientDatum;
				if (this._clientdata == null)
				{
					System.Web.Caching.Cache cache = base.Cache;
					Guid clientId = this.ClientId;
					this._clientdata = (ChatFramePage.ClientData)cache.Get(clientId.ToString());
					if (this._clientdata == null)
					{
						this.ResetVars();
					}
					clientDatum = this._clientdata;
				}
				else
				{
					clientDatum = this._clientdata;
				}
				return clientDatum;
			}
		}

		public Guid ClientId
		{
			get
			{
				return this._clientid;
			}
		}

		[Obsolete("take care.. ", false)]
		public override string ClientID
		{
			get
			{
				return base.ClientID;
			}
		}

		public string ConnectStatus
		{
			get
			{
				return this._connectstatus;
			}
			set
			{
				this._connectstatus = value;
			}
		}

		public string ConnectStatusMessage
		{
			get
			{
				return this._connectstatusmessage;
			}
			set
			{
				this._connectstatusmessage = value;
			}
		}

		public string GuestName
		{
			get
			{
				return this._guestname;
			}
			set
			{
				this._guestname = value;
			}
		}

		public string Password
		{
			get
			{
				return this._password;
			}
			set
			{
				this._password = value;
			}
		}

		public string PlaceName
		{
			get
			{
				return this._place;
			}
		}

		public string ReloadURL
		{
			get
			{
				int num;
				if (this._reloadurl == null)
				{
					string rawUrl = base.Request.RawUrl;
					string str = "reloadtime=";
					int num1 = rawUrl.IndexOf(str);
					if (num1 != -1)
					{
						int num2 = rawUrl.IndexOf('&', num1 + str.Length);
						num = (num2 != -1 ? int.Parse(rawUrl.Substring(num1 + str.Length, num2 - (num1 + str.Length))) : int.Parse(rawUrl.Substring(num1 + str.Length)));
						rawUrl = string.Concat(rawUrl.Substring(0, num1), str, num);
					}
					else
					{
						rawUrl = (rawUrl.IndexOf('?') == -1 ? string.Concat(rawUrl, "?", str, "1") : string.Concat(rawUrl, "&", str, "1"));
					}
					this._reloadurl = rawUrl;
				}
				return this._reloadurl;
			}
		}

		public string UrlQuery
		{
			get
			{
				string str = ChatWebUtility.ReplaceParam(base.Request.Url.Query, "GuestName", this.GuestName);
				return ChatWebUtility.ReplaceParam(str, "Password", this.Password);
			}
		}

		public ChatFramePage()
		{
		}

		private void _Trace(string msg)
		{
		}

		private void AppendHtml(string html)
		{
			this.AppendHtml(html, "info");
		}

		private void AppendHtml(string html, string classname)
		{
			this.chatvars.messagehtmls.Enqueue(string.Concat(new string[] { "<div class=", classname, ">", html, "</div>" }));
		}

		public void Connect()
		{
			ChatResponse chatResponse;
			ChatPortal chatPortal = null;
			this.ResetVars();
			this.chatvars.identity = ChatWebUtility.GetLogonIdentity();
			if (this.chatvars.identity == null)
			{
				string str = ChatWebUtility.InitUniqueId();
				this.chatvars.identity = new AppChatIdentity(this.GuestName, true, str, this.Context.Request.UserHostAddress);
			}
			this.chatvars.cookie.GuestName = this.GuestName;
			this.chatvars.cookie.Password = this.Password;
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				try
				{
					this.chatvars.place = chatPortal.GetPlace(this.PlaceName);
					chatResponse = this.chatvars.place.Manager.Connect(this.chatvars.identity, this.chatvars.cookie, null);
				}
				catch (Exception exception1)
				{
					Exception exception = exception1;
					this._connectstatus = "ERROR";
					this._connectstatusmessage = exception.ToString();
					return;
				}
				this._connectstatus = chatResponse.ReturnCode;
				this._connectstatusmessage = chatResponse.ServerMessage;
				if (chatResponse.ReturnCode == "READY")
				{
					this.chatvars.connected = true;
					this.chatvars.cookie = chatResponse.Cookie;
					this.chatvars.myinfo = this.chatvars.place.FindUser(this.chatvars.identity.UniqueId);
					this.AppendHtml("Connected", "Connection");
				}
			}
		}

		public void Disconnect()
		{
			if (this.chatvars.connected)
			{
				//lock (ChatSystem.Instance.GetCurrentPortal())
				lock (this.chatvars.place.Manager)
				{
					this.chatvars.place.Manager.Disconnect(this.chatvars.identity, this.chatvars.cookie);
				}
			}
		}

		private void DoSync()
		{
			if (this.chatvars.stc.Count != 0)
			{
				string[] array = (string[])this.chatvars.stc.ToArray(typeof(string));
				this.chatvars.stc = new ArrayList();
				string[] strArrays = array;
				for (int i = 0; i < (int)strArrays.Length; i++)
				{
					this.HandleSTC(strArrays[i]);
				}
			}
			if (this.chatvars.syncing == null)
			{
				ChatPortal portal = this.chatvars.place.Portal;
				Monitor.Enter(portal);
				try
				{
					string[] array1 = new string[0];
					if (this.chatvars.cts != null)
					{
						array1 = (string[])this.chatvars.cts.ToArray(typeof(string));
						this.chatvars.cts = new ArrayList();
					}
					this.chatvars.syncing = this.chatvars.place.Manager.BeginSyncData(this.chatvars.identity, this.chatvars.cookie, array1, null, new AsyncCallback(ChatFramePage.SyncCallback), this.chatvars);
					if (this.chatvars.syncing.CompletedSynchronously)
					{
						this.chatvars.syncing = null;
					}
				}
				finally
				{
					Monitor.Exit(portal);
				}
			}
		}

#pragma warning disable CS0436
		private string FormatMessage(ChatMsgData msg, string type)
#pragma warning restore CS0436
		{
			string str;
			StringBuilder stringBuilder;
			StringBuilder stringBuilder1;
			StringBuilder stringBuilder2;
			StringBuilder stringBuilder3;
			StringBuilder stringBuilder4;
			string html = msg.Html;
			if ((html == null ? true : html == ""))
			{
				html = msg.Text ?? "";
				html = base.Server.HtmlEncode(html);
			}
			StringBuilder stringBuilder5 = new StringBuilder();
			string str1 = type;
			if (str1 != null)
			{
				if (str1 == "USER")
				{
					stringBuilder5.Append("<table class='");
					if (!msg.Whisper)
					{
						stringBuilder5.Append("UserMessage");
					}
					else
					{
						stringBuilder5.Append("WhisperUserMessage");
					}
					if (this.chatvars.IsMyInfo(msg.SenderId))
					{
						stringBuilder5.Append(" MyMessage");
					}
					stringBuilder5.Append("'><tr><td>");
					ChatPlaceUser chatPlaceUser = this.chatvars.place.FindUser(msg.SenderId);
					if (chatPlaceUser == null)
					{
						stringBuilder5.Append(msg.Sender);
					}
					else
					{
						stringBuilder5.Append(this.FormatUser(chatPlaceUser));
					}
					stringBuilder5.Append("</td><td>");
					if ((msg.TargetId == null ? false : msg.TargetId != ""))
					{
						if (msg.Whisper)
						{
							stringBuilder5.Append(" whisper ");
							stringBuilder5.Append("</td><td>");
						}
						stringBuilder5.Append(" to ");
						chatPlaceUser = this.chatvars.place.FindUser(msg.TargetId);
						if (chatPlaceUser == null)
						{
							stringBuilder5.Append(msg.Target);
						}
						else
						{
							stringBuilder5.Append(this.FormatUser(chatPlaceUser));
						}
					}
					stringBuilder5.Append("</td><td> : </td><td style='");
					stringBuilder5.Append("'>");
					stringBuilder5.Append(html);
					stringBuilder5.Append("</td></tr></table>");
					str = stringBuilder5.ToString();
					return str;
				}
				else
				{
					if (str1 != "SYSTEM")
					{
						if (msg.Sender != null)
						{
							stringBuilder = stringBuilder5.Append(msg.Sender);
							stringBuilder1 = stringBuilder5.Append(" : ");
						}
						stringBuilder2 = stringBuilder5.Append("<span>");
						stringBuilder3 = stringBuilder5.Append(string.Concat(type, " : ", html));
						stringBuilder4 = stringBuilder5.Append("</span>");
						str = stringBuilder5.ToString();
						return str;
					}
					stringBuilder5.Append("<span class='System'>");
					stringBuilder5.Append(html);
					stringBuilder5.Append("</span>");
					str = stringBuilder5.ToString();
					return str;
				}
			}
			if (msg.Sender != null)
			{
				stringBuilder = stringBuilder5.Append(msg.Sender);
				stringBuilder1 = stringBuilder5.Append(" : ");
			}
			stringBuilder2 = stringBuilder5.Append("<span>");
			stringBuilder3 = stringBuilder5.Append(string.Concat(type, " : ", html));
			stringBuilder4 = stringBuilder5.Append("</span>");
			str = stringBuilder5.ToString();
			return str;
		}

		private string FormatUser(ChatPlaceUser user)
		{
			string str;
			StringBuilder stringBuilder;
			StringBuilder stringBuilder1;
			string publicProperty = user.Info.GetPublicProperty("Avatar");
			publicProperty = (publicProperty != null ? string.Concat("DrawAvatar.Ashx?Avatar=", publicProperty) : "Images/defaultavatar.gif");
			StringBuilder stringBuilder2 = new StringBuilder();
			stringBuilder2.Append("<nobr>");
			stringBuilder2.Append("<img src='");
			stringBuilder2.Append(publicProperty);
			stringBuilder2.Append("' align='absmiddle'/>");
			stringBuilder2.Append(" <span class='");
			if (user.Identity != this.chatvars.identity)
			{
				stringBuilder2.Append("User ");
			}
			else
			{
				stringBuilder2.Append("You ");
			}
			stringBuilder2.Append("'>");
			stringBuilder2.Append(base.Server.HtmlEncode(user.DisplayName));
			string onlineStatus = user.OnlineStatus;
			if (onlineStatus != null)
			{
				if (onlineStatus != "ONLINE")
				{
					goto Label1;
				}
				stringBuilder = stringBuilder2.Append("</span>");
				stringBuilder1 = stringBuilder2.Append("</nobr>");
				str = stringBuilder2.ToString();
				return str;
			}
			StringBuilder stringBuilder3 = stringBuilder2.Append(" (");
			StringBuilder stringBuilder4 = stringBuilder2.Append(user.OnlineStatus);
			StringBuilder stringBuilder5 = stringBuilder2.Append(")");
			stringBuilder = stringBuilder2.Append("</span>");
			stringBuilder1 = stringBuilder2.Append("</nobr>");
			str = stringBuilder2.ToString();
			return str;
		Label1:
			if (!(onlineStatus == "SILENCE") && !(onlineStatus == "BUSY") && !(onlineStatus == "AWAY"))
			{
				stringBuilder3 = stringBuilder2.Append(" (");
				stringBuilder4 = stringBuilder2.Append(user.OnlineStatus);
				stringBuilder5 = stringBuilder2.Append(")");
				stringBuilder = stringBuilder2.Append("</span>");
				stringBuilder1 = stringBuilder2.Append("</nobr>");
				str = stringBuilder2.ToString();
				return str;
			}
			else
			{
				stringBuilder3 = stringBuilder2.Append(" (");
				stringBuilder4 = stringBuilder2.Append(user.OnlineStatus);
				stringBuilder5 = stringBuilder2.Append(")");
				stringBuilder = stringBuilder2.Append("</span>");
				stringBuilder1 = stringBuilder2.Append("</nobr>");
				str = stringBuilder2.ToString();
				return str;
			}
		}

		public string GetMessageHtml()
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (!this.chatvars.connected)
			{
				this.Connect();
			}
			if (!this.chatvars.connected)
			{
				stringBuilder.Append(string.Concat("<div class=Connection>", base.Request.Url, "</div>"));
				stringBuilder.Append("<div class=Connection> Not Connected Yet , Refresh the browser and try again .</div>");
				stringBuilder.Append(string.Concat(new string[] { "<div class=Connection> ", this._connectstatus, ":", this._connectstatusmessage, "</div>" }));
			}
			else
			{
				this.DoSync();
			}
			while (this.chatvars.messagehtmls.Count > 40)
			{
				this.chatvars.messagehtmls.Dequeue();
			}
			ArrayList arrayLists = new ArrayList(this.chatvars.messagehtmls);
			arrayLists.Reverse();
			foreach (string arrayList in arrayLists)
			{
				stringBuilder.Append(arrayList);
			}
			return stringBuilder.ToString();
		}

		public string GetOnlineHtml()
		{
			string str;
			if (this.chatvars.connected)
			{
				StringBuilder stringBuilder = new StringBuilder();
				ArrayList arrayLists = new ArrayList();
				ChatPortal portal = this.chatvars.place.Portal;
				Monitor.Enter(portal);
				try
				{
					ChatPlaceUser chatPlaceUser = this.chatvars.place.FindUser(this.chatvars.identity.UniqueId);
					if (chatPlaceUser != null)
					{
						arrayLists.Add(chatPlaceUser);
						ChatPlaceUser[] allUsers = this.chatvars.place.GetAllUsers();
						for (int i = 0; i < (int)allUsers.Length; i++)
						{
							ChatPlaceUser chatPlaceUser1 = allUsers[i];
							if (chatPlaceUser1 != chatPlaceUser)
							{
								if (chatPlaceUser.IsVisibleConnection(chatPlaceUser1.Connection))
								{
									arrayLists.Add(chatPlaceUser1);
								}
							}
						}
					}
					else
					{
						str = "Loading...";
						return str;
					}
				}
				finally
				{
					Monitor.Exit(portal);
				}
				foreach (ChatPlaceUser arrayList in arrayLists)
				{
					stringBuilder.Append("<div class=UserListItem>");
					stringBuilder.Append(this.FormatUser(arrayList));
					stringBuilder.Append("</div>");
				}
				str = stringBuilder.ToString();
			}
			else
			{
				str = "Loading..";
			}
			return str;
		}

		private void HandleSTC(string msg)
		{
			string str;
			NameValueCollection nameValueCollection;
			string[] strArrays;
			ChatUtility.SplitMsg(msg, out str, out nameValueCollection, out strArrays);
			string str1 = str;
			if (str1 != null)
			{
				if (str1 == "USER_MESSAGE")
				{
					ChatMsgData chatMsgDatum = new ChatMsgData()
					{
						SenderId = strArrays[1],
						Sender = strArrays[2],
						TargetId = strArrays[5],
						Target = strArrays[6]
					};
					bool flag = strArrays[7] == "1";
					bool flag1 = flag;
					chatMsgDatum.Offline = flag;
					chatMsgDatum.Whisper = flag1;
					chatMsgDatum.Text = strArrays[3];
					chatMsgDatum.Html = strArrays[4];
					chatMsgDatum.ClaimNo = strArrays[8];
					this.chatvars.messagehtmls.Enqueue(this.FormatMessage(chatMsgDatum, "USER"));
				}
				else if (str1 == "SYS_INFO_MESSAGE" || str1 == "SYS_ERROR_MESSAGE")
				{
					ChatMsgData chatMsgDatum1 = new ChatMsgData()
					{
						Text = strArrays[0]
					};
					this.chatvars.messagehtmls.Enqueue(this.FormatMessage(chatMsgDatum1, "SYSTEM"));
				}
			}
		}

		protected override void OnInit(EventArgs e)
		{
			ChatUtility.CheckQueryString(this.Context);
			base.OnInit(e);
			base.Response.Cache.SetCacheability(HttpCacheability.NoCache);
			base.Response.Cache.SetAllowResponseInBrowserHistory(false);
			base.Response.Cache.SetMaxAge(TimeSpan.Zero);
			this._place = base.Request.QueryString["Place"];
			this._guestname = base.Request.Params["GuestName"];
			if (this._guestname != null)
			{
				this._guestname = this._guestname.Trim();
				if (this._guestname == "")
				{
					this._guestname = null;
				}
			}
			if (ChatWebUtility.GetLogonIdentity() == null)
			{
				if ((this._guestname == null ? false : this._guestname != ""))
				{
				}
			}
			this._password = base.Request.Params["Password"];
			this._clientid = Guid.NewGuid();
			HttpCookie item = base.Request.Cookies["CCFSClientId"];
			if (item != null)
			{
				this._clientid = new Guid(item.Value);
			}
			else
			{
				item = base.Response.Cookies["CCFSClientId"];
				item.Value = this._clientid.ToString();
				item.Path = "/";
				if (ChatWebUtility.GetConfig("EnableLongTimeCookies") != "False")
				{
					item.Expires = DateTime.Now.AddDays(1);
				}
			}
		}

		private void ResetVars()
		{
			this._clientdata = new ChatFramePage.ClientData();
			System.Web.Caching.Cache cache = base.Cache;
			Guid clientId = this.ClientId;
			cache.Insert(clientId.ToString(), this._clientdata, null, DateTime.MaxValue, TimeSpan.FromMinutes(1), CacheItemPriority.NotRemovable, null);
		}

		public void SendMessage(string text)
		{
			if (this.chatvars.connected)
			{
				string msg = ChatUtility.JoinToMsg("USER_MESSAGE", null, new string[] { text, null, null, null, "0" });
				this.chatvars.cts.Add(msg);
				this.DoSync();
			}
		}

		private static void SyncCallback(IAsyncResult ar)
		{
			ChatResponse chatResponse;
			ChatFramePage.ClientData asyncState = (ChatFramePage.ClientData)ar.AsyncState;
			asyncState.syncing = null;
			try
			{
				chatResponse = asyncState.place.Manager.EndSyncData(ar);
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				asyncState.messagehtmls.Enqueue(string.Concat("<div>", exception.Message, "</div>"));
				return;
			}
			if (chatResponse.ReturnCode == "READY")
			{
				asyncState.cookie = chatResponse.Cookie;
				asyncState.stc.AddRange(chatResponse.Messages);
			}
			else
			{
				ChatPortal portal = asyncState.place.Portal;
				Monitor.Enter(portal);
				try
				{
					asyncState.place.Manager.Disconnect(asyncState.identity, asyncState.cookie);
				}
				finally
				{
					Monitor.Exit(portal);
				}
			}
		}

		private class ClientData
		{
			public ChatCookie cookie;

			public bool connected;

			public ChatIdentity identity;

			public ChatPlace place;

			public ChatPlaceUser myinfo;

			public Queue messagehtmls;

			public ArrayList cts;

			public ArrayList stc;

			public IAsyncResult syncing;

			public ClientData()
			{
			}

			public bool IsMyInfo(string userid)
			{
				bool flag;
				flag = (userid != null ? string.Compare(this.identity.UniqueId, userid, true) == 0 : false);
				return flag;
			}
		}
	}
}