using System;
using System.Collections;
using System.ComponentModel;
using System.Data.OleDb;
using System.Text;
using CuteChat;


namespace CuteChat
{
#pragma warning disable CS0436 // Type conflicts with imported type
    public class MSAccessDataProvider : BaseDataProvider, IAppDataProvider, IChatDataProvider, IDisposable
#pragma warning restore CS0436 // Type conflicts with imported type
    {
		private OleDbConnection conn;

		private string prefix;

		private AppPortal _portal;

		public AppPortal Portal
		{
			get
			{
				return this._portal;
			}
		}

		public MSAccessDataProvider(AppPortal portal, string conectionstring, string objectprefix)
		{
			this._portal = portal;
			this.prefix = objectprefix;
			this.conn = new OleDbConnection(conectionstring);
			this.conn.Open();
		}

		public void AddAgent(int departmentid, string userid)
		{
			if (userid == null)
			{
				throw new ArgumentNullException("userid");
			}
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "SupportAgent (DepartmentId,AgentUserId) VALUES (@depid,@userid)");
			this.SetParameter(oleDbCommand.Parameters, "@depid", departmentid);
			this.SetParameter(oleDbCommand.Parameters, "@userid", userid);
			oleDbCommand.ExecuteNonQuery();
		}

		public void AddDepartment(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "SupportDepartment (DepartmentName) VALUES (@name)");
			this.SetParameter(oleDbCommand.Parameters, "@name", name);
			oleDbCommand.ExecuteNonQuery();
		}

		public void AddFeedback(SupportFeedback feedback)
		{
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "SupportFeedback (FbTime,CustomerId,Name,DisplayName,Email,Title,Content,Comment,CommentBy) VALUES (@FbTime,@CustomerId,@Name,@DisplayName,@Email,@Title,@Content,@Comment,@CommentBy)");
			OleDbParameterCollection parameters = oleDbCommand.Parameters;
			this.SetParameter(parameters, "@FbTime", feedback.FbTime);
			this.SetParameter(parameters, "@CustomerId", feedback.CustomerId);
			this.SetParameter(parameters, "@Name", feedback.Name);
			this.SetParameter(parameters, "@DisplayName", feedback.DisplayName);
			this.SetParameter(parameters, "@Email", feedback.Email);
			this.SetParameter(parameters, "@Title", feedback.Title);
			this.SetParameter(parameters, "@Content", feedback.Content);
			this.SetParameter(parameters, "@Comment", feedback.Comment);
			this.SetParameter(parameters, "@CommentBy", feedback.CommentBy);
			oleDbCommand.ExecuteNonQuery();
		}

		public OleDbCommand CreateCommand()
		{
			return new OleDbCommand()
			{
				Connection = this.conn
			};
		}

		public void CreateLobby(IChatLobby ilobby)
		{
			AppLobby num = (AppLobby)ilobby;
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "Lobby (Title,Topic,Announcement,MaxOnlineCount,Locked,AllowAnonymous,[Password],Description,Integration,ManagerList,MaxIdleMinute,AutoAwayMinute,HistoryDay,HistoryCount,SortIndex) VALUES (@title,@topic,@announce,@maxonline,@locked,@anony,@pwd,@description,@integration,@managerlist,@maxidle,@autoaway,@hisday,@hiscount,@sort)");
			OleDbParameterCollection parameters = oleDbCommand.Parameters;
			this.SetParameter(parameters, "@title", num.Title);
			this.SetParameter(parameters, "@topic", num.Topic);
			this.SetParameter(parameters, "@announce", num.Announcement);
			this.SetParameter(parameters, "@maxonline", num.MaxOnlineCount);
			this.SetParameter(parameters, "@locked", num.Locked);
			this.SetParameter(parameters, "@anony", num.AllowAnonymous);
			this.SetParameter(parameters, "@pwd", num.Password);
			this.SetParameter(parameters, "@description", num.Description);
			this.SetParameter(parameters, "@integration", num.Integration);
			this.SetParameter(parameters, "@managerlist", num.ManagerList);
			this.SetParameter(parameters, "@maxidle", num.MaxIdleMinute);
			this.SetParameter(parameters, "@autoaway", num.AutoAwayMinute);
			this.SetParameter(parameters, "@hisday", num.HistoryDay);
			this.SetParameter(parameters, "@hiscount", num.HistoryCount);
			this.SetParameter(parameters, "@sort", num.SortIndex);
			oleDbCommand.ExecuteNonQuery();
			oleDbCommand.CommandText = string.Concat("SELECT MAX(LobbyID) FROM ", this.prefix, "Lobby");
			num.LobbyId = Convert.ToInt32(oleDbCommand.ExecuteScalar());
		}

		public void CreateRule(IChatRule irule)
		{
			AppRule num = (AppRule)irule;
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "Rule (Category,Disabled,RuleMode,Expression,SortIndex) VALUES (@category,@disabled,@mode,@expression,@sort)");
			OleDbParameterCollection parameters = oleDbCommand.Parameters;
			this.SetParameter(parameters, "@category", num.Category);
			this.SetParameter(parameters, "@disabled", num.Disabled);
			this.SetParameter(parameters, "@mode", num.Mode);
			this.SetParameter(parameters, "@expression", num.Expression);
			this.SetParameter(parameters, "@sort", num.Sort);
			oleDbCommand.ExecuteNonQuery();
			oleDbCommand.CommandText = string.Concat("SELECT MAX(RuleId) FROM ", this.prefix, "Rule");
			num.RuleId = Convert.ToInt32(oleDbCommand.ExecuteScalar());
		}

		public void CreateSession(SupportSession session)
		{
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "SupportSession ([BeginTime], [DepartmentId], [AgentUserId], [CustomerId], [DisplayName], [ActiveTime], [Email], [IPAddress], [Culture], [Platform], [Browser], [AgentRating], [SessionData], [Url],[Referrer]) \r\nVALUES( @BeginTime, @DepartmentId, @AgentUserId, @CustomerId, @DisplayName, @ActiveTime, @Email, @IPAddress, @Culture, @Platform, @Browser, @AgentRating, @SessionData,@Url,@Referrer)");
			OleDbParameterCollection parameters = oleDbCommand.Parameters;
			this.SetParameter(parameters, "@BeginTime", session.BeginTime);
			this.SetParameter(parameters, "@DepartmentId", session.DepartmentId);
			this.SetParameter(parameters, "@AgentUserId", session.AgentUserId);
			this.SetParameter(parameters, "@CustomerId", session.CustomerId);
			this.SetParameter(parameters, "@DisplayName", session.DisplayName);
			this.SetParameter(parameters, "@ActiveTime", session.ActiveTime);
			this.SetParameter(parameters, "@Email", session.Email);
			this.SetParameter(parameters, "@IPAddress", session.IPAddress);
			this.SetParameter(parameters, "@Culture", session.Culture);
			this.SetParameter(parameters, "@Platform", session.Platform);
			this.SetParameter(parameters, "@Browser", session.Browser);
			this.SetParameter(parameters, "@AgentRating", session.AgentRating);
			this.SetParameter(parameters, "@SessionData", session.SessionData);
			this.SetParameter(parameters, "@Url", session.Url);
			this.SetParameter(parameters, "@Referrer", session.Referrer);
			oleDbCommand.ExecuteNonQuery();
			oleDbCommand.CommandText = string.Concat("SELECT Max(SessionId) FROM ", this.prefix, "SupportSession");
			session.SessionId = Convert.ToInt32(oleDbCommand.ExecuteScalar());
		}

		public void DeleteAgentSessions(DateTime start, DateTime endtime, string agentid, string departmentid, string customer, string email, string ipaddr, string culture)
		{
			OleDbCommand str = this.CreateCommand();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("DELETE ").Append(this.prefix).Append("SupportSession WHERE BeginTime>=@start AND BeginTime<=@endtime");
			this.SetParameter(str.Parameters, "@start", start);
			this.SetParameter(str.Parameters, "@endtime", endtime);
			if ((agentid == null ? false : agentid != ""))
			{
				stringBuilder.Append(" AND AgentUserId=@agentid");
				this.SetParameter(str.Parameters, "@agentid", agentid);
			}
			if ((departmentid == null ? false : departmentid != ""))
			{
				stringBuilder.Append(" AND DepartmentId=@departmentid");
				this.SetParameter(str.Parameters, "@departmentid", departmentid);
			}
			if ((customer == null ? false : customer != ""))
			{
				stringBuilder.Append(" AND DisplayName LIKE @customer");
				this.SetParameter(str.Parameters, "@customer", string.Concat("%", customer, "%"));
			}
			if ((email == null ? false : email != ""))
			{
				stringBuilder.Append(" AND Email LIKE @email");
				this.SetParameter(str.Parameters, "@email", string.Concat("%", email, "%"));
			}
			if ((ipaddr == null ? false : ipaddr != ""))
			{
				stringBuilder.Append(" AND IPAddress LIKE @ipaddr");
				this.SetParameter(str.Parameters, "@ipaddr", string.Concat("%", ipaddr, "%"));
			}
			if ((culture == null ? false : culture != ""))
			{
				stringBuilder.Append(" AND Culture LIKE @culture");
				this.SetParameter(str.Parameters, "@culture", string.Concat("%", culture, "%"));
			}
			str.CommandText = stringBuilder.ToString();
			str.ExecuteNonQuery();
		}

		public void DeleteFeedback(int feedbackid)
		{
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("DELETE FROM ", this.prefix, "SupportFeedback WHERE FeedbackId=@FeedbackId");
			this.SetParameter(oleDbCommand.Parameters, "@FeedbackId", feedbackid);
			oleDbCommand.ExecuteNonQuery();
		}

		public void DeleteInstantMessages(string ownerid, string targetid)
		{
			OleDbCommand str = this.CreateCommand();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("DELETE ");
			stringBuilder.Append(this.prefix);
			stringBuilder.Append("InstantMessage ");
			stringBuilder.Append("WHERE 1=1");
			if ((ownerid == null ? false : ownerid != ""))
			{
				if ((targetid == null ? false : targetid != ""))
				{
					stringBuilder.Append(" AND (  (SenderId=@ownerid AND TargetId=@targetid) OR (SenderId=@targetid AND TargetId=@ownerid) ) ");
				}
				else
				{
					stringBuilder.Append(" AND ( SenderId=@ownerid OR TargetId=@ownerid ) ");
				}
				this.SetParameter(str.Parameters, "@ownerid", ownerid);
				this.SetParameter(str.Parameters, "@targetid", targetid);
			}
			else if ((targetid == null ? false : targetid != ""))
			{
				stringBuilder.Append(" AND ( SenderId=@targetid OR TargetId=@targetid ) ");
				this.SetParameter(str.Parameters, "@targetid", targetid);
			}
			str.CommandText = stringBuilder.ToString();
			str.ExecuteNonQuery();
		}

		public void DeleteLobby(IChatLobby ilobby)
		{
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("DELETE ", this.prefix, "Lobby WHERE LobbyId=@lobbyid");
			this.SetParameter(oleDbCommand.Parameters, "@lobbyid", ilobby.LobbyId);
			oleDbCommand.ExecuteNonQuery();
		}

		public void DeleteMessagesBeforeDays(int days)
		{
			DateTime dateTime = DateTime.Now.AddDays((double)(-days));
			OleDbCommand oleDbCommand = this.CreateCommand();
			this.SetParameter(oleDbCommand.Parameters, "@date", dateTime);
			oleDbCommand.CommandText = string.Concat("DELETE FROM ", this.prefix, "SupportMessage WHERE MsgTime<@date");
			oleDbCommand.ExecuteNonQuery();
			oleDbCommand.CommandText = string.Concat("DELETE FROM ", this.prefix, "InstantMessage WHERE MsgTime<@date");
			oleDbCommand.ExecuteNonQuery();
			oleDbCommand.CommandText = string.Concat("DELETE FROM ", this.prefix, "LogMessage WHERE MsgTime<@date");
			oleDbCommand.ExecuteNonQuery();
		}

		public void DeleteRule(IChatRule irule)
		{
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("DELETE ", this.prefix, "Rule WHERE RuleId=@ruleid");
			this.SetParameter(oleDbCommand.Parameters, "@ruleid", irule.RuleId);
			oleDbCommand.ExecuteNonQuery();
		}

		public void Dispose()
		{
			this.conn.Dispose();
		}

		public virtual string GetCustomerData(string userid)
		{
			string str;
			if (userid == null)
			{
				throw new ArgumentNullException("userid");
			}
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT CustomerData FROM ", this.prefix, "SupportCustomer WHERE CustomerId=@userid");
			this.SetParameter(oleDbCommand.Parameters, "@userid", userid);
			using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
			{
				if (oleDbDataReader.Read())
				{
					if (!oleDbDataReader.IsDBNull(0))
					{
						str = oleDbDataReader.GetString(0);
						return str;
					}
					else
					{
						str = null;
						return str;
					}
				}
			}
			str = null;
			return str;
		}

		public int GetCustomerSessionCount(string customerid)
		{
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT COUNT(*) FROM ", this.prefix, "SupportSession WHERE CustomerId=@CustomerId");
			this.SetParameter(oleDbCommand.Parameters, "@CustomerId", customerid);
			return Convert.ToInt32(oleDbCommand.ExecuteScalar());
		}

		public SupportSession GetLastSession(string customerid)
		{
			SupportSession supportSession;
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT TOP 1 * FROM ", this.prefix, "SupportSession WHERE CustomerId=@CustomerId ORDER BY SessionId DESC");
			this.SetParameter(oleDbCommand.Parameters, "@CustomerId", customerid);
			SupportSession[] supportSessionArray = this.LoadSessions(oleDbCommand);
			if ((int)supportSessionArray.Length != 1)
			{
				supportSession = null;
			}
			else
			{
				supportSession = supportSessionArray[0];
			}
			return supportSession;
		}

		public IChatLobby[] GetLobbies()
		{
			ArrayList arrayLists = new ArrayList();
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT LobbyId,Title,Topic,Announcement,MaxOnlineCount,Locked,AllowAnonymous,Password,Description,Integration,ManagerList,MaxIdleMinute,AutoAwayMinute,HistoryCount,HistoryDay,SortIndex FROM ", this.prefix, "Lobby ORDER BY SortIndex ASC,LobbyId ASC");
			using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
			{
				while (oleDbDataReader.Read())
				{
					AppLobby appLobby = new AppLobby()
					{
						LobbyId = oleDbDataReader.GetInt32(0),
						Title = oleDbDataReader.GetString(1),
						Topic = oleDbDataReader.GetString(2),
						Announcement = oleDbDataReader.GetString(3),
						MaxOnlineCount = oleDbDataReader.GetInt32(4),
						Locked = oleDbDataReader.GetBoolean(5),
						AllowAnonymous = oleDbDataReader.GetBoolean(6),
						Password = oleDbDataReader.GetString(7),
						Description = oleDbDataReader.GetString(8),
						Integration = oleDbDataReader.GetString(9),
						ManagerList = oleDbDataReader.GetString(10),
						MaxIdleMinute = oleDbDataReader.GetInt32(11),
						AutoAwayMinute = oleDbDataReader.GetInt32(12),
						HistoryCount = oleDbDataReader.GetInt32(13),
						HistoryDay = oleDbDataReader.GetInt32(14),
						SortIndex = oleDbDataReader.GetInt32(15)
					};
					arrayLists.Add(appLobby);
				}
			}
			return (IChatLobby[])arrayLists.ToArray(typeof(IChatLobby));
		}

		public IChatLobby GetLobby(int lobbyid)
		{
			IChatLobby chatLobby;
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT LobbyId,Title,Topic,Announcement,MaxOnlineCount,Locked,AllowAnonymous,Password,Description,Integration,ManagerList,MaxIdleMinute,AutoAwayMinute,HistoryCount,HistoryDay,SortIndex FROM ", this.prefix, "Lobby WHERE LobbyId=@lobbyid");
			this.SetParameter(oleDbCommand.Parameters, "@lobbyid", lobbyid);
			using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
			{
				if (oleDbDataReader.Read())
				{
					AppLobby appLobby = new AppLobby()
					{
						LobbyId = lobbyid,
						Title = oleDbDataReader.GetString(1),
						Topic = oleDbDataReader.GetString(2),
						Announcement = oleDbDataReader.GetString(3),
						MaxOnlineCount = oleDbDataReader.GetInt32(4),
						Locked = oleDbDataReader.GetBoolean(5),
						AllowAnonymous = oleDbDataReader.GetBoolean(6),
						Password = oleDbDataReader.GetString(7),
						Description = oleDbDataReader.GetString(8),
						Integration = oleDbDataReader.GetString(9),
						ManagerList = oleDbDataReader.GetString(10),
						MaxIdleMinute = oleDbDataReader.GetInt32(11),
						AutoAwayMinute = oleDbDataReader.GetInt32(12),
						HistoryCount = oleDbDataReader.GetInt32(13),
						HistoryDay = oleDbDataReader.GetInt32(14),
						SortIndex = oleDbDataReader.GetInt32(15)
					};
					chatLobby = appLobby;
				}
				else
				{
					chatLobby = null;
				}
			}
			return chatLobby;
		}

		public ChatMsgData[] GetMessages(string location, string placename, int pagesize, int pageindex, out int totalcount)
		{
			string str;
			ChatMsgData[] array;
			if (location != null)
			{
				location = location.ToLower();
				if (location == "messenger")
				{
					str = "InstantMessage";
					location = null;
					placename = null;
				}
				else if (location != "support")
				{
					str = "LogMessage";
				}
				else
				{
					str = "SupportMessage";
					location = null;
					placename = null;
				}
				OleDbCommand oleDbCommand = this.CreateCommand();
				oleDbCommand.CommandText = string.Concat("SELECT MessageId FROM ", this.prefix, str);
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append("SELECT MessageId FROM ").Append(this.prefix).Append(str);
				ArrayList arrayLists = new ArrayList();
				if (location != null)
				{
					this.SetParameter(oleDbCommand.Parameters, "@location", location);
					arrayLists.Add("Location=@location");
				}
				if (placename != null)
				{
					this.SetParameter(oleDbCommand.Parameters, "@placename", placename);
					arrayLists.Add("Place=@placename");
				}
				if (arrayLists.Count != 0)
				{
					for (int i = 0; i < arrayLists.Count; i++)
					{
						if (i != 0)
						{
							stringBuilder.Append(" AND ");
						}
						else
						{
							stringBuilder.Append(" WHERE ");
						}
						stringBuilder.Append(arrayLists[i]);
					}
				}
				ArrayList arrayLists1 = new ArrayList();
				oleDbCommand.CommandText = stringBuilder.ToString();
				using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
				{
					while (oleDbDataReader.Read())
					{
						arrayLists1.Add(oleDbDataReader.GetInt32(0));
					}
				}
				totalcount = arrayLists1.Count;
				stringBuilder = new StringBuilder();
				stringBuilder.Append("SELECT * FROM ").Append(this.prefix).Append(str);
				stringBuilder.Append(" WHERE MessageId IN (");
				int num = pageindex * pagesize;
				int num1 = num + pagesize;
				int num2 = num;
				while (true)
				{
					if ((num2 >= num1 ? true : num2 >= arrayLists1.Count))
					{
						break;
					}
					stringBuilder.Append(arrayLists1[num2]).Append(",");
					num2++;
				}
				stringBuilder.Append("0)");
				ArrayList arrayLists2 = new ArrayList();
				oleDbCommand.CommandText = stringBuilder.ToString();
				using (OleDbDataReader oleDbDataReader1 = oleDbCommand.ExecuteReader())
				{
					int num3 = MSAccessDataProvider.SafeGetOrdinal(oleDbDataReader1, "Location");
					int num4 = MSAccessDataProvider.SafeGetOrdinal(oleDbDataReader1, "Place");
					int num5 = MSAccessDataProvider.SafeGetOrdinal(oleDbDataReader1, "Whisper");
					while (oleDbDataReader1.Read())
					{
						ChatMsgData chatMsgDatum = new ChatMsgData()
						{
							MessageId = oleDbDataReader1.GetInt32(0),
							Text = Convert.ToString(oleDbDataReader1["Text"]),
							Html = Convert.ToString(oleDbDataReader1["Html"]),
							Sender = Convert.ToString(oleDbDataReader1["Sender"]),
							SenderId = Convert.ToString(oleDbDataReader1["SenderId"]),
							Target = Convert.ToString(oleDbDataReader1["Target"]),
							TargetId = Convert.ToString(oleDbDataReader1["TargetId"]),
							Time = Convert.ToDateTime(oleDbDataReader1["MsgTime"]),
							ClaimNo = Convert.ToString(oleDbDataReader1["ClaimNo"])
						};
						if (num3 != -1)
						{
							chatMsgDatum.Location = oleDbDataReader1.GetString(num3);
						}
						if (num4 != -1)
						{
							chatMsgDatum.PlaceName = oleDbDataReader1.GetString(num4);
						}
						if (num5 != -1)
						{
							chatMsgDatum.Whisper = oleDbDataReader1.GetInt32(num5) != 0;
						}
						arrayLists2.Add(chatMsgDatum);
					}
				}
				array = (ChatMsgData[])arrayLists2.ToArray(typeof(ChatMsgData));
			}
			else
			{
				totalcount = 0;
				array = new ChatMsgData[0];
			}
			return array;
		}

		public IChatPortalInfo GetPortalInfo(string portalname)
		{
			string str;
			AppPortalInfo appPortalInfo = new AppPortalInfo(portalname);
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT Properties FROM ", this.prefix, "Portal WHERE PortalName=@portalname");
			this.SetParameter(oleDbCommand.Parameters, "@portalname", portalname);
			using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
			{
				if (oleDbDataReader.Read())
				{
					AppPortalInfo appPortalInfo1 = appPortalInfo;
					if (oleDbDataReader.IsDBNull(0))
					{
						str = null;
					}
					else
					{
						str = oleDbDataReader.GetString(0);
					}
					appPortalInfo1.Properties = str;
				}
			}
			return appPortalInfo;
		}

		public IChatRule GetRule(int ruleid)
		{
			IChatRule chatRule;
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT RuleId,Disabled,RuleMode,Expression,Category,SortIndex FROM ", this.prefix, "Rule WHERE RuleId=@ruleid");
			this.SetParameter(oleDbCommand.Parameters, "@ruleid", ruleid);
			using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
			{
				if (oleDbDataReader.Read())
				{
					AppRule appRule = new AppRule()
					{
						RuleId = ruleid,
						Disabled = oleDbDataReader.GetBoolean(1),
						Mode = oleDbDataReader.GetString(2),
						Expression = oleDbDataReader.GetString(3),
						Category = oleDbDataReader.GetString(4),
						Sort = oleDbDataReader.GetInt32(5)
					};
					chatRule = appRule;
				}
				else
				{
					chatRule = null;
				}
			}
			return chatRule;
		}

		public IChatRule[] GetRules()
		{
			ArrayList arrayLists = new ArrayList();
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT RuleId,Disabled,RuleMode,Expression,Category,SortIndex FROM ", this.prefix, "Rule");
			using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
			{
				while (oleDbDataReader.Read())
				{
					AppRule appRule = new AppRule()
					{
						RuleId = oleDbDataReader.GetInt32(0),
						Disabled = oleDbDataReader.GetBoolean(1),
						Mode = oleDbDataReader.GetString(2),
						Expression = oleDbDataReader.GetString(3),
						Category = oleDbDataReader.GetString(4),
						Sort = oleDbDataReader.GetInt32(5)
					};
					arrayLists.Add(appRule);
				}
			}
			return (IChatRule[])arrayLists.ToArray(typeof(IChatRule));
		}

		public IChatUserInfo GetUserInfo(string userid)
		{
			IChatUserInfo chatUserInfo;
			string str;
			string str1;
			string str2;
			string str3;
			string str4;
			string str5;
			string str6;
			AppUserInfo appUserInfo = new AppUserInfo(userid)
			{
				IsStored = false
			};
			if (ChatProvider.GetInstance(this.Portal).IsRegisteredUser(userid))
			{
				OleDbCommand oleDbCommand = this.CreateCommand();
				oleDbCommand.CommandText = string.Concat("SELECT DisplayName,Description,ServerProperties,PublicProperties,PrivateProperties,BuildinContacts,BuildinIgnores FROM ", this.prefix, "User WHERE UserId=@userid");
				this.SetParameter(oleDbCommand.Parameters, "@userid", userid);
				using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
				{
					if (oleDbDataReader.Read())
					{
						AppUserInfo appUserInfo1 = appUserInfo;
						if (oleDbDataReader.IsDBNull(0))
						{
							str = null;
						}
						else
						{
							str = oleDbDataReader.GetString(0);
						}
						appUserInfo1.DisplayName = str;
						AppUserInfo appUserInfo2 = appUserInfo;
						if (oleDbDataReader.IsDBNull(1))
						{
							str1 = null;
						}
						else
						{
							str1 = oleDbDataReader.GetString(1);
						}
						appUserInfo2.Description = str1;
						AppUserInfo appUserInfo3 = appUserInfo;
						if (oleDbDataReader.IsDBNull(2))
						{
							str2 = null;
						}
						else
						{
							str2 = oleDbDataReader.GetString(2);
						}
						appUserInfo3.ServerProperties = str2;
						AppUserInfo appUserInfo4 = appUserInfo;
						if (oleDbDataReader.IsDBNull(3))
						{
							str3 = null;
						}
						else
						{
							str3 = oleDbDataReader.GetString(3);
						}
						appUserInfo4.PublicProperties = str3;
						AppUserInfo appUserInfo5 = appUserInfo;
						if (oleDbDataReader.IsDBNull(4))
						{
							str4 = null;
						}
						else
						{
							str4 = oleDbDataReader.GetString(4);
						}
						appUserInfo5.PrivateProperties = str4;
						AppUserInfo appUserInfo6 = appUserInfo;
						if (oleDbDataReader.IsDBNull(5))
						{
							str5 = null;
						}
						else
						{
							str5 = oleDbDataReader.GetString(5);
						}
						appUserInfo6.BuildinContacts = str5;
						AppUserInfo appUserInfo7 = appUserInfo;
						if (oleDbDataReader.IsDBNull(6))
						{
							str6 = null;
						}
						else
						{
							str6 = oleDbDataReader.GetString(6);
						}
						appUserInfo7.BuildinIgnores = str6;
						appUserInfo.IsStored = true;
					}
				}
				if (appUserInfo.DisplayName == null)
				{
				}
				chatUserInfo = appUserInfo;
			}
			else
			{
				chatUserInfo = appUserInfo;
			}
			return chatUserInfo;
		}

		public SupportSession[] LoadAgentSessions(string agentid)
		{
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT * FROM ", this.prefix, "SupportSession WHERE AgentUserId=@AgentUserId ORDER BY SessionID ASC");
			this.SetParameter(oleDbCommand.Parameters, "@AgentUserId", agentid);
			return this.LoadSessions(oleDbCommand);
		}
#pragma warning disable CS0436
		public ChatMsgData[] LoadChannelHistoryMessages(string channelname, int maxcount, DateTime afterdate)
#pragma warning restore CS0436
		{
			ArrayList arrayLists = new ArrayList();
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat(new object[] { "SELECT TOP ", maxcount, " MessageId,MsgTime,Location, SenderId,Sender, TargetId,Target,Whisper,[Text],Html,ClaimNo FROM ", this.prefix, "LogMessage WHERE Place=@placename AND MsgTime>=@afterdate ORDER BY MsgTime DESC" });
			this.SetParameter(oleDbCommand.Parameters, "@placename", channelname);
			this.SetParameter(oleDbCommand.Parameters, "@afterdate", afterdate);
			using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
			{
				while (oleDbDataReader.Read())
				{
					ChatMsgData chatMsgDatum = new ChatMsgData()
					{
						MessageId = oleDbDataReader.GetInt32(0),
						Time = oleDbDataReader.GetDateTime(1),
						Location = oleDbDataReader.GetString(2),
						SenderId = oleDbDataReader.GetString(3),
						Sender = oleDbDataReader.GetString(4)						
					};
					if (!oleDbDataReader.IsDBNull(5))
					{
						chatMsgDatum.TargetId = oleDbDataReader.GetString(5);
					}
					if (!oleDbDataReader.IsDBNull(6))
					{
						chatMsgDatum.Target = oleDbDataReader.GetString(6);
					}
					if (!oleDbDataReader.IsDBNull(8))
					{
						chatMsgDatum.Text = oleDbDataReader.GetString(8);
					}
					if (!oleDbDataReader.IsDBNull(9))
					{
						chatMsgDatum.Html = oleDbDataReader.GetString(9);
					}
					if (!oleDbDataReader.IsDBNull(10))
					{
						chatMsgDatum.ClaimNo = oleDbDataReader.GetString(10);
					}

					chatMsgDatum.Whisper = oleDbDataReader.GetInt32(7) == 1;
					arrayLists.Add(chatMsgDatum);
				}
			}
			arrayLists.Reverse();
			return (ChatMsgData[])arrayLists.ToArray(typeof(ChatMsgData));
		}

		public SupportSession[] LoadCustomerSessions(string customerid)
		{
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT * FROM ", this.prefix, "SupportSession WHERE CustomerId=@CustomerId ORDER BY SessionID ASC");
			this.SetParameter(oleDbCommand.Parameters, "@CustomerId", customerid);
			return this.LoadSessions(oleDbCommand);
		}

		public SupportDepartment[] LoadDepartments()
		{
			ArrayList arrayLists = new ArrayList();
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT DepartmentId,DepartmentName FROM ", this.prefix, "SupportDepartment");
			using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
			{
				while (oleDbDataReader.Read())
				{
					SupportDepartment supportDepartment = new SupportDepartment()
					{
						DepartmentId = oleDbDataReader.GetInt32(0),
						Name = oleDbDataReader.GetString(1)
					};
					arrayLists.Add(supportDepartment);
				}
			}
			oleDbCommand.CommandText = string.Concat("SELECT AgentUserId FROM ", this.prefix, "SupportAgent WHERE DepartmentId=@depid");
			OleDbParameter oleDbParameter = new OleDbParameter("@depid", OleDbType.Integer);
			oleDbCommand.Parameters.Add(oleDbParameter);
			ArrayList arrayLists1 = new ArrayList();
			foreach (SupportDepartment arrayList in arrayLists)
			{
				oleDbParameter.Value = arrayList.DepartmentId;
				using (OleDbDataReader oleDbDataReader1 = oleDbCommand.ExecuteReader())
				{
					while (oleDbDataReader1.Read())
					{
						SupportAgent supportAgent = new SupportAgent()
						{
							UserId = oleDbDataReader1.GetString(0),
							Department = arrayList
						};
						arrayLists1.Add(supportAgent);
					}
				}
				if (arrayLists1.Count != 0)
				{
					arrayList.Agents = (SupportAgent[])arrayLists1.ToArray(typeof(SupportAgent));
					arrayLists1 = new ArrayList();
				}
			}
			return (SupportDepartment[])arrayLists.ToArray(typeof(SupportDepartment));
		}

		public SupportFeedback LoadFeedback(int feedbackid)
		{
			SupportFeedback supportFeedback;
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT * FROM ", this.prefix, "SupportFeedback WHERE FeedbackId=@FeedbackId");
			this.SetParameter(oleDbCommand.Parameters, "@FeedbackId", feedbackid);
			using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
			{
				if (oleDbDataReader.Read())
				{
					SupportFeedback supportFeedback1 = new SupportFeedback()
					{
						FeedbackId = (int)oleDbDataReader["FeedbackId"],
						FbTime = (DateTime)oleDbDataReader["FbTime"],
						Name = (string)oleDbDataReader["Name"],
						DisplayName = (string)oleDbDataReader["DisplayName"],
						Email = (string)oleDbDataReader["Email"],
						Title = (string)oleDbDataReader["Title"],
						Content = (string)oleDbDataReader["Content"],
						Comment = oleDbDataReader["Comment"] as string,
						CommentBy = oleDbDataReader["CommentBy"] as string
					};
					supportFeedback = supportFeedback1;
					return supportFeedback;
				}
			}
			supportFeedback = null;
			return supportFeedback;
		}

		public SupportFeedback[] LoadFeedbacks(bool history)
		{
			OleDbCommand oleDbCommand = this.CreateCommand();
			OleDbCommand oleDbCommand1 = oleDbCommand;
			string[] strArrays = new string[] { "SELECT * FROM ", this.prefix, "SupportFeedback ", null, null };
			strArrays[3] = (history ? "" : "WHERE CommentBy IS NULL");
			strArrays[4] = " ORDER BY FeedbackId ASC";
			oleDbCommand1.CommandText = string.Concat(strArrays);
			ArrayList arrayLists = new ArrayList();
			using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
			{
				while (oleDbDataReader.Read())
				{
					SupportFeedback supportFeedback = new SupportFeedback()
					{
						FeedbackId = (int)oleDbDataReader["FeedbackId"],
						FbTime = (DateTime)oleDbDataReader["FbTime"],
						Name = (string)oleDbDataReader["Name"],
						DisplayName = (string)oleDbDataReader["DisplayName"],
						Email = (string)oleDbDataReader["Email"],
						Title = (string)oleDbDataReader["Title"],
						Content = (string)oleDbDataReader["Content"],
						Comment = oleDbDataReader["Comment"] as string,
						CommentBy = oleDbDataReader["CommentBy"] as string
					};
					arrayLists.Add(supportFeedback);
				}
			}
			return (SupportFeedback[])arrayLists.ToArray(typeof(SupportFeedback));
		}

		public ChatMsgData[] LoadInstantMessages(string ownerid, string targetid, bool offlineOnly, int maxid, int pagesize)
		{
			ArrayList arrayLists = new ArrayList();
			OleDbCommand str = this.CreateCommand();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("SELECT ");
			if ((pagesize <= 0 ? false : pagesize < 2147483647))
			{
				stringBuilder.Append("TOP ");
				stringBuilder.Append(pagesize);
				stringBuilder.Append(" ");
			}
			stringBuilder.Append("MessageId,MsgTime,SenderId,Sender,TargetId,Target,Offline,[Text],Html ");
			stringBuilder.Append("From ");
			stringBuilder.Append(this.prefix);
			stringBuilder.Append("InstantMessage ");
			stringBuilder.Append("WHERE ");
			if (offlineOnly)
			{
				if (maxid >= 2147483647)
				{
					stringBuilder.Append(" Offline=1 ");
				}
				else
				{
					stringBuilder.Append(" MessageId<@maxid AND Offline=1 ");
					this.SetParameter(str.Parameters, "@maxid", maxid);
				}
			}
			else if (maxid >= 2147483647)
			{
				stringBuilder.Append(" 1=1 ");
			}
			else
			{
				stringBuilder.Append(" MessageId<@maxid ");
				this.SetParameter(str.Parameters, "@maxid", maxid);
			}
			if ((ownerid == null ? false : ownerid != ""))
			{
				if ((targetid == null ? false : targetid != ""))
				{
					stringBuilder.Append(" AND (  (SenderId=@ownerid AND TargetId=@targetid) OR (SenderId=@targetid AND TargetId=@ownerid) ) ");
				}
				else
				{
					stringBuilder.Append(" AND ( SenderId=@ownerid OR TargetId=@ownerid ) ");
				}
				this.SetParameter(str.Parameters, "@ownerid", ownerid);
			}
			else if ((targetid == null ? false : targetid != ""))
			{
				stringBuilder.Append(" AND ( SenderId=@targetid OR TargetId=@targetid ) ");
				this.SetParameter(str.Parameters, "@targetid", targetid);
			}
			stringBuilder.Append("ORDER BY MsgTime DESC");
			str.CommandText = stringBuilder.ToString();
			using (OleDbDataReader oleDbDataReader = str.ExecuteReader())
			{
				while (oleDbDataReader.Read())
				{
					ChatMsgData chatMsgDatum = new ChatMsgData()
					{
						MessageId = oleDbDataReader.GetInt32(0),
						Time = oleDbDataReader.GetDateTime(1),
						SenderId = oleDbDataReader.GetString(2),
						Sender = oleDbDataReader.GetString(3),
						TargetId = oleDbDataReader.GetString(4),
						Target = oleDbDataReader.GetString(5)
					};
					if (!oleDbDataReader.IsDBNull(7))
					{
						chatMsgDatum.Text = oleDbDataReader.GetString(7);
					}
					if (!oleDbDataReader.IsDBNull(8))
					{
						chatMsgDatum.Html = oleDbDataReader.GetString(8);
					}
					chatMsgDatum.Offline = oleDbDataReader.GetInt32(6) == 1;
					arrayLists.Add(chatMsgDatum);
				}
			}
			return (ChatMsgData[])arrayLists.ToArray(typeof(ChatMsgData));
		}

		public ChatMsgData[] LoadInstantOfflineMessages(string targetid, DateTime time)
		{
			ArrayList arrayLists = new ArrayList();
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT MessageId,MsgTime,SenderId,Sender,TargetId,Target,Offline,[Text],Html FROM ", this.prefix, "InstantMessage WHERE Offline=1 AND MsgTime>@time AND TargetId=@targetid ORDER BY MsgTime DESC");
			this.SetParameter(oleDbCommand.Parameters, "@time", time);
			this.SetParameter(oleDbCommand.Parameters, "@targetid", targetid);
			using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
			{
				while (oleDbDataReader.Read())
				{
					ChatMsgData chatMsgDatum = new ChatMsgData()
					{
						MessageId = oleDbDataReader.GetInt32(0),
						Location = "Messenger",
						Time = oleDbDataReader.GetDateTime(1),
						SenderId = oleDbDataReader.GetString(2),
						Sender = oleDbDataReader.GetString(3),
						TargetId = oleDbDataReader.GetString(4),
						Target = oleDbDataReader.GetString(5)
					};
					if (!oleDbDataReader.IsDBNull(7))
					{
						chatMsgDatum.Text = oleDbDataReader.GetString(7);
					}
					if (!oleDbDataReader.IsDBNull(8))
					{
						chatMsgDatum.Html = oleDbDataReader.GetString(8);
					}
					chatMsgDatum.Offline = oleDbDataReader.GetInt32(6) == 1;
					arrayLists.Add(chatMsgDatum);
				}
			}
			arrayLists.Reverse();
			return (ChatMsgData[])arrayLists.ToArray(typeof(ChatMsgData));
		}

		public ChatMsgData[] LoadSessionMessages(int sessionid)
		{
			ArrayList arrayLists = new ArrayList();
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT MessageId,MsgTime,SenderId,Sender,TargetId,Target,[Text],Html FROM ", this.prefix, "SupportMessage WHERE SessionId=@SessionId ORDER BY MessageID ASC");
			this.SetParameter(oleDbCommand.Parameters, "@SessionId", sessionid);
			using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
			{
				while (oleDbDataReader.Read())
				{
					ChatMsgData chatMsgDatum = new ChatMsgData()
					{
						MessageId = oleDbDataReader.GetInt32(0),
						Time = oleDbDataReader.GetDateTime(1),
						SenderId = oleDbDataReader.GetString(2),
						Sender = oleDbDataReader.GetString(3)
					};
					if (!oleDbDataReader.IsDBNull(4))
					{
						chatMsgDatum.TargetId = oleDbDataReader.GetString(4);
					}
					if (!oleDbDataReader.IsDBNull(5))
					{
						chatMsgDatum.Target = oleDbDataReader.GetString(5);
					}
					if (!oleDbDataReader.IsDBNull(6))
					{
						chatMsgDatum.Text = oleDbDataReader.GetString(6);
					}
					if (!oleDbDataReader.IsDBNull(7))
					{
						chatMsgDatum.Html = oleDbDataReader.GetString(7);
					}
					arrayLists.Add(chatMsgDatum);
				}
			}
			arrayLists.Reverse();
			return (ChatMsgData[])arrayLists.ToArray(typeof(ChatMsgData));
		}

		private SupportSession[] LoadSessions(OleDbCommand cmd)
		{
			ArrayList arrayLists = new ArrayList();
			using (OleDbDataReader oleDbDataReader = cmd.ExecuteReader())
			{
				while (oleDbDataReader.Read())
				{
					SupportSession supportSession = new SupportSession()
					{
						SessionId = (int)oleDbDataReader["SessionId"],
						BeginTime = (DateTime)oleDbDataReader["BeginTime"],
						DepartmentId = (int)oleDbDataReader["DepartmentId"],
						AgentUserId = (string)oleDbDataReader["AgentUserId"],
						CustomerId = (string)oleDbDataReader["CustomerId"],
						DisplayName = (string)oleDbDataReader["DisplayName"],
						ActiveTime = (DateTime)oleDbDataReader["ActiveTime"],
						Email = oleDbDataReader["Email"].ToString(),
						IPAddress = oleDbDataReader["IPAddress"].ToString(),
						Culture = oleDbDataReader["Culture"].ToString(),
						Platform = oleDbDataReader["Platform"].ToString(),
						Browser = oleDbDataReader["Browser"].ToString(),
						AgentRating = Convert.ToInt32(oleDbDataReader["AgentRating"]),
						SessionData = oleDbDataReader["SessionData"] as string,
						Url = oleDbDataReader["Url"] as string,
						Referrer = oleDbDataReader["Referrer"] as string
					};
					arrayLists.Add(supportSession);
				}
			}
			return (SupportSession[])arrayLists.ToArray(typeof(SupportSession));
		}

		public string LoadSetting(string name)
		{
			string str;
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT SettingData FROM ", this.prefix, "Settings WHERE SettingName=@Name");
			this.SetParameter(oleDbCommand.Parameters, "@Name", name);
			using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
			{
				if (oleDbDataReader.Read())
				{
					str = oleDbDataReader.GetString(0);
					return str;
				}
			}
			str = null;
			return str;
		}

		public void LogEvent(ChatPlace place, ChatIdentity user, string category, string message)
		{
			object value;
			object uniqueId;
			if (message.Length <= 3000)
			{
				if (!base.SkipEvent(place, user, category))
				{
					OleDbCommand oleDbCommand = this.CreateCommand();
					oleDbCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "LogEvent (EvtTime,Category,Portal,Place,UserName,UserId,Message) VALUES (@time,@category,@portal,@place,@username,@userid,@message)");
					OleDbParameterCollection parameters = oleDbCommand.Parameters;
					this.SetParameter(parameters, "@time", DateTime.Now);
					this.SetParameter(parameters, "@category", category);
					this.SetParameter(parameters, "@portal", place.Portal.Name);
					this.SetParameter(parameters, "@place", place.PlaceName);
					OleDbParameterCollection oleDbParameterCollection = parameters;
					if (user == null)
					{
						value = DBNull.Value;
					}
					else
					{
						value = user.DisplayName;
					}
					this.SetParameter(oleDbParameterCollection, "@username", value);
					OleDbParameterCollection oleDbParameterCollection1 = parameters;
					if (user == null)
					{
						uniqueId = DBNull.Value;
					}
					else
					{
						uniqueId = user.UniqueId;
					}
					this.SetParameter(oleDbParameterCollection1, "@userid", uniqueId);
					this.SetParameter(parameters, "@message", message);
					oleDbCommand.ExecuteNonQuery();
				}
			}
		}

		public void LogInstantMessage(ChatIdentity sender, string targetid, string targetname, bool offline, string text, string html, string claimNo)
		{
			object value;
			object obj;
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "InstantMessage (MsgTime,Sender,SenderId,Target,TargetId,Offline,IPAddress,[Text],Html,ClaimNo) VALUES (@time,@sender,@senderid,@target,@targetid,@offline,@ipaddr,@text,@html, @claimno)");
			OleDbParameterCollection parameters = oleDbCommand.Parameters;
			this.SetParameter(parameters, "@time", DateTime.Now);
			this.SetParameter(parameters, "@sender", sender.DisplayName);
			this.SetParameter(parameters, "@senderid", sender.UniqueId);
			this.SetParameter(parameters, "@target", targetname);
			this.SetParameter(parameters, "@targetid", targetid);
			this.SetParameter(parameters, "@offline", (offline ? 1 : 0));
			this.SetParameter(parameters, "@ipaddr", sender.IPAddress);
			OleDbParameterCollection oleDbParameterCollection = parameters;
			if (text == null)
			{
				value = DBNull.Value;
			}
			else
			{
				value = text;
			}
			this.SetParameter(oleDbParameterCollection, "@text", value);
			OleDbParameterCollection oleDbParameterCollection1 = parameters;
			if (html == null)
			{
				obj = DBNull.Value;
			}
			else
			{
				obj = html;
			}
			this.SetParameter(oleDbParameterCollection1, "@html", obj);

			OleDbParameterCollection oleDbParameterCollection2 = parameters;
			if (claimNo == null)
			{
				obj = DBNull.Value;
			}
			else
			{
				obj = claimNo;
			}
			this.SetParameter(oleDbParameterCollection2, "@claimno", obj);
			oleDbCommand.ExecuteNonQuery();
		}

		public void LogMessage(ChatPlace place, ChatIdentity sender, ChatIdentity target, bool whisper, string text, string html, string claimNo)
		{
			object value;
			object uniqueId;
			object obj;
			object value1;
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "LogMessage (MsgTime,Location,Place,Sender,SenderId,Target,TargetId,Whisper,IPAddress,[Text],Html, ClaimNo) VALUES (@time,@location,@place,@sender,@senderid,@target,@targetid,@whisper,@ipaddr,@text,@html,@claimno)");
			OleDbParameterCollection parameters = oleDbCommand.Parameters;
			this.SetParameter(parameters, "@time", DateTime.Now);
			this.SetParameter(parameters, "@location", place.Location);
			this.SetParameter(parameters, "@place", place.PlaceName);
			this.SetParameter(parameters, "@sender", sender.DisplayName);
			this.SetParameter(parameters, "@senderid", sender.UniqueId);
			OleDbParameterCollection oleDbParameterCollection = parameters;
			if (target == null)
			{
				value = DBNull.Value;
			}
			else
			{
				value = target.DisplayName;
			}
			this.SetParameter(oleDbParameterCollection, "@target", value);
			OleDbParameterCollection oleDbParameterCollection1 = parameters;
			if (target == null)
			{
				uniqueId = DBNull.Value;
			}
			else
			{
				uniqueId = target.UniqueId;
			}
			this.SetParameter(oleDbParameterCollection1, "@targetid", uniqueId);
			this.SetParameter(parameters, "@whisper", (whisper ? 1 : 0));
			this.SetParameter(parameters, "@ipaddr", sender.IPAddress);
			OleDbParameterCollection oleDbParameterCollection2 = parameters;
			if (text == null)
			{
				obj = DBNull.Value;
			}
			else
			{
				obj = text;
			}
			this.SetParameter(oleDbParameterCollection2, "@text", obj);
			OleDbParameterCollection oleDbParameterCollection3 = parameters;
			if (html == null)
			{
				value1 = DBNull.Value;
			}
			else
			{
				value1 = html;
			}
			this.SetParameter(oleDbParameterCollection3, "@html", value1);

			OleDbParameterCollection oleDbParameterCollection4 = parameters;
			if (claimNo == null)
			{
				value1 = DBNull.Value;
			}
			else
			{
				value1 = claimNo;
			}
			this.SetParameter(oleDbParameterCollection3, "@claimno", value1);

			oleDbCommand.ExecuteNonQuery();
		}

		public void LogSupportMessage(ChatPlace place, SupportSession session, string type, ChatIdentity sender, ChatIdentity target, string text, string html, string claimNo)
		{
			object value;
			object uniqueId;
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "SupportMessage (MsgTime,SessionId,MsgType,Sender,SenderId,Target,TargetId,[Text],Html, ClaimNo) VALUES (@time,@sessionid,@msgtype,@sender,@senderid,@target,@targetid,@text,@html)");
			OleDbParameterCollection parameters = oleDbCommand.Parameters;
			this.SetParameter(parameters, "@time", DateTime.Now);
			this.SetParameter(parameters, "@sessionid", session.SessionId);
			this.SetParameter(parameters, "@msgtype", type);
			this.SetParameter(parameters, "@sender", sender.DisplayName);
			this.SetParameter(parameters, "@senderid", sender.UniqueId);
			OleDbParameterCollection oleDbParameterCollection = parameters;
			if (target == null)
			{
				value = DBNull.Value;
			}
			else
			{
				value = target.DisplayName;
			}
			this.SetParameter(oleDbParameterCollection, "@target", value);
			OleDbParameterCollection oleDbParameterCollection1 = parameters;
			if (target == null)
			{
				uniqueId = DBNull.Value;
			}
			else
			{
				uniqueId = target.UniqueId;
			}
			this.SetParameter(oleDbParameterCollection1, "@targetid", uniqueId);
			this.SetParameter(parameters, "@text", text);
			this.SetParameter(parameters, "@html", html);
			this.SetParameter(parameters, "@claimno", claimNo);
			oleDbCommand.ExecuteNonQuery();
		}

		public void RemoveAgent(int departmentid, string userid)
		{
			if (userid == null)
			{
				throw new ArgumentNullException("userid");
			}
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("DELETE FROM ", this.prefix, "SupportAgent WHERE DepartmentId=@depid AND AgentUserId=@userid");
			this.SetParameter(oleDbCommand.Parameters, "@depid", departmentid);
			this.SetParameter(oleDbCommand.Parameters, "@userid", userid);
			oleDbCommand.ExecuteNonQuery();
		}

		public void RemoveDepartment(int departmentid)
		{
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("DELETE FROM ", this.prefix, "SupportDepartment WHERE DepartmentId=@depid");
			this.SetParameter(oleDbCommand.Parameters, "@depid", departmentid);
			oleDbCommand.ExecuteNonQuery();
		}

		public void RenameDepartment(int departmentid, string newname)
		{
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("UPDATE ", this.prefix, "SupportDepartment SET DepartmentName=@newname WHERE DepartmentId=@depid");
			this.SetParameter(oleDbCommand.Parameters, "@newname", newname);
			this.SetParameter(oleDbCommand.Parameters, "@depid", departmentid);
			oleDbCommand.ExecuteNonQuery();
		}

		private static int SafeGetOrdinal(OleDbDataReader reader, string name)
		{
			int ordinal;
			try
			{
				ordinal = reader.GetOrdinal(name);
			}
			catch
			{
				ordinal = -1;
			}
			return ordinal;
		}

		public void SaveSetting(string name, string data)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			OleDbCommand oleDbCommand = this.CreateCommand();
			if (data != null)
			{
				oleDbCommand.CommandText = string.Concat("UPDATE ", this.prefix, "Settings SET SettingData=@Data WHERE SettingName=@Name");
				this.SetParameter(oleDbCommand.Parameters, "@Data", data);
				this.SetParameter(oleDbCommand.Parameters, "@Name", name);
				if (oleDbCommand.ExecuteNonQuery() == 0)
				{
					oleDbCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "Settings (SettingData,SettingName) VALUES (@Data,@Name)");
					oleDbCommand.ExecuteNonQuery();
				}
			}
			else
			{
				oleDbCommand.CommandText = string.Concat("DELETE ", this.prefix, "Settings WHERE SettingName=@Name");
				this.SetParameter(oleDbCommand.Parameters, "@Name", name);
				oleDbCommand.ExecuteNonQuery();
			}
		}

		public SupportSession[] SearchAgentSessions(DateTime start, DateTime endtime, string agentid, string departmentid, string customer, string email, string ipaddr, string culture)
		{
			OleDbCommand str = this.CreateCommand();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("SELECT * FROM ").Append(this.prefix).Append("SupportSession WHERE BeginTime>=@start AND BeginTime<=@endtime");
			this.SetParameter(str.Parameters, "@start", start);
			this.SetParameter(str.Parameters, "@endtime", endtime);
			if ((agentid == null ? false : agentid != ""))
			{
				stringBuilder.Append(" AND AgentUserId=@agentid");
				this.SetParameter(str.Parameters, "@agentid", agentid);
			}
			if ((departmentid == null ? false : departmentid != ""))
			{
				stringBuilder.Append(" AND DepartmentId=@departmentid");
				this.SetParameter(str.Parameters, "@departmentid", departmentid);
			}
			if ((customer == null ? false : customer != ""))
			{
				stringBuilder.Append(" AND DisplayName LIKE @customer");
				this.SetParameter(str.Parameters, "@customer", string.Concat("%", customer, "%"));
			}
			if ((email == null ? false : email != ""))
			{
				stringBuilder.Append(" AND Email LIKE @email");
				this.SetParameter(str.Parameters, "@email", string.Concat("%", email, "%"));
			}
			if ((ipaddr == null ? false : ipaddr != ""))
			{
				stringBuilder.Append(" AND IPAddress LIKE @ipaddr");
				this.SetParameter(str.Parameters, "@ipaddr", string.Concat("%", ipaddr, "%"));
			}
			if ((culture == null ? false : culture != ""))
			{
				stringBuilder.Append(" AND Culture LIKE @culture");
				this.SetParameter(str.Parameters, "@culture", string.Concat("%", culture, "%"));
			}
			stringBuilder.Append(" ORDER BY SessionID ASC");
			str.CommandText = stringBuilder.ToString();
			return this.LoadSessions(str);
		}

		public SupportSession SelectSession(string sessionid)
		{
			SupportSession supportSession;
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT * FROM ", this.prefix, "SupportSession WHERE SessionId=@SessionId");
			this.SetParameter(oleDbCommand.Parameters, "@SessionId", sessionid);
			SupportSession[] supportSessionArray = this.LoadSessions(oleDbCommand);
			if ((int)supportSessionArray.Length != 1)
			{
				supportSession = null;
			}
			else
			{
				supportSession = supportSessionArray[0];
			}
			return supportSession;
		}

		public virtual void SetCustomerData(string userid, string value)
		{
			if (userid == null)
			{
				throw new ArgumentNullException("userid");
			}
			OleDbCommand oleDbCommand = this.CreateCommand();
			if (value != null)
			{
				this.SetParameter(oleDbCommand.Parameters, "@value", value);
				this.SetParameter(oleDbCommand.Parameters, "@userid", userid);
				oleDbCommand.CommandText = string.Concat("UPDATE ", this.prefix, "SupportCustomer SET CustomerData=@value WHERE CustomerId=@userid");
				if (oleDbCommand.ExecuteNonQuery() == 0)
				{
					oleDbCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "SupportCustomer (CustomerData,CustomerId) VALUES (@value,@userid)");
					oleDbCommand.ExecuteNonQuery();
				}
			}
			else
			{
				this.SetParameter(oleDbCommand.Parameters, "@userid", userid);
				oleDbCommand.CommandText = string.Concat("DELETE ", this.prefix, "SupportCustomer WHERE CustomerId=@userid");
				oleDbCommand.ExecuteNonQuery();
			}
		}

		private void SetParameter(OleDbParameterCollection pcoll, string pname, object obj)
		{
			if (pcoll == null)
			{
				throw new ArgumentNullException("pcoll");
			}
			if ((obj == null ? true : Convert.IsDBNull(obj)))
			{
				pcoll.Add(pname, OleDbType.VarWChar, 50).Value = DBNull.Value;
			}
			else if (!(obj is DateTime))
			{
				pcoll.Add(pname, obj);
			}
			else
			{
				DateTime dateTime = (DateTime)obj;
				pcoll.Add(pname, OleDbType.Char, 18).Value = dateTime.ToString("yyyy-MM-dd HH:mm:ss");
			}
		}

		public void UpdateFeedback(SupportFeedback feedback)
		{
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("UPDATE ", this.prefix, "SupportFeedback SET FbTime=@FbTime,CustomerId=@CustomerId,Name=@Name,DisplayName=@DisplayName,Email=@Email,Title=@Title,Content=@Content,Comment=@Comment,CommentBy=@CommentBy WHERE FeedbackId=@FeedbackId");
			OleDbParameterCollection parameters = oleDbCommand.Parameters;
			this.SetParameter(parameters, "@FbTime", feedback.FbTime);
			this.SetParameter(parameters, "@CustomerId", feedback.CustomerId);
			this.SetParameter(parameters, "@Name", feedback.Name);
			this.SetParameter(parameters, "@DisplayName", feedback.DisplayName);
			this.SetParameter(parameters, "@Email", feedback.Email);
			this.SetParameter(parameters, "@Title", feedback.Title);
			this.SetParameter(parameters, "@Content", feedback.Content);
			this.SetParameter(parameters, "@Comment", feedback.Comment);
			this.SetParameter(parameters, "@CommentBy", feedback.CommentBy);
			this.SetParameter(parameters, "@FeedbackId", feedback.FeedbackId);
			oleDbCommand.ExecuteNonQuery();
		}

		public void UpdateLobby(IChatLobby ilobby)
		{
			AppLobby appLobby = (AppLobby)ilobby;
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("UPDATE ", this.prefix, "Lobby SET Title=@title,Topic=@topic,Announcement=@announce,MaxOnlineCount=@maxonline,Locked=@locked,AllowAnonymous=@anony,[Password]=@pwd,Description=@description,Integration=@integration,ManagerList=@managerlist,MaxIdleMinute=@maxidle,AutoAwayMinute=@autoaway,HistoryDay=@hisday,HistoryCount=@hiscount,SortIndex=@sort WHERE LobbyId=@lobbyid");
			OleDbParameterCollection parameters = oleDbCommand.Parameters;
			this.SetParameter(parameters, "@title", appLobby.Title);
			this.SetParameter(parameters, "@topic", appLobby.Topic);
			this.SetParameter(parameters, "@announce", appLobby.Announcement);
			this.SetParameter(parameters, "@maxonline", appLobby.MaxOnlineCount);
			this.SetParameter(parameters, "@locked", appLobby.Locked);
			this.SetParameter(parameters, "@anony", appLobby.AllowAnonymous);
			this.SetParameter(parameters, "@pwd", appLobby.Password);
			this.SetParameter(parameters, "@description", appLobby.Description);
			this.SetParameter(parameters, "@integration", appLobby.Integration);
			this.SetParameter(parameters, "@managerlist", appLobby.ManagerList);
			this.SetParameter(parameters, "@maxidle", appLobby.MaxIdleMinute);
			this.SetParameter(parameters, "@autoaway", appLobby.AutoAwayMinute);
			this.SetParameter(parameters, "@hisday", appLobby.HistoryDay);
			this.SetParameter(parameters, "@hiscount", appLobby.HistoryCount);
			this.SetParameter(parameters, "@sort", appLobby.SortIndex);
			this.SetParameter(parameters, "@lobbyid", appLobby.LobbyId);
			oleDbCommand.ExecuteNonQuery();
		}

		public void UpdatePortalInfo(IChatPortalInfo portalinfo)
		{
			AppPortalInfo appPortalInfo = (AppPortalInfo)portalinfo;
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("UPDATE ", this.prefix, "Portal SET Properties=@props WHERE PortalName=@portalname");
			OleDbParameterCollection parameters = oleDbCommand.Parameters;
			this.SetParameter(parameters, "@props", appPortalInfo.Properties);
			this.SetParameter(parameters, "@portalname", appPortalInfo.PortalName);
			if (oleDbCommand.ExecuteNonQuery() == 0)
			{
				oleDbCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "Portal (Properties,PortalName) VALUES (@props,@portalname)");
				oleDbCommand.ExecuteNonQuery();
			}
		}

		public void UpdateRule(IChatRule irule)
		{
			AppRule appRule = (AppRule)irule;
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("UPDATE ", this.prefix, "Rule SET Category=@category,Disabled=@disabled,RuleMode=@mode,Expression=@expression,SortIndex=@sort WHERE RuleId=@ruleid");
			OleDbParameterCollection parameters = oleDbCommand.Parameters;
			this.SetParameter(parameters, "@category", appRule.Category);
			this.SetParameter(parameters, "@disabled", appRule.Disabled);
			this.SetParameter(parameters, "@mode", appRule.Mode);
			this.SetParameter(parameters, "@expression", appRule.Expression);
			this.SetParameter(parameters, "@sort", appRule.Sort);
			this.SetParameter(parameters, "@ruleid", appRule.RuleId);
			oleDbCommand.ExecuteNonQuery();
		}

		public void UpdateSession(SupportSession session)
		{
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("UPDATE ", this.prefix, "SupportSession SET [BeginTime]=@BeginTime, [DepartmentId]=@DepartmentId, [AgentUserId]=@AgentUserId, [CustomerId]=@CustomerId, [DisplayName]=@DisplayName, [ActiveTime]=@ActiveTime, [Email]=@Email, [IPAddress]=@IPAddress, [Culture]=@Culture, [Platform]=@Platform, [Browser]=@Browser, [AgentRating]=@AgentRating, [SessionData]=@SessionData,[Url]=@Url,[Referrer]=@Referrer WHERE SessionId=@SessionId");
			OleDbParameterCollection parameters = oleDbCommand.Parameters;
			this.SetParameter(parameters, "@BeginTime", session.BeginTime);
			this.SetParameter(parameters, "@DepartmentId", session.DepartmentId);
			this.SetParameter(parameters, "@AgentUserId", session.AgentUserId);
			this.SetParameter(parameters, "@CustomerId", session.CustomerId);
			this.SetParameter(parameters, "@DisplayName", session.DisplayName);
			this.SetParameter(parameters, "@ActiveTime", session.ActiveTime);
			this.SetParameter(parameters, "@Email", session.Email);
			this.SetParameter(parameters, "@IPAddress", session.IPAddress);
			this.SetParameter(parameters, "@Culture", session.Culture);
			this.SetParameter(parameters, "@Platform", session.Platform);
			this.SetParameter(parameters, "@Browser", session.Browser);
			this.SetParameter(parameters, "@AgentRating", session.AgentRating);
			this.SetParameter(parameters, "@SessionData", session.SessionData);
			this.SetParameter(parameters, "@Url", session.Url);
			this.SetParameter(parameters, "@Referrer", session.Referrer);
			this.SetParameter(parameters, "@SessionId", session.SessionId);
			oleDbCommand.ExecuteNonQuery();
		}

		public void UpdateUserInfo(IChatUserInfo chatinfo)
		{
			AppUserInfo appUserInfo = (AppUserInfo)chatinfo;
			if (ChatProvider.GetInstance(this.Portal).IsRegisteredUser(appUserInfo.UserId))
			{
				OleDbCommand oleDbCommand = this.CreateCommand();
				oleDbCommand.CommandText = string.Concat("UPDATE ", this.prefix, "User SET DisplayName=@name,Description=@desc,ServerProperties=@server,PublicProperties=@public,PrivateProperties=@private,BuildinContacts=@contacts,BuildinIgnores=@ignores WHERE UserId=@userid");
				OleDbParameterCollection parameters = oleDbCommand.Parameters;
				this.SetParameter(parameters, "@name", appUserInfo.DisplayName);
				this.SetParameter(parameters, "@desc", appUserInfo.Description);
				this.SetParameter(parameters, "@server", appUserInfo.ServerProperties);
				this.SetParameter(parameters, "@public", appUserInfo.PublicProperties);
				this.SetParameter(parameters, "@private", appUserInfo.PrivateProperties);
				this.SetParameter(parameters, "@contacts", appUserInfo.BuildinContacts);
				this.SetParameter(parameters, "@ignores", appUserInfo.BuildinIgnores);
				this.SetParameter(parameters, "@userid", appUserInfo.UserId);
				if (oleDbCommand.ExecuteNonQuery() == 0)
				{
					oleDbCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "USER (DisplayName,Description,ServerProperties,PublicProperties,PrivateProperties,BuildinContacts,BuildinIgnores,UserId) VALUES (@name,@desc,@server,@public,@private,@contacts,@ignores,@userid)");
					oleDbCommand.ExecuteNonQuery();
				}
				appUserInfo.IsStored = true;
			}
		}
	}
}