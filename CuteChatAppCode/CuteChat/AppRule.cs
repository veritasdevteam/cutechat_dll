using System;

namespace CuteChat
{
	[Serializable]
	public class AppRule : IChatRule
	{
		private int _ruleid = -1;

		private string _category = "";

		private bool _disabled = false;

		private string _mode = "";

		private string _expression = "";

		private int _sort = 0;

		public string Category
		{
			get
			{
				return this._category;
			}
			set
			{
				this._category = value;
			}
		}

		public bool Disabled
		{
			get
			{
				return this._disabled;
			}
			set
			{
				this._disabled = value;
			}
		}

		public string Expression
		{
			get
			{
				return this._expression;
			}
			set
			{
				this._expression = value;
			}
		}

		public string Mode
		{
			get
			{
				return this._mode;
			}
			set
			{
				this._mode = value;
			}
		}

		public int RuleId
		{
			get
			{
				return JustDecompileGenerated_get_RuleId();
			}
			set
			{
				JustDecompileGenerated_set_RuleId(value);
			}
		}

		public int JustDecompileGenerated_get_RuleId()
		{
			return this._ruleid;
		}

		public void JustDecompileGenerated_set_RuleId(int value)
		{
			this._ruleid = value;
		}

		public int Sort
		{
			get
			{
				return this._sort;
			}
			set
			{
				this._sort = value;
			}
		}

		public AppRule()
		{
		}
	}
}