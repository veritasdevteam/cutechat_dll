using System;
using System.Collections;

namespace CuteChat
{
	public class AppDataManager : _InternalDataManager
	{
		private IChatRule[] _rulecache;

		private Hashtable _ipchecked;    

#pragma warning disable CS0618 // Type or member is obsolete
        private Hashtable _settings = new Hashtable(new CaseInsensitiveHashCodeProvider(), new CaseInsensitiveComparer());
#pragma warning restore CS0618 // Type or member is obsolete

		private SupportDepartment[] _deps;
		private Hashtable _isagents = null;

		public new AppPortal Portal
		{
			get
			{
				return (AppPortal)base.Portal;
			}
		}

		public AppDataManager(AppPortal portal) : base(portal)
		{
		}
		

		private void _ClearDepartmentCache()
		{
			this._deps = null;
			this._isagents = null;
		}

		public override void AddContact(ChatIdentity identity, string userid)
		{
			base.AddContact(identity, userid);
		}

		public override void AddIgnore(ChatIdentity identity, string userid)
		{
			base.AddIgnore(identity, userid);
		}

		public override void AsyncLogMessage(ChatPlace place, ChatIdentity sender, ChatIdentity target, bool whisper, string text, string html)
		{
			base.AsyncLogMessage(place, sender, target, whisper, text, html);
		}

		public override bool CheckIP(string ipaddr)
		{
			if (ipaddr == null)
			{
				throw new ArgumentNullException("ipaddr");
			}
			if (this._ipchecked == null)
			{
				this._ipchecked = new Hashtable();
			}
			object item = this._ipchecked[ipaddr];
			if (item == null)
			{
				item = base.CheckIP(ipaddr);
				this._ipchecked[ipaddr] = item;
			}
			return (bool)item;
		}

		public IAppDataProvider CreateAppDataProvider()
		{
			IAppDataProvider appDataProvider = ChatProvider.GetInstance(this.Portal).CreateDataProvider(this.Portal);
			return appDataProvider;
		}

        public override IChatLobby CreateLobbyInstance()
		{
			return new AppLobby();
		}

		public override void CreateRule(IChatRule rule)
		{
			base.CreateRule(rule);
			this._rulecache = null;
			this._ipchecked = null;
		}

		public override IChatRule CreateRuleInstance()
		{
			return new AppRule();
		}

		public override void DeleteRule(IChatRule rule)
		{
			base.DeleteRule(rule);
			this._rulecache = null;
			this._ipchecked = null;
		}

		public override IChatUserInfo[] GetContacts(ChatIdentity identity)
		{
			return base.GetContacts(identity);
		}

		public override SupportDepartment[] GetDepartments()
		{
			if (this._deps == null)
			{
				this._deps = base.GetDepartments();
			}
			return this._deps;
		}

		public override IChatUserInfo[] GetIgnores(ChatIdentity identity)
		{
			return base.GetIgnores(identity);
		}

		public new AppLobby GetLobby(int lobbyid)
		{
			return (AppLobby)base.GetLobby(lobbyid);
		}

		public override string GetMemberDisplayName(string userid)
		{
			string str;
			if ((userid == null ? false : userid.Length != 0))
			{
				string str1 = ChatProvider.GetInstance(this.Portal).FromUserId(userid);
				if (str1 != null)
				{
					string str2 = null;
					ChatProvider.GetInstance(this.Portal).GetUserInfo(str1, ref str2);
					str = str2;
				}
				else
				{
					str = null;
				}
			}
			else
			{
				str = null;
			}
			return str;
		}

		public override string GetMemberUserName(string userid)
		{
			string str;
			if ((userid == null ? false : userid.Length != 0))
			{
				string str1 = ChatProvider.GetInstance(this.Portal).FromUserId(userid);
				str = str1;
			}
			else
			{
				str = null;
			}
			return str;
		}

		public new AppRule GetRule(int ruleid)
		{
			return (AppRule)base.GetRule(ruleid);
		}

		public override IChatRule[] GetRules()
		{
			if (this._rulecache == null)
			{
				this._rulecache = base.GetRules();
			}
			return this._rulecache;
		}

		public override IChatUserInfo GetUserInfo(string userid)
		{
			IChatUserInfo userInfo;
			IChatUserInfo chatUserInfo = base.GetUserInfo(userid);
			if (chatUserInfo != null)
			{
				userInfo = ChatProvider.GetInstance(this.Portal).GetUserInfo(chatUserInfo);
			}
			else
			{
				userInfo = null;
			}
			return userInfo;
		}

		public override bool IsAdministratorId(string userid)
		{
			return ChatProvider.GetInstance(this.Portal).IsAdministrator(userid);
		}

		public override bool IsAgent(string userid)
		{
			if (userid == null)
			{
				throw new ArgumentNullException("userid");
			}
			if (this._isagents == null)
			{
				Hashtable hashtables = new Hashtable();
				SupportDepartment[] departments = this.GetDepartments();
				for (int i = 0; i < (int)departments.Length; i++)
				{
					SupportAgent[] agents = departments[i].Agents;
					for (int j = 0; j < (int)agents.Length; j++)
					{
						SupportAgent supportAgent = agents[j];
						hashtables[supportAgent.UserId.ToLower()] = "1";
					}
				}
				this._isagents = hashtables;
			}
			return this._isagents.Contains(userid.ToLower());
		}

		public override bool IsLobbyAdminId(IChatLobby lobby, string userid)
		{
			bool flag;
			flag = (!base.IsLobbyAdminId(lobby, userid) ? ChatProvider.GetInstance(this.Portal).IsLobbyAdmin(lobby, userid) : true);
			return flag;
		}

		public override string IsMemberDisplayName(string username)
		{
			return ChatProvider.GetInstance(this.Portal).GetRegisteredUserId(username);
		}

		public override bool IsMemberId(string userid)
		{
			return ChatProvider.GetInstance(this.Portal).IsRegisteredUser(userid);
		}

#pragma warning disable CS0436
		public override ChatMsgData[] LoadChannelHistoryMessages(string channelname, int maxcount, DateTime afterdate)
#pragma warning restore CS0436
		{
#pragma warning disable CS0436
			ChatMsgData[] cdm = LoadChannelHistoryMessages(channelname, maxcount, afterdate);
#pragma warning restore CS0436
			return cdm as ChatMsgData[];
        }

        public override string LoadSetting(string name)
		{
			object item = this._settings[name];
			if (item == null)
			{
				item = base.LoadSetting(name);
				if (item == null)
				{
					item = DBNull.Value;
				}
				this._settings[name] = item;
			}
			return item as string;
		}

		public override void OnDepartmentChanged()
		{
			this._ClearDepartmentCache();
			base.OnDepartmentChanged();
		}

		public override void RemoveContact(ChatIdentity identity, string userid)
		{
			base.RemoveContact(identity, userid);
		}

		public override void RemoveIgnore(ChatIdentity identity, string userid)
		{
			base.RemoveIgnore(identity, userid);
		}

		public override void SaveSetting(string name, string data)
		{
			base.SaveSetting(name, data);
			object value = data;
			if (value == null)
			{
				value = DBNull.Value;
			}
			this._settings[name] = value;
		}

		public override void UpdateRule(IChatRule rule)
		{
			base.UpdateRule(rule);
			this._rulecache = null;
			this._ipchecked = null;
		}

		public override void UpdateUserInfo(IChatUserInfo info)
		{
			base.UpdateUserInfo(info);
			ChatProvider.GetInstance(this.Portal).UpdateUserInfo(info);
		}
	}
}