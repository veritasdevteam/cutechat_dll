using System;
using System.Collections;
using System.ComponentModel;
using System.Data.OleDb;
using System.Text;


namespace CuteChat
{
#pragma warning disable CS0436
	public class OracleOleDbDataProvider : BaseDataProvider, IAppDataProvider, IChatDataProvider, IDisposable
#pragma warning restore CS0436
	{
		private OleDbConnection conn;

		private string prefix;

		private AppPortal _portal;

		public AppPortal Portal
		{
			get
			{
				return this._portal;
			}
		}

		public OracleOleDbDataProvider(AppPortal portal, string conectionstring, string objectprefix)
		{
			this._portal = portal;
			this.prefix = objectprefix;
			this.conn = new OleDbConnection(conectionstring);
			this.conn.Open();
		}

		public void AddAgent(int departmentid, string userid)
		{
			if (userid == null)
			{
				throw new ArgumentNullException("userid");
			}
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "SupportAgent (DepartmentId,AgentUserId) VALUES (@depid,@userid)");
			this.SetParameter(oleDbCommand, "@depid", departmentid);
			this.SetParameter(oleDbCommand, "@userid", userid);
			oleDbCommand.ExecuteNonQuery();
		}

		public void AddDepartment(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "SupportDepartment (DepartmentId,DepartmentName) VALUES (@id,@name)");
			this.SetParameter(oleDbCommand, "@id", this.NextIdentity(string.Concat(this.prefix, "SupportDepartment"), "DepartmentId"));
			this.SetParameter(oleDbCommand, "@name", name);
			oleDbCommand.ExecuteNonQuery();
		}

		public void AddFeedback(SupportFeedback feedback)
		{
			feedback.FeedbackId = this.NextIdentity(string.Concat(this.prefix, "SupportFeedback"), "FeedbackId");
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "SupportFeedback (FeedbackId,FbTime,CustomerId,Name,DisplayName,Email,Title,Content,f_Comment,CommentBy) VALUES (@id,@FbTime,@CustomerId,@Name,@DisplayName,@Email,@Title,@Content,@f_Comment,@CommentBy)");
			OleDbParameterCollection parameters = oleDbCommand.Parameters;
			this.SetParameter(oleDbCommand, "@id", feedback.FeedbackId);
			this.SetParameter(oleDbCommand, "@FbTime", feedback.FbTime);
			this.SetParameter(oleDbCommand, "@CustomerId", feedback.CustomerId);
			this.SetParameter(oleDbCommand, "@Name", feedback.Name);
			this.SetParameter(oleDbCommand, "@DisplayName", feedback.DisplayName);
			this.SetParameter(oleDbCommand, "@Email", feedback.Email);
			this.SetParameter(oleDbCommand, "@Title", feedback.Title);
			this.SetParameter(oleDbCommand, "@Content", feedback.Content);
			this.SetParameter(oleDbCommand, "@f_Comment", feedback.Comment);
			this.SetParameter(oleDbCommand, "@CommentBy", feedback.CommentBy);
			try
			{
				oleDbCommand.ExecuteNonQuery();
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				throw new Exception(string.Concat(exception.Message, " : ", oleDbCommand.CommandText));
			}
		}

		private void ApplyParameters(OleDbCommand cmd)
		{
			string commandText = cmd.CommandText;
			foreach (OleDbParameter parameter in cmd.Parameters)
			{
				if (commandText.IndexOf(parameter.ParameterName) != -1)
				{
					commandText = commandText.Replace(parameter.ParameterName, this.Translate(parameter.Value));
				}
			}
			cmd.CommandText = commandText;
		}

		public OleDbCommand CreateCommand()
		{
			return new OleDbCommand()
			{
				Connection = this.conn
			};
		}

		public void CreateLobby(IChatLobby ilobby)
		{
			AppLobby appLobby = (AppLobby)ilobby;
			appLobby.LobbyId = this.NextIdentity(string.Concat(this.prefix, "Lobby"), "LobbyId");
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "Lobby (LobbyId,Title,Topic,Announcement,MaxOnlineCount,Locked,AllowAnonymous,Password,Description,Integration,ManagerList,MaxIdleMinute,AutoAwayMinute,HistoryDay,HistoryCount,SortIndex) VALUES (@id,@title,@topic,@announce,@maxonline,@locked,@anony,@pwd,@description,@integration,@managerlist,@maxidle,@autoaway,@hisday,@hiscount,@sort)");
			OleDbParameterCollection parameters = oleDbCommand.Parameters;
			this.SetParameter(oleDbCommand, "@id", appLobby.LobbyId);
			this.SetParameter(oleDbCommand, "@title", appLobby.Title);
			this.SetParameter(oleDbCommand, "@topic", appLobby.Topic);
			this.SetParameter(oleDbCommand, "@announce", appLobby.Announcement);
			this.SetParameter(oleDbCommand, "@maxonline", appLobby.MaxOnlineCount);
			this.SetParameter(oleDbCommand, "@locked", appLobby.Locked);
			this.SetParameter(oleDbCommand, "@anony", appLobby.AllowAnonymous);
			this.SetParameter(oleDbCommand, "@pwd", appLobby.Password);
			this.SetParameter(oleDbCommand, "@description", appLobby.Description);
			this.SetParameter(oleDbCommand, "@integration", appLobby.Integration);
			this.SetParameter(oleDbCommand, "@managerlist", appLobby.ManagerList);
			this.SetParameter(oleDbCommand, "@maxidle", appLobby.MaxIdleMinute);
			this.SetParameter(oleDbCommand, "@autoaway", appLobby.AutoAwayMinute);
			this.SetParameter(oleDbCommand, "@hisday", appLobby.HistoryDay);
			this.SetParameter(oleDbCommand, "@hiscount", appLobby.HistoryCount);
			this.SetParameter(oleDbCommand, "@sort", appLobby.SortIndex);
			oleDbCommand.ExecuteNonQuery();
		}

		public void CreateRule(IChatRule irule)
		{
			AppRule appRule = (AppRule)irule;
			appRule.RuleId = this.NextIdentity(string.Concat(this.prefix, "Rule"), "RuleId");
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "Rule (RuleId,Category,Disabled,RuleMode,Expression,SortIndex) VALUES (@id,@category,@disabled,@mode,@expression,@sort)");
			OleDbParameterCollection parameters = oleDbCommand.Parameters;
			this.SetParameter(oleDbCommand, "@id", appRule.RuleId);
			this.SetParameter(oleDbCommand, "@category", appRule.Category);
			this.SetParameter(oleDbCommand, "@disabled", appRule.Disabled);
			this.SetParameter(oleDbCommand, "@mode", appRule.Mode);
			this.SetParameter(oleDbCommand, "@expression", appRule.Expression);
			this.SetParameter(oleDbCommand, "@sort", appRule.Sort);
			oleDbCommand.ExecuteNonQuery();
		}

		public void CreateSession(SupportSession session)
		{
			OleDbCommand oleDbCommand = this.CreateCommand();
			session.SessionId = this.NextIdentity(string.Concat(this.prefix, "SupportSession"), "SessionId");
			oleDbCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "SupportSession (SessionId,BeginTime, DepartmentId, AgentUserId, CustomerId, DisplayName, ActiveTime, Email, IPAddress, Culture, Platform, Browser, AgentRating, SessionData,Url,Referrer) \r\nVALUES(@id, @BeginTime, @DepartmentId, @AgentUserId, @CustomerId, @DisplayName, @ActiveTime, @Email, @IPAddress, @Culture, @Platform, @Browser, @AgentRating, @SessionData,@Url,@Referrer)");
			OleDbParameterCollection parameters = oleDbCommand.Parameters;
			this.SetParameter(oleDbCommand, "@id", session.SessionId);
			this.SetParameter(oleDbCommand, "@BeginTime", session.BeginTime);
			this.SetParameter(oleDbCommand, "@DepartmentId", session.DepartmentId);
			this.SetParameter(oleDbCommand, "@AgentUserId", session.AgentUserId);
			this.SetParameter(oleDbCommand, "@CustomerId", session.CustomerId);
			this.SetParameter(oleDbCommand, "@DisplayName", session.DisplayName);
			this.SetParameter(oleDbCommand, "@ActiveTime", session.ActiveTime);
			this.SetParameter(oleDbCommand, "@Email", session.Email);
			this.SetParameter(oleDbCommand, "@IPAddress", session.IPAddress);
			this.SetParameter(oleDbCommand, "@Culture", session.Culture);
			this.SetParameter(oleDbCommand, "@Platform", session.Platform);
			this.SetParameter(oleDbCommand, "@Browser", session.Browser);
			this.SetParameter(oleDbCommand, "@AgentRating", session.AgentRating);
			this.SetParameter(oleDbCommand, "@SessionData", session.SessionData);
			this.SetParameter(oleDbCommand, "@Url", session.Url);
			this.SetParameter(oleDbCommand, "@Referrer", session.Referrer);
			oleDbCommand.ExecuteNonQuery();
		}

		public void DeleteAgentSessions(DateTime start, DateTime endtime, string agentid, string departmentid, string customer, string email, string ipaddr, string culture)
		{
			OleDbCommand str = this.CreateCommand();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("DELETE ").Append(this.prefix).Append("SupportSession WHERE BeginTime>=@start AND BeginTime<=@endtime");
			this.SetParameter(str, "@start", start);
			this.SetParameter(str, "@endtime", endtime);
			if ((agentid == null ? false : agentid != ""))
			{
				stringBuilder.Append(" AND AgentUserId=@agentid");
				this.SetParameter(str, "@agentid", agentid);
			}
			if ((departmentid == null ? false : departmentid != ""))
			{
				stringBuilder.Append(" AND DepartmentId=@departmentid");
				this.SetParameter(str, "@departmentid", departmentid);
			}
			if ((customer == null ? false : customer != ""))
			{
				stringBuilder.Append(" AND DisplayName LIKE @customer");
				this.SetParameter(str, "@customer", string.Concat("%", customer, "%"));
			}
			if ((email == null ? false : email != ""))
			{
				stringBuilder.Append(" AND Email LIKE @email");
				this.SetParameter(str, "@email", string.Concat("%", email, "%"));
			}
			if ((ipaddr == null ? false : ipaddr != ""))
			{
				stringBuilder.Append(" AND IPAddress LIKE @ipaddr");
				this.SetParameter(str, "@ipaddr", string.Concat("%", ipaddr, "%"));
			}
			if ((culture == null ? false : culture != ""))
			{
				stringBuilder.Append(" AND Culture LIKE @culture");
				this.SetParameter(str, "@culture", string.Concat("%", culture, "%"));
			}
			str.CommandText = stringBuilder.ToString();
			this.ApplyParameters(str);
			str.ExecuteNonQuery();
		}

		public void DeleteFeedback(int feedbackid)
		{
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("DELETE FROM ", this.prefix, "SupportFeedback WHERE FeedbackId=@FeedbackId");
			this.SetParameter(oleDbCommand, "@FeedbackId", feedbackid);
			oleDbCommand.ExecuteNonQuery();
		}

		public void DeleteInstantMessages(string ownerid, string targetid)
		{
			OleDbCommand str = this.CreateCommand();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("DELETE ");
			stringBuilder.Append(this.prefix);
			stringBuilder.Append("InstantMessage ");
			stringBuilder.Append("WHERE 1=1");
			if ((ownerid == null ? false : ownerid != ""))
			{
				if ((targetid == null ? false : targetid != ""))
				{
					stringBuilder.Append(" AND (  (SenderId=@ownerid AND TargetId=@targetid) OR (SenderId=@targetid AND TargetId=@ownerid) ) ");
				}
				else
				{
					stringBuilder.Append(" AND ( SenderId=@ownerid OR TargetId=@ownerid ) ");
				}
				this.SetParameter(str, "@ownerid", ownerid);
				this.SetParameter(str, "@targetid", targetid);
			}
			else if ((targetid == null ? false : targetid != ""))
			{
				stringBuilder.Append(" AND ( SenderId=@targetid OR TargetId=@targetid ) ");
				this.SetParameter(str, "@targetid", targetid);
			}
			str.CommandText = stringBuilder.ToString();
			this.ApplyParameters(str);
			str.ExecuteNonQuery();
		}

		public void DeleteLobby(IChatLobby ilobby)
		{
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("DELETE ", this.prefix, "Lobby WHERE LobbyId=@lobbyid");
			this.SetParameter(oleDbCommand, "@lobbyid", ilobby.LobbyId);
			oleDbCommand.ExecuteNonQuery();
		}

		public void DeleteMessagesBeforeDays(int days)
		{
			DateTime dateTime = DateTime.Now.AddDays((double)(-days));
			OleDbCommand oleDbCommand = this.CreateCommand();
			this.SetParameter(oleDbCommand, "@date", dateTime);
			oleDbCommand.CommandText = string.Concat("DELETE FROM ", this.prefix, "SupportMessage WHERE MsgTime<@date");
			oleDbCommand.ExecuteNonQuery();
			oleDbCommand.CommandText = string.Concat("DELETE FROM ", this.prefix, "InstantMessage WHERE MsgTime<@date");
			oleDbCommand.ExecuteNonQuery();
			oleDbCommand.CommandText = string.Concat("DELETE FROM ", this.prefix, "LogMessage WHERE MsgTime<@date");
			oleDbCommand.ExecuteNonQuery();
		}

		public void DeleteRule(IChatRule irule)
		{
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("DELETE ", this.prefix, "Rule WHERE RuleId=@ruleid");
			this.SetParameter(oleDbCommand, "@ruleid", irule.RuleId);
			oleDbCommand.ExecuteNonQuery();
		}

		public void Dispose()
		{
			this.conn.Dispose();
		}

		public virtual string GetCustomerData(string userid)
		{
			string str;
			if (userid == null)
			{
				throw new ArgumentNullException("userid");
			}
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT CustomerData FROM ", this.prefix, "SupportCustomer WHERE CustomerId=@userid");
			this.SetParameter(oleDbCommand, "@userid", userid);
			using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
			{
				if (oleDbDataReader.Read())
				{
					if (!oleDbDataReader.IsDBNull(0))
					{
						str = Convert.ToString(oleDbDataReader.GetValue(0));
						return str;
					}
					else
					{
						str = null;
						return str;
					}
				}
			}
			str = null;
			return str;
		}

		public int GetCustomerSessionCount(string customerid)
		{
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT COUNT(*) FROM ", this.prefix, "SupportSession WHERE CustomerId=@CustomerId");
			this.SetParameter(oleDbCommand, "@CustomerId", customerid);
			return Convert.ToInt32(oleDbCommand.ExecuteScalar());
		}

		public SupportSession GetLastSession(string customerid)
		{
			SupportSession supportSession;
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT * FROM (SELECT * FROM ", this.prefix, "SupportSession WHERE CustomerId=@CustomerId ORDER BY SessionId DESC) WHERE rownum<=1");
			this.SetParameter(oleDbCommand, "@CustomerId", customerid);
			SupportSession[] supportSessionArray = this.LoadSessions(oleDbCommand);
			if ((int)supportSessionArray.Length != 1)
			{
				supportSession = null;
			}
			else
			{
				supportSession = supportSessionArray[0];
			}
			return supportSession;
		}

		public IChatLobby[] GetLobbies()
		{
			ArrayList arrayLists = new ArrayList();
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT LobbyId,Title,Topic,Announcement,MaxOnlineCount,Locked,AllowAnonymous,Password,Description,Integration,ManagerList,MaxIdleMinute,AutoAwayMinute,HistoryCount,HistoryDay,SortIndex FROM ", this.prefix, "Lobby ORDER BY SortIndex ASC,LobbyId ASC");
			using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
			{
				while (oleDbDataReader.Read())
				{
					AppLobby appLobby = new AppLobby()
					{
						LobbyId = Convert.ToInt32(oleDbDataReader.GetValue(0)),
						Title = Convert.ToString(oleDbDataReader.GetValue(1)),
						Topic = Convert.ToString(oleDbDataReader.GetValue(2)),
						Announcement = Convert.ToString(oleDbDataReader.GetValue(3)),
						MaxOnlineCount = Convert.ToInt32(oleDbDataReader.GetValue(4)),
						Locked = Convert.ToInt32(oleDbDataReader.GetValue(5)) > 0,
						AllowAnonymous = Convert.ToInt32(oleDbDataReader.GetValue(6)) > 0,
						Password = Convert.ToString(oleDbDataReader.GetValue(7)),
						Description = Convert.ToString(oleDbDataReader.GetValue(8)),
						Integration = Convert.ToString(oleDbDataReader.GetValue(9)),
						ManagerList = Convert.ToString(oleDbDataReader.GetValue(10)),
						MaxIdleMinute = Convert.ToInt32(oleDbDataReader.GetValue(11)),
						AutoAwayMinute = Convert.ToInt32(oleDbDataReader.GetValue(12)),
						HistoryCount = Convert.ToInt32(oleDbDataReader.GetValue(13)),
						HistoryDay = Convert.ToInt32(oleDbDataReader.GetValue(14)),
						SortIndex = Convert.ToInt32(oleDbDataReader.GetValue(15))
					};
					arrayLists.Add(appLobby);
				}
			}
			return (IChatLobby[])arrayLists.ToArray(typeof(IChatLobby));
		}

		public IChatLobby GetLobby(int lobbyid)
		{
			IChatLobby chatLobby;
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT LobbyId,Title,Topic,Announcement,MaxOnlineCount,Locked,AllowAnonymous,Password,Description,Integration,ManagerList,MaxIdleMinute,AutoAwayMinute,HistoryCount,HistoryDay,SortIndex FROM ", this.prefix, "Lobby WHERE LobbyId=@lobbyid");
			this.SetParameter(oleDbCommand, "@lobbyid", lobbyid);
			using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
			{
				if (oleDbDataReader.Read())
				{
					AppLobby appLobby = new AppLobby()
					{
						LobbyId = lobbyid,
						Title = Convert.ToString(oleDbDataReader.GetValue(1)),
						Topic = Convert.ToString(oleDbDataReader.GetValue(2)),
						Announcement = Convert.ToString(oleDbDataReader.GetValue(3)),
						MaxOnlineCount = Convert.ToInt32(oleDbDataReader.GetValue(4)),
						Locked = Convert.ToInt32(oleDbDataReader.GetValue(5)) > 0,
						AllowAnonymous = Convert.ToInt32(oleDbDataReader.GetValue(6)) > 0,
						Password = Convert.ToString(oleDbDataReader.GetValue(7)),
						Description = Convert.ToString(oleDbDataReader.GetValue(8)),
						Integration = Convert.ToString(oleDbDataReader.GetValue(9)),
						ManagerList = Convert.ToString(oleDbDataReader.GetValue(10)),
						MaxIdleMinute = Convert.ToInt32(oleDbDataReader.GetValue(11)),
						AutoAwayMinute = Convert.ToInt32(oleDbDataReader.GetValue(12)),
						HistoryCount = Convert.ToInt32(oleDbDataReader.GetValue(13)),
						HistoryDay = Convert.ToInt32(oleDbDataReader.GetValue(14)),
						SortIndex = Convert.ToInt32(oleDbDataReader.GetValue(15))
					};
					chatLobby = appLobby;
				}
				else
				{
					chatLobby = null;
				}
			}
			return chatLobby;
		}

#pragma warning disable CS0436 // Type conflicts with imported type
		public ChatMsgData[] GetMessages(string location, string placename, int pagesize, int pageindex, out int totalcount)
#pragma warning restore CS0436 // Type conflicts with imported type
		{
			string str;
#pragma warning disable CS0436 // Type conflicts with imported type
			ChatMsgData[] array;
#pragma warning restore CS0436 // Type conflicts with imported type
			if (location != null)
			{
				location = location.ToLower();
				if (location == "messenger")
				{
					str = "InstantMessage";
					location = null;
					placename = null;
				}
				else if (location != "support")
				{
					str = "LogMessage";
				}
				else
				{
					str = "SupportMessage";
					location = null;
					placename = null;
				}
				OleDbCommand oleDbCommand = this.CreateCommand();
				oleDbCommand.CommandText = string.Concat("SELECT MessageId FROM ", this.prefix, str);
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append("SELECT MessageId FROM ").Append(this.prefix).Append(str);
				ArrayList arrayLists = new ArrayList();
				if (location != null)
				{
					this.SetParameter(oleDbCommand, "@location", location);
					arrayLists.Add("Location=@location");
				}
				if (placename != null)
				{
					this.SetParameter(oleDbCommand, "@placename", placename);
					arrayLists.Add("Place=@placename");
				}
				if (arrayLists.Count != 0)
				{
					for (int i = 0; i < arrayLists.Count; i++)
					{
						if (i != 0)
						{
							stringBuilder.Append(" AND ");
						}
						else
						{
							stringBuilder.Append(" WHERE ");
						}
						stringBuilder.Append(arrayLists[i]);
					}
				}
				ArrayList arrayLists1 = new ArrayList();
				oleDbCommand.CommandText = stringBuilder.ToString();
				this.ApplyParameters(oleDbCommand);
				using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
				{
					while (oleDbDataReader.Read())
					{
						arrayLists1.Add(oleDbDataReader.GetInt32(0));
					}
				}
				totalcount = arrayLists1.Count;
				stringBuilder = new StringBuilder();
				stringBuilder.Append("SELECT * FROM ").Append(this.prefix).Append(str);
				stringBuilder.Append(" WHERE MessageId IN (");
				int num = pageindex * pagesize;
				int num1 = num + pagesize;
				int num2 = num;
				while (true)
				{
					if ((num2 >= num1 ? true : num2 >= arrayLists1.Count))
					{
						break;
					}
					stringBuilder.Append(arrayLists1[num2]).Append(",");
					num2++;
				}
				stringBuilder.Append("0)");
				ArrayList arrayLists2 = new ArrayList();
				oleDbCommand.CommandText = stringBuilder.ToString();
				this.ApplyParameters(oleDbCommand);
				using (OleDbDataReader oleDbDataReader1 = oleDbCommand.ExecuteReader())
				{
					int num3 = OracleOleDbDataProvider.SafeGetOrdinal(oleDbDataReader1, "Location");
					int num4 = OracleOleDbDataProvider.SafeGetOrdinal(oleDbDataReader1, "Place");
					int num5 = OracleOleDbDataProvider.SafeGetOrdinal(oleDbDataReader1, "Whisper");
					while (oleDbDataReader1.Read())
					{
#pragma warning disable CS0436 // Type conflicts with imported type
						ChatMsgData chatMsgDatum = new ChatMsgData()
#pragma warning restore CS0436 // Type conflicts with imported type
						{
							MessageId = oleDbDataReader1.GetInt32(0),
							Text = Convert.ToString(oleDbDataReader1["Text"]),
							Html = Convert.ToString(oleDbDataReader1["Html"]),
							ClaimNo = Convert.ToString(oleDbDataReader1["ClaimNo"]),
							Sender = Convert.ToString(oleDbDataReader1["Sender"]),
							SenderId = Convert.ToString(oleDbDataReader1["SenderId"]),
							Target = Convert.ToString(oleDbDataReader1["Target"]),
							TargetId = Convert.ToString(oleDbDataReader1["TargetId"]),
							Time = Convert.ToDateTime(oleDbDataReader1["MsgTime"])
						};
						if (num3 != -1)
						{
							chatMsgDatum.Location = oleDbDataReader1.GetString(num3);
						}
						if (num4 != -1)
						{
							chatMsgDatum.PlaceName = oleDbDataReader1.GetString(num4);
						}
						if (num5 != -1)
						{
							chatMsgDatum.Whisper = oleDbDataReader1.GetInt32(num5) != 0;
						}
						arrayLists2.Add(chatMsgDatum);
					}
				}
#pragma warning disable CS0436 // Type conflicts with imported type
				array = (ChatMsgData[])arrayLists2.ToArray(typeof(ChatMsgData));
#pragma warning restore CS0436 // Type conflicts with imported type
			}
			else
			{
				totalcount = 0;
#pragma warning disable CS0436 // Type conflicts with imported type
				array = new ChatMsgData[0];
#pragma warning restore CS0436 // Type conflicts with imported type
			}
			return array;
		}

		public IChatPortalInfo GetPortalInfo(string portalname)
		{
			string str;
			AppPortalInfo appPortalInfo = new AppPortalInfo(portalname);
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT Properties FROM ", this.prefix, "Portal WHERE PortalName=@portalname");
			this.SetParameter(oleDbCommand, "@portalname", portalname);
			using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
			{
				if (oleDbDataReader.Read())
				{
					AppPortalInfo appPortalInfo1 = appPortalInfo;
					if (oleDbDataReader.IsDBNull(0))
					{
						str = null;
					}
					else
					{
						str = Convert.ToString(oleDbDataReader.GetValue(0));
					}
					appPortalInfo1.Properties = str;
				}
			}
			return appPortalInfo;
		}

		public IChatRule GetRule(int ruleid)
		{
			IChatRule chatRule;
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT RuleId,Disabled,RuleMode,Expression,Category,SortIndex FROM ", this.prefix, "Rule WHERE RuleId=@ruleid");
			this.SetParameter(oleDbCommand, "@ruleid", ruleid);
			using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
			{
				if (oleDbDataReader.Read())
				{
					AppRule appRule = new AppRule()
					{
						RuleId = ruleid,
						Disabled = Convert.ToInt32(oleDbDataReader.GetValue(1)) > 0,
						Mode = Convert.ToString(oleDbDataReader.GetValue(2)),
						Expression = Convert.ToString(oleDbDataReader.GetValue(3)),
						Category = Convert.ToString(oleDbDataReader.GetValue(4)),
						Sort = Convert.ToInt32(oleDbDataReader.GetValue(5))
					};
					chatRule = appRule;
				}
				else
				{
					chatRule = null;
				}
			}
			return chatRule;
		}

		public IChatRule[] GetRules()
		{
			ArrayList arrayLists = new ArrayList();
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT RuleId,Disabled,RuleMode,Expression,Category,SortIndex FROM ", this.prefix, "Rule");
			using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
			{
				while (oleDbDataReader.Read())
				{
					AppRule appRule = new AppRule()
					{
						RuleId = Convert.ToInt32(oleDbDataReader.GetValue(0)),
						Disabled = Convert.ToInt32(oleDbDataReader.GetValue(1)) > 0,
						Mode = Convert.ToString(oleDbDataReader.GetValue(2)),
						Expression = Convert.ToString(oleDbDataReader.GetValue(3)),
						Category = Convert.ToString(oleDbDataReader.GetValue(4)),
						Sort = Convert.ToInt32(oleDbDataReader.GetValue(5))
					};
					arrayLists.Add(appRule);
				}
			}
			return (IChatRule[])arrayLists.ToArray(typeof(IChatRule));
		}

		public IChatUserInfo GetUserInfo(string userid)
		{
			IChatUserInfo chatUserInfo;
			string str;
			string str1;
			string str2;
			string str3;
			string str4;
			string str5;
			string str6;
			AppUserInfo appUserInfo = new AppUserInfo(userid)
			{
				IsStored = false
			};
			if (ChatProvider.GetInstance(this.Portal).IsRegisteredUser(userid))
			{
				OleDbCommand oleDbCommand = this.CreateCommand();
				oleDbCommand.CommandText = string.Concat("SELECT DisplayName,Description,ServerProperties,PublicProperties,PrivateProperties,BuildinContacts,BuildinIgnores FROM ", this.prefix, "User WHERE UserId=@userid");
				this.SetParameter(oleDbCommand, "@userid", userid);
				using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
				{
					if (oleDbDataReader.Read())
					{
						AppUserInfo appUserInfo1 = appUserInfo;
						if (oleDbDataReader.IsDBNull(0))
						{
							str = null;
						}
						else
						{
							str = Convert.ToString(oleDbDataReader.GetValue(0));
						}
						appUserInfo1.DisplayName = str;
						AppUserInfo appUserInfo2 = appUserInfo;
						if (oleDbDataReader.IsDBNull(1))
						{
							str1 = null;
						}
						else
						{
							str1 = Convert.ToString(oleDbDataReader.GetValue(1));
						}
						appUserInfo2.Description = str1;
						AppUserInfo appUserInfo3 = appUserInfo;
						if (oleDbDataReader.IsDBNull(2))
						{
							str2 = null;
						}
						else
						{
							str2 = Convert.ToString(oleDbDataReader.GetValue(2));
						}
						appUserInfo3.ServerProperties = str2;
						AppUserInfo appUserInfo4 = appUserInfo;
						if (oleDbDataReader.IsDBNull(3))
						{
							str3 = null;
						}
						else
						{
							str3 = Convert.ToString(oleDbDataReader.GetValue(3));
						}
						appUserInfo4.PublicProperties = str3;
						AppUserInfo appUserInfo5 = appUserInfo;
						if (oleDbDataReader.IsDBNull(4))
						{
							str4 = null;
						}
						else
						{
							str4 = Convert.ToString(oleDbDataReader.GetValue(4));
						}
						appUserInfo5.PrivateProperties = str4;
						AppUserInfo appUserInfo6 = appUserInfo;
						if (oleDbDataReader.IsDBNull(5))
						{
							str5 = null;
						}
						else
						{
							str5 = Convert.ToString(oleDbDataReader.GetValue(5));
						}
						appUserInfo6.BuildinContacts = str5;
						AppUserInfo appUserInfo7 = appUserInfo;
						if (oleDbDataReader.IsDBNull(6))
						{
							str6 = null;
						}
						else
						{
							str6 = Convert.ToString(oleDbDataReader.GetValue(6));
						}
						appUserInfo7.BuildinIgnores = str6;
						appUserInfo.IsStored = true;
					}
				}
				if (appUserInfo.DisplayName == null)
				{
				}
				chatUserInfo = appUserInfo;
			}
			else
			{
				chatUserInfo = appUserInfo;
			}
			return chatUserInfo;
		}

		public SupportSession[] LoadAgentSessions(string agentid)
		{
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT * FROM ", this.prefix, "SupportSession WHERE AgentUserId=@AgentUserId ORDER BY SessionID ASC");
			this.SetParameter(oleDbCommand, "@AgentUserId", agentid);
			return this.LoadSessions(oleDbCommand);
		}

#pragma warning disable CS0436 // Type conflicts with imported type
		public ChatMsgData[] LoadChannelHistoryMessages(string channelname, int maxcount, DateTime afterdate)
#pragma warning restore CS0436 // Type conflicts with imported type
		{
			ArrayList arrayLists = new ArrayList();
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat(new object[] { "SELECT * FROM (SELECT MessageId,MsgTime,Location,SenderId,Sender,TargetId,Target,Whisper,Text,Html,ClaimNo FROM ", this.prefix, "LogMessage WHERE Place=@placename AND MsgTime>=@afterdate ORDER BY MsgTime DESC ) WHERE rownum<=", maxcount });
			this.SetParameter(oleDbCommand, "@placename", channelname);
			this.SetParameter(oleDbCommand, "@afterdate", afterdate);
			using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
			{
				while (oleDbDataReader.Read())
				{
#pragma warning disable CS0436 // Type conflicts with imported type
					ChatMsgData chatMsgDatum = new ChatMsgData()
#pragma warning restore CS0436 // Type conflicts with imported type
					{
						MessageId = Convert.ToInt32(oleDbDataReader.GetValue(0)),
						Time = oleDbDataReader.GetDateTime(1),
						Location = Convert.ToString(oleDbDataReader.GetValue(2)),
						SenderId = Convert.ToString(oleDbDataReader.GetValue(3)),
						Sender = Convert.ToString(oleDbDataReader.GetValue(4))
					};
					if (!oleDbDataReader.IsDBNull(5))
					{
						chatMsgDatum.TargetId = Convert.ToString(oleDbDataReader.GetValue(5));
					}
					if (!oleDbDataReader.IsDBNull(6))
					{
						chatMsgDatum.Target = Convert.ToString(oleDbDataReader.GetValue(6));
					}
					if (!oleDbDataReader.IsDBNull(8))
					{
						chatMsgDatum.Text = Convert.ToString(oleDbDataReader.GetValue(8));
					}
					if (!oleDbDataReader.IsDBNull(9))
					{
						chatMsgDatum.Html = Convert.ToString(oleDbDataReader.GetValue(9));
					}
					if (!oleDbDataReader.IsDBNull(10))
					{
						chatMsgDatum.ClaimNo = Convert.ToString(oleDbDataReader.GetValue(10));
					}
					chatMsgDatum.Whisper = Convert.ToInt32(oleDbDataReader.GetValue(7)) == 1;
					arrayLists.Add(chatMsgDatum);
				}
			}
			arrayLists.Reverse();
#pragma warning disable CS0436 // Type conflicts with imported type
			return (ChatMsgData[])arrayLists.ToArray(typeof(ChatMsgData));
#pragma warning restore CS0436 // Type conflicts with imported type
		}

		public SupportSession[] LoadCustomerSessions(string customerid)
		{
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT * FROM ", this.prefix, "SupportSession WHERE CustomerId=@CustomerId ORDER BY SessionID ASC");
			this.SetParameter(oleDbCommand, "@CustomerId", customerid);
			return this.LoadSessions(oleDbCommand);
		}

		public SupportDepartment[] LoadDepartments()
		{
			ArrayList arrayLists = new ArrayList();
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT DepartmentId,DepartmentName FROM ", this.prefix, "SupportDepartment");
			using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
			{
				while (oleDbDataReader.Read())
				{
					SupportDepartment supportDepartment = new SupportDepartment()
					{
						DepartmentId = Convert.ToInt32(oleDbDataReader.GetValue(0)),
						Name = Convert.ToString(oleDbDataReader.GetValue(1))
					};
					arrayLists.Add(supportDepartment);
				}
			}
			ArrayList arrayLists1 = new ArrayList();
			foreach (SupportDepartment arrayList in arrayLists)
			{
				oleDbCommand.CommandText = string.Concat("SELECT AgentUserId FROM ", this.prefix, "SupportAgent WHERE DepartmentId=@depid");
				this.SetParameter(oleDbCommand, "@depid", arrayList.DepartmentId);
				using (OleDbDataReader oleDbDataReader1 = oleDbCommand.ExecuteReader())
				{
					while (oleDbDataReader1.Read())
					{
						SupportAgent supportAgent = new SupportAgent()
						{
							UserId = Convert.ToString(oleDbDataReader1.GetValue(0)),
							Department = arrayList
						};
						arrayLists1.Add(supportAgent);
					}
				}
				if (arrayLists1.Count != 0)
				{
					arrayList.Agents = (SupportAgent[])arrayLists1.ToArray(typeof(SupportAgent));
					arrayLists1 = new ArrayList();
				}
			}
			return (SupportDepartment[])arrayLists.ToArray(typeof(SupportDepartment));
		}

		public SupportFeedback LoadFeedback(int feedbackid)
		{
			SupportFeedback supportFeedback;
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT * FROM ", this.prefix, "SupportFeedback WHERE FeedbackId=@FeedbackId");
			this.SetParameter(oleDbCommand, "@FeedbackId", feedbackid);
			using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
			{
				if (oleDbDataReader.Read())
				{
					SupportFeedback supportFeedback1 = new SupportFeedback()
					{
						FeedbackId = Convert.ToInt32(oleDbDataReader["FeedbackId"]),
						FbTime = (DateTime)oleDbDataReader["FbTime"],
						Name = (string)oleDbDataReader["Name"],
						DisplayName = (string)oleDbDataReader["DisplayName"],
						Email = (string)oleDbDataReader["Email"],
						Title = (string)oleDbDataReader["Title"],
						Content = (string)oleDbDataReader["Content"],
						Comment = oleDbDataReader["f_Comment"] as string,
						CommentBy = oleDbDataReader["CommentBy"] as string
					};
					supportFeedback = supportFeedback1;
					return supportFeedback;
				}
			}
			supportFeedback = null;
			return supportFeedback;
		}

		public SupportFeedback[] LoadFeedbacks(bool history)
		{
			OleDbCommand oleDbCommand = this.CreateCommand();
			OleDbCommand oleDbCommand1 = oleDbCommand;
			string[] strArrays = new string[] { "SELECT * FROM ", this.prefix, "SupportFeedback ", null, null };
			strArrays[3] = (history ? "" : "WHERE CommentBy IS NULL");
			strArrays[4] = " ORDER BY FeedbackId ASC";
			oleDbCommand1.CommandText = string.Concat(strArrays);
			ArrayList arrayLists = new ArrayList();
			using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
			{
				while (oleDbDataReader.Read())
				{
					SupportFeedback supportFeedback = new SupportFeedback()
					{
						FeedbackId = Convert.ToInt32(oleDbDataReader["FeedbackId"]),
						FbTime = (DateTime)oleDbDataReader["FbTime"],
						Name = oleDbDataReader["Name"].ToString(),
						DisplayName = oleDbDataReader["DisplayName"].ToString(),
						Email = oleDbDataReader["Email"].ToString(),
						Title = oleDbDataReader["Title"].ToString(),
						Content = oleDbDataReader["Content"].ToString(),
						Comment = oleDbDataReader["f_Comment"] as string,
						CommentBy = oleDbDataReader["CommentBy"] as string
					};
					arrayLists.Add(supportFeedback);
				}
			}
			return (SupportFeedback[])arrayLists.ToArray(typeof(SupportFeedback));
		}

#pragma warning disable CS0436 // Type conflicts with imported type
		public ChatMsgData[] LoadInstantMessages(string ownerid, string targetid, bool offlineOnly, int maxid, int pagesize)
#pragma warning restore CS0436 // Type conflicts with imported type
		{
			ArrayList arrayLists = new ArrayList();
			OleDbCommand str = this.CreateCommand();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("SELECT ");
			if ((pagesize <= 0 ? false : pagesize < 2147483647))
			{
				stringBuilder.Append("TOP ");
				stringBuilder.Append(pagesize);
				stringBuilder.Append(" ");
			}
			stringBuilder.Append("MessageId,MsgTime,SenderId,Sender,TargetId,Target,f_Offline,Text,Html ");
			stringBuilder.Append("From ");
			stringBuilder.Append(this.prefix);
			stringBuilder.Append("InstantMessage ");
			stringBuilder.Append("WHERE ");
			if (offlineOnly)
			{
				if (maxid >= 2147483647)
				{
					stringBuilder.Append(" f_Offline=1 ");
				}
				else
				{
					stringBuilder.Append(" MessageId<@maxid AND f_Offline=1 ");
					this.SetParameter(str, "@maxid", maxid);
				}
			}
			else if (maxid >= 2147483647)
			{
				stringBuilder.Append(" 1=1 ");
			}
			else
			{
				stringBuilder.Append(" MessageId<@maxid ");
				this.SetParameter(str, "@maxid", maxid);
			}
			if ((ownerid == null ? false : ownerid != ""))
			{
				if ((targetid == null ? false : targetid != ""))
				{
					stringBuilder.Append(" AND (  (SenderId=@ownerid AND TargetId=@targetid) OR (SenderId=@targetid AND TargetId=@ownerid) ) ");
				}
				else
				{
					stringBuilder.Append(" AND ( SenderId=@ownerid OR TargetId=@ownerid ) ");
				}
				this.SetParameter(str, "@ownerid", ownerid);
			}
			else if ((targetid == null ? false : targetid != ""))
			{
				stringBuilder.Append(" AND ( SenderId=@targetid OR TargetId=@targetid ) ");
				this.SetParameter(str, "@targetid", targetid);
			}
			stringBuilder.Append("ORDER BY MsgTime DESC");
			str.CommandText = stringBuilder.ToString();
			this.ApplyParameters(str);
			using (OleDbDataReader oleDbDataReader = str.ExecuteReader())
			{
				while (oleDbDataReader.Read())
				{
#pragma warning disable CS0436 // Type conflicts with imported type
					ChatMsgData chatMsgDatum = new ChatMsgData()
#pragma warning restore CS0436 // Type conflicts with imported type
					{
						MessageId = Convert.ToInt32(oleDbDataReader.GetValue(0)),
						Time = oleDbDataReader.GetDateTime(1),
						SenderId = Convert.ToString(oleDbDataReader.GetValue(2)),
						Sender = Convert.ToString(oleDbDataReader.GetValue(3)),
						TargetId = Convert.ToString(oleDbDataReader.GetValue(4)),
						Target = Convert.ToString(oleDbDataReader.GetValue(5))
					};
					if (!oleDbDataReader.IsDBNull(7))
					{
						chatMsgDatum.Text = Convert.ToString(oleDbDataReader.GetValue(7));
					}
					if (!oleDbDataReader.IsDBNull(8))
					{
						chatMsgDatum.Html = Convert.ToString(oleDbDataReader.GetValue(8));
					}
					chatMsgDatum.Offline = Convert.ToInt32(oleDbDataReader.GetValue(6)) == 1;
					arrayLists.Add(chatMsgDatum);
				}
			}

#pragma warning disable CS0436 // Type conflicts with imported type
			return arrayLists.ToArray(typeof(ChatMsgData)) as ChatMsgData[];
#pragma warning restore CS0436 // Type conflicts with imported type

		}

#pragma warning disable CS0436 // Type conflicts with imported type
		public ChatMsgData[] LoadInstantOfflineMessages(string targetid, DateTime time)
#pragma warning restore CS0436 // Type conflicts with imported type
		{
			ArrayList arrayLists = new ArrayList();
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT MessageId,MsgTime,SenderId,Sender,TargetId,Target,f_Offline,Text,Html FROM ", this.prefix, "InstantMessage WHERE f_Offline=1 AND MsgTime>@time AND TargetId=@targetid ORDER BY MsgTime DESC");
			this.SetParameter(oleDbCommand, "@time", time);
			this.SetParameter(oleDbCommand, "@targetid", targetid);
			using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
			{
				while (oleDbDataReader.Read())
				{
#pragma warning disable CS0436 // Type conflicts with imported type
					ChatMsgData chatMsgDatum = new ChatMsgData()
#pragma warning restore CS0436 // Type conflicts with imported type
					{
						MessageId = Convert.ToInt32(oleDbDataReader.GetValue(0)),
						Location = "Messenger",
						Time = oleDbDataReader.GetDateTime(1),
						SenderId = Convert.ToString(oleDbDataReader.GetValue(2)),
						Sender = Convert.ToString(oleDbDataReader.GetValue(3)),
						TargetId = Convert.ToString(oleDbDataReader.GetValue(4)),
						Target = Convert.ToString(oleDbDataReader.GetValue(5))
					};
					if (!oleDbDataReader.IsDBNull(7))
					{
						chatMsgDatum.Text = Convert.ToString(oleDbDataReader.GetValue(7));
					}
					if (!oleDbDataReader.IsDBNull(8))
					{
						chatMsgDatum.Html = Convert.ToString(oleDbDataReader.GetValue(8));
					}
					chatMsgDatum.Offline = Convert.ToInt32(oleDbDataReader.GetValue(6)) == 1;
					arrayLists.Add(chatMsgDatum);
				}
			}
			arrayLists.Reverse();
#pragma warning disable CS0436 // Type conflicts with imported type
			return (ChatMsgData[])arrayLists.ToArray(typeof(ChatMsgData));
#pragma warning restore CS0436 // Type conflicts with imported type
		}

#pragma warning disable CS0436 // Type conflicts with imported type
		public ChatMsgData[] LoadSessionMessages(int sessionid)
#pragma warning restore CS0436 // Type conflicts with imported type
		{
			ArrayList arrayLists = new ArrayList();
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT MessageId,MsgTime,SenderId,Sender,TargetId,Target,Text,Html FROM ", this.prefix, "SupportMessage WHERE SessionId=@SessionId ORDER BY MessageID ASC");
			this.SetParameter(oleDbCommand, "@SessionId", sessionid);
			using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
			{
				while (oleDbDataReader.Read())
				{
#pragma warning disable CS0436 // Type conflicts with imported type
					ChatMsgData chatMsgDatum = new ChatMsgData()
#pragma warning restore CS0436 // Type conflicts with imported type
					{
						MessageId = Convert.ToInt32(oleDbDataReader.GetValue(0)),
						Time = oleDbDataReader.GetDateTime(1),
						SenderId = Convert.ToString(oleDbDataReader.GetValue(2)),
						Sender = Convert.ToString(oleDbDataReader.GetValue(3))
					};
					if (!oleDbDataReader.IsDBNull(4))
					{
						chatMsgDatum.TargetId = Convert.ToString(oleDbDataReader.GetValue(4));
					}
					if (!oleDbDataReader.IsDBNull(5))
					{
						chatMsgDatum.Target = Convert.ToString(oleDbDataReader.GetValue(5));
					}
					if (!oleDbDataReader.IsDBNull(6))
					{
						chatMsgDatum.Text = Convert.ToString(oleDbDataReader.GetValue(6));
					}
					if (!oleDbDataReader.IsDBNull(7))
					{
						chatMsgDatum.Html = Convert.ToString(oleDbDataReader.GetValue(7));
					}
					arrayLists.Add(chatMsgDatum);
				}
			}
			arrayLists.Reverse();

#pragma warning disable CS0436 // Type conflicts with imported type
			return (ChatMsgData[])arrayLists.ToArray(typeof(ChatMsgData));
#pragma warning restore CS0436 // Type conflicts with imported type
		}

		private SupportSession[] LoadSessions(OleDbCommand cmd)
		{
			ArrayList arrayLists = new ArrayList();
			using (OleDbDataReader oleDbDataReader = cmd.ExecuteReader())
			{
				while (oleDbDataReader.Read())
				{
					SupportSession supportSession = new SupportSession()
					{
						SessionId = Convert.ToInt32(oleDbDataReader["SessionId"]),
						BeginTime = (DateTime)oleDbDataReader["BeginTime"],
						DepartmentId = Convert.ToInt32(oleDbDataReader["DepartmentId"]),
						AgentUserId = (string)oleDbDataReader["AgentUserId"],
						CustomerId = (string)oleDbDataReader["CustomerId"],
						DisplayName = (string)oleDbDataReader["DisplayName"],
						ActiveTime = (DateTime)oleDbDataReader["ActiveTime"],
						Email = oleDbDataReader["Email"].ToString(),
						IPAddress = oleDbDataReader["IPAddress"].ToString(),
						Culture = oleDbDataReader["Culture"].ToString(),
						Platform = oleDbDataReader["Platform"].ToString(),
						Browser = oleDbDataReader["Browser"].ToString(),
						AgentRating = Convert.ToInt32(oleDbDataReader["AgentRating"]),
						SessionData = oleDbDataReader["SessionData"] as string,
						Url = oleDbDataReader["Url"] as string,
						Referrer = oleDbDataReader["Referrer"] as string
					};
					arrayLists.Add(supportSession);
				}
			}
			return (SupportSession[])arrayLists.ToArray(typeof(SupportSession));
		}

		public string LoadSetting(string name)
		{
			string str;
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT SettingData FROM ", this.prefix, "Settings WHERE SettingName=@Name");
			this.SetParameter(oleDbCommand, "@Name", name);
			using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
			{
				if (oleDbDataReader.Read())
				{
					str = Convert.ToString(oleDbDataReader.GetValue(0));
					return str;
				}
			}
			str = null;
			return str;
		}

		public void LogEvent(ChatPlace place, ChatIdentity user, string category, string message)
		{
			object value;
			object uniqueId;
			if (message.Length <= 3000)
			{
				if (!base.SkipEvent(place, user, category))
				{
					OleDbCommand oleDbCommand = this.CreateCommand();
					oleDbCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "LogEvent (EventId,EvtTime,Category,Portal,Place,UserName,UserId,Message) VALUES (@id,@time,@category,@portal,@place,@username,@userid,@message)");
					OleDbParameterCollection parameters = oleDbCommand.Parameters;
					this.SetParameter(oleDbCommand, "@id", this.NextIdentity(string.Concat(this.prefix, "LogEvent"), "EventId"));
					this.SetParameter(oleDbCommand, "@time", DateTime.Now);
					this.SetParameter(oleDbCommand, "@category", category);
					this.SetParameter(oleDbCommand, "@portal", place.Portal.Name);
					this.SetParameter(oleDbCommand, "@place", place.PlaceName);
					OleDbCommand oleDbCommand1 = oleDbCommand;
					if (user == null)
					{
						value = DBNull.Value;
					}
					else
					{
						value = user.DisplayName;
					}
					this.SetParameter(oleDbCommand1, "@username", value);
					OleDbCommand oleDbCommand2 = oleDbCommand;
					if (user == null)
					{
						uniqueId = DBNull.Value;
					}
					else
					{
						uniqueId = user.UniqueId;
					}
					this.SetParameter(oleDbCommand2, "@userid", uniqueId);
					this.SetParameter(oleDbCommand, "@message", message);
					oleDbCommand.ExecuteNonQuery();
				}
			}
		}

		public void LogInstantMessage(ChatIdentity sender, string targetid, string targetname, bool offline, string text, string html, string claimNo)
		{
			object value;
			object obj;
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "InstantMessage (MessageId,MsgTime,Sender,SenderId,Target,TargetId,f_Offline,IPAddress,Text,Html, ClaimNo) VALUES (@id,@time,@sendername,@senderid,@targetname,@targetid,@offline,@ipaddr,@text,@html, @claimno)");
			OleDbParameterCollection parameters = oleDbCommand.Parameters;
			this.SetParameter(oleDbCommand, "@id", this.NextIdentity(string.Concat(this.prefix, "InstantMessage"), "MessageId"));
			this.SetParameter(oleDbCommand, "@time", DateTime.Now);
			this.SetParameter(oleDbCommand, "@sendername", sender.DisplayName);
			this.SetParameter(oleDbCommand, "@senderid", sender.UniqueId);
			this.SetParameter(oleDbCommand, "@targetname", targetname);
			this.SetParameter(oleDbCommand, "@targetid", targetid);
			this.SetParameter(oleDbCommand, "@offline", (offline ? 1 : 0));
			this.SetParameter(oleDbCommand, "@ipaddr", sender.IPAddress);
			OleDbCommand oleDbCommand1 = oleDbCommand;
			if (text == null)
			{
				value = DBNull.Value;
			}
			else
			{
				value = text;
			}
			this.SetParameter(oleDbCommand1, "@text", value);
			OleDbCommand oleDbCommand2 = oleDbCommand;
			if (html == null)
			{
				obj = DBNull.Value;
			}
			else
			{
				obj = html;
			}
			this.SetParameter(oleDbCommand2, "@html", obj);

			OleDbCommand oleDbCommand3 = oleDbCommand;
			if (claimNo == null)
			{
				obj = DBNull.Value;
			}
			else
			{
				obj = html;
			}
			this.SetParameter(oleDbCommand3, "@claimno", obj);
			oleDbCommand.ExecuteNonQuery();
		}

		public void LogMessage(ChatPlace place, ChatIdentity sender, ChatIdentity target, bool whisper, string text, string html, string claimNo)
		{
			object value;
			object uniqueId;
			object obj;
			object value1;
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "LogMessage (MessageId,MsgTime,Location,Place,Sender,SenderId,Target,TargetId,Whisper,IPAddress,Text,Html,ClaimNo) VALUES (@id,@time,@location,@place,@sendername,@senderid,@targetname,@targetid,@whisper,@ipaddr,@text,@html, @claimno)");
			OleDbParameterCollection parameters = oleDbCommand.Parameters;
			this.SetParameter(oleDbCommand, "@id", this.NextIdentity(string.Concat(this.prefix, "LogMessage"), "MessageId"));
			this.SetParameter(oleDbCommand, "@time", DateTime.Now);
			this.SetParameter(oleDbCommand, "@location", place.Location);
			this.SetParameter(oleDbCommand, "@place", place.PlaceName);
			this.SetParameter(oleDbCommand, "@sendername", sender.DisplayName);
			this.SetParameter(oleDbCommand, "@senderid", sender.UniqueId);
			OleDbCommand oleDbCommand1 = oleDbCommand;
			if (target == null)
			{
				value = DBNull.Value;
			}
			else
			{
				value = target.DisplayName;
			}
			this.SetParameter(oleDbCommand1, "@targetname", value);
			OleDbCommand oleDbCommand2 = oleDbCommand;
			if (target == null)
			{
				uniqueId = DBNull.Value;
			}
			else
			{
				uniqueId = target.UniqueId;
			}
			this.SetParameter(oleDbCommand2, "@targetid", uniqueId);
			this.SetParameter(oleDbCommand, "@whisper", (whisper ? 1 : 0));
			this.SetParameter(oleDbCommand, "@ipaddr", sender.IPAddress);
			OleDbCommand oleDbCommand3 = oleDbCommand;
			if (text == null)
			{
				obj = DBNull.Value;
			}
			else
			{
				obj = text;
			}
			this.SetParameter(oleDbCommand3, "@text", obj);
			OleDbCommand oleDbCommand4 = oleDbCommand;
			if (html == null)
			{
				value1 = DBNull.Value;
			}
			else
			{
				value1 = html;
			}
			this.SetParameter(oleDbCommand4, "@html", value1);

			OleDbCommand oleDbCommand5 = oleDbCommand;
			if (claimNo == null)
			{
				value1 = DBNull.Value;
			}
			else
			{
				value1 = html;
			}
			this.SetParameter(oleDbCommand4, "@claimno", value1);
			oleDbCommand.ExecuteNonQuery();
		}

		public void LogSupportMessage(ChatPlace place, SupportSession session, string type, ChatIdentity sender, ChatIdentity target, string text, string html, string claimNo)
		{
			object value;
			object uniqueId;
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "SupportMessage (MessageId,MsgTime,SessionId,MsgType,Sender,SenderId,Target,TargetId,Text,Html,ClaimNo) VALUES (@id,@time,@sessionid,@msgtype,@sendername,@senderid,@targetname,@targetid,@text,@html, @claimno)");
			OleDbParameterCollection parameters = oleDbCommand.Parameters;
			this.SetParameter(oleDbCommand, "@id", this.NextIdentity(string.Concat(this.prefix, "SupportMessage"), "MessageId"));
			this.SetParameter(oleDbCommand, "@time", DateTime.Now);
			this.SetParameter(oleDbCommand, "@sessionid", session.SessionId);
			this.SetParameter(oleDbCommand, "@msgtype", type);
			this.SetParameter(oleDbCommand, "@sendername", sender.DisplayName);
			this.SetParameter(oleDbCommand, "@senderid", sender.UniqueId);
			OleDbCommand oleDbCommand1 = oleDbCommand;
			if (target == null)
			{
				value = DBNull.Value;
			}
			else
			{
				value = target.DisplayName;
			}
			this.SetParameter(oleDbCommand1, "@targetname", value);
			OleDbCommand oleDbCommand2 = oleDbCommand;
			if (target == null)
			{
				uniqueId = DBNull.Value;
			}
			else
			{
				uniqueId = target.UniqueId;
			}
			this.SetParameter(oleDbCommand2, "@targetid", uniqueId);
			this.SetParameter(oleDbCommand, "@text", text);
			this.SetParameter(oleDbCommand, "@html", html);
			this.SetParameter(oleDbCommand, "@claimno", claimNo);
			oleDbCommand.ExecuteNonQuery();
		}

		private int NextIdentity(string tablename, string fieldname)
		{
			int num;
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT MAX(", fieldname, ") FROM ", tablename);
			using (OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader())
			{
				if (oleDbDataReader.Read())
				{
					num = (!oleDbDataReader.IsDBNull(0) ? Convert.ToInt32(oleDbDataReader.GetValue(0)) + 1 : 1);
				}
				else
				{
					num = 1;
				}
			}
			return num;
		}

		public void RemoveAgent(int departmentid, string userid)
		{
			if (userid == null)
			{
				throw new ArgumentNullException("userid");
			}
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("DELETE FROM ", this.prefix, "SupportAgent WHERE DepartmentId=@depid AND AgentUserId=@userid");
			this.SetParameter(oleDbCommand, "@depid", departmentid);
			this.SetParameter(oleDbCommand, "@userid", userid);
			oleDbCommand.ExecuteNonQuery();
		}

		public void RemoveDepartment(int departmentid)
		{
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("DELETE FROM ", this.prefix, "SupportDepartment WHERE DepartmentId=@depid");
			this.SetParameter(oleDbCommand, "@depid", departmentid);
			oleDbCommand.ExecuteNonQuery();
		}

		public void RenameDepartment(int departmentid, string newname)
		{
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("UPDATE ", this.prefix, "SupportDepartment SET DepartmentName=@newname WHERE DepartmentId=@depid");
			this.SetParameter(oleDbCommand, "@newname", newname);
			this.SetParameter(oleDbCommand, "@depid", departmentid);
			oleDbCommand.ExecuteNonQuery();
		}

		private static int SafeGetOrdinal(OleDbDataReader reader, string name)
		{
			int ordinal;
			try
			{
				ordinal = reader.GetOrdinal(name);
			}
			catch
			{
				ordinal = -1;
			}
			return ordinal;
		}

		public void SaveSetting(string name, string data)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			OleDbCommand oleDbCommand = this.CreateCommand();
			if (data != null)
			{
				oleDbCommand.CommandText = string.Concat("UPDATE ", this.prefix, "Settings SET SettingData=@Data WHERE SettingName=@Name");
				this.SetParameter(oleDbCommand, "@Data", data);
				this.SetParameter(oleDbCommand, "@Name", name);
				if (oleDbCommand.ExecuteNonQuery() == 0)
				{
					oleDbCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "Settings (SettingData,SettingName) VALUES (@Data,@Name)");
					this.ApplyParameters(oleDbCommand);
					oleDbCommand.ExecuteNonQuery();
				}
			}
			else
			{
				oleDbCommand.CommandText = string.Concat("DELETE ", this.prefix, "Settings WHERE SettingName=@Name");
				this.SetParameter(oleDbCommand, "@Name", name);
				oleDbCommand.ExecuteNonQuery();
			}
		}

		public SupportSession[] SearchAgentSessions(DateTime start, DateTime endtime, string agentid, string departmentid, string customer, string email, string ipaddr, string culture)
		{
			OleDbCommand str = this.CreateCommand();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("SELECT * FROM ").Append(this.prefix).Append("SupportSession WHERE BeginTime>=@start AND BeginTime<=@endtime");
			this.SetParameter(str, "@start", start);
			this.SetParameter(str, "@endtime", endtime);
			if ((agentid == null ? false : agentid != ""))
			{
				stringBuilder.Append(" AND AgentUserId=@agentid");
				this.SetParameter(str, "@agentid", agentid);
			}
			if ((departmentid == null ? false : departmentid != ""))
			{
				stringBuilder.Append(" AND DepartmentId=@departmentid");
				this.SetParameter(str, "@departmentid", departmentid);
			}
			if ((customer == null ? false : customer != ""))
			{
				stringBuilder.Append(" AND DisplayName LIKE @customer");
				this.SetParameter(str, "@customer", string.Concat("%", customer, "%"));
			}
			if ((email == null ? false : email != ""))
			{
				stringBuilder.Append(" AND Email LIKE @email");
				this.SetParameter(str, "@email", string.Concat("%", email, "%"));
			}
			if ((ipaddr == null ? false : ipaddr != ""))
			{
				stringBuilder.Append(" AND IPAddress LIKE @ipaddr");
				this.SetParameter(str, "@ipaddr", string.Concat("%", ipaddr, "%"));
			}
			if ((culture == null ? false : culture != ""))
			{
				stringBuilder.Append(" AND Culture LIKE @culture");
				this.SetParameter(str, "@culture", string.Concat("%", culture, "%"));
			}
			stringBuilder.Append(" ORDER BY SessionID ASC");
			str.CommandText = stringBuilder.ToString();
			this.ApplyParameters(str);
			return this.LoadSessions(str);
		}

		public SupportSession SelectSession(string sessionid)
		{
			SupportSession supportSession;
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("SELECT * FROM ", this.prefix, "SupportSession WHERE SessionId=@SessionId");
			this.SetParameter(oleDbCommand, "@SessionId", sessionid);
			SupportSession[] supportSessionArray = this.LoadSessions(oleDbCommand);
			if ((int)supportSessionArray.Length != 1)
			{
				supportSession = null;
			}
			else
			{
				supportSession = supportSessionArray[0];
			}
			return supportSession;
		}

		public virtual void SetCustomerData(string userid, string value)
		{
			if (userid == null)
			{
				throw new ArgumentNullException("userid");
			}
			OleDbCommand oleDbCommand = this.CreateCommand();
			if (value != null)
			{
				this.SetParameter(oleDbCommand, "@value", value);
				this.SetParameter(oleDbCommand, "@userid", userid);
				oleDbCommand.CommandText = string.Concat("UPDATE ", this.prefix, "SupportCustomer SET CustomerData=@value WHERE CustomerId=@userid");
				this.ApplyParameters(oleDbCommand);
				if (oleDbCommand.ExecuteNonQuery() == 0)
				{
					oleDbCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "SupportCustomer (CustomerData,CustomerId) VALUES (@value,@userid)");
					this.ApplyParameters(oleDbCommand);
					oleDbCommand.ExecuteNonQuery();
				}
			}
			else
			{
				this.SetParameter(oleDbCommand, "@userid", userid);
				oleDbCommand.CommandText = string.Concat("DELETE ", this.prefix, "SupportCustomer WHERE CustomerId=@userid");
				this.ApplyParameters(oleDbCommand);
				oleDbCommand.ExecuteNonQuery();
			}
		}

		private void SetParameter(OleDbCommand cmd, string pname, object obj)
		{
			if (cmd == null)
			{
				throw new ArgumentNullException("cmd");
			}
			OleDbParameterCollection parameters = cmd.Parameters;
			if (parameters.Contains(pname))
			{
				parameters.Remove(parameters[pname]);
			}
			if ((obj == null ? false : !Convert.IsDBNull(obj)))
			{
				parameters.Add(pname, obj);
				this.ApplyParameters(cmd);
			}
			else
			{
				parameters.Add(pname, OleDbType.VarWChar, 50).Value = DBNull.Value;
				this.ApplyParameters(cmd);
			}
		}

		public string Translate(object value)
		{
			string str;
			if ((value == null ? false : !Convert.IsDBNull(value)))
			{
				string fullName = value.GetType().FullName;
				if (fullName != null)
				{
					switch (fullName)
					{
						case "System.Char":
						{
							str = string.Concat("'", value.ToString().Replace("'", "''"), "'");
							return str;
						}
						case "System.String":
						{
							if (value.ToString().Length == 0)
							{
							}
							string str1 = string.Concat("'", value.ToString().Replace("'", "''"), "'");
							if (str1.IndexOf('@') == -1)
							{
								str = str1;
								return str;
							}
							else if (str1.IndexOf('~') == -1)
							{
								str1 = str1.Replace('@', '~');
								str = string.Concat("REPLACE(", str1, ",'~',CHR(64))");
								return str;
							}
							else
							{
								str1 = str1.Replace("~", "~1");
								str1 = str1.Replace("@", "~2");
								str = string.Concat("REPLACE(REPLACE(", str1, ",'~2',CHR(64)),'~1','~')");
								return str;
							}
						}
						case "System.DateTime":
						{
							DateTime dateTime = (DateTime)value;
							str = string.Concat("timestamp'", dateTime.ToString("yyyy-MM-dd HH:mm:ss.fff"), "'");
							return str;
						}
						case "System.Boolean":
						{
							str = ((bool)value ? "1" : "0");
							return str;
						}
						case "System.Guid":
						{
							str = string.Concat("'{", value.ToString(), "}'");
							return str;
						}
						case "System.Byte[]":
						{
							str = this.TranslateBytes((byte[])value);
							return str;
						}
						case "System.Byte":
						case "System.SByte":
						case "System.UInt16":
						case "System.UInt32":
						case "System.UInt64":
						case "System.Int16":
						case "System.Int32":
						case "System.Int64":
						case "System.Decimal":
						{
							str = value.ToString();
							return str;
						}
						case "System.Single":
						{
							str = this.TranslateDouble((double)((float)value));
							return str;
						}
						default:
						{
							if (fullName != "System.Double")
							{
								goto Label1;
							}
							str = this.TranslateDouble((double)value);
							return str;
						}
					}
				}
			Label1:
				str = string.Concat("'", value.ToString().Replace("'", "''"), "'");
			}
			else
			{
				str = "NULL";
			}
			return str;
		}

		private string TranslateBytes(byte[] value)
		{
			string str;
			int length = (int)value.Length;
			if (length != 0)
			{
				StringBuilder stringBuilder = new StringBuilder();
				for (int i = 0; i < length; i++)
				{
					stringBuilder.Append(OracleOleDbDataProvider.Bytes.Strs[value[i]]);
				}
				str = stringBuilder.ToString();
			}
			else
			{
				str = "NULL";
			}
			return str;
		}

		private string TranslateDouble(double value)
		{
			if (double.IsInfinity(value))
			{
				throw new ArgumentException("number value is Infinity");
			}
			if (double.IsNaN(value))
			{
				throw new ArgumentException("number value is NaN");
			}
			return value.ToString();
		}

		public void UpdateFeedback(SupportFeedback feedback)
		{
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("UPDATE ", this.prefix, "SupportFeedback SET FbTime=@FbTime,CustomerId=@CustomerId,Name=@Name,DisplayName=@DisplayName,Email=@Email,Title=@Title,Content=@Content,f_Comment=@f_Comment,CommentBy=@CommentBy WHERE FeedbackId=@FeedbackId");
			OleDbParameterCollection parameters = oleDbCommand.Parameters;
			this.SetParameter(oleDbCommand, "@FbTime", feedback.FbTime);
			this.SetParameter(oleDbCommand, "@CustomerId", feedback.CustomerId);
			this.SetParameter(oleDbCommand, "@Name", feedback.Name);
			this.SetParameter(oleDbCommand, "@DisplayName", feedback.DisplayName);
			this.SetParameter(oleDbCommand, "@Email", feedback.Email);
			this.SetParameter(oleDbCommand, "@Title", feedback.Title);
			this.SetParameter(oleDbCommand, "@Content", feedback.Content);
			this.SetParameter(oleDbCommand, "@f_Comment", feedback.Comment);
			this.SetParameter(oleDbCommand, "@CommentBy", feedback.CommentBy);
			this.SetParameter(oleDbCommand, "@FeedbackId", feedback.FeedbackId);
			oleDbCommand.ExecuteNonQuery();
		}

		public void UpdateLobby(IChatLobby ilobby)
		{
			AppLobby appLobby = (AppLobby)ilobby;
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("UPDATE ", this.prefix, "Lobby SET Title=@title,Topic=@topic,Announcement=@announce,MaxOnlineCount=@maxonline,Locked=@locked,AllowAnonymous=@anony,Password=@pwd,Description=@description,Integration=@integration,ManagerList=@managerlist,MaxIdleMinute=@maxidle,AutoAwayMinute=@autoaway,HistoryDay=@hisday,HistoryCount=@hiscount,SortIndex=@sort WHERE LobbyId=@lobbyid");
			OleDbParameterCollection parameters = oleDbCommand.Parameters;
			this.SetParameter(oleDbCommand, "@title", appLobby.Title);
			this.SetParameter(oleDbCommand, "@topic", appLobby.Topic);
			this.SetParameter(oleDbCommand, "@announce", appLobby.Announcement);
			this.SetParameter(oleDbCommand, "@maxonline", appLobby.MaxOnlineCount);
			this.SetParameter(oleDbCommand, "@locked", appLobby.Locked);
			this.SetParameter(oleDbCommand, "@anony", appLobby.AllowAnonymous);
			this.SetParameter(oleDbCommand, "@pwd", appLobby.Password);
			this.SetParameter(oleDbCommand, "@description", appLobby.Description);
			this.SetParameter(oleDbCommand, "@integration", appLobby.Integration);
			this.SetParameter(oleDbCommand, "@managerlist", appLobby.ManagerList);
			this.SetParameter(oleDbCommand, "@maxidle", appLobby.MaxIdleMinute);
			this.SetParameter(oleDbCommand, "@autoaway", appLobby.AutoAwayMinute);
			this.SetParameter(oleDbCommand, "@hisday", appLobby.HistoryDay);
			this.SetParameter(oleDbCommand, "@hiscount", appLobby.HistoryCount);
			this.SetParameter(oleDbCommand, "@sort", appLobby.SortIndex);
			this.SetParameter(oleDbCommand, "@lobbyid", appLobby.LobbyId);
			oleDbCommand.ExecuteNonQuery();
		}

		public void UpdatePortalInfo(IChatPortalInfo portalinfo)
		{
			AppPortalInfo appPortalInfo = (AppPortalInfo)portalinfo;
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("UPDATE ", this.prefix, "Portal SET Properties=@props WHERE PortalName=@portalname");
			OleDbParameterCollection parameters = oleDbCommand.Parameters;
			this.SetParameter(oleDbCommand, "@props", appPortalInfo.Properties);
			this.SetParameter(oleDbCommand, "@portalname", appPortalInfo.PortalName);
			if (oleDbCommand.ExecuteNonQuery() == 0)
			{
				oleDbCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "Portal (Properties,PortalName) VALUES (@props,@portalname)");
				this.ApplyParameters(oleDbCommand);
				oleDbCommand.ExecuteNonQuery();
			}
		}

		public void UpdateRule(IChatRule irule)
		{
			AppRule appRule = (AppRule)irule;
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("UPDATE ", this.prefix, "Rule SET Category=@category,Disabled=@disabled,RuleMode=@mode,Expression=@expression,SortIndex=@sort WHERE RuleId=@ruleid");
			OleDbParameterCollection parameters = oleDbCommand.Parameters;
			this.SetParameter(oleDbCommand, "@category", appRule.Category);
			this.SetParameter(oleDbCommand, "@disabled", appRule.Disabled);
			this.SetParameter(oleDbCommand, "@mode", appRule.Mode);
			this.SetParameter(oleDbCommand, "@expression", appRule.Expression);
			this.SetParameter(oleDbCommand, "@sort", appRule.Sort);
			this.SetParameter(oleDbCommand, "@ruleid", appRule.RuleId);
			oleDbCommand.ExecuteNonQuery();
		}

		public void UpdateSession(SupportSession session)
		{
			OleDbCommand oleDbCommand = this.CreateCommand();
			oleDbCommand.CommandText = string.Concat("UPDATE ", this.prefix, "SupportSession SET BeginTime=@BeginTime, DepartmentId=@DepartmentId, AgentUserId=@AgentUserId, CustomerId=@CustomerId, DisplayName=@DisplayName, ActiveTime=@ActiveTime, Email=@Email, IPAddress=@IPAddress, Culture=@Culture, Platform=@Platform, Browser=@Browser, AgentRating=@AgentRating, SessionData=@SessionData,Url=@Url,Referrer=@Referrer WHERE SessionId=@SessionId");
			OleDbParameterCollection parameters = oleDbCommand.Parameters;
			this.SetParameter(oleDbCommand, "@BeginTime", session.BeginTime);
			this.SetParameter(oleDbCommand, "@DepartmentId", session.DepartmentId);
			this.SetParameter(oleDbCommand, "@AgentUserId", session.AgentUserId);
			this.SetParameter(oleDbCommand, "@CustomerId", session.CustomerId);
			this.SetParameter(oleDbCommand, "@DisplayName", session.DisplayName);
			this.SetParameter(oleDbCommand, "@ActiveTime", session.ActiveTime);
			this.SetParameter(oleDbCommand, "@Email", session.Email);
			this.SetParameter(oleDbCommand, "@IPAddress", session.IPAddress);
			this.SetParameter(oleDbCommand, "@Culture", session.Culture);
			this.SetParameter(oleDbCommand, "@Platform", session.Platform);
			this.SetParameter(oleDbCommand, "@Browser", session.Browser);
			this.SetParameter(oleDbCommand, "@AgentRating", session.AgentRating);
			this.SetParameter(oleDbCommand, "@SessionData", session.SessionData);
			this.SetParameter(oleDbCommand, "@Url", session.Url);
			this.SetParameter(oleDbCommand, "@Referrer", session.Referrer);
			this.SetParameter(oleDbCommand, "@SessionId", session.SessionId);
			oleDbCommand.ExecuteNonQuery();
		}

		public void UpdateUserInfo(IChatUserInfo chatinfo)
		{
			AppUserInfo appUserInfo = (AppUserInfo)chatinfo;
			if (ChatProvider.GetInstance(this.Portal).IsRegisteredUser(appUserInfo.UserId))
			{
				OleDbCommand oleDbCommand = this.CreateCommand();
				oleDbCommand.CommandText = string.Concat("UPDATE ", this.prefix, "User SET DisplayName=@name,Description=@desc,ServerProperties=@server,PublicProperties=@public,PrivateProperties=@private,BuildinContacts=@contacts,BuildinIgnores=@ignores WHERE UserId=@userid");
				OleDbParameterCollection parameters = oleDbCommand.Parameters;
				this.SetParameter(oleDbCommand, "@name", appUserInfo.DisplayName);
				this.SetParameter(oleDbCommand, "@desc", appUserInfo.Description);
				this.SetParameter(oleDbCommand, "@server", appUserInfo.ServerProperties);
				this.SetParameter(oleDbCommand, "@public", appUserInfo.PublicProperties);
				this.SetParameter(oleDbCommand, "@private", appUserInfo.PrivateProperties);
				this.SetParameter(oleDbCommand, "@contacts", appUserInfo.BuildinContacts);
				this.SetParameter(oleDbCommand, "@ignores", appUserInfo.BuildinIgnores);
				this.SetParameter(oleDbCommand, "@userid", appUserInfo.UserId);
				if (oleDbCommand.ExecuteNonQuery() == 0)
				{
					oleDbCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "USER (DisplayName,Description,ServerProperties,PublicProperties,PrivateProperties,BuildinContacts,BuildinIgnores,UserId) VALUES (@name,@desc,@server,@public,@private,@contacts,@ignores,@userid)");
					this.ApplyParameters(oleDbCommand);
					oleDbCommand.ExecuteNonQuery();
				}
				appUserInfo.IsStored = true;
			}
		}

		private class Bytes
		{
			public static string[] Strs;

			static Bytes()
			{
				string str = "0123456789ABCDEF";
				OracleOleDbDataProvider.Bytes.Strs = new string[256];
				for (int i = 0; i < 16; i++)
				{
					for (int j = 0; j < 16; j++)
					{
						string[] strs = OracleOleDbDataProvider.Bytes.Strs;
						string str1 = str[i].ToString();
						char chr = str[j];
						strs[i + j * 16] = string.Concat(str1, chr.ToString());
					}
				}
			}

			public Bytes()
			{
			}
		}
	}
}