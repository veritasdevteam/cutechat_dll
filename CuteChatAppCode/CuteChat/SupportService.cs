using System;
using System.Web.Services;

namespace CuteChat
{
	public class SupportService : WebService
	{
		public const int LoginOK = 0;

		public const int LoginFailed = 1;

		public const int LoginWrongUsername = 2;

		public const int LoginWrongPassword = 3;

		public const int LoginErrorNotAgent = 4;

		public const int LoginNoPermission = 5;

		public SupportService()
		{
		}

		[WebMethod]
		public DateTime GetServerTime()
		{
			return DateTime.Now;
		}

		[WebMethod]
		public string HelloWorld()
		{
			return "Hello World";
		}

		[WebMethod]
		public void SupportInit()
		{
		}

		[WebMethod]
		public bool SupportIsLogon()
		{
			bool flag;
			AppChatIdentity logonIdentity = ChatProvider.Instance.GetLogonIdentity();
			if (logonIdentity == null)
			{
				flag = false;
			}
			else if (!logonIdentity.IsAnonymous)
			{
				if (ChatProvider.Instance.IsAdministrator(logonIdentity.UniqueId))
				{
					ChatWebUtility.SetupAdministratorAgent(logonIdentity.UniqueId);
				}
				flag = true;
			}
			else
			{
				flag = false;
			}
			return flag;
		}

		[WebMethod]
		public int SupportLogin(string loginName, string password)
		{
			int num;
			if (loginName == null)
			{
				throw new ArgumentNullException("loginName");
			}
			if (password == null)
			{
				throw new ArgumentNullException("password");
			}
			loginName = loginName.Trim();
			if (loginName == "")
			{
				num = 2;
			}
			else if (password == "")
			{
				num = 3;
			}
			else if (ChatProvider.Instance.ValidateUser(loginName, password))
			{
				ChatProvider.Instance.ToUserId(loginName);
				num = 0;
			}
			else
			{
				num = 3;
			}
			return num;
		}
	}
}