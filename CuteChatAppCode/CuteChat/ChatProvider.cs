using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.Mail;
using System.Xml;

namespace CuteChat
{
	public abstract class ChatProvider
	{
		private static ChatProvider _inst;

		private Hashtable _userinfomap = new Hashtable();

		private static object filelock;

		internal ExampleDatabaseType DatabaseType
		{
			get
			{
				ExampleDatabaseType exampleDatabaseType;
				string item = ConfigurationManager.AppSettings["DatabaseType"];
				if (item != "MSAccess")
				{
					exampleDatabaseType = (item != "OracleOleDb" ? ExampleDatabaseType.SqlServer : ExampleDatabaseType.OracleOleDb);
				}
				else
				{
					exampleDatabaseType = ExampleDatabaseType.MSAccess;
				}
				return exampleDatabaseType;
			}
		}

		public virtual string DefaultMailAddress
		{
			get
			{
				return this.GetMailSetting("DefaultMailAddress");
			}
		}

		public static ChatProvider Instance
		{
			get
			{
				ChatProvider instance;
				if (ChatProvider._inst == null)
				{
					throw new Exception("ChatProvider not initialized");
				}
				if (!ClusterSupport.IsClusterClient)
				{
					AppPortal currentPortal = (AppPortal)ChatSystem.Instance.GetCurrentPortal();
					instance = ChatProvider.GetInstance(currentPortal);
				}
				else
				{
					instance = ChatProvider._inst;
				}
				return instance;
			}
			set
			{
				ChatProvider._inst = value;
			}
		}

		static ChatProvider()
		{
			ChatProvider.filelock = new object();
		}

		protected ChatProvider()
		{
		}

		protected virtual ChatProvider CloneInstance(AppPortal portal)
		{
			return this;
		}

		public virtual IDbConnection CreateConnection()
		{
			IDbConnection sqlConnection;
			if (this.DatabaseType != ExampleDatabaseType.SqlServer)
			{
				sqlConnection = this.CreateOleDBConnection();
			}
			else
			{
				sqlConnection = new SqlConnection(this.GetConnectionString());
			}
			return sqlConnection;
		}

		public virtual AppDataManager CreateDataManagerInstance(AppPortal portal)
		{
            AppDataManager appDataManager = new AppDataManager(portal);
            return appDataManager;
		}

		public virtual IAppDataProvider CreateDataProvider(AppPortal portal)
		{
			IAppDataProvider sqlServerDataProvider;
			if (this.DatabaseType == ExampleDatabaseType.SqlServer)
			{
				sqlServerDataProvider = new SqlServerDataProvider(portal, this.GetConnectionString(), "CuteChat4_");
			}
			else if (this.DatabaseType != ExampleDatabaseType.OracleOleDb)
			{
				sqlServerDataProvider = new MSAccessDataProvider(portal, this.GetConnectionString(), "CuteChat4_");
			}
			else
			{
				sqlServerDataProvider = new OracleOleDbDataProvider(portal, this.GetConnectionString(), "CuteChat4_");
			}
			return sqlServerDataProvider;
		}

		protected IDbConnection CreateOleDBConnection()
		{
			return new OleDbConnection(this.GetConnectionString());
		}

		public abstract string FindUserLoginName(string nickName);

		public virtual string FromUserId(string userid)
		{
			string str;
			if (userid == null)
			{
				str = null;
			}
			else if (userid.StartsWith("User:"))
			{
				str = userid.Remove(0, 5);
			}
			else
			{
				str = null;
			}
			return str;
		}

		public virtual string GetConnectionString()
		{
			string item;
			if (this.DatabaseType == ExampleDatabaseType.SqlServer)
			{
				item = ConfigurationSettings.AppSettings["ConnectionString"];
			}
			else if (this.DatabaseType != ExampleDatabaseType.OracleOleDb)
			{
				string str = ConfigurationSettings.AppSettings["MSAccessFilePath"];
				str = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, str);
				item = string.Concat("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=", str, ";User Id=Admin;Password=;");
			}
			else
			{
				item = ConfigurationSettings.AppSettings["ConnectionString"];
			}
			return item;
		}

		public virtual string GetCurrentPortalName()
		{
			return "SingletonPortal";
		}

		public static ChatProvider GetInstance(AppPortal portal)
		{
			ChatProvider _Provider;
			if (ChatProvider._inst == null)
			{
				throw new Exception("ChatProvider not initialized");
			}
			if (portal != null)
			{
				if (portal.__provider == null)
				{
					portal.__provider = ChatProvider._inst.CloneInstance(portal);
				}
				if (portal.__provider == null)
				{
					portal.__provider = ChatProvider._inst;
				}
				_Provider = portal.__provider;
			}
			else
			{
				_Provider = ChatProvider._inst;
			}
			return _Provider;
		}

		public virtual AppChatIdentity GetLogonIdentity()
		{
			AppChatIdentity appChatIdentity;
			HttpContext current = HttpContext.Current;
			if (current != null)
			{
				if (current.User.Identity.IsAuthenticated)
				{
					goto Label1;
				}
			}
			appChatIdentity = null;
			return appChatIdentity;
		Label1:
			string name = current.User.Identity.Name;
			string.Concat("NickName:", name);
			string userId = this.ToUserId(name);
			string str = null;
			if (this.GetUserInfo(name, ref str))
			{
				appChatIdentity = new AppChatIdentity(str, false, userId, ChatWebUtility.GetIP(current));
				return appChatIdentity;
			}
			else
			{
				appChatIdentity = null;
				return appChatIdentity;
			}
		}

		public virtual string GetMailSetting(string name)
		{
			string attribute;
			string str = Path.Combine(HttpRuntime.AppDomainAppPath, "CuteChatMail.config");
			if (File.Exists(str))
			{
				XmlDocument xmlDocument = new XmlDocument();
				xmlDocument.Load(str);
				foreach (XmlElement xmlElement in xmlDocument.DocumentElement.SelectNodes("add"))
				{
					if (xmlElement.GetAttribute("key") == name)
					{
						attribute = xmlElement.GetAttribute("value");
						return attribute;
					}
				}
				attribute = null;
			}
			else
			{
				attribute = null;
			}
			return attribute;
		}

		public virtual string GetRegisteredUserId(string nickName)
		{
			string userId;
			if (nickName != null)
			{
				nickName = nickName.Trim();
				if (nickName.Length != 0)
				{
					string str = this.FindUserLoginName(nickName);
					if (str != null)
					{
						userId = this.ToUserId(str);
					}
					else
					{
						userId = null;
					}
				}
				else
				{
					userId = null;
				}
			}
			else
			{
				userId = null;
			}
			return userId;
		}

		public abstract bool GetUserInfo(string loginName, ref string nickName, ref bool isAdmin);

		public virtual bool GetUserInfo(string loginName, ref string nickName)
		{
			bool flag = false;
			return this.GetUserInfoCached(loginName, ref nickName, ref flag);
		}

		public virtual IChatUserInfo GetUserInfo(IChatUserInfo info)
		{
			return info;
		}

		public bool GetUserInfoCached(string loginName, ref string nickName, ref bool isAdmin)
		{
			bool userInfo;
			if (!ClusterSupport.IsClusterClient)
			{
				Hashtable hashtables = this._userinfomap;
				Monitor.Enter(hashtables);
				try
				{
					object[] item = (object[])this._userinfomap[loginName];
					if (item == null)
					{
						item = new object[] { this.GetUserInfo(loginName, ref nickName, ref isAdmin), nickName, isAdmin };
						this._userinfomap[loginName] = item;
						userInfo = (bool)item[0];
					}
					else
					{
						nickName = (string)item[1];
						isAdmin = (bool)item[2];
						userInfo = (bool)item[0];
					}
				}
				finally
				{
					Monitor.Exit(hashtables);
				}
			}
			else
			{
				userInfo = this.GetUserInfo(loginName, ref nickName, ref isAdmin);
			}
			return userInfo;
		}

		public virtual bool IsAdministrator(string userid)
		{
			bool flag;
			if (userid.StartsWith("User:"))
			{
				string str = userid.Remove(0, 5);
				string item = ConfigurationSettings.AppSettings["DefaultAdminUsername"];
				if ((item == null || !(item != "") ? true : string.Compare(item, str, true) != 0))
				{
					string str1 = null;
					bool flag1 = false;
					flag = (!(this.GetUserInfoCached(str, ref str1, ref flag1) & flag1) ? false : true);
				}
				else
				{
					flag = true;
				}
			}
			else
			{
				flag = false;
			}
			return flag;
		}

		public virtual bool IsLobbyAdmin(IChatLobby lobby, string userid)
		{
			return false;
		}

		public virtual bool IsRegisteredUser(string userid)
		{
			return (userid.StartsWith("User:") ? true : false);
		}

		public virtual void LogException(Exception error)
		{
			if (string.Compare(ConfigurationSettings.AppSettings["CuteChat.EnableErrorLog"], "True", true) == 0)
			{
				object obj = ChatProvider.filelock;
				Monitor.Enter(obj);
				try
				{
					DateTime now = DateTime.Now;
					string str = string.Concat("Errors\\", now.ToString("yyyy-MM-dd HH-mm "), error.GetType().Name, ".txt");
					str = Path.Combine(HttpRuntime.AppDomainAppPath, str);
					string directoryName = Path.GetDirectoryName(str);
					if (!Directory.Exists(directoryName))
					{
						Directory.CreateDirectory(directoryName);
					}
					using (StreamWriter streamWriter = new StreamWriter(str, true))
					{
						streamWriter.WriteLine(error.ToString());
						streamWriter.WriteLine();
					}
				}
				finally
				{
					Monitor.Exit(obj);
				}
			}
		}

		public virtual void SendMail(MailMessage msg)
		{
			if (msg == null)
			{
				throw new ArgumentNullException("msg");
			}
			if ((msg.From == null ? true : msg.From == ""))
			{
				msg.From = this.GetMailSetting("SmtpSender");
			}
			if ((msg.From == null ? true : msg.From == ""))
			{
				msg.From = this.DefaultMailAddress;
			}
			msg.Headers.Add("Reply", msg.From);
			msg.Headers.Add("Reply-To", msg.From);
			msg.Headers.Add("Mail-Reply-To", msg.From);
			string mailSetting = this.GetMailSetting("SmtpServer");
			string str = this.GetMailSetting("SmtpServerPort");
			string mailSetting1 = this.GetMailSetting("SmtpUsername");
			string str1 = this.GetMailSetting("SmtpPassword");
			if ((mailSetting == null ? false : mailSetting.Length != 0))
			{
				SmtpMail.SmtpServer = mailSetting;
			}
			if ((str == null ? false : str.Length != 0))
			{
				msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", str);
			}
			if ((mailSetting1 == null || mailSetting1.Length == 0 || str1 == null ? false : str1.Length != 0))
			{
				msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");
				msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", mailSetting1);
				msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", str1);
			}
			string mailSetting2 = this.GetMailSetting("SmtpUseSSL");
			if ((mailSetting2 == null ? false : mailSetting2.Length != 0))
			{
				msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpusessl", mailSetting2);
			}
			ThreadPool.QueueUserWorkItem(new WaitCallback(this.SendMailThread), msg);
		}

		private void SendMailThread(object state)
		{
			try
			{
				SmtpMail.Send((MailMessage)state);
			}
			catch (Exception exception)
			{
				this.LogException(exception);
			}
		}

		public virtual string ToUserId(string loginName)
		{
			return string.Concat("User:", loginName.ToLower());
		}

		public virtual void UpdateUserInfo(IChatUserInfo info)
		{
		}

		public virtual bool UserExists(string loginName)
		{
			string str = null;
			return this.GetUserInfo(loginName, ref str);
		}

		public abstract bool ValidateUser(string loginName, string password);
	}
}