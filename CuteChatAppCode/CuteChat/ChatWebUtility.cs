using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.Configuration;
using System.Web.UI;

namespace CuteChat
{
	[ChatServerType]
	public class ChatWebUtility : ChatApi
	{
		private static string chars;

		public static string AcceptEncodingHeader
		{
			get
			{
				string item;
				string lower = ConfigurationSettings.AppSettings["CuteChat.EnableZipCompress"];
				if (lower != null)
				{
					lower = lower.ToLower();
					if ((lower == "1" || lower == "true" ? true : lower == "on"))
					{
						item = HttpContext.Current.Request.Headers["Accept-Encoding"] ?? "";
						return item;
					}
				}
				item = "";
				return item;
			}
		}

		public static bool AllowChangeName
		{
			get
			{
				string config = ChatWebUtility.GetConfig("AllowChangeName");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static string[] ChannelSkins
		{
			get
			{
				string[] strArrays;
				string config = ChatWebUtility.GetConfig("ChannelSkins");
				if ((config == null ? true : config == ""))
				{
					config = "Normal,Royale,MacWhite,Indigo";
				}
				ArrayList arrayLists = new ArrayList();
				string[] strArrays1 = config.Split(",;|/".ToCharArray());
				for (int i = 0; i < (int)strArrays1.Length; i++)
				{
					string str = strArrays1[i].Trim();
					if (str.Length != 0)
					{
						arrayLists.Add(str);
					}
				}
				strArrays = (arrayLists.Count != 0 ? (string[])arrayLists.ToArray(typeof(string)) : "Normal,Royale,MacWhite,Indigo".Split(new char[] { ',' }));
				return strArrays;
			}
		}

		public static string ClientPath
		{
			get
			{
				string item = ConfigurationSettings.AppSettings["CuteChat.DataDirectory"];
				if ((item == null ? true : item == ""))
				{
					item = "~/CuteSoft_Client/CuteChat/";
				}
				return ChatWebUtility.ProcessWebPath(HttpContext.Current, null, item);
			}
		}

		public static string ColorsArray
		{
			get
			{
				string config = ChatWebUtility.GetConfig("ColorsArray");
				return ((config == null ? true : config == "") ? "#000000,#993300,#333300,#003300,#003366,#000080,#333399,#333333,#800000,#FF6600,#808000,#008000,#008080,#0000FF,#666699,#808080,#FF0000,#FF9900,#99CC00,#339966,#33CCCC,#3366FF,#800080,#999999,#FF00FF,#FFCC00,#FFFF00,#00FF00,#00FFFF,#00CCFF,#993366,#C0C0C0,#FF99CC,#FFCC99,#FFFF99,#CCFFCC,#CCFFFF,#99CCFF,#CC99FF,#FFFFFF" : config);
			}
		}

		public static bool CurrentIdentityIsAdministrator
		{
			get
			{
				bool flag;
				AppChatIdentity logonIdentity = ChatProvider.Instance.GetLogonIdentity();
				flag = (logonIdentity != null ? ChatProvider.Instance.IsAdministrator(logonIdentity.UniqueId) : false);
				return flag;
			}
		}

		public static bool DefaultEnableSound
		{
			get
			{
				string config = ChatWebUtility.GetConfig("DefaultEnableSound");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static bool DisablePrivateChatWhisper
		{
			get
			{
				string config = ChatWebUtility.GetConfig("DisablePrivateChatWhisper");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static bool DisableWhisper
		{
			get
			{
				string config = ChatWebUtility.GetConfig("DisableWhisper");
				return ((config == null ? true : config == "") ? false : Convert.ToBoolean(config));
			}
		}

		public static bool DisplayBottomBanner
		{
			get
			{
				string config = ChatWebUtility.GetConfig("DisplayBottomBanner");
				return ((config == null ? true : config == "") ? false : Convert.ToBoolean(config));
			}
		}

		public static bool DisplayHeaderPanel
		{
			get
			{
				string config = ChatWebUtility.GetConfig("DisplayHeaderPanel");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static bool DisplayLobbyList
		{
			get
			{
				string config = ChatWebUtility.GetConfig("DisplayLobbyList");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static bool DisplayLobbyListEmbed
		{
			get
			{
				string config = ChatWebUtility.GetConfig("DisplayLobbyListEmbed");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static bool DisplayLogo
		{
			get
			{
				string config = ChatWebUtility.GetConfig("DisplayLogo");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static bool DisplayTopBanner
		{
			get
			{
				string config = ChatWebUtility.GetConfig("DisplayTopBanner");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static string EmotionsArray
		{
			get
			{
				string config = ChatWebUtility.GetConfig("EmotionsArray");
				return ((config == null ? true : config == "") ? "emsmile.gif,emteeth.gif,emwink.gif,emsmileo.gif,emsmilep.gif,emsmiled.gif,emangry.gif,emembarrassed.gif,emcrook.gif,emsad.gif,emcry.gif,emdgust.gif,emangel.gif,emlove.gif,emunlove.gif,emmessag.gif,emcat.gif,emdog.gif,emmoon.gif,emstar.gif,emfilm.gif,emnote.gif,emrose.gif,emrosesad.gif,emclock.gif,emlips.gif,emgift.gif,emcake.gif,emphoto.gif,emidea.gif,emtea.gif,emphone.gif,emhug.gif,emhug2.gif,embeer.gif,emcocktl.gif,emmale.gif,emfemale.gif,emthup.gif,emthdown.gif" : config);
			}
		}

		public static string FlashChatServer
		{
			get
			{
				string config = ChatWebUtility.GetConfig("FlashChatServer");
				return ((config == null ? true : config == "") ? "" : config);
			}
		}

		public static int FloodControlCount
		{
			get
			{
				int num;
				string config = ChatWebUtility.GetConfig("FloodControlCount");
				if ((config == null ? false : config != ""))
				{
					try
					{
						num = int.Parse(config);
						return num;
					}
					catch
					{
					}
				}
				num = 5;
				return num;
			}
		}

		public static int FloodControlDelay
		{
			get
			{
				int num;
				string config = ChatWebUtility.GetConfig("FloodControlDelay");
				if ((config == null ? false : config != ""))
				{
					try
					{
						num = int.Parse(config);
						return num;
					}
					catch
					{
					}
				}
				num = 10;
				return num;
			}
		}

		public static bool GlobalAllowOutsiteImage
		{
			get
			{
				string config = ChatWebUtility.GetConfig("AllowOutsiteImage");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static bool GlobalAllowPrivateMessage
		{
			get
			{
				string config = ChatWebUtility.GetConfig("GlobalAllowPrivateMessage");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static bool GlobalAllowSendFile
		{
			get
			{
				string config = ChatWebUtility.GetConfig("GlobalAllowSendFile");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static bool GlobalDenyAnonymous
		{
			get
			{
				string config = ChatWebUtility.GetConfig("GlobalDenyAnonymous");
				return ((config == null ? true : config == "") ? false : Convert.ToBoolean(config));
			}
		}

		public static bool GlobalEnableHtmlBox
		{
			get
			{
				string config = ChatWebUtility.GetConfig("GlobalEnableHtmlBox");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static long GlobalSendFileSize
		{
			get
			{
				long num;
				string config = ChatWebUtility.GetConfig("GlobalSendFileSize");
				if ((config == null ? false : config != ""))
				{
					try
					{
						num = long.Parse(config);
						return num;
					}
					catch
					{
					}
				}
				num = (long)1024000;
				return num;
			}
		}

		public static string GlobalSendFileType
		{
			get
			{
				string config = ChatWebUtility.GetConfig("GlobalSendFileType");
				return ((config == null ? true : config == "") ? "png,gif,jpg" : config);
			}
		}

		public static bool GlobalShowAvatarBeforeMessage
		{
			get
			{
				string config = ChatWebUtility.GetConfig("GlobalShowAvatarBeforeMessage");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static bool GlobalShowBoldButton
		{
			get
			{
				string config = ChatWebUtility.GetConfig("GlobalShowBoldButton");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static bool GlobalShowColorButton
		{
			get
			{
				string config = ChatWebUtility.GetConfig("GlobalShowColorButton");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static bool GlobalShowControlPanel
		{
			get
			{
				string config = ChatWebUtility.GetConfig("GlobalShowControlPanel");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static bool GlobalShowEmotion
		{
			get
			{
				string config = ChatWebUtility.GetConfig("GlobalShowEmotion");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static bool GlobalShowFontName
		{
			get
			{
				string config = ChatWebUtility.GetConfig("GlobalShowFontName");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static bool GlobalShowFontSize
		{
			get
			{
				string config = ChatWebUtility.GetConfig("GlobalShowFontSize");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static bool GlobalShowItalicButton
		{
			get
			{
				string config = ChatWebUtility.GetConfig("GlobalShowItalicButton");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static bool GlobalShowSignoutButton
		{
			get
			{
				string config = ChatWebUtility.GetConfig("GlobalShowSignoutButton");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static bool GlobalShowSkinButton
		{
			get
			{
				string config = ChatWebUtility.GetConfig("GlobalShowSkinButton");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static bool GlobalShowSystemErrors
		{
			get
			{
				string config = ChatWebUtility.GetConfig("GlobalShowSystemErrors");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static bool GlobalShowSystemMessages
		{
			get
			{
				string config = ChatWebUtility.GetConfig("GlobalShowSystemMessages");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static bool GlobalShowTimeStampBeforeMessage
		{
			get
			{
				string config = ChatWebUtility.GetConfig("GlobalShowTimeStampBeforeMessage");
				return ((config == null ? true : config == "") ? false : Convert.ToBoolean(config));
			}
		}

		public static bool GlobalShowTimeStampWebMessenger
		{
			get
			{
				string config = ChatWebUtility.GetConfig("GlobalShowTimeStampWebMessenger");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static bool GlobalShowTypingIndicator
		{
			get
			{
				string config = ChatWebUtility.GetConfig("GlobalShowTypingIndicator");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static bool GlobalShowUnderlineButton
		{
			get
			{
				string config = ChatWebUtility.GetConfig("GlobalShowUnderlineButton");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static bool GlobalTextWrittenfromRighttoLeft
		{
			get
			{
				string config = ChatWebUtility.GetConfig("GlobalTextWrittenfromRighttoLeft");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static bool IsDownLevelBrowser
		{
			get
			{
				return !ChatWebUtility.IsHighLevelBrowser;
			}
		}

		public static bool IsHighLevelBrowser
		{
			get
			{
				bool flag;
				HttpBrowserCapabilities browser = HttpContext.Current.Request.Browser;
				string userAgent = HttpContext.Current.Request.UserAgent;
				if (userAgent != null)
				{
					string lower = userAgent.ToLower();
					if (lower.IndexOf("safari") != -1)
					{
						flag = (Convert.ToInt32(lower.Substring(lower.IndexOf("safari/") + 7, 3)) < 522 ? false : true);
					}
					else if (lower.IndexOf("webtv") != -1)
					{
						flag = false;
					}
					else if (browser.AOL)
					{
						flag = false;
					}
					else if (browser.Browser == "IE")
					{
						flag = (!browser.Platform.StartsWith("Win") ? false : (double)browser.MajorVersion + browser.MinorVersion >= 5.5);
					}
					else if (lower.IndexOf("gecko") == -1)
					{
						if (lower.IndexOf("opera") != -1)
						{
							flag = double.Parse(browser.Version) >= 9;
							return flag;
						}
						flag = false;
						return flag;
					}
					else
					{
						flag = true;
					}
				}
				else
				{
					flag = false;
					return flag;
				}
				return flag;
			}
		}

		public static bool IsIE6
		{
			get
			{
				bool flag;
				HttpBrowserCapabilities browser = HttpContext.Current.Request.Browser;
				string userAgent = HttpContext.Current.Request.UserAgent;
				if (userAgent != null)
				{
					string lower = userAgent.ToLower();
					if (lower.IndexOf("safari") != -1)
					{
						flag = (Convert.ToInt32(lower.Substring(lower.IndexOf("safari/") + 7, 3)) < 522 ? false : true);
					}
					else if (lower.IndexOf("webtv") != -1)
					{
						flag = false;
					}
					else if (!browser.AOL)
					{
						if (browser.Browser == "IE")
						{
							flag = (!browser.Platform.StartsWith("Win") || (double)browser.MajorVersion + browser.MinorVersion < 5.5 ? false : (double)browser.MajorVersion + browser.MinorVersion < 7);
							return flag;
						}
						flag = false;
						return flag;
					}
					else
					{
						flag = false;
					}
				}
				else
				{
					flag = false;
					return flag;
				}
				return flag;
			}
		}

		public static string LoginUrl
		{
			get
			{
				string str;
				string config = ChatWebUtility.GetConfig("LoginUrl");
				str = ((config == null ? true : config == "") ? ChatWebUtility.ProcessWebPath(HttpContext.Current, null, "~/Login.aspx") : ChatWebUtility.ProcessWebPath(HttpContext.Current, null, config));
				return str;
			}
		}

		public static string LogoUrl
		{
			get
			{
				string str;
				string config = ChatWebUtility.GetConfig("LogoUrl");
				str = ((config == null ? true : config == "") ? ChatWebUtility.ProcessWebPath(HttpContext.Current, null, "images/logo.gif") : ChatWebUtility.ProcessWebPath(HttpContext.Current, null, config));
				return str;
			}
		}

		public static bool LogoutCloseMessenger
		{
			get
			{
				bool flag;
				string config = ChatWebUtility.GetConfig("LogoutCloseMessenger");
				if ((config == null ? false : config != ""))
				{
					try
					{
						flag = bool.Parse(config);
						return flag;
					}
					catch
					{
					}
				}
				flag = true;
				return flag;
			}
		}

		public static string LogoutUrl
		{
			get
			{
				string str;
				string config = ChatWebUtility.GetConfig("LogoutUrl");
				str = ((config == null ? true : config == "") ? ChatWebUtility.ProcessWebPath(HttpContext.Current, null, "~/") : ChatWebUtility.ProcessWebPath(HttpContext.Current, null, config));
				return str;
			}
		}

		public static int MaxMSGLength
		{
			get
			{
				int num;
				string config = ChatWebUtility.GetConfig("MaxMSGLength");
				if ((config == null ? false : config != ""))
				{
					try
					{
						num = int.Parse(config);
						return num;
					}
					catch
					{
					}
				}
				num = 10000;
				return num;
			}
		}

		public static int MaxWordLength
		{
			get
			{
				int num;
				string config = ChatWebUtility.GetConfig("MaxWordLength");
				if ((config == null ? false : config != ""))
				{
					try
					{
						num = int.Parse(config);
						return num;
					}
					catch
					{
					}
				}
				num = 100;
				return num;
			}
		}

		public static bool MessengerAllowSendFile
		{
			get
			{
				string config = ChatWebUtility.GetConfig("MessengerAllowSendFile");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static long MessengerSendFileSize
		{
			get
			{
				long num;
				string config = ChatWebUtility.GetConfig("MessengerSendFileSize");
				if ((config == null ? false : config != ""))
				{
					try
					{
						num = long.Parse(config);
						return num;
					}
					catch
					{
					}
				}
				num = (long)1024000;
				return num;
			}
		}

		public static string MessengerSendFileType
		{
			get
			{
				string config = ChatWebUtility.GetConfig("MessengerSendFileType");
				return ((config == null ? true : config == "") ? "txt,chm,doc,xls,psd,pdf,ppt,zip,rar,gz,bmp,png,gif,jpg,swf,mp3,wmv,rm,rmvb" : config);
			}
		}

		public static bool RequireReSyncForCTS
		{
			get
			{
				return false;
			}
		}

		public static bool ShowJoinLeaveMessage
		{
			get
			{
				string config = ChatWebUtility.GetConfig("ShowJoinLeaveMessage");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static bool ShowVideoButton
		{
			get
			{
				bool flag;
				string config = ChatWebUtility.GetConfig("ShowVideoButton");
				if ((config == null ? false : config != ""))
				{
					try
					{
						flag = bool.Parse(config);
						return flag;
					}
					catch
					{
					}
				}
				flag = true;
				return flag;
			}
		}

		public static string SkinName
		{
			get
			{
				string str;
				string value = null;
				ChatIdentity logonIdentity = ChatWebUtility.GetLogonIdentity();
				if (logonIdentity == null)
				{
					HttpCookie item = HttpContext.Current.Request.Cookies["CuteChatSkin"];
					if (item != null)
					{
						value = item.Value;
					}
				}
				else
				{
					IChatUserInfo chatUserInfo = ChatApi.GetChatUserInfo(logonIdentity.UniqueId);
					if (chatUserInfo != null)
					{
						value = chatUserInfo.GetPrivateProperty("Skin");
						if (value != null)
						{
							str = value;
							return str;
						}
					}
				}
				if ((value == null ? true : value.Length == 0))
				{
					value = "Normal";
				}
				str = value;
				return str;
			}
		}

		public static bool SupportAllowSendFile
		{
			get
			{
				string config = ChatWebUtility.GetConfig("SupportAllowSendFile");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static bool SupportAllowSendMail
		{
			get
			{
				string config = ChatWebUtility.GetConfig("SupportAllowSendMail");
				return ((config == null ? true : config == "") ? true : Convert.ToBoolean(config));
			}
		}

		public static string SupportFeedbackImage
		{
			get
			{
				string config = ChatWebUtility.GetConfig("SupportFeedbackImage");
				return ((config == null ? true : config == "") ? "images/yoursite2.gif" : config);
			}
		}

		public static bool SupportRequireMail
		{
			get
			{
				bool flag;
				string config = ChatWebUtility.GetConfig("SupportRequireMail");
				if ((config == null ? false : config != ""))
				{
					try
					{
						flag = bool.Parse(config);
						return flag;
					}
					catch
					{
					}
				}
				flag = false;
				return flag;
			}
		}

		public static string SupportRoomTitle
		{
			get
			{
				string config = ChatWebUtility.GetConfig("SupportRoomTitle");
				return ((config == null ? true : config == "") ? "Welcome to yoursite.com" : config);
			}
		}

		public static long SupportSendFileSize
		{
			get
			{
				long num;
				string config = ChatWebUtility.GetConfig("SupportSendFileSize");
				if ((config == null ? false : config != ""))
				{
					try
					{
						num = long.Parse(config);
						return num;
					}
					catch
					{
					}
				}
				num = (long)1024000;
				return num;
			}
		}

		public static string SupportSendFileType
		{
			get
			{
				string config = ChatWebUtility.GetConfig("SupportSendFileType");
				return ((config == null ? true : config == "") ? "txt,chm,doc,xls,psd,pdf,ppt,zip,rar,gz,bmp,png,gif,jpg,swf,mp3,wmv,rm,rmvb" : config);
			}
		}

		public static string SupportWaitImage
		{
			get
			{
				string config = ChatWebUtility.GetConfig("SupportWaitImage");
				return ((config == null ? true : config == "") ? "images/yoursite.gif" : config);
			}
		}

		static ChatWebUtility()
		{
			ChatWebUtility.chars = "0123456789ABCDEF";
		}

		public ChatWebUtility()
		{
		}

		public static void AddAgent(string depname, string agentid)
		{
			ChatPortal chatPortal = null;
			if (!ClusterSupport.IsClusterClient)
			{
				lock (ChatSystem.Instance.GetCurrentPortal())
				{
					chatPortal.DataManager.AddAgent(depname, agentid);
				}
			}
			else
			{
				ClusterSupport.ExecuteCurrentMethod(new object[] { depname, agentid });
			}
		}

		public static void AddDepartment(string name)
		{
			ChatPortal chatPortal = null;
			if (!ClusterSupport.IsClusterClient)
			{
				lock (ChatSystem.Instance.GetCurrentPortal())
				{
					chatPortal.DataManager.AddDepartment(name);
				}
			}
			else
			{
				ClusterSupport.ExecuteCurrentMethod(new object[] { name });
			}
		}

		public static void AddFeedback(ChatIdentity logonUser, string name, string email, string title, string content)
		{
			ChatPortal chatPortal = null;
			if (!ClusterSupport.IsClusterClient)
			{
				lock (ChatSystem.Instance.GetCurrentPortal())
				{
					SupportAgentChannel place = (SupportAgentChannel)chatPortal.GetPlace("SupportAgent");
					place.AddFeedback(logonUser, name, email, title, content);
				}
			}
			else
			{
				ClusterSupport.ExecuteCurrentMethod(new object[] { logonUser, name, email, title, content });
			}
		}

		public static Guid CreateGuidByDate()
		{
			DateTime now = DateTime.Now;
			byte[] byteArray = Guid.NewGuid().ToByteArray();
			byteArray[0] = 0;
			byte[] bytes = BitConverter.GetBytes(now.Ticks);
			bytes.CopyTo(byteArray, (int)byteArray.Length - (int)bytes.Length);
			return new Guid(byteArray);
		}

		public static void DeleteInstantMessages(string ownerid, string targetid)
		{
			ChatPortal chatPortal = null;
			if (!ClusterSupport.IsClusterClient)
			{
				lock (ChatSystem.Instance.GetCurrentPortal())
				{
					chatPortal.DataManager.DeleteInstantMessages(ownerid, targetid);
				}
			}
			else
			{
				ClusterSupport.ExecuteCurrentMethod(new object[] { ownerid, targetid });
			}
		}

		public static string EncodeJScript(string str)
		{
			StringBuilder stringBuilder = null;
			int length = str.Length;
			for (int i = 0; i < str.Length; i++)
			{
				char chr = str[i];
				if ((chr == '\\' || chr == '\"' || chr == '\'' || chr == '>' || chr == '<' || chr == '&' || chr == '\r' ? true : chr == '\n'))
				{
					if (stringBuilder == null)
					{
						stringBuilder = new StringBuilder();
						if (i > 0)
						{
							stringBuilder.Append(str, 0, i);
						}
					}
					if (chr == '\\')
					{
						stringBuilder.Append("\\x5C");
					}
					else if (chr == '\"')
					{
						stringBuilder.Append("\\x22");
					}
					else if (chr == '\'')
					{
						stringBuilder.Append("\\x27");
					}
					else if (chr == '\r')
					{
						stringBuilder.Append("\\x0D");
					}
					else if (chr == '\n')
					{
						stringBuilder.Append("\\x0A");
					}
					else if (chr == '<')
					{
						stringBuilder.Append("\\x3C");
					}
					else if (chr == '>')
					{
						stringBuilder.Append("\\x3E");
					}
					else if (chr != '&')
					{
						int num = chr;
						int num1 = num & 15;
						int num2 = (num & 240) / 16;
						stringBuilder.Append("\\x");
						stringBuilder.Append(ChatWebUtility.chars[num2]);
						stringBuilder.Append(ChatWebUtility.chars[num1]);
					}
					else
					{
						stringBuilder.Append("\\x26");
					}
				}
				else if (stringBuilder != null)
				{
					stringBuilder.Append(chr);
				}
			}
			return (stringBuilder == null ? str : stringBuilder.ToString());
		}

		public static string EncodeJScriptString(string str, char quote)
		{
			string str1;
			str1 = (str != null ? string.Concat(quote.ToString(), ChatWebUtility.EncodeJScript(str), quote.ToString()) : "null");
			return str1;
		}

		public static string EncodeJScriptString(string str)
		{
			return ChatWebUtility.EncodeJScriptString(str, '\"');
		}

		public static string EncodeJScriptString(string str, bool usesinglequote)
		{
			return ChatWebUtility.EncodeJScriptString(str, (usesinglequote ? '\'' : '\"'));
		}

		public static string EncodeJScriptString(string[] strs)
		{
			string str;
			if (strs != null)
			{
				ArrayList arrayLists = new ArrayList();
				string[] strArrays = strs;
				for (int i = 0; i < (int)strArrays.Length; i++)
				{
					string str1 = strArrays[i];
					arrayLists.Add(ChatWebUtility.EncodeJScriptString(str1));
				}
				str = string.Concat("[", string.Join(",", (string[])arrayLists.ToArray(typeof(string))), "]");
			}
			else
			{
				str = "null";
			}
			return str;
		}

		public static string FormatSize(double fileSize)
		{
			string str;
			if (fileSize < 1024)
			{
				str = string.Format("{0:N0} B", fileSize);
			}
			else if (fileSize >= 1048576)
			{
				str = (fileSize >= 1073741824 ? string.Format("{0:N2} GB", fileSize / 1073741824) : string.Format("{0:N2} MB", fileSize / 1048576));
			}
			else
			{
				str = string.Format("{0:N2} KB", fileSize / 1024);
			}
			return str;
		}

		public static byte[] GetAvatar(string name)
		{
			object item = ChatWebUtility.GetAvatarTable()[name];
			if (item == null)
			{
				throw new ArgumentException(string.Concat("Avatar not found:", name), "name");
			}
			return (byte[])item;
		}

		private static Hashtable GetAvatarTable()
		{
			byte[] numArray;
			Hashtable item = (Hashtable)HttpRuntime.Cache["CuteChat.Avatars"];
			if (item == null)
			{
				item = new Hashtable();
				string[] files = Directory.GetFiles(ChatSystem.CombineApplicationPath("Avatars"), "*.gif");
				for (int i = 0; i < (int)files.Length; i++)
				{
					string str = files[i];
					using (FileStream fileStream = new FileStream(str, FileMode.Open, FileAccess.Read, FileShare.Read))
					{
						numArray = new byte[(int)checked((IntPtr)fileStream.Length)];
						fileStream.Read(numArray, 0, (int)numArray.Length);
					}
					item.Add(Path.GetFileName(str), numArray);
				}
				HttpRuntime.Cache["CuteChat.Avatars"] = item;
			}
			return item;
		}

		public static string[] GetAvatarTable(EnumAvatarType at)
		{
			ArrayList item = (ArrayList)HttpRuntime.Cache[string.Concat("CuteChat.Avatars.", at.ToString())];
			if (item == null)
			{
				item = new ArrayList();
				string lower = string.Concat(at.ToString(), "_");
				lower = lower.ToLower();
				foreach (string key in ChatWebUtility.GetAvatarTable().Keys)
				{
					if (key.IndexOf("_") == -1)
					{
						item.Add(key);
					}
					else if (key.ToLower().StartsWith(lower))
					{
						item.Add(key);
					}
				}
				HttpRuntime.Cache[string.Concat("CuteChat.Avatars.", at.ToString())] = item;
			}
			return (string[])item.ToArray(typeof(string));
		}

		public static string GetBrowser()
		{
			string browser;
			string userAgent = HttpContext.Current.Request.UserAgent;
			if (userAgent != null)
			{
				userAgent = userAgent.ToLower();
				if (userAgent.IndexOf("msie 8") != -1)
				{
					browser = "IE8";
				}
				else if (userAgent.IndexOf("msie 7") != -1)
				{
					browser = "IE7";
				}
				else if (userAgent.IndexOf("msie 6") != -1)
				{
					browser = "IE6";
				}
				else if (userAgent.IndexOf("msie 5") != -1)
				{
					browser = "IE5";
				}
				else if (userAgent.IndexOf("firefox") != -1)
				{
					browser = "FireFox";
				}
				else if (userAgent.IndexOf("opera") != -1)
				{
					browser = "Opera";
				}
				else if (userAgent.IndexOf("chrome") == -1)
				{
					if (userAgent.IndexOf("applewebkit") != -1)
					{
						browser = "Safari";
						return browser;
					}
					browser = HttpContext.Current.Request.Browser.Browser;
					return browser;
				}
				else
				{
					browser = "Chrome";
				}
			}
			else
			{
				browser = HttpContext.Current.Request.Browser.Browser;
				return browser;
			}
			return browser;
		}

		public static new string GetConfig(string name)
		{
			string config;
			if (!ClusterSupport.IsClusterClient)
			{
				config = ChatApi.GetConfig(name);
			}
			else
			{
				string str = "ChatWebUtility_Configs";
				Hashtable item = (Hashtable)HttpRuntime.Cache[str];
				if (item == null)
				{
					item = ChatApi.LoadConfigs();
					Cache cache = HttpRuntime.Cache;
					DateTime now = DateTime.Now;
					cache.Insert(str, item, null, now.AddMinutes(1), TimeSpan.Zero, CacheItemPriority.Low, null);
				}
				config = item[name] as string;
			}
			return config;
		}

		public static string GetDefaultAvatar(EnumAvatarType at)
		{
			return string.Concat(at.ToString(), "_default.gif");
		}

		public static string GetIP(HttpContext context)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}
			string item = context.Request.ServerVariables["HTTP_X_FORWARD_FOR"];
			return ((item == null ? true : item.Length == 0) ? context.Request.UserHostAddress : item);
		}

		public static AppChatIdentity GetLogonIdentity()
		{
			return ChatProvider.Instance.GetLogonIdentity();
		}

		public static ArrayList GetManagerList(int lobbyid)
		{
			ChatPortal chatPortal = null;
			ArrayList arrayLists;
			if (!ClusterSupport.IsClusterClient)
			{
				ArrayList arrayLists1 = new ArrayList();
				lock (ChatSystem.Instance.GetCurrentPortal())
				{
					IChatLobby lobby = chatPortal.DataManager.GetLobby(lobbyid);
					if (lobby != null)
					{
						string[] strArrays = lobby.ManagerList.Split(new char[] { ',' });
						for (int i = 0; i < (int)strArrays.Length; i++)
						{
							string str = strArrays[i];
							if (str.Length != 0)
							{
								IChatUserInfo userInfo = chatPortal.DataManager.GetUserInfo(str);
								if (userInfo != null)
								{
									arrayLists1.Add(userInfo);
								}
							}
						}
					}
				}
				arrayLists = arrayLists1;
			}
			else
			{
				arrayLists = (ArrayList)ClusterSupport.ExecuteCurrentMethod(new object[] { lobbyid });
			}
			return arrayLists;
		}

		public static string GetMessengerOnlineStatus(string userloginName)
		{
			ChatPortal chatPortal = null;
			string onlineStatus;
			if (!ClusterSupport.IsClusterClient)
			{
				string userId = ChatProvider.Instance.ToUserId(userloginName);
				lock (ChatSystem.Instance.GetCurrentPortal())
				{
					if (chatPortal.IsMessengerStarted)
					{
						ChatPlaceUser chatPlaceUser = chatPortal.Messenger.FindUser(userId);
						if (chatPlaceUser != null)
						{
							onlineStatus = chatPlaceUser.OnlineStatus;
							return onlineStatus;
						}
					}
				}
				onlineStatus = "OFFLINE";
			}
			else
			{
				onlineStatus = (string)ClusterSupport.ExecuteCurrentMethod(new object[] { userloginName });
			}
			return onlineStatus;
		}

		public static string GetPlatform()
		{
			string platform;
			string userAgent = HttpContext.Current.Request.UserAgent;
			if (userAgent != null)
			{
				userAgent = userAgent.ToLower();
				if (userAgent.IndexOf("windows nt 6.0") != -1)
				{
					platform = "Vista";
				}
				else if (userAgent.IndexOf("windows nt 5.2") != -1)
				{
					platform = "Win2003";
				}
				else if (userAgent.IndexOf("windows nt 5.1") != -1)
				{
					platform = "WinXP";
				}
				else if (userAgent.IndexOf("windows nt 5.0") != -1)
				{
					platform = "Win2000";
				}
				else if (userAgent.IndexOf("windows me") != -1)
				{
					platform = "WinME";
				}
				else if (userAgent.IndexOf("windows 98") == -1)
				{
					if (userAgent.IndexOf("windows 95") != -1)
					{
						platform = "Win95";
						return platform;
					}
					platform = HttpContext.Current.Request.Browser.Platform;
					return platform;
				}
				else
				{
					platform = "Win98";
				}
			}
			else
			{
				platform = HttpContext.Current.Request.Browser.Platform;
				return platform;
			}
			return platform;
		}

		public static string HandleCustomerWait(ChatIdentity customerid, string name, string ipaddr, string culture, string platform, string browser, string url, string referrer, string email, string department, string question, string customdata, out string detail)
		{
			string str = ChatWebUtility.HandleCustomerWait2(customerid, name, ipaddr, culture, platform, browser, url, referrer, email, department, question, customdata);
			string[] strArrays = str.Split(new char[] { ':' }, 2);
			string str1 = strArrays[0];
			detail = strArrays[1];
			return str1;
		}

		public static string HandleCustomerWait2(ChatIdentity customerid, string name, string ipaddr, string culture, string platform, string browser, string url, string referrer, string email, string department, string question, string customerdata)
		{
			string str;
			string str1;
			ChatPortal chatPortal = null;
			string str2;
			if (!ClusterSupport.IsClusterClient)
			{
				lock (ChatSystem.Instance.GetCurrentPortal())
				{
					SupportAgentChannel place = (SupportAgentChannel)chatPortal.GetPlace("SupportAgent");
					str = place.HandleCustomerWait(customerid, name, ipaddr, culture, platform, browser, url, referrer, email, department, question, customerdata, out str1);
				}
				str2 = string.Concat(str, ":", str1);
			}
			else
			{
				str2 = (string)ClusterSupport.ExecuteCurrentMethod(new object[] { customerid, name, ipaddr, culture, platform, browser, url, referrer, email, department, question, customerdata });
			}
			return str2;
		}

		public static string InitUniqueId()
		{
			bool flag;
			return ChatWebUtility.InitUniqueId(out flag);
		}

		public static string InitUniqueId(out bool isnewid)
		{
			string uniqueId;
			isnewid = false;
			string str = string.Concat(new object[] { HttpContext.Current.Request.Url, "&", HttpContext.Current.Request.UrlReferrer, "&" });
			if (str.IndexOf("CCCustomerId") != -1)
			{
				string str1 = (new Regex(".*[&\\?]{1}CCCustomerId=([0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}).*", RegexOptions.IgnoreCase | RegexOptions.Compiled)).Replace(str, "$1");
				if (str1 != "")
				{
					try
					{
						uniqueId = string.Concat("Guest:", new Guid(str1));
						return uniqueId;
					}
					catch
					{
					}
				}
			}
			AppChatIdentity logonIdentity = ChatWebUtility.GetLogonIdentity();
			if ((logonIdentity == null ? true : logonIdentity.IsAnonymous))
			{
				HttpCookie item = HttpContext.Current.Request.Cookies["CCCustomerId"];
				if (item != null)
				{
					Guid empty = Guid.Empty;
					try
					{
						empty = new Guid(item.Value);
					}
					catch (Exception exception)
					{
					}
					if (empty != Guid.Empty)
					{
						uniqueId = string.Concat("Guest:", empty);
						return uniqueId;
					}
				}
				isnewid = true;
				Guid guid = ChatWebUtility.CreateGuidByDate();
				string path = "/";
				if (item != null)
				{
					path = item.Path;
				}
				item = HttpContext.Current.Response.Cookies["CCCustomerId"];
				item.Value = guid.ToString();
				item.Path = path;
				if (ChatWebUtility.GetConfig("EnableLongTimeCookies") != "False")
				{
					item.Expires = DateTime.Now.AddYears(10);
				}
				uniqueId = string.Concat("Guest:", guid);
			}
			else
			{
				uniqueId = logonIdentity.UniqueId;
			}
			return uniqueId;
		}

		public static void InstallGzipForResponse()
		{
			if (ChatWebUtility.AcceptEncodingHeader.ToLower().IndexOf("gzip") != -1)
			{
				HttpContext current = HttpContext.Current;
				if (current != null)
				{
					if (current.Items["ZipBy"] == null)
					{
						current.Items["ZipBy"] = "ChatWebUtility";
						current.Response.AddHeader("-gzip-by-assembly-", "CuteChat");
						current.Response.AddHeader("Content-Encoding", "gzip");
						current.Response.Filter = GZip.CreateGZipStream(new BufferedStream(current.Response.Filter));
					}
				}
			}
		}

		private static bool IsDateGuid(Guid guid)
		{
			bool flag;
			byte[] byteArray = guid.ToByteArray();
			if (byteArray[0] == 0)
			{
				byte[] numArray = new byte[8];
				long num = BitConverter.ToInt64(byteArray, (int)byteArray.Length - 8);
				DateTime dateTime = new DateTime(num);
				TimeSpan now = DateTime.Now - dateTime;
				flag = (now.Ticks <= (long)0 ? false : now.TotalDays < 7);
			}
			else
			{
				flag = false;
			}
			return flag;
		}

		public static string IsMemberName(string username)
		{
			ChatPortal chatPortal = null;
			string str;
			if (!ClusterSupport.IsClusterClient)
			{
				lock (ChatSystem.Instance.GetCurrentPortal())
				{
					str = chatPortal.DataManager.IsMemberDisplayName(username);
				}
			}
			else
			{
				str = (string)ClusterSupport.ExecuteCurrentMethod(new object[] { username });
			}
			return str;
		}

		public static SupportDepartment[] LoadDepartments()
		{
			ChatPortal chatPortal = null;
			SupportDepartment[] departments;
			if (!ClusterSupport.IsClusterClient)
			{
				lock (ChatSystem.Instance.GetCurrentPortal())
				{
					departments = chatPortal.DataManager.GetDepartments();
				}
			}
			else
			{
				departments = (SupportDepartment[])ClusterSupport.ExecuteCurrentMethod(new object[0]);
			}
			return departments;
		}

		public static ChatMsgData[] LoadInstantMessages(string ownerid, string targetid, bool offlineOnly, int maxid, int pagesize)
		{
			ChatPortal chatPortal = null;
			ChatMsgData[] chatMsgDataArray;
			if (!ClusterSupport.IsClusterClient)
			{
				lock (ChatSystem.Instance.GetCurrentPortal())
				{
					chatMsgDataArray = chatPortal.DataManager.LoadInstantMessages(ownerid, targetid, offlineOnly, maxid, pagesize);
				}
			}
			else
			{
				chatMsgDataArray = (ChatMsgData[])ClusterSupport.ExecuteCurrentMethod(new object[] { ownerid, targetid, offlineOnly, maxid, pagesize });
			}
			return chatMsgDataArray;
		}

		public static string ProcessPhyPath(HttpContext context, Control ctrl, string path)
		{
			string str;
			bool flag;
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}
			if (path == null)
			{
				throw new ArgumentNullException("path");
			}
			if (path.StartsWith("\\\\"))
			{
				flag = true;
			}
			else
			{
				flag = (path.Length <= 1 ? false : path[1] == ':');
			}
			str = (!flag ? context.Server.MapPath(ChatWebUtility.ProcessWebPath(context, ctrl, path)) : path);
			return str;
		}

		public static string ProcessWebPath(HttpContext context, Control ctrl, string path)
		{
			string templateSourceDirectory;
			string str;
			bool flag;
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}
			if (path == null)
			{
				throw new ArgumentNullException("path");
			}
			if (path.StartsWith("\\\\"))
			{
				flag = true;
			}
			else
			{
				flag = (path.Length <= 1 ? false : path[1] == ':');
			}
			if (flag)
			{
				path = path.Replace('/', '\\');
				string physicalApplicationPath = context.Request.PhysicalApplicationPath;
				if (path.ToLower().IndexOf(physicalApplicationPath.ToLower()) != 0)
				{
					physicalApplicationPath = context.Server.MapPath("/");
					if (path.ToLower().IndexOf(physicalApplicationPath.ToLower()) != 0)
					{
						throw new Exception(string.Concat("Unable to translate physical path ", path, " to virtual path"));
					}
					str = string.Concat("/", path.Remove(0, physicalApplicationPath.Length).Replace('\\', '/').TrimStart(new char[] { '/' }));
				}
				else
				{
					str = string.Concat("/", path.Remove(0, physicalApplicationPath.Length).Replace('\\', '/').TrimStart(new char[] { '/' }));
				}
			}
			else if (path.StartsWith("/"))
			{
				str = path;
			}
			else if (path.StartsWith("~/"))
			{
				str = string.Concat(context.Request.ApplicationPath.TrimEnd(new char[] { '/' }), path.Remove(0, 1));
			}
			else if (path.IndexOf("://") == -1)
			{
				if (ctrl == null)
				{
					templateSourceDirectory = context.Request.Path;
					templateSourceDirectory = templateSourceDirectory.Substring(0, templateSourceDirectory.LastIndexOf("/"));
				}
				else
				{
					templateSourceDirectory = ctrl.TemplateSourceDirectory;
				}
				str = string.Concat(templateSourceDirectory.TrimEnd(new char[] { '/' }), "/", path);
			}
			else
			{
				str = path;
			}
			return str;
		}

		public static void RemoveAgent(string depname, string agentid)
		{
			ChatPortal chatPortal = null;
			if (!ClusterSupport.IsClusterClient)
			{
				lock (ChatSystem.Instance.GetCurrentPortal())
				{
					chatPortal.DataManager.RemoveAgent(depname, agentid);
				}
			}
			else
			{
				ClusterSupport.ExecuteCurrentMethod(new object[] { depname, agentid });
			}
		}

		public static void RemoveDepartment(string name)
		{
			ChatPortal chatPortal = null;
			if (!ClusterSupport.IsClusterClient)
			{
				lock (ChatSystem.Instance.GetCurrentPortal())
				{
					chatPortal.DataManager.RemoveDepartment(name);
				}
			}
			else
			{
				ClusterSupport.ExecuteCurrentMethod(new object[] { name });
			}
		}

		public static string ReplaceParam(string url, string pname, string pvalue)
		{
			string str;
			if (url.IndexOf("?") != -1)
			{
				string[] strArrays = url.Split("?".ToCharArray(), 2);
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append(strArrays[0]);
				stringBuilder.Append("?");
				bool flag = false;
				string[] strArrays1 = strArrays[1].Split("&".ToCharArray());
				for (int i = 0; i < (int)strArrays1.Length; i++)
				{
					if (i != 0)
					{
						stringBuilder.Append("&");
					}
					string[] strArrays2 = strArrays1[i].Split("=".ToCharArray(), 2);
					if ((int)strArrays2.Length == 2)
					{
						stringBuilder.Append(strArrays2[0]);
						stringBuilder.Append("=");
						if (string.Compare(strArrays2[0], pname, true) != 0)
						{
							stringBuilder.Append(strArrays2[1]);
						}
						else
						{
							flag = true;
							stringBuilder.Append(pvalue);
						}
					}
					else
					{
						stringBuilder.Append(strArrays2[0]);
					}
				}
				if (!flag)
				{
					stringBuilder.Append("&");
					stringBuilder.Append(pname);
					stringBuilder.Append("=");
					stringBuilder.Append(pvalue);
				}
				str = stringBuilder.ToString();
			}
			else
			{
				str = string.Concat(new string[] { url, "?", pname, "=", pvalue });
			}
			return str;
		}

		public static string ResolveResource(HttpContext context, string path)
		{
			string str;
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}
			if (path != null)
			{
				if (path.StartsWith("/"))
				{
					str = path;
					return str;
				}
				else if (!path.StartsWith("~/"))
				{
					if (path.IndexOf("://") != -1)
					{
						str = path;
						return str;
					}
					if (path != "")
					{
						path = string.Concat("/", path);
					}
				}
				else
				{
					str = string.Concat(context.Request.ApplicationPath.TrimEnd(new char[] { '/' }), path.Remove(0, 1));
					return str;
				}
			}
			str = string.Concat(ChatWebUtility.ProcessWebPath(context, null, ChatWebUtility.ClientPath).TrimEnd(new char[] { '/' }), path);
			return str;
		}

		public static int SetupAdministratorAgent(string userid)
		{
			ChatPortal chatPortal = null;
			int num;
			if (!ClusterSupport.IsClusterClient)
			{
				lock (ChatSystem.Instance.GetCurrentPortal())
				{
					if (chatPortal.DataManager.IsAgent(userid))
					{
						num = 2;
					}
					else if (!ChatProvider.Instance.IsAdministrator(userid))
					{
						num = 0;
					}
					else
					{
						ChatWebUtility.SetupAdministratorAgent(chatPortal, userid);
						num = 1;
					}
				}
			}
			else
			{
				num = (int)ClusterSupport.ExecuteCurrentMethod(new object[] { userid });
			}
			return num;
		}

		private static void SetupAdministratorAgent(ChatPortal portal, string adminid)
		{
			AppDataManager dataManager = (AppDataManager)portal.DataManager;
			SupportDepartment[] departments = dataManager.GetDepartments();
			if (departments.Length == 0)
			{
				dataManager.AddDepartment("Default");
				departments = dataManager.GetDepartments();
			}
			SupportDepartment[] supportDepartmentArray = departments;
			int num = 0;
			if (num < (int)supportDepartmentArray.Length)
			{
				dataManager.AddAgent(supportDepartmentArray[num].DepartmentId, adminid);
			}
		}
	}
}