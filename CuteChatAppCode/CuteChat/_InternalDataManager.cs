using System;

namespace CuteChat
{
	public abstract class _InternalDataManager : ChatDataManager
	{
		public _InternalDataManager(ChatPortal portal) : base(portal)
		{
		}

		public override void CreateLobby(IChatLobby lobby)
		{
			base.CreateLobby(lobby);
			if (lobby.IsAvatarChat)
			{
				IChatPortalInfo portalInfo = this.GetPortalInfo();
				portalInfo.SetProperty(string.Concat("Lobby:", lobby.LobbyId, ":IsAvatarChat"), "1");
				portalInfo.SetProperty(string.Concat("Lobby:", lobby.LobbyId, ":AvatarChatURL"), lobby.AvatarChatURL);
				this.UpdatePortalInfo(portalInfo);
			}
		}

		public override void DeleteLobby(IChatLobby lobby)
		{
			base.DeleteLobby(lobby);
			if (lobby.IsAvatarChat)
			{
				IChatPortalInfo portalInfo = this.GetPortalInfo();
				portalInfo.SetProperty(string.Concat("Lobby:", lobby.LobbyId, ":AvatarChatURL"), "");
				portalInfo.SetProperty(string.Concat("Lobby:", lobby.LobbyId, ":IsAvatarChat"), null);
				this.UpdatePortalInfo(portalInfo);
			}
		}

		public override IChatLobby GetLobby(int lobbyid)
		{
			IChatLobby lobby = base.GetLobby(lobbyid);
			if (lobby != null)
			{
				lobby.IsAvatarChat = this.GetPortalInfo().GetProperty(string.Concat("Lobby:", lobbyid, ":IsAvatarChat")) == "1";
				lobby.AvatarChatURL = this.GetPortalInfo().GetProperty(string.Concat("Lobby:", lobbyid, ":AvatarChatURL"));
			}
			return lobby;
		}

		public override void UpdateLobby(IChatLobby lobby)
		{
			base.UpdateLobby(lobby);
			IChatPortalInfo portalInfo = this.GetPortalInfo();
			if (!lobby.IsAvatarChat)
			{
				portalInfo.SetProperty(string.Concat("Lobby:", lobby.LobbyId, ":IsAvatarChat"), null);
			}
			else
			{
				portalInfo.SetProperty(string.Concat("Lobby:", lobby.LobbyId, ":IsAvatarChat"), "1");
				portalInfo.SetProperty(string.Concat("Lobby:", lobby.LobbyId, ":AvatarChatURL"), lobby.AvatarChatURL);
			}
			this.UpdatePortalInfo(portalInfo);
		}
	}
}