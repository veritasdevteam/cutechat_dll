using System;

namespace CuteChat
{
	public class AppLobbyChannel : ChatLobbyChannel
	{
		public new AppLobby Lobby
		{
			get
			{
				return (AppLobby)base.Lobby;
			}
		}

		public new AppManager Manager
		{
			get
			{
				return (AppManager)base.Manager;
			}
		}

		public new AppPortal Portal
		{
			get
			{
				return (AppPortal)base.Portal;
			}
		}

		public AppLobbyChannel(AppPortal portal, string name, AppLobby lobby) : base(portal, name, lobby)
		{
		}

		protected override ChatManager CreateManagerInstance()
		{
			return new AppManager(this);
		}
	}
}