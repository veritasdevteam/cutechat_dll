using System;

namespace CuteChat
{
	public class AppPortal : ChatPortal
	{
		internal ChatProvider __provider;

		public new AppDataManager DataManager
		{
			get
			{
				return (AppDataManager)base.DataManager;
			}
		}

		public new AppMessenger Messenger
		{
			get
			{
				return (AppMessenger)base.Messenger;
			}
		}

		public AppPortal(string name) : base(name)
		{
		}

		public override ChatChannel CreateChannel(string name)
		{
			ChatChannel appGlobalChannel;
			string str;
			SupportSession lastSession;
			string str1;
			int num;
			if (name != "GLOBAL")
			{
				name = name.ToLower();
				if (name.StartsWith("SupportSession:".ToLower()))
				{
					str = name.Remove(0, "SupportSession:".Length);
					lastSession = this.DataManager.GetLastSession(str);
					if ((lastSession == null ? false : (DateTime.Now - lastSession.ActiveTime) < TimeSpan.FromMinutes(30)))
					{
						goto Label1;
					}
				}
				string[] strArrays = name.Split(new char[] { '-' });
				if ((int)strArrays.Length == 2)
				{
					string str2 = strArrays[0];
					str1 = strArrays[1];
					if (str2 == "lobby")
					{
						goto Label2;
					}
				}
				appGlobalChannel = base.CreateChannel(name);
			}
			else
			{
				appGlobalChannel = new AppGlobalChannel(this, name);
			}
			return appGlobalChannel;
		Label1:
			ChatIdentity appChatIdentity = new AppChatIdentity(lastSession.DisplayName, !ChatProvider.GetInstance(this).IsRegisteredUser(str), str, lastSession.IPAddress);
			SupportCustomerItem supportCustomerItem = new SupportCustomerItem(base.GetPlace("SupportAgent"), appChatIdentity)
			{
				DisplayName = lastSession.DisplayName
			};
			string displayName = this.DataManager.GetUserInfo(lastSession.AgentUserId).DisplayName;
			supportCustomerItem.Agent = new AppChatIdentity(displayName, false, lastSession.AgentUserId, "127.0.0.1");
			supportCustomerItem.BeginTime = lastSession.BeginTime;
			supportCustomerItem.Browser = lastSession.Browser;
			supportCustomerItem.Culture = lastSession.Culture;
			supportCustomerItem.Department = "";
			supportCustomerItem.Email = lastSession.Email;
			supportCustomerItem.IPAddress = lastSession.IPAddress;
			supportCustomerItem.Platform = lastSession.Platform;
			supportCustomerItem.Question = "";
			supportCustomerItem.Referrer = lastSession.Referrer;
			supportCustomerItem.Url = lastSession.Url;
			supportCustomerItem.Status = "SERVICE";
			SupportSessionChannel supportSessionChannel = new SupportSessionChannel(this, name, supportCustomerItem);
			supportSessionChannel.RestoreSession(lastSession);
			appGlobalChannel = supportSessionChannel;
			return appGlobalChannel;
		Label2:
			try
			{
				num = int.Parse(str1);
			}
			catch
			{
				appGlobalChannel = null;
				return appGlobalChannel;
			}
			AppLobby lobby = this.DataManager.GetLobby(num);
			if (lobby != null)
			{
				appGlobalChannel = new AppLobbyChannel(this, name, lobby);
				return appGlobalChannel;
			}
			else
			{
				appGlobalChannel = null;
				return appGlobalChannel;
			}
		}

		protected override ChatConnection CreateConnectionInstance(ChatIdentity user)
		{
			return new AppWebConnection(this, user);
		}

		protected override ChatDataManager CreateDataManagerInstance()
		{
			return ChatProvider.GetInstance(this).CreateDataManagerInstance(this);
		}

		protected override ChatMessenger CreateMessengerInstance()
		{
			return new AppMessenger(this);
		}
	}
}