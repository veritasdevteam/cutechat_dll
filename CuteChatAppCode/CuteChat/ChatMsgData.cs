﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CuteChat
{
    public class ChatMsgData
    {
        public ChatMsgData()
        { }
        public int MessageId { get; set; }
        public string Location { get; set; }
        public string PlaceName { get; set; }
        public DateTime Time { get; set; }
        public string Sender { get; set; }
        public string SenderId { get; set; }
        public string Target { get; set; }
        public string TargetId { get; set; }
        public string Text { get; set; }
        public string Html { get; set; }
        public bool Offline { get; set; }
        public bool Whisper { get; set; }
        public string ClaimNo { get; set; }
    }
}
