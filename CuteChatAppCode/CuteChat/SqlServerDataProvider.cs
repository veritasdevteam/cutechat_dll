using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace CuteChat
{
#pragma warning disable CS0436
    public class SqlServerDataProvider : BaseDataProvider, IAppDataProvider, IChatDataProvider, IDisposable

#pragma warning restore CS0436
    {
        private SqlConnection conn;

        private string prefix;

        private AppPortal _portal;

        public AppPortal Portal
        {
            get
            {
                return this._portal;
            }
        }

        public SqlServerDataProvider(AppPortal portal, string conectionstring, string objectprefix)
        {
            this._portal = portal;
            this.prefix = objectprefix;
            this.conn = new SqlConnection(conectionstring);
            this.conn.Open();
        }

        public void AddAgent(int departmentid, string userid)
        {
            if (userid == null)
            {
                throw new ArgumentNullException("userid");
            }
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "SupportAgent (DepartmentId,AgentUserId) VALUES (@depid,@userid)");
            this.SetParameter(sqlCommand.Parameters, "@depid", departmentid);
            this.SetParameter(sqlCommand.Parameters, "@userid", userid);
            sqlCommand.ExecuteNonQuery();
        }

        public void AddDepartment(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException("name");
            }
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "SupportDepartment (DepartmentName) VALUES (@name)");
            this.SetParameter(sqlCommand.Parameters, "@name", name);
            sqlCommand.ExecuteNonQuery();
        }

        public void AddFeedback(SupportFeedback feedback)
        {
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "SupportFeedback (FbTime,CustomerId,Name,DisplayName,Email,Title,Content,Comment,CommentBy) VALUES (@FbTime,@CustomerId,@Name,@DisplayName,@Email,@Title,@Content,@Comment,@CommentBy)");
            SqlParameterCollection parameters = sqlCommand.Parameters;
            this.SetParameter(parameters, "@FbTime", feedback.FbTime);
            this.SetParameter(parameters, "@CustomerId", feedback.CustomerId);
            this.SetParameter(parameters, "@Name", feedback.Name);
            this.SetParameter(parameters, "@DisplayName", feedback.DisplayName);
            this.SetParameter(parameters, "@Email", feedback.Email);
            this.SetParameter(parameters, "@Title", feedback.Title);
            this.SetParameter(parameters, "@Content", feedback.Content);
            this.SetParameter(parameters, "@Comment", feedback.Comment);
            this.SetParameter(parameters, "@CommentBy", feedback.CommentBy);
            sqlCommand.ExecuteNonQuery();
        }

        public SqlCommand CreateCommand()
        {
            return new SqlCommand()
            {
                Connection = this.conn
            };
        }

        public void CreateLobby(IChatLobby ilobby)
        {
            AppLobby num = (AppLobby)ilobby;
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "Lobby (Title,Topic,Announcement,MaxOnlineCount,Locked,AllowAnonymous,[Password],Description,Integration,ManagerList,MaxIdleMinute,AutoAwayMinute,HistoryDay,HistoryCount,SortIndex) VALUES (@title,@topic,@announce,@maxonline,@locked,@anony,@pwd,@description,@integration,@managerlist,@maxidle,@autoaway,@hisday,@hiscount,@sort)");
            SqlParameterCollection parameters = sqlCommand.Parameters;
            this.SetParameter(parameters, "@title", num.Title);
            this.SetParameter(parameters, "@topic", num.Topic);
            this.SetParameter(parameters, "@announce", num.Announcement);
            this.SetParameter(parameters, "@maxonline", num.MaxOnlineCount);
            this.SetParameter(parameters, "@locked", num.Locked);
            this.SetParameter(parameters, "@anony", num.AllowAnonymous);
            this.SetParameter(parameters, "@pwd", num.Password);
            this.SetParameter(parameters, "@description", num.Description);
            this.SetParameter(parameters, "@integration", num.Integration);
            this.SetParameter(parameters, "@managerlist", num.ManagerList);
            this.SetParameter(parameters, "@maxidle", num.MaxIdleMinute);
            this.SetParameter(parameters, "@autoaway", num.AutoAwayMinute);
            this.SetParameter(parameters, "@hisday", num.HistoryDay);
            this.SetParameter(parameters, "@hiscount", num.HistoryCount);
            this.SetParameter(parameters, "@sort", num.SortIndex);
            sqlCommand.ExecuteNonQuery();
            sqlCommand.CommandText = string.Concat("SELECT MAX(LobbyID) FROM ", this.prefix, "Lobby");
            num.LobbyId = Convert.ToInt32(sqlCommand.ExecuteScalar());
        }

        public void CreateRule(IChatRule irule)
        {
            AppRule num = (AppRule)irule;
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "Rule (Category,Disabled,RuleMode,Expression,SortIndex) VALUES (@category,@disabled,@mode,@expression,@sort)");
            SqlParameterCollection parameters = sqlCommand.Parameters;
            this.SetParameter(parameters, "@category", num.Category);
            this.SetParameter(parameters, "@disabled", num.Disabled);
            this.SetParameter(parameters, "@mode", num.Mode);
            this.SetParameter(parameters, "@expression", num.Expression);
            this.SetParameter(parameters, "@sort", num.Sort);
            sqlCommand.ExecuteNonQuery();
            sqlCommand.CommandText = string.Concat("SELECT MAX(RuleId) FROM ", this.prefix, "Rule");
            num.RuleId = Convert.ToInt32(sqlCommand.ExecuteScalar());
        }

        public void CreateSession(SupportSession session)
        {
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "SupportSession ([BeginTime], [DepartmentId], [AgentUserId], [CustomerId], [DisplayName], [ActiveTime], [Email], [IPAddress], [Culture], [Platform], [Browser], [AgentRating], [SessionData],[Url],[Referrer]) \r\nVALUES( @BeginTime, @DepartmentId, @AgentUserId, @CustomerId, @DisplayName, @ActiveTime, @Email, @IPAddress, @Culture, @Platform, @Browser, @AgentRating, @SessionData,@Url,@Referrer)\r\nSELECT SCOPE_IDENTITY()");
            SqlParameterCollection parameters = sqlCommand.Parameters;
            this.SetParameter(parameters, "@BeginTime", session.BeginTime);
            this.SetParameter(parameters, "@DepartmentId", session.DepartmentId);
            this.SetParameter(parameters, "@AgentUserId", session.AgentUserId);
            this.SetParameter(parameters, "@CustomerId", session.CustomerId);
            this.SetParameter(parameters, "@DisplayName", session.DisplayName);
            this.SetParameter(parameters, "@ActiveTime", session.ActiveTime);
            this.SetParameter(parameters, "@Email", session.Email);
            this.SetParameter(parameters, "@IPAddress", session.IPAddress);
            this.SetParameter(parameters, "@Culture", session.Culture);
            this.SetParameter(parameters, "@Platform", session.Platform);
            this.SetParameter(parameters, "@Browser", session.Browser);
            this.SetParameter(parameters, "@AgentRating", session.AgentRating);
            this.SetParameter(parameters, "@SessionData", session.SessionData);
            this.SetParameter(parameters, "@Url", session.Url);
            this.SetParameter(parameters, "@Referrer", session.Referrer);
            session.SessionId = Convert.ToInt32(sqlCommand.ExecuteScalar());
        }

        public void DeleteAgentSessions(DateTime start, DateTime endtime, string agentid, string departmentid, string customer, string email, string ipaddr, string culture)
        {
            SqlCommand str = this.CreateCommand();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("DELETE ").Append(this.prefix).Append("SupportSession WHERE BeginTime>=@start AND BeginTime<=@endtime");
            this.SetParameter(str.Parameters, "@start", start);
            this.SetParameter(str.Parameters, "@endtime", endtime);
            if ((agentid == null ? false : agentid != ""))
            {
                stringBuilder.Append(" AND AgentUserId=@agentid");
                this.SetParameter(str.Parameters, "@agentid", agentid);
            }
            if ((departmentid == null ? false : departmentid != ""))
            {
                stringBuilder.Append(" AND DepartmentId=@departmentid");
                this.SetParameter(str.Parameters, "@departmentid", departmentid);
            }
            if ((customer == null ? false : customer != ""))
            {
                stringBuilder.Append(" AND DisplayName LIKE @customer");
                this.SetParameter(str.Parameters, "@customer", string.Concat("%", customer, "%"));
            }
            if ((email == null ? false : email != ""))
            {
                stringBuilder.Append(" AND Email LIKE @email");
                this.SetParameter(str.Parameters, "@email", string.Concat("%", email, "%"));
            }
            if ((ipaddr == null ? false : ipaddr != ""))
            {
                stringBuilder.Append(" AND IPAddress LIKE @ipaddr");
                this.SetParameter(str.Parameters, "@ipaddr", string.Concat("%", ipaddr, "%"));
            }
            if ((culture == null ? false : culture != ""))
            {
                stringBuilder.Append(" AND Culture LIKE @culture");
                this.SetParameter(str.Parameters, "@culture", string.Concat("%", culture, "%"));
            }
            str.CommandText = stringBuilder.ToString();
            str.ExecuteNonQuery();
        }

        public void DeleteFeedback(int feedbackid)
        {
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("DELETE FROM ", this.prefix, "SupportFeedback WHERE FeedbackId=@FeedbackId");
            this.SetParameter(sqlCommand.Parameters, "@FeedbackId", feedbackid);
            sqlCommand.ExecuteNonQuery();
        }

        public void DeleteInstantMessages(string ownerid, string targetid)
        {
            if ((ownerid == null ? true : ownerid == ""))
            {
                throw new ArgumentNullException("ownerid");
            }
            bool flag = (targetid == null ? true : targetid == "");
            SqlCommand str = this.CreateCommand();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("UPDATE ");
            stringBuilder.Append(this.prefix);
            stringBuilder.Append("InstantMessage ");
            stringBuilder.Append("SET DeletedBySender=1 ");
            stringBuilder.Append("WHERE SenderId=@ownerid ");
            this.SetParameter(str.Parameters, "@ownerid", ownerid);
            if (!flag)
            {
                stringBuilder.Append("AND TargetId=@targetid");
                this.SetParameter(str.Parameters, "@targetid", targetid);
            }
            str.CommandText = stringBuilder.ToString();
            str.ExecuteNonQuery();
            str = this.CreateCommand();
            stringBuilder = new StringBuilder();
            stringBuilder.Append("UPDATE ");
            stringBuilder.Append(this.prefix);
            stringBuilder.Append("InstantMessage ");
            stringBuilder.Append("SET DeletedByTarget=1 ");
            stringBuilder.Append("WHERE TargetId=@ownerid ");
            this.SetParameter(str.Parameters, "@ownerid", ownerid);
            if (!flag)
            {
                stringBuilder.Append("AND SenderId=@targetid");
                this.SetParameter(str.Parameters, "@targetid", targetid);
            }
            str.CommandText = stringBuilder.ToString();
            str.ExecuteNonQuery();
        }

        public void DeleteLobby(IChatLobby ilobby)
        {
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("DELETE ", this.prefix, "Lobby WHERE LobbyId=@lobbyid");
            this.SetParameter(sqlCommand.Parameters, "@lobbyid", ilobby.LobbyId);
            sqlCommand.ExecuteNonQuery();
        }

        public void DeleteMessagesBeforeDays(int days)
        {
            DateTime dateTime = DateTime.Now.AddDays((double)(-days));
            SqlCommand sqlCommand = this.CreateCommand();
            this.SetParameter(sqlCommand.Parameters, "@date", dateTime);
            sqlCommand.CommandText = string.Concat("DELETE FROM ", this.prefix, "SupportMessage WHERE MsgTime<@date");
            sqlCommand.ExecuteNonQuery();
            sqlCommand.CommandText = string.Concat("DELETE FROM ", this.prefix, "InstantMessage WHERE MsgTime<@date");
            sqlCommand.ExecuteNonQuery();
            sqlCommand.CommandText = string.Concat("DELETE FROM ", this.prefix, "LogMessage WHERE MsgTime<@date");
            sqlCommand.ExecuteNonQuery();
        }

        public void DeleteRule(IChatRule irule)
        {
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("DELETE ", this.prefix, "Rule WHERE RuleId=@ruleid");
            this.SetParameter(sqlCommand.Parameters, "@ruleid", irule.RuleId);
            sqlCommand.ExecuteNonQuery();
        }

        public void Dispose()
        {
            this.conn.Dispose();
        }

        public virtual string GetCustomerData(string userid)
        {
            string str;
            if (userid == null)
            {
                throw new ArgumentNullException("userid");
            }
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("SELECT CustomerData FROM ", this.prefix, "SupportCustomer WHERE CustomerId=@userid");
            this.SetParameter(sqlCommand.Parameters, "@userid", userid);
            using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
            {
                if (sqlDataReader.Read())
                {
                    if (!sqlDataReader.IsDBNull(0))
                    {
                        str = sqlDataReader.GetString(0);
                        return str;
                    }
                    else
                    {
                        str = null;
                        return str;
                    }
                }
            }
            str = null;
            return str;
        }

        public int GetCustomerSessionCount(string customerid)
        {
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("SELECT COUNT(*) FROM ", this.prefix, "SupportSession WHERE CustomerId=@CustomerId");
            this.SetParameter(sqlCommand.Parameters, "@CustomerId", customerid);
            return Convert.ToInt32(sqlCommand.ExecuteScalar());
        }

        public SupportSession GetLastSession(string customerid)
        {
            SupportSession supportSession;
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("SELECT TOP 1 * FROM ", this.prefix, "SupportSession WHERE CustomerId=@CustomerId ORDER BY SessionId DESC");
            this.SetParameter(sqlCommand.Parameters, "@CustomerId", customerid);
            SupportSession[] supportSessionArray = this.LoadSessions(sqlCommand);
            if ((int)supportSessionArray.Length != 1)
            {
                supportSession = null;
            }
            else
            {
                supportSession = supportSessionArray[0];
            }
            return supportSession;
        }

        public IChatLobby[] GetLobbies()
        {
            ArrayList arrayLists = new ArrayList();
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("SELECT LobbyId,Title,Topic,Announcement,MaxOnlineCount,Locked,AllowAnonymous,Password,Description,Integration,ManagerList,MaxIdleMinute,AutoAwayMinute,HistoryCount,HistoryDay,SortIndex FROM ", this.prefix, "Lobby ORDER BY SortIndex ASC,LobbyId ASC");
            using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
            {
                while (sqlDataReader.Read())
                {
                    AppLobby appLobby = new AppLobby()
                    {
                        LobbyId = sqlDataReader.GetInt32(0),
                        Title = sqlDataReader.GetString(1),
                        Topic = sqlDataReader.GetString(2),
                        Announcement = sqlDataReader.GetString(3),
                        MaxOnlineCount = sqlDataReader.GetInt32(4),
                        Locked = sqlDataReader.GetBoolean(5),
                        AllowAnonymous = sqlDataReader.GetBoolean(6),
                        Password = sqlDataReader.GetString(7),
                        Description = sqlDataReader.GetString(8),
                        Integration = sqlDataReader.GetString(9),
                        ManagerList = sqlDataReader.GetString(10),
                        MaxIdleMinute = sqlDataReader.GetInt32(11),
                        AutoAwayMinute = sqlDataReader.GetInt32(12),
                        HistoryCount = sqlDataReader.GetInt32(13),
                        HistoryDay = sqlDataReader.GetInt32(14),
                        SortIndex = sqlDataReader.GetInt32(15)
                    };
                    arrayLists.Add(appLobby);
                }
            }
            return (IChatLobby[])arrayLists.ToArray(typeof(IChatLobby));
        }

        public IChatLobby GetLobby(int lobbyid)
        {
            IChatLobby chatLobby;
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("SELECT LobbyId,Title,Topic,Announcement,MaxOnlineCount,Locked,AllowAnonymous,Password,Description,Integration,ManagerList,MaxIdleMinute,AutoAwayMinute,HistoryCount,HistoryDay,SortIndex FROM ", this.prefix, "Lobby WHERE LobbyId=@lobbyid");
            this.SetParameter(sqlCommand.Parameters, "@lobbyid", lobbyid);
            using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
            {
                if (sqlDataReader.Read())
                {
                    AppLobby appLobby = new AppLobby()
                    {
                        LobbyId = lobbyid,
                        Title = sqlDataReader.GetString(1),
                        Topic = sqlDataReader.GetString(2),
                        Announcement = sqlDataReader.GetString(3),
                        MaxOnlineCount = sqlDataReader.GetInt32(4),
                        Locked = sqlDataReader.GetBoolean(5),
                        AllowAnonymous = sqlDataReader.GetBoolean(6),
                        Password = sqlDataReader.GetString(7),
                        Description = sqlDataReader.GetString(8),
                        Integration = sqlDataReader.GetString(9),
                        ManagerList = sqlDataReader.GetString(10),
                        MaxIdleMinute = sqlDataReader.GetInt32(11),
                        AutoAwayMinute = sqlDataReader.GetInt32(12),
                        HistoryCount = sqlDataReader.GetInt32(13),
                        HistoryDay = sqlDataReader.GetInt32(14),
                        SortIndex = sqlDataReader.GetInt32(15)
                    };
                    chatLobby = appLobby;
                }
                else
                {
                    chatLobby = null;
                }
            }
            return chatLobby;
        }

#pragma warning disable CS0436 // Type conflicts with imported type
        public CuteChat.ChatMsgData[] GetMessages(string location, string placename, int pagesize, int pageindex, out int totalcount)
#pragma warning restore CS0436 // Type conflicts with imported type
        {
            string str;
#pragma warning disable CS0436 // Type conflicts with imported type
            ChatMsgData[] array;
#pragma warning restore CS0436 // Type conflicts with imported type
            if (location != null)
            {
                location = location.ToLower();
                if (location == "messenger")
                {
                    str = "InstantMessage";
                    location = null;
                    placename = null;
                }
                else if (location != "support")
                {
                    str = "LogMessage";
                }
                else
                {
                    str = "SupportMessage";
                    location = null;
                    placename = null;
                }
                SqlCommand sqlCommand = this.CreateCommand();
                sqlCommand.CommandText = string.Concat("SELECT MessageId FROM ", this.prefix, str);
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("SELECT MessageId FROM ").Append(this.prefix).Append(str);
                ArrayList arrayLists = new ArrayList();
                if (location != null)
                {
                    this.SetParameter(sqlCommand.Parameters, "@location", location);
                    arrayLists.Add("Location=@location");
                }
                if (placename != null)
                {
                    this.SetParameter(sqlCommand.Parameters, "@placename", placename);
                    arrayLists.Add("Place=@placename");
                }
                if (arrayLists.Count != 0)
                {
                    for (int i = 0; i < arrayLists.Count; i++)
                    {
                        if (i != 0)
                        {
                            stringBuilder.Append(" AND ");
                        }
                        else
                        {
                            stringBuilder.Append(" WHERE ");
                        }
                        stringBuilder.Append(arrayLists[i]);
                    }
                }
                stringBuilder.Append(" ORDER BY MessageId DESC ");
                ArrayList arrayLists1 = new ArrayList();
                sqlCommand.CommandText = stringBuilder.ToString();
                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    while (sqlDataReader.Read())
                    {
                        arrayLists1.Add(sqlDataReader.GetInt32(0));
                    }
                }
                totalcount = arrayLists1.Count;
                stringBuilder = new StringBuilder();
                stringBuilder.Append("SELECT * FROM ").Append(this.prefix).Append(str);
                stringBuilder.Append(" WHERE MessageId IN (");
                int num = pageindex * pagesize;
                int num1 = num + pagesize;
                int num2 = num;
                while (true)
                {
                    if ((num2 >= num1 ? true : num2 >= arrayLists1.Count))
                    {
                        break;
                    }
                    stringBuilder.Append(arrayLists1[num2]).Append(",");
                    num2++;
                }
                stringBuilder.Append("0)");
                ArrayList arrayLists2 = new ArrayList();
                sqlCommand.CommandText = stringBuilder.ToString();
                using (SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader())
                {
                    int num3 = SqlServerDataProvider.SafeGetOrdinal(sqlDataReader1, "Location");
                    int num4 = SqlServerDataProvider.SafeGetOrdinal(sqlDataReader1, "Place");
                    int num5 = SqlServerDataProvider.SafeGetOrdinal(sqlDataReader1, "Whisper");
                    while (sqlDataReader1.Read())
                    {
#pragma warning disable CS0436 // Type conflicts with imported type
#pragma warning disable CS0436 // Type conflicts with imported type
                        ChatMsgData chatMsgDatum = new ChatMsgData()
#pragma warning restore CS0436 // Type conflicts with imported type
#pragma warning restore CS0436 // Type conflicts with imported type
                        {
                            MessageId = sqlDataReader1.GetInt32(0),
                            Text = Convert.ToString(sqlDataReader1["Text"]),
                            Html = Convert.ToString(sqlDataReader1["Html"]),
                            ClaimNo = Convert.ToString(sqlDataReader1["ClaimNo"]),
                            Sender = Convert.ToString(sqlDataReader1["Sender"]),
                            SenderId = Convert.ToString(sqlDataReader1["SenderId"]),
                            Target = Convert.ToString(sqlDataReader1["Target"]),
                            TargetId = Convert.ToString(sqlDataReader1["TargetId"]),
                            Time = Convert.ToDateTime(sqlDataReader1["MsgTime"])
                        };
                        if (num3 != -1)
                        {
                            chatMsgDatum.Location = sqlDataReader1.GetString(num3);
                        }
                        if (num4 != -1)
                        {
                            chatMsgDatum.PlaceName = sqlDataReader1.GetString(num4);
                        }
                        if (num5 != -1)
                        {
                            chatMsgDatum.Whisper = sqlDataReader1.GetInt32(num5) != 0;
                        }
                        arrayLists2.Add(chatMsgDatum);
                    }
                }
#pragma warning disable CS0436 // Type conflicts with imported type
#pragma warning disable CS0436 // Type conflicts with imported type
                array = (ChatMsgData[])arrayLists2.ToArray(typeof(ChatMsgData));
#pragma warning restore CS0436 // Type conflicts with imported type
#pragma warning restore CS0436 // Type conflicts with imported type
            }
            else
            {
                totalcount = 0;
#pragma warning disable CS0436 // Type conflicts with imported type
                array = new ChatMsgData[0];
#pragma warning restore CS0436 // Type conflicts with imported type
            }
            return array;
        }

        public IChatPortalInfo GetPortalInfo(string portalname)
        {
            string str;
            AppPortalInfo appPortalInfo = new AppPortalInfo(portalname);
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("SELECT Properties FROM ", this.prefix, "Portal WHERE PortalName=@portalname");
            this.SetParameter(sqlCommand.Parameters, "@portalname", portalname);
            using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
            {
                if (sqlDataReader.Read())
                {
                    AppPortalInfo appPortalInfo1 = appPortalInfo;
                    if (sqlDataReader.IsDBNull(0))
                    {
                        str = null;
                    }
                    else
                    {
                        str = sqlDataReader.GetString(0);
                    }
                    appPortalInfo1.Properties = str;
                }
            }
            return appPortalInfo;
        }

        public IChatRule GetRule(int ruleid)
        {
            IChatRule chatRule;
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("SELECT RuleId,Disabled,RuleMode,Expression,Category,SortIndex FROM ", this.prefix, "Rule WHERE RuleId=@ruleid");
            this.SetParameter(sqlCommand.Parameters, "@ruleid", ruleid);
            using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
            {
                if (sqlDataReader.Read())
                {
                    AppRule appRule = new AppRule()
                    {
                        RuleId = ruleid,
                        Disabled = sqlDataReader.GetBoolean(1),
                        Mode = sqlDataReader.GetString(2),
                        Expression = sqlDataReader.GetString(3),
                        Category = sqlDataReader.GetString(4),
                        Sort = sqlDataReader.GetInt32(5)
                    };
                    chatRule = appRule;
                }
                else
                {
                    chatRule = null;
                }
            }
            return chatRule;
        }

        public IChatRule[] GetRules()
        {
            ArrayList arrayLists = new ArrayList();
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("SELECT RuleId,Disabled,RuleMode,Expression,Category,SortIndex FROM ", this.prefix, "Rule");
            using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
            {
                while (sqlDataReader.Read())
                {
                    AppRule appRule = new AppRule()
                    {
                        RuleId = sqlDataReader.GetInt32(0),
                        Disabled = sqlDataReader.GetBoolean(1),
                        Mode = sqlDataReader.GetString(2),
                        Expression = sqlDataReader.GetString(3),
                        Category = sqlDataReader.GetString(4),
                        Sort = sqlDataReader.GetInt32(5)
                    };
                    arrayLists.Add(appRule);
                }
            }
            return (IChatRule[])arrayLists.ToArray(typeof(IChatRule));
        }

        public IChatUserInfo GetUserInfo(string userid)
        {
            IChatUserInfo chatUserInfo;
            string str;
            string str1;
            string str2;
            string str3;
            string str4;
            string str5;
            string str6;
            AppUserInfo appUserInfo = new AppUserInfo(userid)
            {
                IsStored = false
            };
            if (ChatProvider.GetInstance(this.Portal).IsRegisteredUser(userid))
            {
                SqlCommand sqlCommand = this.CreateCommand();
                sqlCommand.CommandText = string.Concat("SELECT DisplayName,Description,ServerProperties,PublicProperties,PrivateProperties,BuildinContacts,BuildinIgnores FROM ", this.prefix, "User WHERE UserId=@userid");
                this.SetParameter(sqlCommand.Parameters, "@userid", userid);
                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    if (sqlDataReader.Read())
                    {
                        AppUserInfo appUserInfo1 = appUserInfo;
                        if (sqlDataReader.IsDBNull(0))
                        {
                            str = null;
                        }
                        else
                        {
                            str = sqlDataReader.GetString(0);
                        }
                        appUserInfo1.DisplayName = str;
                        AppUserInfo appUserInfo2 = appUserInfo;
                        if (sqlDataReader.IsDBNull(1))
                        {
                            str1 = null;
                        }
                        else
                        {
                            str1 = sqlDataReader.GetString(1);
                        }
                        appUserInfo2.Description = str1;
                        AppUserInfo appUserInfo3 = appUserInfo;
                        if (sqlDataReader.IsDBNull(2))
                        {
                            str2 = null;
                        }
                        else
                        {
                            str2 = sqlDataReader.GetString(2);
                        }
                        appUserInfo3.ServerProperties = str2;
                        AppUserInfo appUserInfo4 = appUserInfo;
                        if (sqlDataReader.IsDBNull(3))
                        {
                            str3 = null;
                        }
                        else
                        {
                            str3 = sqlDataReader.GetString(3);
                        }
                        appUserInfo4.PublicProperties = str3;
                        AppUserInfo appUserInfo5 = appUserInfo;
                        if (sqlDataReader.IsDBNull(4))
                        {
                            str4 = null;
                        }
                        else
                        {
                            str4 = sqlDataReader.GetString(4);
                        }
                        appUserInfo5.PrivateProperties = str4;
                        AppUserInfo appUserInfo6 = appUserInfo;
                        if (sqlDataReader.IsDBNull(5))
                        {
                            str5 = null;
                        }
                        else
                        {
                            str5 = sqlDataReader.GetString(5);
                        }
                        appUserInfo6.BuildinContacts = str5;
                        AppUserInfo appUserInfo7 = appUserInfo;
                        if (sqlDataReader.IsDBNull(6))
                        {
                            str6 = null;
                        }
                        else
                        {
                            str6 = sqlDataReader.GetString(6);
                        }
                        appUserInfo7.BuildinIgnores = str6;
                        appUserInfo.IsStored = true;
                    }
                }
                if (appUserInfo.DisplayName == null)
                {
                }
                chatUserInfo = appUserInfo;
            }
            else
            {
                chatUserInfo = appUserInfo;
            }
            return chatUserInfo;
        }

        public SupportSession[] LoadAgentSessions(string agentid)
        {
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("SELECT * FROM ", this.prefix, "SupportSession WHERE AgentUserId=@AgentUserId ORDER BY SessionID ASC");
            this.SetParameter(sqlCommand.Parameters, "@AgentUserId", agentid);
            return this.LoadSessions(sqlCommand);
        }

#pragma warning disable CS0436 // Type conflicts with imported type
        public ChatMsgData[] LoadChannelHistoryMessages(string channelname, int maxcount, DateTime afterdate)
#pragma warning restore CS0436 // Type conflicts with imported type
        {
            ArrayList arrayLists = new ArrayList();
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat(new object[] { "SELECT TOP ", maxcount, " MessageId,MsgTime,Location,SenderId,Sender,TargetId,Target,Whisper,Text,Html,ClaimNo FROM ", this.prefix, "LogMessage WHERE Place=@placename AND MsgTime>=@afterdate ORDER BY MsgTime DESC" });
            this.SetParameter(sqlCommand.Parameters, "@placename", channelname);
            this.SetParameter(sqlCommand.Parameters, "@afterdate", afterdate);
            using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
            {
                while (sqlDataReader.Read())
                {
#pragma warning disable CS0436 // Type conflicts with imported type
#pragma warning disable CS0436 // Type conflicts with imported type
                    ChatMsgData chatMsgDatum = new ChatMsgData()
#pragma warning restore CS0436 // Type conflicts with imported type
#pragma warning restore CS0436 // Type conflicts with imported type
                    {
                        MessageId = sqlDataReader.GetInt32(0),
                        Time = sqlDataReader.GetDateTime(1),
                        Location = sqlDataReader.GetString(2),
                        SenderId = sqlDataReader.GetString(3),
                        Sender = sqlDataReader.GetString(4)
                    };
                    if (!sqlDataReader.IsDBNull(5))
                    {
                        chatMsgDatum.TargetId = sqlDataReader.GetString(5);
                    }
                    if (!sqlDataReader.IsDBNull(6))
                    {
                        chatMsgDatum.Target = sqlDataReader.GetString(6);
                    }
                    if (!sqlDataReader.IsDBNull(8))
                    {
                        chatMsgDatum.Text = sqlDataReader.GetString(8);
                    }
                    if (!sqlDataReader.IsDBNull(9))
                    {
                        chatMsgDatum.Html = sqlDataReader.GetString(9);
                    }
                    if (!sqlDataReader.IsDBNull(10))
                    {
                        chatMsgDatum.ClaimNo = sqlDataReader.GetString(10);
                    }
                    chatMsgDatum.Whisper = sqlDataReader.GetInt32(7) == 1;
                    arrayLists.Add(chatMsgDatum);
                }
            }
            arrayLists.Reverse();

#pragma warning disable CS0436 // Type conflicts with imported type
            return (ChatMsgData[])arrayLists.ToArray(typeof(ChatMsgData));
#pragma warning restore CS0436 // Type conflicts with imported type

        }

        public SupportSession[] LoadCustomerSessions(string customerid)
        {
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("SELECT * FROM ", this.prefix, "SupportSession WHERE CustomerId=@CustomerId ORDER BY SessionID ASC");
            this.SetParameter(sqlCommand.Parameters, "@CustomerId", customerid);
            return this.LoadSessions(sqlCommand);
        }

        public SupportDepartment[] LoadDepartments()
        {
            ArrayList arrayLists = new ArrayList();
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("SELECT DepartmentId,DepartmentName FROM ", this.prefix, "SupportDepartment");
            using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
            {
                while (sqlDataReader.Read())
                {
                    SupportDepartment supportDepartment = new SupportDepartment()
                    {
                        DepartmentId = sqlDataReader.GetInt32(0),
                        Name = sqlDataReader.GetString(1)
                    };
                    arrayLists.Add(supportDepartment);
                }
            }
            sqlCommand.CommandText = string.Concat("SELECT AgentUserId FROM ", this.prefix, "SupportAgent WHERE DepartmentId=@depid");
            SqlParameter sqlParameter = new SqlParameter("@depid", SqlDbType.Int);
            sqlCommand.Parameters.Add(sqlParameter);
            ArrayList arrayLists1 = new ArrayList();
            foreach (SupportDepartment arrayList in arrayLists)
            {
                sqlParameter.Value = arrayList.DepartmentId;
                using (SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader())
                {
                    while (sqlDataReader1.Read())
                    {
                        SupportAgent supportAgent = new SupportAgent()
                        {
                            UserId = sqlDataReader1.GetString(0),
                            Department = arrayList
                        };
                        arrayLists1.Add(supportAgent);
                    }
                }
                if (arrayLists1.Count != 0)
                {
                    arrayList.Agents = (SupportAgent[])arrayLists1.ToArray(typeof(SupportAgent));
                    arrayLists1 = new ArrayList();
                }
            }
            return (SupportDepartment[])arrayLists.ToArray(typeof(SupportDepartment));
        }

        public SupportFeedback LoadFeedback(int feedbackid)
        {
            SupportFeedback supportFeedback;
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("SELECT * FROM ", this.prefix, "SupportFeedback WHERE FeedbackId=@FeedbackId");
            this.SetParameter(sqlCommand.Parameters, "@FeedbackId", feedbackid);
            using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
            {
                if (sqlDataReader.Read())
                {
                    SupportFeedback supportFeedback1 = new SupportFeedback()
                    {
                        FeedbackId = (int)sqlDataReader["FeedbackId"],
                        FbTime = (DateTime)sqlDataReader["FbTime"],
                        Name = (string)sqlDataReader["Name"],
                        DisplayName = (string)sqlDataReader["DisplayName"],
                        Email = (string)sqlDataReader["Email"],
                        Title = (string)sqlDataReader["Title"],
                        Content = (string)sqlDataReader["Content"],
                        Comment = sqlDataReader["Comment"] as string,
                        CommentBy = sqlDataReader["CommentBy"] as string
                    };
                    supportFeedback = supportFeedback1;
                    return supportFeedback;
                }
            }
            supportFeedback = null;
            return supportFeedback;
        }

        public SupportFeedback[] LoadFeedbacks(bool history)
        {
            SqlCommand sqlCommand = this.CreateCommand();
            SqlCommand sqlCommand1 = sqlCommand;
            string[] strArrays = new string[] { "SELECT * FROM ", this.prefix, "SupportFeedback ", null, null };
            strArrays[3] = (history ? "" : "WHERE CommentBy IS NULL");
            strArrays[4] = " ORDER BY FeedbackId ASC";
            sqlCommand1.CommandText = string.Concat(strArrays);
            ArrayList arrayLists = new ArrayList();
            using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
            {
                while (sqlDataReader.Read())
                {
                    SupportFeedback supportFeedback = new SupportFeedback()
                    {
                        FeedbackId = (int)sqlDataReader["FeedbackId"],
                        FbTime = (DateTime)sqlDataReader["FbTime"],
                        Name = (string)sqlDataReader["Name"],
                        DisplayName = (string)sqlDataReader["DisplayName"],
                        Email = (string)sqlDataReader["Email"],
                        Title = (string)sqlDataReader["Title"],
                        Content = (string)sqlDataReader["Content"],
                        Comment = sqlDataReader["Comment"] as string,
                        CommentBy = sqlDataReader["CommentBy"] as string
                    };
                    arrayLists.Add(supportFeedback);
                }
            }
            return (SupportFeedback[])arrayLists.ToArray(typeof(SupportFeedback));
        }

#pragma warning disable CS0436 // Type conflicts with imported type
        public ChatMsgData[] LoadInstantMessages(string ownerid, string targetid, bool offlineOnly, int maxid, int pagesize)
#pragma warning restore CS0436 // Type conflicts with imported type
        {
            if ((ownerid == null ? true : ownerid == ""))
            {
                throw new ArgumentNullException("ownerid");
            }
            ArrayList arrayLists = new ArrayList();
            SqlCommand str = this.CreateCommand();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("SELECT ");
            if ((pagesize <= 0 ? false : pagesize < 2147483647))
            {
                stringBuilder.Append("TOP ");
                stringBuilder.Append(pagesize);
                stringBuilder.Append(" ");
            }
            stringBuilder.Append("MessageId,MsgTime,SenderId,Sender,TargetId,Target,Offline,Text,Html,ClaimNo ");
            stringBuilder.Append("From ");
            stringBuilder.Append(this.prefix);
            stringBuilder.Append("InstantMessage ");
            stringBuilder.Append("WHERE ");
            if (offlineOnly)
            {
                if (maxid >= 2147483647)
                {
                    stringBuilder.Append(" Offline=1 ");
                }
                else
                {
                    stringBuilder.Append(" MessageId<@maxid AND Offline=1 ");
                }
            }
            else if (maxid >= 2147483647)
            {
                stringBuilder.Append(" 1=1 ");
            }
            else
            {
                stringBuilder.Append(" MessageId<@maxid ");
            }
            if ((targetid == null ? false : targetid != ""))
            {
                stringBuilder.Append(" AND (  (SenderId=@ownerid AND TargetId=@targetid AND ISNULL(DeletedBySender,0)<>1) OR (SenderId=@targetid AND TargetId=@ownerid AND ISNULL(DeletedByTarget,0)<>1) ) ");
            }
            else
            {
                stringBuilder.Append(" AND ( (SenderId=@ownerid AND ISNULL(DeletedBySender,0)<>1) OR (TargetId=@ownerid AND ISNULL(DeletedByTarget,0)<>1) ) ");
            }
            stringBuilder.Append("ORDER BY MsgTime DESC");
            str.CommandText = stringBuilder.ToString();
            this.SetParameter(str.Parameters, "@ownerid", ownerid);
            this.SetParameter(str.Parameters, "@targetid", targetid);
            this.SetParameter(str.Parameters, "@maxid", maxid);
            using (SqlDataReader sqlDataReader = str.ExecuteReader())
            {
                while (sqlDataReader.Read())
                {
#pragma warning disable CS0436 // Type conflicts with imported type
#pragma warning disable CS0436 // Type conflicts with imported type
                    ChatMsgData chatMsgDatum = new ChatMsgData()
#pragma warning restore CS0436 // Type conflicts with imported type
#pragma warning restore CS0436 // Type conflicts with imported type
                    {
                        MessageId = sqlDataReader.GetInt32(0),
                        Time = sqlDataReader.GetDateTime(1),
                        SenderId = sqlDataReader.GetString(2),
                        Sender = sqlDataReader.GetString(3),
                        TargetId = sqlDataReader.GetString(4),
                        Target = sqlDataReader.GetString(5)
                    };
                    if (!sqlDataReader.IsDBNull(7))
                    {
                        chatMsgDatum.Text = sqlDataReader.GetString(7);
                    }
                    if (!sqlDataReader.IsDBNull(8))
                    {
                        chatMsgDatum.Html = sqlDataReader.GetString(8);
                    }
                    if (!sqlDataReader.IsDBNull(9))
                    {
                        chatMsgDatum.ClaimNo = sqlDataReader.GetString(9);
                    }

                    chatMsgDatum.Offline = sqlDataReader.GetInt32(6) == 1;
                    arrayLists.Add(chatMsgDatum);
                }
            }
#pragma warning disable CS0436 // Type conflicts with imported type
            return (ChatMsgData[])arrayLists.ToArray(typeof(ChatMsgData));
#pragma warning restore CS0436 // Type conflicts with imported type
        }

#pragma warning disable CS0436 // Type conflicts with imported type
        public ChatMsgData[] LoadInstantOfflineMessages(string targetid, DateTime time)
#pragma warning restore CS0436 // Type conflicts with imported type
        {
            ArrayList arrayLists = new ArrayList();
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("SELECT MessageId,MsgTime,SenderId,Sender,TargetId,Target,Offline,Text,Html,ClaimNo FROM ", this.prefix, "InstantMessage WHERE Offline=1 AND MsgTime>@time AND TargetId=@targetid ORDER BY MsgTime DESC");
            this.SetParameter(sqlCommand.Parameters, "@targetid", targetid);
            this.SetParameter(sqlCommand.Parameters, "@time", time);
            using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
            {
                while (sqlDataReader.Read())
                {
#pragma warning disable CS0436 // Type conflicts with imported type
                    ChatMsgData chatMsgDatum = new ChatMsgData()
#pragma warning restore CS0436 // Type conflicts with imported type
                    {
                        MessageId = sqlDataReader.GetInt32(0),
                        Location = "Messenger",
                        Time = sqlDataReader.GetDateTime(1),
                        SenderId = sqlDataReader.GetString(2),
                        Sender = sqlDataReader.GetString(3),
                        TargetId = sqlDataReader.GetString(4),
                        Target = sqlDataReader.GetString(5)
                    };
                    if (!sqlDataReader.IsDBNull(7))
                    {
                        chatMsgDatum.Text = sqlDataReader.GetString(7);
                    }
                    if (!sqlDataReader.IsDBNull(8))
                    {
                        chatMsgDatum.Html = sqlDataReader.GetString(8);
                    }

                    if (!sqlDataReader.IsDBNull(9))
                    {
                        chatMsgDatum.ClaimNo = sqlDataReader.GetString(9);
                    }
                    chatMsgDatum.Offline = sqlDataReader.GetInt32(6) == 1;
                    arrayLists.Add(chatMsgDatum);
                }
            }
            arrayLists.Reverse();
#pragma warning disable CS0436 // Type conflicts with imported type
            return (ChatMsgData[])arrayLists.ToArray(typeof(ChatMsgData));
#pragma warning restore CS0436 // Type conflicts with imported type
        }

#pragma warning disable CS0436 // Type conflicts with imported type
        public ChatMsgData[] LoadSessionMessages(int sessionid)
#pragma warning restore CS0436 // Type conflicts with imported type
        {
            ArrayList arrayLists = new ArrayList();
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("SELECT MessageId,MsgTime,SenderId,Sender,TargetId,Target,Text,Html,ClaimNo FROM ", this.prefix, "SupportMessage WHERE SessionId=@SessionId ORDER BY MessageID ASC");
            this.SetParameter(sqlCommand.Parameters, "@SessionId", sessionid);
            using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
            {
                while (sqlDataReader.Read())
                {
#pragma warning disable CS0436 // Type conflicts with imported type
                    ChatMsgData chatMsgDatum = new ChatMsgData()
#pragma warning restore CS0436 // Type conflicts with imported type
                    {
                        MessageId = sqlDataReader.GetInt32(0),
                        Time = sqlDataReader.GetDateTime(1),
                        SenderId = sqlDataReader.GetString(2),
                        Sender = sqlDataReader.GetString(3)
                    };
                    if (!sqlDataReader.IsDBNull(4))
                    {
                        chatMsgDatum.TargetId = sqlDataReader.GetString(4);
                    }
                    if (!sqlDataReader.IsDBNull(5))
                    {
                        chatMsgDatum.Target = sqlDataReader.GetString(5);
                    }
                    if (!sqlDataReader.IsDBNull(6))
                    {
                        chatMsgDatum.Text = sqlDataReader.GetString(6);
                    }
                    if (!sqlDataReader.IsDBNull(7))
                    {
                        chatMsgDatum.Html = sqlDataReader.GetString(7);
                    }

                    if (!sqlDataReader.IsDBNull(8))
                    {
                        chatMsgDatum.ClaimNo = sqlDataReader.GetString(8);
                    }
                    arrayLists.Add(chatMsgDatum);
                }
            }
            arrayLists.Reverse();
#pragma warning disable CS0436 // Type conflicts with imported type
            return (ChatMsgData[])arrayLists.ToArray(typeof(ChatMsgData));
#pragma warning restore CS0436 // Type conflicts with imported type
        }

        private SupportSession[] LoadSessions(SqlCommand cmd)
        {
            ArrayList arrayLists = new ArrayList();
            using (SqlDataReader sqlDataReader = cmd.ExecuteReader())
            {
                while (sqlDataReader.Read())
                {
                    SupportSession supportSession = new SupportSession()
                    {
                        SessionId = (int)sqlDataReader["SessionId"],
                        BeginTime = (DateTime)sqlDataReader["BeginTime"],
                        DepartmentId = (int)sqlDataReader["DepartmentId"],
                        AgentUserId = (string)sqlDataReader["AgentUserId"],
                        CustomerId = (string)sqlDataReader["CustomerId"],
                        DisplayName = (string)sqlDataReader["DisplayName"],
                        ActiveTime = (DateTime)sqlDataReader["ActiveTime"],
                        Email = sqlDataReader["Email"].ToString(),
                        IPAddress = sqlDataReader["IPAddress"].ToString(),
                        Culture = sqlDataReader["Culture"].ToString(),
                        Platform = sqlDataReader["Platform"].ToString(),
                        Browser = sqlDataReader["Browser"].ToString(),
                        AgentRating = Convert.ToInt32(sqlDataReader["AgentRating"]),
                        SessionData = sqlDataReader["SessionData"] as string,
                        Url = sqlDataReader["Url"] as string,
                        Referrer = sqlDataReader["Referrer"] as string
                    };
                    arrayLists.Add(supportSession);
                }
            }
            return (SupportSession[])arrayLists.ToArray(typeof(SupportSession));
        }

        public string LoadSetting(string name)
        {
            string str;
            if (name == null)
            {
                throw new ArgumentNullException("name");
            }
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("SELECT SettingData FROM ", this.prefix, "Settings WHERE SettingName=@Name");
            this.SetParameter(sqlCommand.Parameters, "@Name", name);
            using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
            {
                if (sqlDataReader.Read())
                {
                    str = sqlDataReader.GetString(0);
                    return str;
                }
            }
            str = null;
            return str;
        }

        public void LogEvent(ChatPlace place, ChatIdentity user, string category, string message)
        {
            object value;
            object uniqueId;
            if (message.Length <= 3000)
            {
                if (!base.SkipEvent(place, user, category))
                {
                    SqlCommand sqlCommand = this.CreateCommand();
                    sqlCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "LogEvent (EvtTime,Category,Portal,Place,UserName,UserId,Message) VALUES (@time,@category,@portal,@place,@username,@userid,@message)");
                    SqlParameterCollection parameters = sqlCommand.Parameters;
                    this.SetParameter(parameters, "@time", DateTime.Now);
                    this.SetParameter(parameters, "@category", category);
                    this.SetParameter(parameters, "@portal", place.Portal.Name);
                    this.SetParameter(parameters, "@place", place.PlaceName);
                    SqlParameterCollection sqlParameterCollection = parameters;
                    if (user == null)
                    {
                        value = DBNull.Value;
                    }
                    else
                    {
                        value = user.DisplayName;
                    }
                    this.SetParameter(sqlParameterCollection, "@username", value);
                    SqlParameterCollection sqlParameterCollection1 = parameters;
                    if (user == null)
                    {
                        uniqueId = DBNull.Value;
                    }
                    else
                    {
                        uniqueId = user.UniqueId;
                    }
                    this.SetParameter(sqlParameterCollection1, "@userid", uniqueId);
                    this.SetParameter(parameters, "@message", message);
                    sqlCommand.ExecuteNonQuery();
                }
            }
        }

        public void LogInstantMessage(ChatIdentity sender, string targetid, string targetname, bool offline, string text, string html, string claimNo)
        {
            object value;
            object obj;
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "InstantMessage (MsgTime,Sender,SenderId,Target,TargetId,Offline,IPAddress,Text,Html,claimNo) VALUES (@time,@sender,@senderid,@target,@targetid,@offline,@ipaddr,@text,@html, @claimno)");
            SqlParameterCollection parameters = sqlCommand.Parameters;
            this.SetParameter(parameters, "@time", DateTime.Now);
            this.SetParameter(parameters, "@sender", sender.DisplayName);
            this.SetParameter(parameters, "@senderid", sender.UniqueId);
            this.SetParameter(parameters, "@target", targetname);
            this.SetParameter(parameters, "@targetid", targetid);
            this.SetParameter(parameters, "@offline", (offline ? 1 : 0));
            this.SetParameter(parameters, "@ipaddr", sender.IPAddress);
            SqlParameterCollection sqlParameterCollection = parameters;
            if (text == null)
            {
                value = DBNull.Value;
            }
            else
            {
                value = text;
            }
            this.SetParameter(sqlParameterCollection, "@text", value);
            SqlParameterCollection sqlParameterCollection1 = parameters;
            if (html == null)
            {
                obj = DBNull.Value;
            }
            else
            {
                obj = html;
            }
            this.SetParameter(sqlParameterCollection1, "@html", obj);

            if (claimNo == null)
            {
                obj = DBNull.Value;
            }
            else
            {
                obj = claimNo;
            }
            this.SetParameter(sqlParameterCollection1, "@claimno", obj);


            sqlCommand.ExecuteNonQuery();
        }

        public void LogMessage(ChatPlace place, ChatIdentity sender, ChatIdentity target, bool whisper, string text, string html, string claimNo)
        {
            object value;
            object uniqueId;
            object obj;
            object value1;
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "LogMessage (MsgTime,Location,Place,Sender,SenderId,Target,TargetId,Whisper,IPAddress,Text,Html, ClaimNo) VALUES (@time,@location,@place,@sender,@senderid,@target,@targetid,@whisper,@ipaddr,@text,@html, @claimno)");
            SqlParameterCollection parameters = sqlCommand.Parameters;
            this.SetParameter(parameters, "@time", DateTime.Now);
            this.SetParameter(parameters, "@location", place.Location);
            this.SetParameter(parameters, "@place", place.PlaceName);
            this.SetParameter(parameters, "@sender", sender.DisplayName);
            this.SetParameter(parameters, "@senderid", sender.UniqueId);
            SqlParameterCollection sqlParameterCollection = parameters;
            if (target == null)
            {
                value = DBNull.Value;
            }
            else
            {
                value = target.DisplayName;
            }
            this.SetParameter(sqlParameterCollection, "@target", value);
            SqlParameterCollection sqlParameterCollection1 = parameters;
            if (target == null)
            {
                uniqueId = DBNull.Value;
            }
            else
            {
                uniqueId = target.UniqueId;
            }
            this.SetParameter(sqlParameterCollection1, "@targetid", uniqueId);
            this.SetParameter(parameters, "@whisper", (whisper ? 1 : 0));
            this.SetParameter(parameters, "@ipaddr", sender.IPAddress);
            SqlParameterCollection sqlParameterCollection2 = parameters;
            if (text == null)
            {
                obj = DBNull.Value;
            }
            else
            {
                obj = text;
            }
            this.SetParameter(sqlParameterCollection2, "@text", obj);

            SqlParameterCollection sqlParameterCollection3 = parameters;
            if (html == null)
            {
                value1 = DBNull.Value;
            }
            else
            {
                value1 = html;
            }
            this.SetParameter(sqlParameterCollection3, "@html", value1);


            SqlParameterCollection sqlParameterCollection4 = parameters;
            if (claimNo == null)
            {
                value1 = DBNull.Value;
            }
            else
            {
                value1 = claimNo;
            }
            this.SetParameter(sqlParameterCollection4, "@claimno", value1);
            sqlCommand.ExecuteNonQuery();
        }

        public void LogSupportMessage(ChatPlace place, SupportSession session, string type, ChatIdentity sender, ChatIdentity target, string text, string html, string claimno)
        {
            object value;
            object uniqueId;
            object obj;
            object value1;
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "SupportMessage (MsgTime,SessionId,MsgType,Sender,SenderId,Target,TargetId,Text,Html,ClaimNo) VALUES (@time,@sessionid,@msgtype,@sender,@senderid,@target,@targetid,@text,@html,@claimno)");
            SqlParameterCollection parameters = sqlCommand.Parameters;
            this.SetParameter(parameters, "@time", DateTime.Now);
            this.SetParameter(parameters, "@sessionid", session.SessionId);
            this.SetParameter(parameters, "@msgtype", type);
            this.SetParameter(parameters, "@sender", sender.DisplayName);
            this.SetParameter(parameters, "@senderid", sender.UniqueId);
            SqlParameterCollection sqlParameterCollection = parameters;
            if (target == null)
            {
                value = DBNull.Value;
            }
            else
            {
                value = target.DisplayName;
            }
            this.SetParameter(sqlParameterCollection, "@target", value);
            SqlParameterCollection sqlParameterCollection1 = parameters;
            if (target == null)
            {
                uniqueId = DBNull.Value;
            }
            else
            {
                uniqueId = target.UniqueId;
            }
            this.SetParameter(sqlParameterCollection1, "@targetid", uniqueId);
            SqlParameterCollection sqlParameterCollection2 = parameters;
            if (text == null)
            {
                obj = DBNull.Value;
            }
            else
            {
                obj = text;
            }
            this.SetParameter(sqlParameterCollection2, "@text", obj);

            SqlParameterCollection sqlParameterCollection3 = parameters;
            if (html == null)
            {
                value1 = DBNull.Value;
            }
            else
            {
                value1 = html;
            }
            this.SetParameter(sqlParameterCollection3, "@html", value1);

            SqlParameterCollection sqlParameterCollection4 = parameters;
            if (claimno == null)
            {
                value1 = DBNull.Value;
            }
            else
            {
                value1 = claimno;
            }
            this.SetParameter(sqlParameterCollection4, "@claimno", value1);

            sqlCommand.ExecuteNonQuery();
        }

        public void RemoveAgent(int departmentid, string userid)
        {
            if (userid == null)
            {
                throw new ArgumentNullException("userid");
            }
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("DELETE FROM ", this.prefix, "SupportAgent WHERE DepartmentId=@depid AND AgentUserId=@userid");
            this.SetParameter(sqlCommand.Parameters, "@depid", departmentid);
            this.SetParameter(sqlCommand.Parameters, "@userid", userid);
            sqlCommand.ExecuteNonQuery();
        }

        public void RemoveDepartment(int departmentid)
        {
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("DELETE FROM ", this.prefix, "SupportDepartment WHERE DepartmentId=@depid");
            this.SetParameter(sqlCommand.Parameters, "@depid", departmentid);
            sqlCommand.ExecuteNonQuery();
        }

        public void RenameDepartment(int departmentid, string newname)
        {
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("UPDATE ", this.prefix, "SupportDepartment SET DepartmentName=@newname WHERE DepartmentId=@depid");
            this.SetParameter(sqlCommand.Parameters, "@depid", departmentid);
            this.SetParameter(sqlCommand.Parameters, "@newname", newname);
            sqlCommand.ExecuteNonQuery();
        }

        private static int SafeGetOrdinal(SqlDataReader reader, string name)
        {
            int ordinal;
            try
            {
                ordinal = reader.GetOrdinal(name);
            }
            catch
            {
                ordinal = -1;
            }
            return ordinal;
        }

        public void SaveSetting(string name, string data)
        {
            if (name == null)
            {
                throw new ArgumentNullException("name");
            }
            SqlCommand sqlCommand = this.CreateCommand();
            this.SetParameter(sqlCommand.Parameters, "@Name", name);
            if (data != null)
            {
                sqlCommand.CommandText = string.Concat("UPDATE ", this.prefix, "Settings SET SettingData=@Data WHERE SettingName=@Name");
                this.SetParameter(sqlCommand.Parameters, "@Data", data);
                if (sqlCommand.ExecuteNonQuery() == 0)
                {
                    sqlCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "Settings (SettingName,SettingData) VALUES (@Name,@Data)");
                    sqlCommand.ExecuteNonQuery();
                }
            }
            else
            {
                sqlCommand.CommandText = string.Concat("DELETE ", this.prefix, "Settings WHERE SettingName=@Name");
                sqlCommand.ExecuteNonQuery();
            }
        }

        public SupportSession[] SearchAgentSessions(DateTime start, DateTime endtime, string agentid, string departmentid, string customer, string email, string ipaddr, string culture)
        {
            SqlCommand str = this.CreateCommand();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("SELECT * FROM ").Append(this.prefix).Append("SupportSession WHERE BeginTime>=@start AND BeginTime<=@endtime");
            this.SetParameter(str.Parameters, "@start", start);
            this.SetParameter(str.Parameters, "@endtime", endtime);
            if ((agentid == null ? false : agentid != ""))
            {
                stringBuilder.Append(" AND AgentUserId=@agentid");
                this.SetParameter(str.Parameters, "@agentid", agentid);
            }
            if ((departmentid == null ? false : departmentid != ""))
            {
                stringBuilder.Append(" AND DepartmentId=@departmentid");
                this.SetParameter(str.Parameters, "@departmentid", departmentid);
            }
            if ((customer == null ? false : customer != ""))
            {
                stringBuilder.Append(" AND DisplayName LIKE @customer");
                this.SetParameter(str.Parameters, "@customer", string.Concat("%", customer, "%"));
            }
            if ((email == null ? false : email != ""))
            {
                stringBuilder.Append(" AND Email LIKE @email");
                this.SetParameter(str.Parameters, "@email", string.Concat("%", email, "%"));
            }
            if ((ipaddr == null ? false : ipaddr != ""))
            {
                stringBuilder.Append(" AND IPAddress LIKE @ipaddr");
                this.SetParameter(str.Parameters, "@ipaddr", string.Concat("%", ipaddr, "%"));
            }
            if ((culture == null ? false : culture != ""))
            {
                stringBuilder.Append(" AND Culture LIKE @culture");
                this.SetParameter(str.Parameters, "@culture", string.Concat("%", culture, "%"));
            }
            stringBuilder.Append(" ORDER BY SessionID ASC");
            str.CommandText = stringBuilder.ToString();
            return this.LoadSessions(str);
        }

        public SupportSession SelectSession(string sessionid)
        {
            SupportSession supportSession;
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("SELECT * FROM ", this.prefix, "SupportSession WHERE SessionId=@SessionId");
            this.SetParameter(sqlCommand.Parameters, "@SessionId", sessionid);
            SupportSession[] supportSessionArray = this.LoadSessions(sqlCommand);
            if ((int)supportSessionArray.Length != 1)
            {
                supportSession = null;
            }
            else
            {
                supportSession = supportSessionArray[0];
            }
            return supportSession;
        }

        public virtual void SetCustomerData(string userid, string value)
        {
            if (userid == null)
            {
                throw new ArgumentNullException("userid");
            }
            SqlCommand sqlCommand = this.CreateCommand();
            if (value != null)
            {
                this.SetParameter(sqlCommand.Parameters, "@value", value);
                this.SetParameter(sqlCommand.Parameters, "@userid", userid);
                sqlCommand.CommandText = string.Concat("UPDATE ", this.prefix, "SupportCustomer SET CustomerData=@value WHERE CustomerId=@userid");
                if (sqlCommand.ExecuteNonQuery() == 0)
                {
                    sqlCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "SupportCustomer (CustomerData,CustomerId) VALUES (@value,@userid)");
                    sqlCommand.ExecuteNonQuery();
                }
            }
            else
            {
                this.SetParameter(sqlCommand.Parameters, "@userid", userid);
                sqlCommand.CommandText = string.Concat("DELETE ", this.prefix, "SupportCustomer WHERE CustomerId=@userid");
                sqlCommand.ExecuteNonQuery();
            }
        }

        private void SetParameter(SqlParameterCollection pcoll, string pname, object obj)
        {
            if (pcoll == null)
            {
                throw new ArgumentNullException("pcoll");
            }
            if ((obj == null ? true : Convert.IsDBNull(obj)))
            {
                pcoll.Add(pname, SqlDbType.NVarChar, 50).Value = DBNull.Value;
            }
            else if (!(obj is string))
            {
                if (!(obj is byte[]))
                {
                    if (!(obj is SqlParameter))
                    {
                        goto Label1;
                    }
                    pcoll.Add((SqlParameter)obj);
                    return;
                }
                else
                {
                    byte[] numArray = (byte[])obj;
                    if ((int)numArray.Length > 8000)
                    {
                        pcoll.Add(pname, SqlDbType.Binary).Value = numArray;
                        return;
                    }
                }
                Label1:
                pcoll.Add(pname, obj);
            }
            else
            {
                int length = obj.ToString().Length;
                if (length > 4000)
                {
                    pcoll.Add(pname, SqlDbType.NText).Value = obj;
                }
                else if (length > 1000)
                {
                    pcoll.Add(pname, SqlDbType.NVarChar, 4000).Value = obj;
                }
                else if (length > 500)
                {
                    pcoll.Add(pname, SqlDbType.NVarChar, 1000).Value = obj;
                }
                else if (length <= 100)
                {
                    pcoll.Add(pname, SqlDbType.NVarChar, 100).Value = obj;
                }
                else
                {
                    pcoll.Add(pname, SqlDbType.NVarChar, 500).Value = obj;
                }
            }
        }

        public void UpdateFeedback(SupportFeedback feedback)
        {
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("UPDATE ", this.prefix, "SupportFeedback SET FbTime=@FbTime,CustomerId=@CustomerId,Name=@Name,DisplayName=@DisplayName,Email=@Email,Title=@Title,Content=@Content,Comment=@Comment,CommentBy=@CommentBy WHERE FeedbackId=@FeedbackId");
            SqlParameterCollection parameters = sqlCommand.Parameters;
            this.SetParameter(parameters, "@FeedbackId", feedback.FeedbackId);
            this.SetParameter(parameters, "@FbTime", feedback.FbTime);
            this.SetParameter(parameters, "@CustomerId", feedback.CustomerId);
            this.SetParameter(parameters, "@Name", feedback.Name);
            this.SetParameter(parameters, "@DisplayName", feedback.DisplayName);
            this.SetParameter(parameters, "@Email", feedback.Email);
            this.SetParameter(parameters, "@Title", feedback.Title);
            this.SetParameter(parameters, "@Content", feedback.Content);
            this.SetParameter(parameters, "@Comment", feedback.Comment);
            this.SetParameter(parameters, "@CommentBy", feedback.CommentBy);
            sqlCommand.ExecuteNonQuery();
        }

        public void UpdateLobby(IChatLobby ilobby)
        {
            AppLobby appLobby = (AppLobby)ilobby;
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("UPDATE ", this.prefix, "Lobby SET Title=@title,Topic=@topic,Announcement=@announce,MaxOnlineCount=@maxonline,Locked=@locked,AllowAnonymous=@anony,[Password]=@pwd,Description=@description,Integration=@integration,ManagerList=@managerlist,MaxIdleMinute=@maxidle,AutoAwayMinute=@autoaway,HistoryDay=@hisday,HistoryCount=@hiscount,SortIndex=@sort WHERE LobbyId=@lobbyid");
            SqlParameterCollection parameters = sqlCommand.Parameters;
            this.SetParameter(parameters, "@title", appLobby.Title);
            this.SetParameter(parameters, "@topic", appLobby.Topic);
            this.SetParameter(parameters, "@announce", appLobby.Announcement);
            this.SetParameter(parameters, "@maxonline", appLobby.MaxOnlineCount);
            this.SetParameter(parameters, "@locked", appLobby.Locked);
            this.SetParameter(parameters, "@anony", appLobby.AllowAnonymous);
            this.SetParameter(parameters, "@pwd", appLobby.Password);
            this.SetParameter(parameters, "@description", appLobby.Description);
            this.SetParameter(parameters, "@integration", appLobby.Integration);
            this.SetParameter(parameters, "@managerlist", appLobby.ManagerList);
            this.SetParameter(parameters, "@maxidle", appLobby.MaxIdleMinute);
            this.SetParameter(parameters, "@autoaway", appLobby.AutoAwayMinute);
            this.SetParameter(parameters, "@hisday", appLobby.HistoryDay);
            this.SetParameter(parameters, "@hiscount", appLobby.HistoryCount);
            this.SetParameter(parameters, "@sort", appLobby.SortIndex);
            this.SetParameter(parameters, "@lobbyid", appLobby.LobbyId);
            sqlCommand.ExecuteNonQuery();
        }

        public void UpdatePortalInfo(IChatPortalInfo portalinfo)
        {
            AppPortalInfo appPortalInfo = (AppPortalInfo)portalinfo;
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("UPDATE ", this.prefix, "Portal SET Properties=@props WHERE PortalName=@portalname SELECT @@ROWCOUNT");
            SqlParameterCollection parameters = sqlCommand.Parameters;
            this.SetParameter(parameters, "@portalname", appPortalInfo.PortalName);
            this.SetParameter(parameters, "@props", appPortalInfo.Properties);
            if (Convert.ToInt32(sqlCommand.ExecuteScalar()) == 0)
            {
                sqlCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "Portal (PortalName,Properties) VALUES (@portalname,@props)");
                sqlCommand.ExecuteNonQuery();
            }
        }

        public void UpdateRule(IChatRule irule)
        {
            AppRule appRule = (AppRule)irule;
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("UPDATE ", this.prefix, "Rule SET Category=@category,Disabled=@disabled,RuleMode=@mode,Expression=@expression,SortIndex=@sort WHERE RuleId=@ruleid");
            SqlParameterCollection parameters = sqlCommand.Parameters;
            this.SetParameter(parameters, "@category", appRule.Category);
            this.SetParameter(parameters, "@disabled", appRule.Disabled);
            this.SetParameter(parameters, "@mode", appRule.Mode);
            this.SetParameter(parameters, "@expression", appRule.Expression);
            this.SetParameter(parameters, "@sort", appRule.Sort);
            this.SetParameter(parameters, "@ruleid", appRule.RuleId);
            sqlCommand.ExecuteNonQuery();
        }

        public void UpdateSession(SupportSession session)
        {
            SqlCommand sqlCommand = this.CreateCommand();
            sqlCommand.CommandText = string.Concat("UPDATE ", this.prefix, "SupportSession SET [BeginTime]=@BeginTime, [DepartmentId]=@DepartmentId, [AgentUserId]=@AgentUserId, [CustomerId]=@CustomerId, [DisplayName]=@DisplayName, [ActiveTime]=@ActiveTime, [Email]=@Email, [IPAddress]=@IPAddress, [Culture]=@Culture, [Platform]=@Platform, [Browser]=@Browser, [AgentRating]=@AgentRating, [SessionData]=@SessionData,[Url]=@Url,[Referrer]=@Referrer WHERE SessionId=@SessionId");
            SqlParameterCollection parameters = sqlCommand.Parameters;
            this.SetParameter(parameters, "@BeginTime", session.BeginTime);
            this.SetParameter(parameters, "@DepartmentId", session.DepartmentId);
            this.SetParameter(parameters, "@AgentUserId", session.AgentUserId);
            this.SetParameter(parameters, "@CustomerId", session.CustomerId);
            this.SetParameter(parameters, "@DisplayName", session.DisplayName);
            this.SetParameter(parameters, "@ActiveTime", session.ActiveTime);
            this.SetParameter(parameters, "@Email", session.Email);
            this.SetParameter(parameters, "@IPAddress", session.IPAddress);
            this.SetParameter(parameters, "@Culture", session.Culture);
            this.SetParameter(parameters, "@Platform", session.Platform);
            this.SetParameter(parameters, "@Browser", session.Browser);
            this.SetParameter(parameters, "@AgentRating", session.AgentRating);
            this.SetParameter(parameters, "@SessionData", session.SessionData);
            this.SetParameter(parameters, "@Url", session.Url);
            this.SetParameter(parameters, "@Referrer", session.Referrer);
            this.SetParameter(parameters, "@SessionId", session.SessionId);
            sqlCommand.ExecuteNonQuery();
        }

        public void UpdateUserInfo(IChatUserInfo chatinfo)
        {
            AppUserInfo appUserInfo = (AppUserInfo)chatinfo;
            if (ChatProvider.GetInstance(this.Portal).IsRegisteredUser(appUserInfo.UserId))
            {
                SqlCommand sqlCommand = this.CreateCommand();
                sqlCommand.CommandText = string.Concat("UPDATE ", this.prefix, "User SET DisplayName=@name,Description=@desc,ServerProperties=@server,PublicProperties=@public,PrivateProperties=@private,BuildinContacts=@contacts,BuildinIgnores=@ignores WHERE UserId=@userid SELECT @@ROWCOUNT");
                SqlParameterCollection parameters = sqlCommand.Parameters;
                this.SetParameter(parameters, "@userid", appUserInfo.UserId);
                this.SetParameter(parameters, "@name", appUserInfo.DisplayName);
                this.SetParameter(parameters, "@desc", appUserInfo.Description);
                this.SetParameter(parameters, "@server", appUserInfo.ServerProperties);
                this.SetParameter(parameters, "@public", appUserInfo.PublicProperties);
                this.SetParameter(parameters, "@private", appUserInfo.PrivateProperties);
                this.SetParameter(parameters, "@contacts", appUserInfo.BuildinContacts);
                this.SetParameter(parameters, "@ignores", appUserInfo.BuildinIgnores);
                if (Convert.ToInt32(sqlCommand.ExecuteScalar()) == 0)
                {
                    sqlCommand.CommandText = string.Concat("INSERT INTO ", this.prefix, "USER (UserId,DisplayName,Description,ServerProperties,PublicProperties,PrivateProperties,BuildinContacts,BuildinIgnores) VALUES (@userid,@name,@desc,@server,@public,@private,@contacts,@ignores)");
                    sqlCommand.ExecuteNonQuery();
                }
                appUserInfo.IsStored = true;
            }
        }
    }
}