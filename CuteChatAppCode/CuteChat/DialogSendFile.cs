using System;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CuteChat
{
	[ChatServerType]
	public class DialogSendFile : ChatPageBase
	{
		protected Button btnUpload;

		protected Label result;

		protected HtmlInputFile input_file;

		protected bool setting_allowsendfile = false;

		protected long setting_maxfilesize = (long)1024000;

		protected string setting_filetype = "";

		protected bool isMessenger = false;

		protected string contactid;

		protected string placename;

		protected string myguid;

		protected ChatIdentity identity;

		protected bool filesendok = false;

		public DialogSendFile()
		{
		}

		private void btnUpload_Click(object sender, EventArgs e)
		{
			string str;
			if ((this.setting_allowsendfile ? true : ChatWebUtility.CurrentIdentityIsAdministrator))
			{
				HttpPostedFile postedFile = this.input_file.PostedFile;
				if (postedFile == null)
				{
					this.result.Text = "[[UI_Send_NoFile]]";
				}
				else if ((postedFile.FileName == "" || postedFile.FileName == null ? false : postedFile.InputStream.Length != (long)0))
				{
					string str1 = Path.GetExtension(postedFile.FileName).Trim(new char[] { '.' });
					if (this.CheckExt(str1))
					{
						long length = postedFile.InputStream.Length;
						if (length <= this.setting_maxfilesize)
						{
							string fileName = Path.GetFileName(postedFile.FileName);
							string contentType = postedFile.ContentType;
							string str2 = DateTime.Now.ToString("yyyy-MM-dd");
							string str3 = Guid.NewGuid().ToString();
							string str4 = base.ResolveUrl(string.Concat(new string[] { "DownloadFile.ashx?date=", str2, "&guid=", str3, "&filename=", HttpUtility.UrlEncode(fileName), "&mime=", contentType }));
							try
							{
								string item = ConfigurationSettings.AppSettings["CuteChat.SendFile.Directory"];
								if ((item == null ? true : item.Length == 0))
								{
									item = "~/CuteChatSendFiles";
								}
								string str5 = string.Concat(new string[] { item, "/", str2, "/", str3, ".licx" });
								if ((str5.StartsWith("~/") ? false : !str5.StartsWith("/")))
								{
									str = str5.Replace('/', '\\');
								}
								else
								{
									str5 = ChatWebUtility.ProcessWebPath(this.Context, null, str5);
									str = base.Server.MapPath(str5);
								}
								string directoryName = Path.GetDirectoryName(str);
								if (!Directory.Exists(directoryName))
								{
									Directory.CreateDirectory(directoryName);
								}
								postedFile.SaveAs(str);
								DialogSendFile.SendFileImpl(ChatWebUtility.GetLogonIdentity(), this.isMessenger, this.placename, this.myguid, this.contactid, fileName, str4, contentType, length);
							}
							catch (Exception exception1)
							{
								Exception exception = exception1;
								this.result.Text = string.Concat("Failed ! ", exception.Message);
								this.result.ToolTip = exception.ToString();
								return;
							}
							this.result.Text = "[[UI_Send_UploadSuccessfully]]";
							this.filesendok = true;
						}
						else
						{
							this.result.Text = string.Concat("[[MaxFileSizeMustBeLessThan]] ", ChatWebUtility.FormatSize((double)this.setting_maxfilesize), ".");
						}
					}
					else
					{
						this.result.Text = string.Concat("[[FileformatNotsupported]]: ", str1);
					}
				}
				else
				{
					this.result.Text = "[[UI_Send_NoFile]]";
				}
			}
			else
			{
				this.result.Text = "[[UI_FeatureDisabled]]";
			}
		}

		private bool CheckExt(string ext)
		{
			bool flag;
			string[] strArrays = this.setting_filetype.Split(",|".ToCharArray());
			int num = 0;
			while (true)
			{
				if (num < (int)strArrays.Length)
				{
					string str = strArrays[num];
					if (string.Compare(ext, str.Trim(new char[] { '.' }), true) != 0)
					{
						num++;
					}
					else
					{
						flag = true;
						break;
					}
				}
				else
				{
					flag = false;
					break;
				}
			}
			return flag;
		}

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			this.btnUpload.Click += new EventHandler(this.btnUpload_Click);
			try
			{
				this.contactid = base.Request.QueryString["ContactId"];
				if ((this.contactid == null ? false : this.contactid != ""))
				{
					this.isMessenger = true;
				}
			}
			catch
			{
			}
			this.identity = ChatWebUtility.GetLogonIdentity();
			if (!this.isMessenger)
			{
				this.placename = base.Request.QueryString["PlaceName"];
				this.myguid = base.Request.QueryString["MyGuid"];
			}
			else if (this.identity == null)
			{
				base.Response.Redirect(ChatWebUtility.ResolveResource(this.Context, ChatWebUtility.LoginUrl));
			}
			if (this.isMessenger)
			{
				this.setting_allowsendfile = ChatWebUtility.MessengerAllowSendFile;
				this.setting_maxfilesize = ChatWebUtility.MessengerSendFileSize;
				this.setting_filetype = ChatWebUtility.MessengerSendFileType;
			}
			else if (!this.placename.StartsWith("SupportSession:"))
			{
				this.setting_allowsendfile = ChatWebUtility.GlobalAllowSendFile;
				this.setting_maxfilesize = ChatWebUtility.GlobalSendFileSize;
				this.setting_filetype = ChatWebUtility.GlobalSendFileType;
			}
			else
			{
				this.setting_allowsendfile = ChatWebUtility.SupportAllowSendFile;
				this.setting_maxfilesize = ChatWebUtility.SupportSendFileSize;
				this.setting_filetype = ChatWebUtility.SupportSendFileType;
			}
			this.result.Text = string.Concat("[[UI_AllowedFiles]]", this.setting_filetype);
			this.btnUpload.Attributes["onclick"] = "var b=this;setTimeout(function(){b.disabled=true},100);";
		}

		[ChatServerType]
		public static void SendFileImpl(ChatIdentity identity, bool isMessenger, string placename, string myguid, string contactid, string filename, string filelink, string filemime, long len)
		{
			ChatPortal chatPortal = null;
			if (!ClusterSupport.IsClusterClient)
			{
				lock (ChatSystem.Instance.GetCurrentPortal())
				{
					ChatPortal chatPortal1 = chatPortal;
					Monitor.Enter(chatPortal1);
					try
					{
						ChatPlace place = null;
						ChatPlaceUser chatPlaceUser = null;
						ChatPlaceUser chatPlaceUser1 = null;
						if (!isMessenger)
						{
							if (!chatPortal.IsPlaceStarted(placename))
							{
								throw new Exception(string.Concat("Invalid Place:", placename));
							}
							place = chatPortal.GetPlace(placename);
							chatPlaceUser1 = place.FindItem(new ChatGuid(myguid)) as ChatPlaceUser;
							if (chatPlaceUser1 == null)
							{
								throw new Exception("Invalid User");
							}
							if (identity == null)
							{
								if (!chatPlaceUser1.Identity.IsAnonymous)
								{
									throw new Exception("Invalid User");
								}
							}
							else if (identity != chatPlaceUser1.Identity)
							{
								throw new Exception("Invalid User");
							}
						}
						else
						{
							place = chatPortal.Messenger;
							chatPlaceUser1 = place.FindUser(identity.UniqueId);
							chatPlaceUser = place.FindUser(contactid);
							if (chatPlaceUser == null)
							{
								throw new Exception("Invalid User");
							}
						}
						place.SendFileMessage(chatPlaceUser1.Connection, chatPlaceUser, filename, len, filelink, filemime);
					}
					finally
					{
						Monitor.Exit(chatPortal1);
					}
				}
			}
			else
			{
				if (identity != null)
				{
					identity = new DialogSendFile.TempIdentity(identity.DisplayName, identity.IsAnonymous, identity.UniqueId, identity.IPAddress);
				}
				ClusterSupport.ExecuteCurrentMethod(new object[] { identity, isMessenger, placename, myguid, contactid, filename, filelink, filemime, len });
			}
		}

		[Serializable]
		public class TempIdentity : ChatIdentity
		{
			private string _name;

			private bool _isanony;

			private string _uniqueid;

			private string _ipaddress;

			public override string DisplayName
			{
				get
				{
					return this._name;
				}
			}

			public override string IPAddress
			{
				get
				{
					return this._ipaddress;
				}
			}

			public override bool IsAnonymous
			{
				get
				{
					return this._isanony;
				}
			}

			public override string UniqueId
			{
				get
				{
					return this._uniqueid;
				}
			}

			public TempIdentity(string name, bool isanonymous, string uniqueid, string ipaddress)
			{
				if ((name == null ? true : name.Length == 0))
				{
					throw new ArgumentNullException("name");
				}
				if ((uniqueid == null ? true : uniqueid.Length == 0))
				{
					throw new ArgumentNullException("uniqueid");
				}
				this._name = name;
				this._isanony = isanonymous;
				this._uniqueid = uniqueid;
				this._ipaddress = ipaddress;
				if (this._ipaddress == null)
				{
					this._ipaddress = "";
				}
			}
		}
	}
}