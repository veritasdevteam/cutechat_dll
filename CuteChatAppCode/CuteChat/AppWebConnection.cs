using System;

namespace CuteChat
{
	public class AppWebConnection : AppBaseConnection
	{
		public AppWebConnection(ChatPortal portal, ChatIdentity identity) : base(portal, identity)
		{
		}
	}
}