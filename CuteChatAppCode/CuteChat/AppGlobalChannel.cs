using System;
using CuteChat;

namespace CuteChat
{
	public class AppGlobalChannel : ChatChannel
	{
		public override string Location
		{
			get
			{
				return "Unknown";
			}
		}

		public new AppManager Manager
		{
			get
			{
				return (AppManager)base.Manager;
			}
		}

		public override string PlaceTitle
		{
			get
			{
				return "GlobalChannel";
			}
		}

		public AppGlobalChannel(AppPortal portal, string name) : base(portal, name)
		{
		}

		protected override ChatManager CreateManagerInstance()
		{
			return new AppManager(this);
		}
	}
}