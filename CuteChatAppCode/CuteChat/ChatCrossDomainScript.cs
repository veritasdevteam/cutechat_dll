using System;
using System.Collections.Specialized;
using System.Web;
using System.Web.UI;

namespace CuteChat
{
	[ChatServerType]
	public class ChatCrossDomainScript : System.Web.UI.Page
	{
		protected string baseurl;

		protected string type;

		protected string status;

		protected string result;

		public ChatCrossDomainScript()
		{
		}

		public static string ExecuteRequest(string requestType, ChatIdentity identity, string customerid, string name, string ipaddr, string culture, string platform, string browser, string url, string referrer, string email, string department, string question)
		{
			ChatPortal chatPortal = null;
			string str;
			if (!ClusterSupport.IsClusterClient)
			{
				string str1 = null;
				string str2 = null;
				lock (ChatSystem.Instance.GetCurrentPortal())
				{
					SupportAgentChannel place = (SupportAgentChannel)chatPortal.GetPlace("SupportAgent");
					if (requestType == "VISIT")
					{
						str1 = place.HandleCustomerVisit(identity, name, ipaddr, culture, platform, browser, url, referrer, out str2);
					}
					if (requestType == "ACCEPT")
					{
						str1 = place.HandleCustomerAcceptInvite(customerid, out str2);
					}
					if (requestType == "REJECT")
					{
						str1 = place.HandleCustomerRejectInvite(customerid, out str2);
					}
				}
				str = string.Concat(str1, ":", str2);
			}
			else
			{
				str = (string)ClusterSupport.ExecuteCurrentMethod(new object[] { requestType, identity, customerid, name, ipaddr, culture, platform, browser, url, referrer, email, department, question });
			}
			return str;
		}

		protected override void OnInit(EventArgs args)
		{
			ChatUtility.CheckQueryString(this.Context);
			base.OnInit(args);
			HttpContext context = this.Context;
			this.baseurl = base.Request.Url.ToString();
			int num = this.baseurl.IndexOf("?");
			if (num != -1)
			{
				this.baseurl = this.baseurl.Substring(0, num);
			}
			this.baseurl = this.baseurl.Substring(0, this.baseurl.LastIndexOf("/") + 1);
			string str = ChatWebUtility.InitUniqueId();
			string item = context.Request.QueryString["Name"];
			string p = ChatWebUtility.GetIP(context);
			string browser = ChatWebUtility.GetBrowser();
			string platform = ChatWebUtility.GetPlatform();
			AppChatIdentity logonIdentity = ChatWebUtility.GetLogonIdentity();
			if ((item == null ? true : item == ""))
			{
				if (logonIdentity != null)
				{
					item = logonIdentity.DisplayName;
				}
				if ((item == null ? true : item == ""))
				{
					HttpCookie httpCookie = context.Request.Cookies["CCLSGN"];
					if (httpCookie != null)
					{
						item = httpCookie.Value;
					}
				}
				if ((item == null ? true : item == ""))
				{
					item = str;
					int num1 = item.IndexOf('-');
					if (num1 != -1)
					{
						item = item.Substring(0, num1);
					}
					item = item.Replace(':', '\u005F');
				}
			}
			HttpCookie httpCookie1 = new HttpCookie("CCLSGN", item);
			if (ChatWebUtility.GetConfig("EnableLongTimeCookies") != "False")
			{
				httpCookie1.Expires = DateTime.Now.AddYears(1);
			}
			context.Response.Cookies.Add(httpCookie1);
			if (logonIdentity == null)
			{
				logonIdentity = new AppChatIdentity(item, true, str, ChatWebUtility.GetIP(context));
			}
			string userLanguages = "";
			try
			{
				userLanguages = context.Request.UserLanguages[0] ?? "";
			}
			catch
			{
			}
			string item1 = context.Request.QueryString["Url"];
			if ((item1 == null ? true : item1 == ""))
			{
				Uri urlReferrer = context.Request.UrlReferrer;
				item1 = (urlReferrer == null ? "" : urlReferrer.AbsoluteUri);
			}
			string str1 = context.Request.QueryString["Referrer"] ?? "";
			string str2 = null;
			string str3 = null;
			string item2 = context.Request.QueryString["Type"];
			if ((item2 == null ? false : item2 != ""))
			{
				string str4 = null;
				string str5 = null;
				string str6 = null;
				string str7 = ChatCrossDomainScript.ExecuteRequest(item2, logonIdentity, str, item, p, userLanguages, platform, browser, item1, str1, str4, str5, str6);
				string[] strArrays = str7.Split(new char[] { ':' }, 2);
				str2 = strArrays[0];
				str3 = strArrays[1];
				this.type = item2;
				this.status = str2;
				this.result = str3;
			}
			if ((this.status == null ? true : this.status == ""))
			{
				string str8 = "LOADING";
				string str9 = str8;
				this.result = str8;
				this.status = str9;
			}
		}
	}
}