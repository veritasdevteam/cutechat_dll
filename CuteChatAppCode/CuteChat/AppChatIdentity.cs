using System;

namespace CuteChat
{
	[Serializable]
	public class AppChatIdentity : ChatIdentity
	{
		private string _name;

		private bool _isanony;

		private string _uniqueid;

		private string _ipaddress;

		public override string DisplayName
		{
			get
			{
				return this._name;
			}
		}

		public override string IPAddress
		{
			get
			{
				return this._ipaddress;
			}
		}

		public override bool IsAnonymous
		{
			get
			{
				return this._isanony;
			}
		}

		public override string UniqueId
		{
			get
			{
				return this._uniqueid;
			}
		}

		public AppChatIdentity(string name, bool isanonymous, string uniqueid, string ipaddress)
		{
			if ((name == null ? true : name.Length == 0))
			{
				throw new ArgumentNullException("name");
			}
			if ((uniqueid == null ? true : uniqueid.Length == 0))
			{
				throw new ArgumentNullException("uniqueid");
			}
			this._name = name;
			this._isanony = isanonymous;
			this._uniqueid = uniqueid;
			this._ipaddress = ipaddress;
			if (this._ipaddress == null)
			{
				this._ipaddress = "";
			}
		}
	}
}