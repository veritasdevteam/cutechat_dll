using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Reflection;
using System.Web;
using System.Xml;

namespace CuteChat
{
	public class SupportAgentChannel : ChatChannel
	{
		private Hashtable @ӓ = new Hashtable(new CaseInsensitiveHashCodeProvider(), new CaseInsensitiveComparer());

		public override bool AllowAnonymous
		{
			get
			{
				return false;
			}
			set
			{
			}
		}

		public override string Location
		{
			get
			{
				return "Support";
			}
		}

		public SupportAgentChannel(ChatPortal portal, string name) : base(portal, name)
		{
		}

		internal void @Ӗ(ChatIdentity u0040ӓ)
		{
			SupportCustomerItem item = (SupportCustomerItem)this.@ӓ[u0040ӓ.UniqueId];
			if (item == null)
			{
				return;
			}
			if (item.Status == "SERVICE")
			{
				item.SetActive();
			}
			if (item.Status == "VISIT")
			{
				item.Status = "SERVICE";
				base.Manager.UpdatePlaceItem(item);
			}
		}

		internal void @ӕ(ChatConnection u0040ӓ, string u0040Ӕ, string u0040ӕ, string u0040Ӗ)
		{
			IChatUserInfo userInfo = base.Portal.DataManager.GetUserInfo(u0040Ӕ);
			if (userInfo == null)
			{
				return;
			}
			userInfo.SetPublicProperty(u0040ӕ, u0040Ӗ);
			base.Portal.DataManager.UpdateUserInfo(userInfo);
			ChatPlaceUser chatPlaceUser = base.FindUser(u0040Ӕ);
			if (chatPlaceUser != null)
			{
				base.Manager.UpdatePlaceItem(chatPlaceUser);
			}
		}

		private string @Ӕ()
		{
			StringWriter stringWriter = new StringWriter();
			XmlWriter xmlTextWriter = new XmlTextWriter(stringWriter);
			xmlTextWriter.WriteStartElement("departments");
			SupportDepartment[] departments = base.Portal.DataManager.GetDepartments();
			for (int i = 0; i < (int)departments.Length; i++)
			{
				SupportDepartment supportDepartment = departments[i];
				xmlTextWriter.WriteStartElement("department");
				int departmentId = supportDepartment.DepartmentId;
				xmlTextWriter.WriteAttributeString("departmentid", departmentId.ToString());
				xmlTextWriter.WriteAttributeString("name", supportDepartment.Name);
				SupportAgent[] agents = supportDepartment.Agents;
				for (departmentId = 0; departmentId < (int)agents.Length; departmentId++)
				{
					SupportAgent supportAgent = agents[departmentId];
					IChatUserInfo userInfo = base.Portal.DataManager.GetUserInfo(supportAgent.UserId);
					xmlTextWriter.WriteStartElement("agent");
					xmlTextWriter.WriteAttributeString("userid", supportAgent.UserId);
					if (userInfo.DisplayName != null)
					{
						xmlTextWriter.WriteAttributeString("name", userInfo.DisplayName);
					}
					xmlTextWriter.WriteEndElement();
				}
				xmlTextWriter.WriteEndElement();
			}
			xmlTextWriter.WriteEndElement();
			return stringWriter.ToString();
		}

		public void AddFeedback(ChatIdentity logonUser, string name, string email, string title, string content)
		{
			SupportFeedback supportFeedback = new SupportFeedback()
			{
				FbTime = DateTime.Now,
				Name = name,
				Email = email,
				Title = title,
				Content = content
			};
			if (logonUser == null)
			{
				supportFeedback.DisplayName = name;
			}
			else
			{
				supportFeedback.CustomerId = logonUser.UniqueId;
				supportFeedback.DisplayName = logonUser.DisplayName;
			}
			base.Portal.DataManager.AddFeedback(supportFeedback);
			this.SendFeedbacks(supportFeedback, "FEEDBACK");
			Strings strings = ChatApi.Strings;
			string str = string.Format(strings.GetString("FEEDBACKEMAILTITLEFORMAT"), new object[] { name, email, title, content });
			string str1 = string.Format(strings.GetString("FEEDBACKEMAILCONTENTFORMAT"), new object[] { name, email, title, content });
			ChatSystem.Instance.SendMail(str, str1, ChatSystem.Instance.DefaultMailAddress);
		}

		protected internal override void AddItem(ChatPlaceItem item)
		{
			SupportCustomerItem supportCustomerItem = item as SupportCustomerItem;
			if (supportCustomerItem != null)
			{
				this.@ӓ.Add(supportCustomerItem.Identity.UniqueId, supportCustomerItem);
			}
			base.AddItem(item);
		}

		public void ChangeToOffline(string userid)
		{
			ChatPlaceUser chatPlaceUser = base.FindUser(userid);
			if (chatPlaceUser != null)
			{
				chatPlaceUser.OnlineStatus = "APPEAROFFLINE";
				chatPlaceUser.AppearOffline = true;
				base.Manager.UpdatePlaceItem(chatPlaceUser);
			}
		}

		protected override ChatManager CreateManagerInstance()
		{
			return new SupportChannelManager(this);
		}

		public void DeleteAgentSessions(ChatConnection conn, string[] args)
		{
			DateTime dateTime = DateTime.ParseExact(args[1], "yyyy-MM-dd", null);
			DateTime dateTime1 = DateTime.ParseExact(args[2], "yyyy-MM-dd", null);
			dateTime1 = dateTime1.AddDays(1);
			DateTime dateTime2 = dateTime1.AddMinutes(-1);
			string str = args[3];
			string str1 = args[4];
			string str2 = args[5];
			string str3 = args[6];
			string str4 = args[7];
			string str5 = args[8];
			base.Portal.DataManager.DeleteAgentSessions(dateTime, dateTime2, str, str1, str2, str3, str4, str5);
		}

		public void DeleteFeedback(ChatConnection conn, int feedbackid)
		{
			SupportFeedback supportFeedback = base.Portal.DataManager.LoadFeedback(feedbackid);
			base.Portal.DataManager.DeleteFeedback(feedbackid);
			if (supportFeedback == null)
			{
				return;
			}
			this.SendFeedbacks(supportFeedback, "FEEDBACKDELETE");
		}

		public string GetServerStatusXml()
		{
			return SupportAgentChannel.@ӓ.Dump(this);
		}

		public void HandleAgentAcceptCustomerWait(ChatConnection conn, string customerid)
		{
			SupportCustomerItem item = (SupportCustomerItem)this.@ӓ[customerid];
			if (item == null)
			{
				base.Manager.PushSTCMessage(conn, "LSAGENT_COMMAND", null, new string[] { "ACCEPTCUSTOMER", customerid, "EXPIRED" });
				return;
			}
			if (item.Status != "WAIT")
			{
				base.Manager.PushSTCMessage(conn, "LSAGENT_COMMAND", null, new string[] { "ACCEPTCUSTOMER", customerid, "INVALIDSTATUS", item.Status });
				return;
			}
			item.Agent = conn.Identity;
			item.Status = "GOSERVICE";
			base.Manager.UpdatePlaceItem(item);
			string msg = ChatUtility.JoinToMsg("LSAGENT_COMMAND", null, new string[] { "ACCEPTCUSTOMER", customerid, "ACCEPT" });
			ChatConnection[] allConnections = base.GetAllConnections();
			for (int i = 0; i < (int)allConnections.Length; i++)
			{
				ChatConnection chatConnection = allConnections[i];
				base.Manager.PushSTCMessage(chatConnection, msg);
			}
		}

		public void HandleAgentInviteCustomer(ChatConnection conn, string customerid)
		{
			SupportCustomerItem item = (SupportCustomerItem)this.@ӓ[customerid];
			if (item == null)
			{
				base.Manager.PushSTCMessage(conn, "LSAGENT_COMMAND", null, new string[] { "INVITECUSTOMER", customerid, "EXPIRED" });
				return;
			}
			if (item.Status != "VISIT")
			{
				base.Manager.PushSTCMessage(conn, "LSAGENT_COMMAND", null, new string[] { "INVITECUSTOMER", customerid, "INVALIDSTATUS", item.Status });
				return;
			}
			item.Agent = conn.Identity;
			item.Status = "INVITE";
			base.Manager.UpdatePlaceItem(item);
			string msg = ChatUtility.JoinToMsg("LSAGENT_COMMAND", null, new string[] { "INVITECUSTOMER", customerid, "INVITE" });
			ChatConnection[] allConnections = base.GetAllConnections();
			for (int i = 0; i < (int)allConnections.Length; i++)
			{
				ChatConnection chatConnection = allConnections[i];
				base.Manager.PushSTCMessage(chatConnection, msg);
			}
		}

		public string HandleCustomerAcceptInvite(string customerid, out string detail)
		{
			detail = null;
			SupportCustomerItem item = (SupportCustomerItem)this.@ӓ[customerid];
			if (item == null)
			{
				detail = "InvalidState";
				return "EXPIRED";
			}
			if (item.Status != "INVITE")
			{
				detail = "InvalidState";
				return "ERROR";
			}
			if (base.FindUser(item.Agent.UniqueId) == null)
			{
				detail = "AgentOffline";
				return "ERROR";
			}
			item.Status = "SERVICE";
			item.SetActive();
			base.Manager.UpdatePlaceItem(item);
			detail = string.Concat("SupportSession:", item.Identity.UniqueId);
			if (!base.Portal.IsPlaceStarted(detail))
			{
				SupportSessionChannel supportSessionChannel = new SupportSessionChannel(base.Portal, detail, item);
				base.Portal.AddPlace(supportSessionChannel);
			}
			string msg = ChatUtility.JoinToMsg("LSCUSTOMER_ACCEPTINVITE", null, new string[] { customerid });
			ChatConnection[] allConnections = base.GetAllConnections();
			for (int i = 0; i < (int)allConnections.Length; i++)
			{
				ChatConnection chatConnection = allConnections[i];
				base.Manager.PushSTCMessage(chatConnection, msg);
			}
			return "READY";
		}

		public string HandleCustomerRejectInvite(string customerid, out string detail)
		{
			detail = null;
			SupportCustomerItem item = (SupportCustomerItem)this.@ӓ[customerid];
			if (item == null)
			{
				detail = "InvalidState";
				return "EXPIRED";
			}
			if (item.Status != "INVITE")
			{
				detail = "InvalidState";
				return "ERROR";
			}
			if (base.FindUser(item.Agent.UniqueId) == null)
			{
				detail = "AgentOffline";
				return "ERROR";
			}
			string msg = ChatUtility.JoinToMsg("LSCUSTOMER_REJECTINVITE", null, new string[] { customerid });
			ChatConnection[] allConnections = base.GetAllConnections();
			for (int i = 0; i < (int)allConnections.Length; i++)
			{
				ChatConnection chatConnection = allConnections[i];
				base.Manager.PushSTCMessage(chatConnection, msg);
			}
			item.Status = "NOASK";
			item.SetActive();
			base.Manager.UpdatePlaceItem(item);
			return "READY";
		}

		public string HandleCustomerVisit(ChatIdentity customerid, string name, string ipaddr, string culture, string platform, string browser, string url, string referrer, out string detail)
		{
			detail = null;
			SupportCustomerItem item = (SupportCustomerItem)this.@ӓ[customerid.UniqueId];
			bool flag = false;
			if (item != null)
			{
				if (item.Status == "WAIT" || item.Status == "SERVICE" || item.Status == "GOSERVICE")
				{
					return "READY";
				}
				item.SetActive();
				if (item.Status == "INVITE")
				{
					ChatPlaceUser chatPlaceUser = base.FindUser(item.Agent.UniqueId);
					if (chatPlaceUser == null)
					{
						detail = "AgentNotOnline";
						return "ERROR";
					}
					detail = chatPlaceUser.DisplayName;
					return "INVITE";
				}
				if (item.Status != "NOASK")
				{
					item.Status = "VISIT";
				}
			}
			else
			{
				flag = true;
				item = new SupportCustomerItem(this, customerid)
				{
					Status = "VISIT"
				};
			}
			item.DisplayName = name;
			item.Culture = culture;
			item.Browser = browser;
			item.Platform = platform;
			item.IPAddress = ipaddr;
			item.Url = url;
			item.Referrer = referrer;
			if (!flag)
			{
				base.Manager.UpdatePlaceItem(item);
			}
			else
			{
				base.Manager.AddPlaceItem(item);
			}
			return "READY";
		}

		public string HandleCustomerWait(ChatIdentity customerid, string name, string ipaddr, string culture, string platform, string browser, string url, string referrer, string email, string department, string question, string customdata, out string detail)
		{
			detail = null;
			bool flag = false;
			ChatPlaceUser[] allUsers = base.GetAllUsers();
			int i = 0;
			while (i < (int)allUsers.Length)
			{
				if (allUsers[i].AppearOffline)
				{
					i++;
				}
				else
				{
					flag = true;
					break;
				}
			}
			if (!flag)
			{
				return "NOAGENT";
			}
			SupportCustomerItem item = (SupportCustomerItem)this.@ӓ[customerid.UniqueId];
			bool flag1 = false;
			if (item != null)
			{
				if (item.Status == "GOSERVICE")
				{
					item.SetActive();
					item.Status = "SERVICE";
					base.Manager.UpdatePlaceItem(item);
					string str = string.Concat("SupportSession:", item.Identity.UniqueId);
					if (!base.Portal.IsPlaceStarted(str))
					{
						SupportSessionChannel supportSessionChannel = new SupportSessionChannel(base.Portal, str, item);
						base.Portal.AddPlace(supportSessionChannel);
					}
					string msg = ChatUtility.JoinToMsg("LSAGENT_COMMAND", null, new string[] { "ACCEPTCUSTOMER", item.Identity.UniqueId, "REDIRECT" });
					ChatConnection[] allConnections = base.GetAllConnections();
					for (i = 0; i < (int)allConnections.Length; i++)
					{
						ChatConnection chatConnection = allConnections[i];
						base.Manager.PushSTCMessage(chatConnection, msg);
					}
					detail = string.Concat(str, "&", item.Agent.DisplayName);
					return "READY";
				}
				item.SetActive();
				if (item.Status == "NOASK")
				{
					item.BeginTime = DateTime.Now;
				}
				if (item.Status == "VISIT")
				{
					item.BeginTime = DateTime.Now;
				}
				if (item.Status == "INVITE")
				{
					item.BeginTime = DateTime.Now;
				}
				if (item.Status == "SERVICE")
				{
					item.BeginTime = DateTime.Now;
				}
				item.Status = "WAIT";
			}
			else
			{
				flag1 = true;
				item = new SupportCustomerItem(this, customerid)
				{
					BeginTime = DateTime.Now,
					Status = "WAIT"
				};
			}
			item.DisplayName = name;
			item.Culture = culture;
			item.Browser = browser;
			item.Platform = platform;
			item.IPAddress = ipaddr;
			item.Url = url;
			item.Referrer = referrer;
			item.Email = email;
			item.Question = question;
			item.Department = department;
			item.CustomData = customdata;
			if (!flag1)
			{
				base.Manager.UpdatePlaceItem(item);
			}
			else
			{
				base.Manager.AddPlaceItem(item);
			}
			int num = 1;
			foreach (SupportCustomerItem value in this.@ӓ.Values)
			{
				if (!(value.Status == "WAIT") || !(value.BeginTime < item.BeginTime))
				{
					continue;
				}
				num++;
			}
			detail = num.ToString();
			return "WAITING";
		}

		public void HandleGoSession(ChatConnection conn, bool transfer, string customerid, string agentid)
		{
			SupportCustomerItem item = (SupportCustomerItem)this.@ӓ[customerid];
			if (item == null)
			{
				base.Manager.PushSTCMessage(conn, "LSAGENT_COMMAND", null, new string[] { "GOSESSION", customerid, "NOCUSTOMER" });
				return;
			}
			ChatPlaceUser chatPlaceUser = base.FindUser(agentid);
			if (chatPlaceUser == null)
			{
				base.Manager.PushSTCMessage(conn, "LSAGENT_COMMAND", null, new string[] { "GOSESSION", customerid, "NOAGENT" });
				return;
			}
			SupportGoSessionItem supportGoSessionItem = new SupportGoSessionItem(this, conn.PlaceUser, item.Identity, chatPlaceUser, transfer);
			base.Manager.AddPlaceItem(supportGoSessionItem);
			string[] displayName = new string[] { "GOSESSION", customerid, agentid, null, null };
			displayName[3] = (transfer ? "TRANSFER" : "INVITE");
			displayName[4] = conn.PlaceUser.DisplayName;
			string msg = ChatUtility.JoinToMsg("LSAGENT_COMMAND", null, displayName);
			ChatConnection[] allConnections = base.GetAllConnections();
			for (int i = 0; i < (int)allConnections.Length; i++)
			{
				ChatConnection chatConnection = allConnections[i];
				base.Manager.PushSTCMessage(chatConnection, msg);
			}
		}

		public void HandleGoSession2(ChatConnection conn, string guid, bool accept)
		{
			SupportGoSessionItem supportGoSessionItem = base.FindItem(new ChatGuid(guid)) as SupportGoSessionItem;
			if (supportGoSessionItem == null)
			{
				base.Manager.PushSTCMessage(conn, "LSAGENT_COMMAND", null, new string[] { "GOSESSION2", "EXPIRED" });
				return;
			}
			SupportCustomerItem item = (SupportCustomerItem)this.@ӓ[supportGoSessionItem.CustomerId.UniqueId];
			if (item == null)
			{
				base.Manager.PushSTCMessage(conn, "LSAGENT_COMMAND", null, new string[] { "GOSESSION2", "EXPIRED" });
				return;
			}
			if (accept && supportGoSessionItem.Transfer)
			{
				SupportSessionChannel place = (SupportSessionChannel)base.Portal.GetPlace(string.Concat("SupportSession:", item.Identity.UniqueId));
				item.Agent = conn.Identity;
				base.Manager.UpdatePlaceItem(item);
				place.SetOwnerAgent(conn.Identity);
			}
			base.Manager.RemovePlaceItem(supportGoSessionItem, (accept ? "ACCEPT" : "REJECT"));
		}

		public void LoadAgentSessions(ChatConnection conn, string agentid)
		{
			base.Manager.PushSTCMessage(conn, "AGENTSESSIONSTART", null, new string[0]);
			SupportSession[] supportSessionArray = base.Portal.DataManager.LoadAgentSessions(agentid);
			for (int i = 0; i < (int)supportSessionArray.Length; i++)
			{
				SupportSession supportSession = supportSessionArray[i];
				ChatManager manager = base.Manager;
				string[] str = new string[16];
				str[0] = supportSession.SessionId.ToString();
				DateTime universalTime = supportSession.BeginTime.ToUniversalTime();
				long ticks = universalTime.Ticks;
				str[1] = ticks.ToString();
				str[2] = supportSession.DepartmentId.ToString();
				str[3] = supportSession.AgentUserId;
				str[4] = supportSession.CustomerId;
				str[5] = supportSession.DisplayName;
				universalTime = supportSession.ActiveTime.ToUniversalTime();
				ticks = universalTime.Ticks;
				str[6] = ticks.ToString();
				str[7] = supportSession.Email;
				str[8] = supportSession.IPAddress;
				str[9] = supportSession.Culture;
				str[10] = supportSession.Platform;
				str[11] = supportSession.Browser;
				str[12] = supportSession.AgentRating.ToString();
				str[13] = supportSession.SessionData;
				str[14] = supportSession.Url;
				str[15] = supportSession.Referrer;
				manager.PushSTCMessage(conn, "AGENTSESSION", null, str);
			}
			base.Manager.PushSTCMessage(conn, "AGENTSESSIONEND", null, new string[0]);
		}

		public void LoadCustomerData(ChatConnection conn, string userid)
		{
			string customerData = base.Portal.DataManager.GetCustomerData(userid);
			base.Manager.PushSTCMessage(conn, "LSAGENT_COMMAND", null, new string[] { "CUSTOMERDATA", userid, customerData });
		}

		public void LoadCustomerSessions(ChatConnection conn, string customerid)
		{
			base.Manager.PushSTCMessage(conn, "CUSTOMERSESSIONSTART", null, new string[0]);
			SupportSession[] supportSessionArray = base.Portal.DataManager.LoadCustomerSessions(customerid);
			for (int i = 0; i < (int)supportSessionArray.Length; i++)
			{
				SupportSession supportSession = supportSessionArray[i];
				ChatManager manager = base.Manager;
				string[] str = new string[16];
				str[0] = supportSession.SessionId.ToString();
				DateTime universalTime = supportSession.BeginTime.ToUniversalTime();
				long ticks = universalTime.Ticks;
				str[1] = ticks.ToString();
				str[2] = supportSession.DepartmentId.ToString();
				str[3] = supportSession.AgentUserId;
				str[4] = supportSession.CustomerId;
				str[5] = supportSession.DisplayName;
				universalTime = supportSession.ActiveTime.ToUniversalTime();
				ticks = universalTime.Ticks;
				str[6] = ticks.ToString();
				str[7] = supportSession.Email;
				str[8] = supportSession.IPAddress;
				str[9] = supportSession.Culture;
				str[10] = supportSession.Platform;
				str[11] = supportSession.Browser;
				str[12] = supportSession.AgentRating.ToString();
				str[13] = supportSession.SessionData;
				str[14] = supportSession.Url;
				str[15] = supportSession.Referrer;
				manager.PushSTCMessage(conn, "CUSTOMERSESSION", null, str);
			}
			base.Manager.PushSTCMessage(conn, "CUSTOMERSESSIONEND", null, new string[0]);
		}

		public void LoadServerStatus(ChatConnection conn)
		{
			base.Manager.PushSTCMessage(conn, "SERVERSTATUS", null, new string[] { this.GetServerStatusXml() });
		}

		public void LoadSessionData(ChatConnection conn, string sessionid)
		{
			string sessionData = base.Portal.DataManager.GetSessionData(sessionid);
			base.Manager.PushSTCMessage(conn, "LSAGENT_COMMAND", null, new string[] { "SESSIONDATA", sessionid, sessionData });
		}

		public void LoadSessionMessages(ChatConnection conn, int sessionid)
		{
			base.Manager.PushSTCMessage(conn, "SESSIONMESSAGESTART", null, new string[0]);
			ChatMsgData[] chatMsgDataArray = base.Portal.DataManager.LoadSessionMessages(sessionid);
			for (int i = 0; i < (int)chatMsgDataArray.Length; i++)
			{
				ChatMsgData chatMsgDatum = chatMsgDataArray[i];
				ChatManager manager = base.Manager;
				string[] senderId = new string[] { "", chatMsgDatum.SenderId, chatMsgDatum.Sender, chatMsgDatum.Text, chatMsgDatum.Html, chatMsgDatum.TargetId, chatMsgDatum.Target, "0", null };
				DateTime universalTime = chatMsgDatum.Time.ToUniversalTime();
				senderId[8] = universalTime.Ticks.ToString();
				manager.PushSTCMessage(conn, "SESSIONMESSAGE", null, senderId);
			}
			base.Manager.PushSTCMessage(conn, "SESSIONMESSAGEEND", null, new string[0]);
		}

		public void LoadSetting(ChatConnection conn, string name)
		{
			if (!name.StartsWith("LS"))
			{
				throw new Exception("Invalid Setting");
			}
			string str = base.Portal.DataManager.LoadSetting(name);
			base.Manager.PushSTCMessage(conn, "LOADSETTING", null, new string[] { name, str });
		}

		protected internal override void Maintain()
		{
			base.Maintain();
		}

		public override bool PreProcessCTSMessage(ChatConnection conn, string msgid, string[] args, NameValueCollection nvc)
		{
			if (msgid == "LSAGENT_COMMAND")
			{
				string str = args[0];
				if (str == "INVITETOSESSION")
				{
					this.HandleGoSession(conn, false, args[1], args[2]);
					return true;
				}
				if (str == "TRANSFERTOSESSION")
				{
					this.HandleGoSession(conn, true, args[1], args[2]);
					return true;
				}
				if (str == "ANSWERGOSESSION")
				{
					this.HandleGoSession2(conn, args[1], args[2] == "1");
					return true;
				}
				if (str == "INVITECUSTOMER")
				{
					this.HandleAgentInviteCustomer(conn, args[1]);
					return true;
				}
				if (str == "ACCEPTCUSTOMER")
				{
					this.HandleAgentAcceptCustomerWait(conn, args[1]);
					return true;
				}
				if (str == "DEPARTMENT_CREATE")
				{
					base.Portal.DataManager.AddDepartment(args[1]);
					return true;
				}
				if (str == "DEPARTMENT_DELETE")
				{
					base.Portal.DataManager.RemoveDepartment(args[1]);
					return true;
				}
				if (str == "DEPARTMENT_RENAME")
				{
					base.Portal.DataManager.RenameDepartment(args[1], args[2]);
					return true;
				}
				if (str == "DEPARTMENT_ADDAGENT")
				{
					string str1 = base.Portal.DataManager.IsMemberDisplayName(args[2]);
					if (str1 == null)
					{
						base.Manager.PushSTCMessage(conn, "COMMAND_RETURN", null, new string[] { "MESSAGE_ADDAGENT_NOUSER", args[2] });
					}
					else
					{
						base.Portal.DataManager.AddAgent(args[1], str1);
					}
					return true;
				}
				if (str == "DEPARTMENT_REMOVEAGENT")
				{
					base.Portal.DataManager.RemoveAgent(args[1], args[2]);
					return true;
				}
				if (str == "CHANGETOOFFLINE")
				{
					this.ChangeToOffline(args[1]);
					return true;
				}
				if (str == "FEEDBACK_HISTORY")
				{
					this.SendFeedbacks(conn, true);
					return true;
				}
				if (str == "FEEDBACK_COMMENT")
				{
					this.SetFeedbackComment(conn, int.Parse(args[1]), args[2]);
					return true;
				}
				if (str == "FEEDBACK_DELETE")
				{
					this.DeleteFeedback(conn, int.Parse(args[1]));
					return true;
				}
				if (str == "LOADCUSTOMERSESSIONS")
				{
					this.LoadCustomerSessions(conn, args[1]);
					return true;
				}
				if (str == "LOADAGENTSESSIONS")
				{
					this.LoadAgentSessions(conn, args[1]);
					return true;
				}
				if (str == "DELETEAGENTSESSIONS")
				{
					this.DeleteAgentSessions(conn, args);
					return true;
				}
				if (str == "SEARCHAGENTSESSIONS")
				{
					this.SearchAgentSessions(conn, args);
					return true;
				}
				if (str == "LOADSESSIONMESSAGES")
				{
					this.LoadSessionMessages(conn, int.Parse(args[1]));
					return true;
				}
				if (str == "LOADSETTING")
				{
					this.LoadSetting(conn, args[1]);
					return true;
				}
				if (str == "SAVESETTING")
				{
					this.SaveSetting(conn, args[1], args[2]);
					return true;
				}
				if (str == "SENDFILE")
				{
					this.SaveFile(conn, args[1], args[2]);
					return true;
				}
				if (str == "SENDMAIL")
				{
					this.SendMail(conn, args[1], args[2], args[3], args[4]);
					return true;
				}
				if (str == "LOADCUSTOMERDATA")
				{
					this.LoadCustomerData(conn, args[1]);
					return true;
				}
				if (str == "LOADSESSIONDATA")
				{
					this.LoadSessionData(conn, args[1]);
					return true;
				}
				if (str == "SAVECUSTOMERDATA")
				{
					this.SaveCustomerData(conn, args[1], args[2]);
					return true;
				}
				if (str == "SAVESESSIONDATA")
				{
					this.SaveSessionData(conn, args[1], args[2]);
					return true;
				}
			}
			if (msgid == "LSADMIN_COMMAND")
			{
				if (!conn.PlaceUser.IsModerator)
				{
					return false;
				}
				string str2 = args[0];
				if (str2 == "SENDANNOUNCEMENT")
				{
					this.SendAnnouncement(conn, args[1], args[2]);
					return true;
				}
				if (str2 == "LOADSERVERSTATUS")
				{
					this.LoadServerStatus(conn);
					return true;
				}
				if (str2 == "SETAGENTPUBLICPROPERTY")
				{
					this.@ӕ(conn, args[1], args[2], args[3]);
					return true;
				}
			}
			return base.PreProcessCTSMessage(conn, msgid, args, nvc);
		}

		protected internal override void RemoveItem(ChatPlaceItem item)
		{
			SupportCustomerItem supportCustomerItem = item as SupportCustomerItem;
			if (supportCustomerItem != null)
			{
				this.@ӓ.Remove(supportCustomerItem.Identity.UniqueId);
			}
			base.RemoveItem(item);
		}

		public void SaveCustomerData(ChatConnection conn, string userid, string data)
		{
			base.Portal.DataManager.SetCutomerData(userid, data);
			base.Manager.PushSTCMessage(conn, "LSAGENT_COMMAND", null, new string[] { "CUSTOMERDATA", userid, data });
		}

		public void SaveFile(ChatConnection conn, string filename, string base64data)
		{
			byte[] numArray = Convert.FromBase64String(base64data);
			string mimeType = @Հ.GetMimeType(filename);
			string str = DateTime.Now.ToString("yyyy-MM-dd");
			string str1 = Guid.NewGuid().ToString();
			string str2 = string.Concat(new string[] { "CuteChatSendFiles/", str, "/", str1, ".licx" });
			string str3 = Path.Combine(HttpRuntime.AppDomainAppPath, str2);
			string directoryName = Path.GetDirectoryName(str3);
			if (!Directory.Exists(directoryName))
			{
				Directory.CreateDirectory(directoryName);
			}
			using (FileStream fileStream = new FileStream(str3, FileMode.Create, FileAccess.Write))
			{
				fileStream.Write(numArray, 0, (int)numArray.Length);
			}
			int length = (int)numArray.Length / 1000;
			string str4 = string.Concat(filename, "(", length.ToString("###,##0"), ")");
			string str5 = string.Concat(new string[] { "DownloadFile.ashx?date=", str, "&guid=", str1, "&filename=", HttpUtility.UrlEncode(filename), "&mime=", mimeType });
			string[] strArrays = new string[] { "<a target='_blank' href='", HttpUtility.HtmlEncode(str5), "'>", HttpUtility.HtmlEncode(filename), "(", null, null };
			length = (int)numArray.Length / 1000;
			strArrays[5] = length.ToString("###,##0");
			strArrays[6] = "KB)</a>";
			string str6 = string.Concat(strArrays);
			base.Manager.HandleCTSUserMessage(conn, null, new string[] { str4, str6, null, null, null });
		}

		public void SaveSessionData(ChatConnection conn, string sessionid, string data)
		{
			base.Portal.DataManager.SetSessionData(sessionid, data);
			base.Manager.PushSTCMessage(conn, "LSAGENT_COMMAND", null, new string[] { "SESSIONDATA", sessionid, data });
		}

		public void SaveSetting(ChatConnection conn, string name, string data)
		{
			if (!name.StartsWith("LS"))
			{
				throw new Exception("Invalid Setting");
			}
			base.Portal.DataManager.SaveSetting(name, data);
			base.Manager.PushSTCMessage(conn, "SAVESETTING", null, new string[] { name, data });
		}

		public void SearchAgentSessions(ChatConnection conn, string[] args)
		{
			DateTime dateTime = DateTime.ParseExact(args[1], "yyyy-MM-dd", null);
			DateTime universalTime = DateTime.ParseExact(args[2], "yyyy-MM-dd", null);
			universalTime = universalTime.AddDays(1);
			DateTime dateTime1 = universalTime.AddMinutes(-1);
			string str = args[3];
			string str1 = args[4];
			string str2 = args[5];
			string str3 = args[6];
			string str4 = args[7];
			string str5 = args[8];
			base.Manager.PushSTCMessage(conn, "AGENTSESSIONSTART", null, new string[0]);
			SupportSession[] supportSessionArray = base.Portal.DataManager.SearchAgentSessions(dateTime, dateTime1, str, str1, str2, str3, str4, str5);
			for (int i = 0; i < (int)supportSessionArray.Length; i++)
			{
				SupportSession supportSession = supportSessionArray[i];
				ChatManager manager = base.Manager;
				string[] agentUserId = new string[16];
				agentUserId[0] = supportSession.SessionId.ToString();
				universalTime = supportSession.BeginTime.ToUniversalTime();
				long ticks = universalTime.Ticks;
				agentUserId[1] = ticks.ToString();
				agentUserId[2] = supportSession.DepartmentId.ToString();
				agentUserId[3] = supportSession.AgentUserId;
				agentUserId[4] = supportSession.CustomerId;
				agentUserId[5] = supportSession.DisplayName;
				universalTime = supportSession.ActiveTime.ToUniversalTime();
				ticks = universalTime.Ticks;
				agentUserId[6] = ticks.ToString();
				agentUserId[7] = supportSession.Email;
				agentUserId[8] = supportSession.IPAddress;
				agentUserId[9] = supportSession.Culture;
				agentUserId[10] = supportSession.Platform;
				agentUserId[11] = supportSession.Browser;
				agentUserId[12] = supportSession.AgentRating.ToString();
				agentUserId[13] = supportSession.SessionData;
				agentUserId[14] = supportSession.Url;
				agentUserId[15] = supportSession.Referrer;
				manager.PushSTCMessage(conn, "AGENTSESSION", null, agentUserId);
			}
			base.Manager.PushSTCMessage(conn, "AGENTSESSIONEND", null, new string[0]);
		}

		public void SendAnnouncement(ChatConnection conn, string text, string html)
		{
			int i;
			ChatConnection[] allConnections;
			string msg = ChatUtility.JoinToMsg("SYS_ANNOUNCEMENT", null, new string[] { text, html });
			ChatChannel[] startedChannels = base.Portal.GetStartedChannels();
			for (i = 0; i < (int)startedChannels.Length; i++)
			{
				SupportSessionChannel supportSessionChannel = startedChannels[i] as SupportSessionChannel;
				if (supportSessionChannel != null)
				{
					allConnections = supportSessionChannel.GetAllConnections();
					for (int j = 0; j < (int)allConnections.Length; j++)
					{
						ChatConnection chatConnection = allConnections[j];
						supportSessionChannel.Manager.PushSTCMessage(chatConnection, msg);
					}
				}
			}
			allConnections = base.GetAllConnections();
			for (i = 0; i < (int)allConnections.Length; i++)
			{
				ChatConnection chatConnection1 = allConnections[i];
				base.Manager.PushSTCMessage(chatConnection1, msg);
			}
		}

		public void SendDepartmentTree(ChatConnection conn)
		{
			base.Manager.PushSTCMessage(conn, "LSDEPARTMENTTREE", null, new string[] { this.@Ӕ() });
		}

		public void SendDepartmentTree()
		{
			string str = this.@Ӕ();
			ChatConnection[] allConnections = base.GetAllConnections();
			for (int i = 0; i < (int)allConnections.Length; i++)
			{
				ChatConnection chatConnection = allConnections[i];
				base.Manager.PushSTCMessage(chatConnection, "LSDEPARTMENTTREE", null, new string[] { str });
			}
		}

		public void SendFeedbacks(SupportFeedback feedback, string msgid)
		{
			string[] str = new string[] { feedback.FeedbackId.ToString(), null, null, null, null, null, null, null, null, null };
			DateTime universalTime = feedback.FbTime.ToUniversalTime();
			str[1] = universalTime.Ticks.ToString();
			str[2] = feedback.CustomerId;
			str[3] = feedback.Name;
			str[4] = feedback.DisplayName;
			str[5] = feedback.Email;
			str[6] = feedback.Title;
			str[7] = feedback.Content;
			str[8] = feedback.Comment;
			str[9] = feedback.CommentBy;
			string msg = ChatUtility.JoinToMsg(msgid, null, str);
			ChatConnection[] allConnections = base.GetAllConnections();
			for (int i = 0; i < (int)allConnections.Length; i++)
			{
				ChatConnection chatConnection = allConnections[i];
				base.Manager.PushSTCMessage(chatConnection, msg);
			}
		}

		public void SendFeedbacks(ChatConnection conn, bool history)
		{
			string str = "FEEDBACK";
			if (history)
			{
				str = "FEEDBACKHISTORY";
				base.Manager.PushSTCMessage(conn, "FEEDBACKHISTORYSTART", null, new string[0]);
			}
			SupportFeedback[] supportFeedbackArray = base.Portal.DataManager.LoadFeedbacks(history);
			for (int i = 0; i < (int)supportFeedbackArray.Length; i++)
			{
				SupportFeedback supportFeedback = supportFeedbackArray[i];
				string[] customerId = new string[] { supportFeedback.FeedbackId.ToString(), null, null, null, null, null, null, null, null, null };
				DateTime universalTime = supportFeedback.FbTime.ToUniversalTime();
				customerId[1] = universalTime.Ticks.ToString();
				customerId[2] = supportFeedback.CustomerId;
				customerId[3] = supportFeedback.Name;
				customerId[4] = supportFeedback.DisplayName;
				customerId[5] = supportFeedback.Email;
				customerId[6] = supportFeedback.Title;
				customerId[7] = supportFeedback.Content;
				customerId[8] = supportFeedback.Comment;
				customerId[9] = supportFeedback.CommentBy;
				string msg = ChatUtility.JoinToMsg(str, null, customerId);
				base.Manager.PushSTCMessage(conn, msg);
			}
			if (history)
			{
				base.Manager.PushSTCMessage(conn, "FEEDBACKHISTORYEND", null, new string[0]);
			}
		}

		protected internal override void SendInitializeMessages(ChatConnection conn)
		{
			base.SendInitializeMessages(conn);
			Hashtable hashtables = base.Portal.Info.LoadProperties();
			foreach (string key in hashtables.Keys)
			{
				if (!key.StartsWith("LS_"))
				{
					continue;
				}
				string item = hashtables[key] as string;
				if (item == null)
				{
					continue;
				}
				base.Manager.PushSTCMessage(conn, "LS_PROPERTY", null, new string[] { key, item });
			}
			this.SendDepartmentTree(conn);
			this.SendFeedbacks(conn, false);
		}

		public void SendMail(ChatConnection conn, string from, string to, string subject, string content)
		{
			ChatSystem.Instance.SendMail(subject, content, to);
		}

		public void SetFeedbackComment(ChatConnection conn, int feedbackid, string comment)
		{
			SupportFeedback displayName = base.Portal.DataManager.LoadFeedback(feedbackid);
			if (displayName != null)
			{
				displayName.Comment = comment;
				displayName.CommentBy = conn.Identity.DisplayName;
				base.Portal.DataManager.UpdateFeedback(displayName);
				this.SendFeedbacks(displayName, "FEEDBACKUPDATE");
			}
		}

		protected internal override void ValidateIdentity(ChatIdentity identity)
		{
			base.ValidateIdentity(identity);
			if (base.FindUser(identity.UniqueId) != null)
			{
				return;
			}
			if (!base.Portal.DataManager.IsAgent(identity.UniqueId))
			{
				throw new Exception("Require Agent");
			}
		}

		private class @ӓ
		{
			public @ӓ()
			{
			}

			private static void @ӓ(XmlTextWriter u0040ӓ, ChatPlace u0040Ӕ)
			{
				u0040ӓ.WriteAttributeString("class", u0040Ӕ.GetType().Name);
				u0040ӓ.WriteAttributeString("guid", u0040Ӕ.ObjectGuid);
				u0040ӓ.WriteAttributeString("name", u0040Ӕ.PlaceName);
				u0040ӓ.WriteAttributeString("title", u0040Ӕ.PlaceTitle);
				u0040Ӕ.Dump(u0040ӓ);
				ChatConnection[] allConnections = u0040Ӕ.GetAllConnections();
				int length = (int)allConnections.Length;
				u0040ӓ.WriteAttributeString("connections", length.ToString());
				ChatPlaceItem[] allItems = u0040Ӕ.GetAllItems();
				u0040ӓ.WriteStartElement("connections");
				ChatConnection[] chatConnectionArray = allConnections;
				for (length = 0; length < (int)chatConnectionArray.Length; length++)
				{
					SupportAgentChannel.@ӓ.@Ӕ(u0040ӓ, chatConnectionArray[length]);
				}
				u0040ӓ.WriteEndElement();
				u0040ӓ.WriteStartElement("users");
				ChatPlaceItem[] chatPlaceItemArray = allItems;
				for (length = 0; length < (int)chatPlaceItemArray.Length; length++)
				{
					ChatPlaceItem chatPlaceItem = chatPlaceItemArray[length];
					if (chatPlaceItem is ChatPlaceUser)
					{
						SupportAgentChannel.@ӓ.@ӕ(u0040ӓ, chatPlaceItem);
					}
				}
				u0040ӓ.WriteEndElement();
			}

			private static void @ӕ(XmlTextWriter u0040ӓ, ChatPlaceItem u0040Ӕ)
			{
				u0040ӓ.WriteStartElement(u0040Ӕ.GetType().Name);
				u0040ӓ.WriteAttributeString("guid", u0040Ӕ.ObjectGuid);
				u0040ӓ.WriteAttributeString("time", u0040Ӕ.ObjectTime.ToString("HH:mm:ss"));
				u0040Ӕ.Dump(u0040ӓ);
				u0040ӓ.WriteEndElement();
			}

			private static void @Ӕ(XmlTextWriter u0040ӓ, ChatConnection u0040Ӕ)
			{
				u0040ӓ.WriteStartElement("connection");
				u0040ӓ.WriteAttributeString("guid", u0040Ӕ.ObjectGuid);
				u0040ӓ.WriteAttributeString("name", u0040Ӕ.Identity.DisplayName);
				u0040ӓ.WriteAttributeString("login", (u0040Ӕ.Identity.IsAnonymous ? "0" : "1"));
				u0040ӓ.WriteAttributeString("id", u0040Ӕ.Identity.UniqueId);
				u0040ӓ.WriteAttributeString("ip", u0040Ӕ.Identity.IPAddress);
				DateTime objectTime = u0040Ӕ.ObjectTime;
				u0040ӓ.WriteAttributeString("start", objectTime.ToString("yyyy-MM-dd HH:mm:ss"));
				objectTime = u0040Ӕ.ActivateTime;
				u0040ӓ.WriteAttributeString("active", objectTime.ToString("yyyy-MM-dd HH:mm:ss"));
				u0040ӓ.WriteEndElement();
			}

			public static string Dump(SupportAgentChannel agentchannel)
			{
				StringWriter stringWriter = new StringWriter();
				XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter)
				{
					Indentation = 1,
					IndentChar = '\t',
					Formatting = Formatting.Indented
				};
				xmlTextWriter.WriteStartDocument();
				xmlTextWriter.WriteStartElement("portal");
				xmlTextWriter.WriteAttributeString("name", agentchannel.Portal.Name);
				ChatConnection[] allConnections = agentchannel.Portal.GetAllConnections();
				ChatChannel[] startedChannels = agentchannel.Portal.GetStartedChannels();
				int length = (int)allConnections.Length;
				xmlTextWriter.WriteAttributeString("connections", length.ToString());
				length = (int)startedChannels.Length;
				xmlTextWriter.WriteAttributeString("channels", length.ToString());
				xmlTextWriter.WriteStartElement("place");
				SupportAgentChannel.@ӓ.@ӓ(xmlTextWriter, agentchannel);
				xmlTextWriter.WriteStartElement("customers");
				foreach (SupportCustomerItem value in agentchannel.@ӓ.Values)
				{
					xmlTextWriter.WriteStartElement("customer");
					value.Dump(xmlTextWriter);
					xmlTextWriter.WriteEndElement();
				}
				xmlTextWriter.WriteEndElement();
				xmlTextWriter.WriteEndElement();
				ChatChannel[] chatChannelArray = startedChannels;
				for (length = 0; length < (int)chatChannelArray.Length; length++)
				{
					ChatChannel chatChannel = chatChannelArray[length];
					if (chatChannel is SupportSessionChannel)
					{
						xmlTextWriter.WriteStartElement("place");
						SupportAgentChannel.@ӓ.@ӓ(xmlTextWriter, chatChannel);
						xmlTextWriter.WriteEndElement();
					}
				}
				xmlTextWriter.WriteEndElement();
				xmlTextWriter.Flush();
				return stringWriter.ToString();
			}
		}
	}
}