using System;

namespace CuteChat
{
	[Flags]
	public enum EnumChatLicense
	{
		CuteChat,
		CuteMessenger,
		CuteLiveSupport
	}
}