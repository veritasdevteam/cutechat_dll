using System;
using System.Text;

namespace CuteChat
{
	[Serializable]
	public class LobbyInfo
	{
		private IChatLobby _lobby;

		private string _clients;

		private int _clientcount;

		public int ClientCount
		{
			get
			{
				return this._clientcount;
			}
		}

		public IChatLobby Lobby
		{
			get
			{
				return this._lobby;
			}
		}

		public int LobbyId
		{
			get
			{
				return this.Lobby.LobbyId;
			}
		}

		public string Name
		{
			get
			{
				return this.Lobby.Title;
			}
		}

		public LobbyInfo()
		{
			throw new NotSupportedException();
		}

		public LobbyInfo(IChatLobby lobby)
		{
			this._lobby = lobby;
		}

		public string JoinClient(string spliter)
		{
			return string.Join(spliter, this._clients.Split(new char[] { '|' }));
		}

		internal void LoadPortalData(ChatPortal portal)
		{
			lock (0)
			{
				if (portal.IsPlaceStarted(string.Concat("Lobby-", this.Lobby.LobbyId)))
				{
					ChatPlace place = portal.GetPlace(string.Concat("Lobby-", this.Lobby.LobbyId));
					StringBuilder stringBuilder = new StringBuilder();
					ChatPlaceUser[] allUsers = place.GetAllUsers();
					for (int i = 0; i < (int)allUsers.Length; i++)
					{
						ChatPlaceUser chatPlaceUser = allUsers[i];
						if (!chatPlaceUser.AppearOffline && !(chatPlaceUser.OnlineStatus == "OFFLINE"))
						{
							this._clientcount++;
							if (stringBuilder.Length != 0)
							{
								stringBuilder.Append("|");
							}
							stringBuilder.Append(chatPlaceUser.DisplayName);
						}
					}
					this._clients = stringBuilder.ToString();
				}
				else
				{
					this._clients = "";
				}
			}
		}
	}
}