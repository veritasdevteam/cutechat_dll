using System;

namespace CuteChat
{
	public class EnumConfigNames
	{
		public const string FloodControlCount = "FloodControlCount";

		public const string FloodControlDelay = "FloodControlDelay";

		public const string MaxMSGLength = "MaxMSGLength";

		public const string MaxWordLength = "MaxWordLength";

		public const string ChannelOnlineExpires = "ChannelOnlineExpires";

		public const string ColorsArray = "ColorsArray";

		public const string EmotionsArray = "EmotionsArray";

		public const string DisplayLobbyList = "DisplayLobbyList";

		public const string DisplayHeaderPanel = "DisplayHeaderPanel";

		public const string DisplayLogo = "DisplayLogo";

		public const string DisplayTopBanner = "DisplayTopBanner";

		public const string DisplayBottomBanner = "DisplayBottomBanner";

		public const string DisableWhisper = "DisableWhisper";

		public const string GlobalShowBoldButton = "GlobalShowBoldButton";

		public const string GlobalShowItalicButton = "GlobalShowItalicButton";

		public const string GlobalShowUnderlineButton = "GlobalShowUnderlineButton";

		public const string GlobalShowColorButton = "GlobalShowColorButton";

		public const string GlobalShowFontName = "GlobalShowFontName";

		public const string GlobalShowFontSize = "GlobalShowFontSize";

		public const string GlobalShowEmotion = "GlobalShowEmotion";

		public const string GlobalShowControlPanel = "GlobalShowControlPanel";

		public const string GlobalShowSkinButton = "GlobalShowSkinButton";

		public const string GlobalShowSignoutButton = "GlobalShowSignoutButton";

		public const string GlobalShowSystemMessages = "GlobalShowSystemMessages";

		public const string GlobalShowSystemErrors = "GlobalShowSystemErrors";

		public const string GlobalShowTypingIndicator = "GlobalShowTypingIndicator";

		public const string GlobalTextWrittenfromRighttoLeft = "GlobalTextWrittenfromRighttoLeft";

		public const string GlobalShowAvatarBeforeMessage = "GlobalShowAvatarBeforeMessage";

		public const string GlobalShowTimeStampWebMessenger = "GlobalShowTimeStampWebMessenger";

		public const string GlobalShowTimeStampBeforeMessage = "GlobalShowTimeStampBeforeMessage";

		public const string DataProviderType = "DataProviderType";

		public const string UserAdapterType = "UserAdapterType";

		public const string Announcement = "Announcement";

		public const string SupportSendFileSize = "SupportSendFileSize";

		public const string SupportSendFileType = "SupportSendFileType";

		public const string SupportAllowSendFile = "SupportAllowSendFile";

		public const string SupportAllowSendMail = "SupportAllowSendMail";

		public const string GlobalSendFileSize = "GlobalSendFileSize";

		public const string GlobalSendFileType = "GlobalSendFileType";

		public const string GlobalAllowSendFile = "GlobalAllowSendFile";

		public const string MessengerSendFileSize = "MessengerSendFileSize";

		public const string MessengerSendFileType = "MessengerSendFileType";

		public const string MessengerAllowSendFile = "MessengerAllowSendFile";

		public const string GlobalEnableHtmlBox = "GlobalEnableHtmlBox";

		public const string GlobalDenyAnonymous = "GlobalDenyAnonymous";

		public const string GlobalAllowPrivateMessage = "GlobalAllowPrivateMessage";

		public const string CultureType = "CultureType";

		public const string CustomCulture = "CustomCulture";

		public const string ClientPath = "ClientPath";

		public const string LogoUrl = "LogoUrl";

		public const string LoginUrl = "LoginUrl";

		public const string LogoutUrl = "LogoutUrl";

		public const string ChannelSkins = "ChannelSkins";

		public const string ServerAvgTime = "ServerAvgTime";

		public const string ChannelSyncInterval = "ChannelSyncInterval";

		public const string ChannelSyncIntervalMin = "ChannelSyncIntervalMin";

		public const string ChannelSyncIntervalMax = "ChannelSyncIntervalMax";

		public const string InstantSyncInterval = "InstantSyncInterval";

		public const string InstantSyncIntervalMin = "InstantSyncIntervalMin";

		public const string InstantSyncIntervalMax = "InstantSyncIntervalMax";

		public EnumConfigNames()
		{
		}
	}
}