using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;

namespace CuteChat
{
	public abstract class ChatPlace : ChatPortalDataItem
	{
		private string @ӓ;

		private int @Ӕ = 2147483647;

		private string @ӕ;

		private bool @Ӗ;

		private bool @ӗ = true;

		private int @Ә = 20;

		private int @ә = 20;

		private int @Ӛ = 2147483647;

		private int @ӛ = 2147483647;

		private bool @Ӝ;

		private ArrayList @ӝ;

		private ArrayList @Ӟ;

		private Hashtable @ӟ;

		private DateTime @Ӡ = DateTime.Now;

		private ChatManager @ӡ;

		private ChatConnectionCollection @Ӣ = new ChatConnectionCollection();

		private Hashtable @ӣ = new Hashtable();

		private Hashtable @Ӥ = new Hashtable(new CaseInsensitiveHashCodeProvider(), new CaseInsensitiveComparer());

		public DateTime ActivateTime
		{
			get
			{
				return this.@Ӡ;
			}
		}

		public virtual bool AllowAnonymous
		{
			get
			{
				return this.@ӗ;
			}
			set
			{
				this.@ӗ = value;
			}
		}

		public virtual int AutoAwayMinute
		{
			get
			{
				return this.@ӛ;
			}
			set
			{
				this.@ӛ = value;
			}
		}

		public virtual int HistoryCount
		{
			get
			{
				return this.@Ә;
			}
			set
			{
				this.@Ә = value;
			}
		}

		public virtual int HistoryDay
		{
			get
			{
				return this.@ә;
			}
			set
			{
				this.@ә = value;
			}
		}

		public virtual bool IsExpired
		{
			get
			{
				return (DateTime.Now - this.@Ӡ).TotalSeconds > 300;
			}
		}

		public abstract string Location
		{
			get;
		}

		public virtual bool Locked
		{
			get
			{
				return this.@Ӗ;
			}
			set
			{
				this.@Ӗ = value;
			}
		}

		public ChatManager Manager
		{
			get
			{
				if (this.@ӡ == null)
				{
					this.@ӡ = this.CreateManagerInstance();
				}
				return this.@ӡ;
			}
		}

		public virtual int MaxIdleMinute
		{
			get
			{
				return this.@Ӛ;
			}
			set
			{
				this.@Ӛ = value;
			}
		}

		public virtual int MaxOnlineCount
		{
			get
			{
				return this.@Ӕ;
			}
			set
			{
				this.@Ӕ = value;
			}
		}

		public bool ModerateMode
		{
			get
			{
				return this.@Ӝ;
			}
			set
			{
				this.@Ӝ = value;
			}
		}

		public virtual string Password
		{
			get
			{
				return this.@ӕ;
			}
			set
			{
				this.@ӕ = value;
			}
		}

		public string PlaceName
		{
			get
			{
				return this.@ӓ;
			}
		}

		public virtual string PlaceTitle
		{
			get
			{
				return "NoTitle";
			}
		}

		public ChatPlace(ChatPortal portal, string name) : base(portal)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			this.@ӓ = name;
		}

		private string @ӥ(string u0040ӓ, bool u0040Ӕ)
		{
			StringBuilder stringBuilder = new StringBuilder();
			string str = u0040ӓ;
			for (int i = 0; i < str.Length; i++)
			{
				char chr = str[i];
				if (char.IsLetterOrDigit(chr))
				{
					stringBuilder.Append(chr);
				}
				else if (!u0040Ӕ || chr != '*')
				{
					stringBuilder.Append("\\u");
					string str1 = chr.ToString("X");
					if (str1.Length == 1)
					{
						stringBuilder.Append("000");
					}
					else if (str1.Length == 2)
					{
						stringBuilder.Append("00");
					}
					else if (str1.Length == 3)
					{
						stringBuilder.Append("0");
					}
					stringBuilder.Append(str1);
				}
				else
				{
					stringBuilder.Append(".*");
				}
			}
			return stringBuilder.ToString();
		}

		private bool @Ӧ(string u0040ӓ, IChatRule u0040Ӕ)
		{
			if (u0040Ӕ.Mode == "Contains")
			{
				return u0040ӓ.IndexOf(u0040Ӕ.Expression.ToLower()) != -1;
			}
			if (u0040Ӕ.Mode == "Word")
			{
				return (new Regex(string.Concat("\\b", this.@ӥ(u0040Ӕ.Expression, false), "\\b"), RegexOptions.IgnoreCase | RegexOptions.Compiled)).IsMatch(u0040ӓ);
			}
			if (u0040Ӕ.Mode == "Wide")
			{
				return (new Regex(this.@ӥ(u0040Ӕ.Expression, true), RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Singleline)).IsMatch(u0040ӓ);
			}
			if (u0040Ӕ.Mode != "RegExp")
			{
				throw new Exception(string.Concat("Invaild Expression Mode:", u0040Ӕ.Mode));
			}
			return (new Regex(u0040Ӕ.Expression, RegexOptions.IgnoreCase | RegexOptions.Compiled)).IsMatch(u0040ӓ);
		}

		protected internal virtual void AcceptConnection(ChatConnection conn)
		{
			this.@Ӣ.Add(conn);
		}

		protected internal virtual void AddItem(ChatPlaceItem item)
		{
			this.@ӣ.Add(item.ObjectGuid, item);
			ChatPlaceUser chatPlaceUser = item as ChatPlaceUser;
			if (chatPlaceUser != null)
			{
				this.@Ӥ.Add(chatPlaceUser.Identity.UniqueId, chatPlaceUser);
			}
		}

		public virtual void AddKickedIP(string ipaddr)
		{
			if (this.@Ӟ == null)
			{
				this.@Ӟ = new ArrayList();
			}
			if (!this.@Ӟ.Contains(ipaddr))
			{
				this.@Ӟ.Add(ipaddr);
			}
		}

		public virtual void AddKickedUser(ChatIdentity identity)
		{
			if (this.@ӝ == null)
			{
				this.@ӝ = new ArrayList();
			}
			if (!this.@ӝ.Contains(identity))
			{
				this.@ӝ.Add(identity);
			}
		}

		public virtual void AddModerator(string userid)
		{
			if (this.@ӟ == null)
			{
				this.@ӟ = new Hashtable();
			}
			this.@ӟ[userid.ToLower()] = userid;
		}

		protected bool CheckMessage(ChatConnection conn, string text, ref string html, out string errormsg)
		{
			errormsg = null;
			if (html == "")
			{
				html = null;
			}
			if (html != null)
			{
				@Ծ _u0040Ծ = new @Ծ();
				_u0040Ծ.Sinks.Add(new @Խ());
				_u0040Ծ.@Ӕ(html);
				html = _u0040Ծ.ToString();
			}
			if (conn.PlaceUser.IsModerator)
			{
				return true;
			}
			int property = base.Portal.GetProperty("MaxMSGLength", 10000);
			int num = base.Portal.GetProperty("MaxWordLength", 100);
			if (text.Length >= property)
			{
				errormsg = string.Concat(new object[] { conn.Strings.MsgExceededMaxLen, " (", property, ")." });
				return false;
			}
			string[] strArrays = text.Split(new char[] { ' ', '\\', '/', '-', '.', ',', '@', '#', '$', '?', '|' });
			if ((int)strArrays.Length >= 1)
			{
				for (int i = 0; i < (int)strArrays.Length; i++)
				{
					if (strArrays[i].Length >= num)
					{
						errormsg = string.Concat(new object[] { conn.Strings.WordExceededMaxLen, " (", num, ")." });
						return false;
					}
				}
			}
			IChatRule[] badWords = base.Portal.DataManager.GetBadWords();
			if (badWords == null || badWords.Length == 0)
			{
				return true;
			}
			text = text.ToLower();
			IChatRule[] chatRuleArray = badWords;
			for (int j = 0; j < (int)chatRuleArray.Length; j++)
			{
				IChatRule chatRule = chatRuleArray[j];
				if (this.@Ӧ(text, chatRule))
				{
					errormsg = string.Concat(conn.Strings.MsgMessageContainsBadWord, chatRule.Expression);
					return false;
				}
			}
			return true;
		}

		public virtual void ClearKickedIPs()
		{
			this.@Ӟ = null;
		}

		public virtual void ClearKickedUsers()
		{
			this.@ӝ = null;
		}

		protected virtual ChatManager CreateManagerInstance()
		{
			return new ChatManager(this);
		}

		protected internal virtual ChatPlaceUser CreatePlaceUser(ChatConnection conn)
		{
			return new ChatPlaceUser(this, conn);
		}

		protected internal virtual void DispatchMessage(ChatConnection conn, NameValueCollection nvc, string[] args)
		{
		}

		public override void Dispose()
		{
			int i;
			ChatConnection[] allConnections = this.GetAllConnections();
			for (i = 0; i < (int)allConnections.Length; i++)
			{
				allConnections[i].Dispose();
			}
			ChatPlaceItem[] allItems = this.GetAllItems();
			for (i = 0; i < (int)allItems.Length; i++)
			{
				allItems[i].Dispose();
			}
			this.Manager.Dispose();
			base.Dispose();
		}

		public virtual void Dump(XmlWriter writer)
		{
		}

		public ChatConnection FindConnection(ChatGuid guid)
		{
			return this.@Ӣ[guid];
		}

		public ChatPlaceItem FindItem(ChatGuid guid)
		{
			return (ChatPlaceItem)this.@ӣ[guid];
		}

		public ChatPlaceUser FindUser(string userid)
		{
			return (ChatPlaceUser)this.@Ӥ[userid];
		}

		public ChatConnection[] GetAllConnections()
		{
			return this.@Ӣ.ToArray();
		}

		public ChatPlaceItem[] GetAllItems()
		{
			ChatPlaceItem[] chatPlaceItemArray = new ChatPlaceItem[this.@ӣ.Count];
			this.@ӣ.Values.CopyTo(chatPlaceItemArray, 0);
			return chatPlaceItemArray;
		}

		public ChatPlaceUser[] GetAllUsers()
		{
			ChatPlaceUser[] chatPlaceUserArray = new ChatPlaceUser[this.@Ӥ.Count];
			this.@Ӥ.Values.CopyTo(chatPlaceUserArray, 0);
			return chatPlaceUserArray;
		}

		public int GetConnectionCount()
		{
			return this.@Ӣ.Count;
		}

		public virtual string GetDisplayName(ChatIdentity identity)
		{
			return identity.DisplayName;
		}

		public int GetItemCount()
		{
			return this.@ӣ.Count;
		}

		public int GetUserCount()
		{
			return this.@Ӥ.Count;
		}

		public virtual void Init()
		{
		}

		public virtual bool IsKicked(ChatIdentity identity)
		{
			if (this.@ӝ == null)
			{
				return false;
			}
			return this.@ӝ.Contains(identity);
		}

		public virtual bool IsKickedIP(string ipaddr)
		{
			if (this.@Ӟ == null)
			{
				return false;
			}
			return this.@Ӟ.Contains(ipaddr);
		}

		public virtual bool IsModerator(ChatIdentity identity)
		{
			if (this.@ӟ != null && this.@ӟ.ContainsKey(identity.UniqueId.ToLower()))
			{
				return true;
			}
			return base.Portal.DataManager.IsAdministratorId(identity.UniqueId);
		}

		protected internal virtual void LoadHistoryMessages(ChatConnection conn)
		{
		}

		protected internal virtual void Maintain()
		{
		}

		public virtual bool PreProcessCTSMessage(ChatConnection conn, string msgid, string[] args, NameValueCollection nvc)
		{
			return false;
		}

		protected internal virtual void RemoveConnection(ChatConnection conn, string reason)
		{
			this.@Ӣ.Remove(conn.ObjectGuid);
		}

		protected internal virtual void RemoveItem(ChatPlaceItem item)
		{
			this.@ӣ.Remove(item.ObjectGuid);
			ChatPlaceUser chatPlaceUser = item as ChatPlaceUser;
			if (chatPlaceUser != null)
			{
				this.@Ӥ.Remove(chatPlaceUser.Identity.UniqueId);
			}
		}

		public virtual void RemoveModerator(string userid)
		{
			if (this.@ӟ != null)
			{
				this.@ӟ.Remove(userid.ToLower());
			}
		}

		public virtual void SendFileMessage(ChatConnection conn, ChatPlaceUser target, string filename, long len, string filelink, string filemime)
		{
			long num = len / (long)1000;
			string str = string.Concat(filename, "(", num.ToString("###,##0"), ")");
			string[] strArrays = new string[] { "<a target='_blank' href='", HttpUtility.HtmlEncode(filelink), "'>", HttpUtility.HtmlEncode(filename), "(", null, null };
			double num1 = Math.Ceiling((double)((float)len / 1000f));
			strArrays[5] = num1.ToString("###,##0");
			strArrays[6] = "KB)</a>";
			string str1 = string.Concat(strArrays);
			if (filemime != null && filemime.ToLower().StartsWith("image/"))
			{
				str1 = string.Concat(new string[] { "<img src='", HttpUtility.HtmlEncode(filelink), "' alt='", HttpUtility.HtmlEncode(filename), "' border='0' /><br/>", str1 });
			}
			string uniqueId = null;
			string displayName = null;
			if (target != null)
			{
				uniqueId = target.Identity.UniqueId;
				displayName = target.DisplayName;
			}
			this.Manager.HandleCTSUserMessage(conn, null, new string[] { str, str1, uniqueId, displayName, null });
		}

		protected internal virtual void SendInitializeMessages(ChatConnection conn)
		{
		}

		public void SendUpdatedMessage()
		{
			ChatConnection[] allConnections = this.GetAllConnections();
			for (int i = 0; i < (int)allConnections.Length; i++)
			{
				this.SendUpdatedMessage(allConnections[i]);
			}
		}

		public virtual void SendUpdatedMessage(ChatConnection conn)
		{
			ChatManager manager = this.Manager;
			ChatConnection chatConnection = conn;
			string[] objectGuid = new string[] { base.ObjectGuid, this.PlaceName, this.PlaceTitle, this.Location, null, null, null };
			objectGuid[4] = (this.Locked ? "1" : "0");
			objectGuid[5] = (this.AllowAnonymous ? "1" : "0");
			objectGuid[6] = (this.ModerateMode ? "1" : "0");
			manager.PushSTCMessage(chatConnection, "PLACE_UPDATED", null, objectGuid);
			this.Manager.PushSTCMessage(conn, "PLACE_AUTOAWAYMINUTE", null, new string[] { this.AutoAwayMinute.ToString() });
		}

		public virtual void SetActivate()
		{
			this.@Ӡ = DateTime.Now;
		}

		protected internal virtual void ValidateIdentity(ChatIdentity identity)
		{
		}
	}
}