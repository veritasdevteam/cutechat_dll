using System;
using System.Collections;

namespace CuteChat
{
	public class ChatQuestionItem : ChatPlaceItem
	{
		public string Question;

		public ChatQuestionItem(ChatPlace place) : base(place)
		{
		}

		protected override bool GetItemInfo(ChatConnection conn, string formsg, out string type, out string[] args)
		{
			type = "QUESTIONITEM";
			ArrayList arrayLists = new ArrayList();
			arrayLists.Add(this.Question);
			args = (string[])arrayLists.ToArray(typeof(string));
			return true;
		}

		public override bool IsVisibleConnection(ChatConnection conn)
		{
			return base.Place.IsModerator(conn.Identity);
		}
	}
}