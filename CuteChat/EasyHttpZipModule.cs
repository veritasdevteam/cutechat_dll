using CuteChat.Configuration;
using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;

namespace CuteChat
{
	public class EasyHttpZipModule : IHttpModule
	{
		private HttpApplication @ӓ;

		public EasyHttpZipModule()
		{
		}

		private void @ӗ(HttpApplication u0040ӓ)
		{
			if (u0040ӓ.Context.Items["ZipBy"] != null)
			{
				return;
			}
			if (!this.@Ә(u0040ӓ.Context))
			{
				return;
			}
			u0040ӓ.Context.Items["ZipBy"] = "EasyHttpZipModule";
			u0040ӓ.Response.AddHeader("-gzip-by-assembly-", this.GetType().Assembly.GetName(false).Name);
			u0040ӓ.Response.AddHeader("Content-Encoding", "gzip");
			u0040ӓ.Response.Filter = GZip.CreateGZipStream(new BufferedStream(u0040ӓ.Response.Filter));
		}

		private void @Ӗ(object u0040ӓ, EventArgs u0040Ӕ)
		{
		}

		private bool @ә(Config u0040ӓ, string u0040Ӕ)
		{
			if (u0040ӓ == null)
			{
				return false;
			}
			if (!u0040ӓ.Enable)
			{
				return false;
			}
			Regex[] urlPatterns = u0040ӓ.UrlPatterns;
			for (int i = 0; i < (int)urlPatterns.Length; i++)
			{
				if (urlPatterns[i].IsMatch(u0040Ӕ))
				{
					return true;
				}
			}
			return false;
		}

		private bool @Ә(HttpContext u0040ӓ)
		{
			if ((u0040ӓ.Request.Headers["Accept-Encoding"] ?? "").ToLower().IndexOf("gzip") == -1)
			{
				return false;
			}
			string str = u0040ӓ.Request.Url.ToString();
			if (this.@ә((Config)u0040ӓ.GetConfig(Config.ConfigSectionName), str))
			{
				return true;
			}
			if (this.@ә(this.@Ӛ(u0040ӓ), str))
			{
				return true;
			}
			return false;
		}

		private @Ռ @ӛ(HttpContext u0040ӓ, string u0040Ӕ)
		{
			string str = null;
			if (u0040Ӕ != "")
			{
				str = u0040Ӕ.Substring(0, u0040Ӕ.LastIndexOf('/'));
			}
			u0040Ӕ = u0040Ӕ.ToLower();
			string str1 = Path.Combine(u0040ӓ.Server.MapPath(string.Concat(u0040Ӕ, "/")), Config.ConfigFileName);
			string str2 = string.Concat(Config.ConfigFileName, ":", str1);
			@Ռ item = (@Ռ)u0040ӓ.Cache[str2];
			if (item == null)
			{
				item = new @Ռ(str1);
				if (str != null)
				{
					string applicationPath = u0040ӓ.Request.ApplicationPath;
					if (str.ToLower().StartsWith(applicationPath.ToLower()))
					{
						item.Parent = this.@ӛ(u0040ӓ, str);
					}
				}
				u0040ӓ.Cache[str2] = item;
			}
			return item;
		}

		private Config @Ӛ(HttpContext u0040ӓ)
		{
			string filePath = u0040ӓ.Request.FilePath;
			string str = filePath.Substring(0, filePath.LastIndexOf('/'));
			return this.@ӛ(u0040ӓ, str).GetConfig();
		}

		private void @ӕ(object u0040ӓ, EventArgs u0040Ӕ)
		{
			this.@ӗ((HttpApplication)u0040ӓ);
		}

		private void @Ӕ(object u0040ӓ, EventArgs u0040Ӕ)
		{
		}

		public void Dispose()
		{
			this.@ӓ.BeginRequest -= new EventHandler(this.@Ӕ);
			this.@ӓ.PreRequestHandlerExecute -= new EventHandler(this.@ӕ);
			this.@ӓ.PostRequestHandlerExecute -= new EventHandler(this.@Ӗ);
		}

		public void Init(HttpApplication context)
		{
			this.@ӓ = context;
			this.@ӓ.BeginRequest += new EventHandler(this.@Ӕ);
			this.@ӓ.PreRequestHandlerExecute += new EventHandler(this.@ӕ);
			this.@ӓ.PostRequestHandlerExecute += new EventHandler(this.@Ӗ);
		}
	}
}