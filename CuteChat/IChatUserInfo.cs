using System;

namespace CuteChat
{
	public interface IChatUserInfo
	{
		string BuildinContacts
		{
			get;
			set;
		}

		string BuildinIgnores
		{
			get;
			set;
		}

		string Description
		{
			get;
			set;
		}

		string DisplayName
		{
			get;
			set;
		}

		bool IsStored
		{
			get;
		}

		string PrivateProperties
		{
			get;
			set;
		}

		string PublicProperties
		{
			get;
			set;
		}

		string ServerProperties
		{
			get;
			set;
		}

		string UserId
		{
			get;
		}

		string UserName
		{
			get;
			set;
		}

		string GetPrivateProperty(string name);

		string GetPublicProperty(string name);

		string GetServerProperty(string name);

		void SetPrivateProperty(string name, string value);

		void SetPublicProperty(string name, string value);

		void SetServerProperty(string name, string value);
	}
}