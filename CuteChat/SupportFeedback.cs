using System;

namespace CuteChat
{
	[Serializable]
	public class SupportFeedback
	{
		public int FeedbackId;

		public DateTime FbTime;

		public string CustomerId;

		public string DisplayName;

		public string Name;

		public string Email;

		public string Title;

		public string Content;

		public string Comment;

		public string CommentBy;

		public SupportFeedback()
		{
		}
	}
}