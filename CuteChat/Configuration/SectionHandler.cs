using System;
using System.Configuration;
using System.Xml;

namespace CuteChat.Configuration
{
	public class SectionHandler : IConfigurationSectionHandler
	{
		public SectionHandler()
		{
		}

		public object Create(object parent, object configContext, XmlNode section)
		{
			return new Config((Config)parent, section);
		}
	}
}