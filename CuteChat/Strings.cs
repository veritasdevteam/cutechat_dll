using System;
using System.Collections.Specialized;

namespace CuteChat
{
	public abstract class Strings
	{
		private string @ӓ;

		private string @Ӕ;

		private string @ӕ;

		private string @Ӗ;

		private string @ӗ;

		private string @Ә;

		private string @ә;

		private string @Ӛ;

		private string @ӛ;

		private string @Ӝ;

		private string @ӝ;

		private string @Ӟ;

		private string @ӟ;

		private string @Ӡ;

		private string @ӡ;

		private string @Ӣ;

		private string @ӣ;

		private string @Ӥ;

		private string @ӥ;

		private string @Ӧ;

		private string @ӧ;

		private string @Ө;

		private string @ө;

		private string @Ӫ;

		private string @ӫ;

		private string @Ӭ;

		private string @ӭ;

		private string @Ӯ;

		private string @ӯ;

		private string @Ӱ;

		private string @ӱ;

		private string @Ӳ;

		private string @ӳ;

		private string @Ӵ;

		private string @ӵ;

		private string @Ӷ;

		private string @ӷ;

		private string @Ӹ;

		private string @ӹ;

		private string @Ӻ;

		private string @ӻ;

		private string @Ӽ;

		private string @ӽ;

		private string @Ӿ;

		private string @ӿ;

		private string @Ԁ;

		private string @ԁ;

		private string @Ԃ;

		private string @ԃ;

		private string @Ԅ;

		private string @ԅ;

		private string @Ԇ;

		private string @ԇ;

		private string @Ԉ;

		private string @ԉ;

		private string @Ԋ;

		private string @ԋ;

		private string @Ԍ;

		private string @ԍ;

		private string @Ԏ;

		private string @ԏ;

		private string @Ԑ;

		private string @ԑ;

		private string @Ԓ;

		private string @ԓ;

		private string @Ԕ;

		private string @ԕ;

		private string @Ԗ;

		private string @ԗ;

		private string @Ԙ;

		private string @ԙ;

		private string @Ԛ;

		private string @ԛ;

		private string @Ԝ;

		private string @ԝ;

		private string @Ԟ;

		private string @ԟ;

		private string @Ԡ;

		private string @ԡ;

		private string @Ԣ;

		private string @ԣ;

		private string @Ԥ;

		private string @ԥ;

		private string @Ԧ;

		private string @ԧ;

		private string @Ԩ;

		private string @ԩ;

		private string @Ԫ;

		private string @ԫ;

		private string @Ԭ;

		private string @ԭ;

		private string @Ԯ;

		private string @ԯ;

		private string @԰;

		private string @Ա;

		private string @Բ;

		private string @Գ;

		private string @Դ;

		private string @Ե;

		private string @Զ;

		private string @Է;

		public string Accept
		{
			get
			{
				return this.@ӓ;
			}
		}

		public string AccountIsUsing
		{
			get
			{
				return this.@Ӕ;
			}
		}

		public string Announcement
		{
			get
			{
				return this.@ӕ;
			}
		}

		public string AnonymousCantChangeAvatar
		{
			get
			{
				return this.@Ӗ;
			}
		}

		public string AreYouSureToClear
		{
			get
			{
				return this.@ӗ;
			}
		}

		public string AutoFocus
		{
			get
			{
				return this.@Ә;
			}
		}

		public string AutoScroll
		{
			get
			{
				return this.@ә;
			}
		}

		public string Avatar
		{
			get
			{
				return this.@Ӛ;
			}
		}

		public string Bold
		{
			get
			{
				return this.@ӛ;
			}
		}

		public string Cancel
		{
			get
			{
				return this.@Ӝ;
			}
		}

		public string CantInviteSelf
		{
			get
			{
				return this.@ӝ;
			}
		}

		public string ChatPtopWelcomeMessage
		{
			get
			{
				return this.@Ӟ;
			}
		}

		public string ChatSupportServerWelcomeMessage
		{
			get
			{
				return this.@ӟ;
			}
		}

		public string ChatSupportWelcomeMessage
		{
			get
			{
				return this.@Ӡ;
			}
		}

		public string Clear
		{
			get
			{
				return this.@ӡ;
			}
		}

		public string CloseCurrentSession
		{
			get
			{
				return this.@Ӣ;
			}
		}

		public string CommandPtopOnly
		{
			get
			{
				return this.@ӣ;
			}
		}

		public string CommandRoomOnly
		{
			get
			{
				return this.@Ӥ;
			}
		}

		public string ConnectingToServer
		{
			get
			{
				return this.@ӥ;
			}
		}

		public string ConnectionFailed
		{
			get
			{
				return this.@Ӧ;
			}
		}

		public string ConnectionOK
		{
			get
			{
				return this.@ӧ;
			}
		}

		public string ConnectionRejected
		{
			get
			{
				return this.@Ө;
			}
		}

		public string CustomersWaitSupport
		{
			get
			{
				return this.@ө;
			}
		}

		public string DenyAllAnonymous
		{
			get
			{
				return this.@Ӫ;
			}
		}

		public string DenyRoomAnonymous
		{
			get
			{
				return this.@ӫ;
			}
		}

		public string Disconnected
		{
			get
			{
				return this.@Ӭ;
			}
		}

		public string Disconnecting
		{
			get
			{
				return this.@ӭ;
			}
		}

		public string Email
		{
			get
			{
				return this.@Ӯ;
			}
		}

		public string Emotion
		{
			get
			{
				return this.@ӯ;
			}
		}

		public string Enquiry
		{
			get
			{
				return this.@Ӱ;
			}
		}

		public string ExpiredTask
		{
			get
			{
				return this.@ԅ;
			}
		}

		public string FontColor
		{
			get
			{
				return this.@ӱ;
			}
		}

		public string FontSize
		{
			get
			{
				return this.@Ӳ;
			}
		}

		public string GuestNameMessage
		{
			get
			{
				return this.@ӳ;
			}
		}

		public string Help
		{
			get
			{
				return this.@Ӵ;
			}
		}

		public string IPAddressRejected
		{
			get
			{
				return this.@ӵ;
			}
		}

		public string IsNotOnline
		{
			get
			{
				return this.@ԭ;
			}
		}

		public string Italic
		{
			get
			{
				return this.@Ӷ;
			}
		}

		public string LeaveMessage
		{
			get
			{
				return this.@ӷ;
			}
		}

		public string ListGuestMessages
		{
			get
			{
				return this.@Ӹ;
			}
		}

		public string ListWaitingCustomers
		{
			get
			{
				return this.@ӹ;
			}
		}

		public string MaxFileSizeMustBeLessThan
		{
			get
			{
				return this.@Է;
			}
		}

		public string MessageSendFailed
		{
			get
			{
				return this.@Ӻ;
			}
		}

		public string MissingNickName
		{
			get
			{
				return this.@ӻ;
			}
		}

		public string MissingPassword
		{
			get
			{
				return this.@Ӽ;
			}
		}

		public string MsgAvatarChanged
		{
			get
			{
				return this.@ӽ;
			}
		}

		public string MsgChooseAvatar
		{
			get
			{
				return this.@Ӿ;
			}
		}

		public string MsgCommandError
		{
			get
			{
				return this.@ӿ;
			}
		}

		public string MsgCommandUnknown
		{
			get
			{
				return this.@Ԁ;
			}
		}

		public string MsgExceededMaxLen
		{
			get
			{
				return this.@ԇ;
			}
		}

		public string MsgHelpModerator
		{
			get
			{
				return this.@ԁ;
			}
		}

		public string MsgHelpNormal
		{
			get
			{
				return this.@Ԃ;
			}
		}

		public string MsgMessageContainsBadWord
		{
			get
			{
				return this.@ԃ;
			}
		}

		public string MsgNotSent
		{
			get
			{
				return this.@Ԇ;
			}
		}

		public string MsgUserExpired
		{
			get
			{
				return this.@Ԅ;
			}
		}

		public string MsgUserJoinRoom
		{
			get
			{
				return this.@ԉ;
			}
		}

		public string MsgUserQuitRoom
		{
			get
			{
				return this.@Ԋ;
			}
		}

		public string MsgUserTryJoinRoom
		{
			get
			{
				return this.@ԋ;
			}
		}

		public string My_USER_BLOCK
		{
			get
			{
				return this.@ԫ;
			}
		}

		public string Name
		{
			get
			{
				return this.@Ԍ;
			}
		}

		public string NickNameIsExists
		{
			get
			{
				return this.@ԍ;
			}
		}

		public string NickNameRestriction
		{
			get
			{
				return this.@Ԏ;
			}
		}

		public string NoAccountInfo
		{
			get
			{
				return this.@ԏ;
			}
		}

		public string NoCustomerWaitSupport
		{
			get
			{
				return this.@Ԑ;
			}
		}

		public string NoManagerWindow
		{
			get
			{
				return this.@ԑ;
			}
		}

		public string NoModerators
		{
			get
			{
				return this.@Ԓ;
			}
		}

		public string NumPeoplesChatting
		{
			get
			{
				return this.@ԓ;
			}
		}

		public string OK
		{
			get
			{
				return this.@Ԕ;
			}
		}

		public string PleaseInputPersonalInformation
		{
			get
			{
				return this.@ԕ;
			}
		}

		public string PleaseInputRoomPassword
		{
			get
			{
				return this.@Ԗ;
			}
		}

		public string PleaseInputYourNickName
		{
			get
			{
				return this.@ԗ;
			}
		}

		public string PrivateChat
		{
			get
			{
				return this.@Ԙ;
			}
		}

		public string PrivateChatTitle
		{
			get
			{
				return this.@ԙ;
			}
		}

		public string ReadyDoSupport
		{
			get
			{
				return this.@Ԛ;
			}
		}

		public string Reject
		{
			get
			{
				return this.@ԛ;
			}
		}

		public string ReloadSupport
		{
			get
			{
				return this.@Ԝ;
			}
		}

		public string RequirePassword
		{
			get
			{
				return this.@ԝ;
			}
		}

		public string Retry
		{
			get
			{
				return this.@Ԟ;
			}
		}

		public string Save
		{
			get
			{
				return this.@ԟ;
			}
		}

		public string SelectRoom
		{
			get
			{
				return this.@Ԡ;
			}
		}

		public string Send
		{
			get
			{
				return this.@ԡ;
			}
		}

		public string SpecifyUserName
		{
			get
			{
				return this.@Ԣ;
			}
		}

		public string Subject
		{
			get
			{
				return this.@ԣ;
			}
		}

		public string SupportBegin
		{
			get
			{
				return this.@Ԥ;
			}
		}

		public string SupportFinish
		{
			get
			{
				return this.@ԥ;
			}
		}

		public string SupportMailTemplate
		{
			get
			{
				return this.@Ԧ;
			}
		}

		public string SupportNoOpeartor
		{
			get
			{
				return this.@ԧ;
			}
		}

		public string SupportNoOperator
		{
			get
			{
				return this.@Ԩ;
			}
		}

		public string TargetBusyTryagain
		{
			get
			{
				return this.@Ԫ;
			}
		}

		public string TargetUserAlreadyHere
		{
			get
			{
				return this.@ԩ;
			}
		}

		public string TargetUserNotOnline
		{
			get
			{
				return this.@Ԭ;
			}
		}

		public string TargetUserNotPtop
		{
			get
			{
				return this.@Ԯ;
			}
		}

		public string UI_CONNECTION_Disconnect
		{
			get
			{
				return this.@Ա;
			}
		}

		public string Underline
		{
			get
			{
				return this.@ԯ;
			}
		}

		public string WordExceededMaxLen
		{
			get
			{
				return this.@Ԉ;
			}
		}

		public string WrongPassword
		{
			get
			{
				return this.@԰;
			}
		}

		public string YouAreNotConnect
		{
			get
			{
				return this.@Բ;
			}
		}

		public string YouAreNotModerator
		{
			get
			{
				return this.@Գ;
			}
		}

		public string YouAreRejected
		{
			get
			{
				return this.@Դ;
			}
		}

		public string YouCantGetin
		{
			get
			{
				return this.@Ե;
			}
		}

		public string YouCantSpeak
		{
			get
			{
				return this.@Զ;
			}
		}

		protected Strings()
		{
		}

		public abstract string GetString(string name);

		public abstract string GetStringOrNull(string name);

		public abstract NameValueCollection GetValues();

		protected void InitializeProperties()
		{
			this.@ӓ = this.GetString("Accept");
			this.@Ӕ = this.GetString("AccountIsUsing");
			this.@ӕ = this.GetString("Announcement");
			this.@Ӗ = this.GetString("AnonymousCantChangeAvatar");
			this.@ӗ = this.GetString("AreYouSureToClear");
			this.@Ә = this.GetString("AutoFocus");
			this.@ә = this.GetString("AutoScroll");
			this.@Ӛ = this.GetString("Avatar");
			this.@ӛ = this.GetString("Bold");
			this.@Ӝ = this.GetString("Cancel");
			this.@ӝ = this.GetString("CantInviteSelf");
			this.@Ӟ = this.GetString("ChatPtopWelcomeMessage");
			this.@ӟ = this.GetString("ChatSupportServerWelcomeMessage");
			this.@Ӡ = this.GetString("ChatSupportWelcomeMessage");
			this.@ӡ = this.GetString("Clear");
			this.@Ӣ = this.GetString("CloseCurrentSession");
			this.@ӣ = this.GetString("CommandPtopOnly");
			this.@Ӥ = this.GetString("CommandRoomOnly");
			this.@ӥ = this.GetString("ConnectingToServer");
			this.@Ӧ = this.GetString("ConnectionFailed");
			this.@ӧ = this.GetString("ConnectionOK");
			this.@Ө = this.GetString("ConnectionRejected");
			this.@ө = this.GetString("CustomersWaitSupport");
			this.@Ӫ = this.GetString("DenyAllAnonymous");
			this.@ӫ = this.GetString("DenyRoomAnonymous");
			this.@Ӭ = this.GetString("Disconnected");
			this.@ӭ = this.GetString("Disconnecting");
			this.@Ӯ = this.GetString("Email");
			this.@ӯ = this.GetString("Emotion");
			this.@Ӱ = this.GetString("Enquiry");
			this.@ӱ = this.GetString("FontColor");
			this.@Ӳ = this.GetString("FontSize");
			this.@ӳ = this.GetString("GuestNameMessage");
			this.@Ӵ = this.GetString("Help");
			this.@ӵ = this.GetString("IPAddressRejected");
			this.@Ӷ = this.GetString("Italic");
			this.@ӷ = this.GetString("LeaveMessage");
			this.@Ӹ = this.GetString("ListGuestMessages");
			this.@ӹ = this.GetString("ListWaitingCustomers");
			this.@Ӻ = this.GetString("MessageSendFailed");
			this.@ӻ = this.GetString("MissingNickName");
			this.@Ӽ = this.GetString("MissingPassword");
			this.@ӽ = this.GetString("MsgAvatarChanged");
			this.@Ӿ = this.GetString("MsgChooseAvatar");
			this.@ӿ = this.GetString("MsgCommandError");
			this.@Ԁ = this.GetString("MsgCommandUnknown");
			this.@ԁ = this.GetString("MsgHelpModerator");
			this.@Ԃ = this.GetString("MsgHelpNormal");
			this.@ԃ = this.GetString("MsgMessageContainsBadWord");
			this.@Ԅ = this.GetString("MsgUserExpired");
			this.@ԅ = this.GetString("ExpiredTask");
			this.@ԇ = this.GetString("MsgExceededMaxLen");
			this.@Ԉ = this.GetString("WordExceededMaxLen");
			this.@Ԇ = this.GetString("MsgNotSent");
			this.@ԉ = this.GetString("MsgUserJoinRoom");
			this.@Ԋ = this.GetString("MsgUserQuitRoom");
			this.@ԋ = this.GetString("MsgUserTryJoinRoom");
			this.@Ԍ = this.GetString("Name");
			this.@ԍ = this.GetString("NickNameIsExists");
			this.@Ԏ = this.GetString("NickNameRestriction");
			this.@ԏ = this.GetString("NoAccountInfo");
			this.@Ԑ = this.GetString("NoCustomerWaitSupport");
			this.@ԑ = this.GetString("NoManagerWindow");
			this.@Ԓ = this.GetString("NoModerators");
			this.@ԓ = this.GetString("NumPeoplesChatting");
			this.@Ԕ = this.GetString("OK");
			this.@ԕ = this.GetString("PleaseInputPersonalInformation");
			this.@Ԗ = this.GetString("PleaseInputRoomPassword");
			this.@ԗ = this.GetString("PleaseInputYourNickName");
			this.@Ԙ = this.GetString("PrivateChat");
			this.@ԙ = this.GetString("PrivateChatTitle");
			this.@Ԛ = this.GetString("ReadyDoSupport");
			this.@ԛ = this.GetString("Reject");
			this.@Ԝ = this.GetString("ReloadSupport");
			this.@ԝ = this.GetString("RequirePassword");
			this.@Ԟ = this.GetString("Retry");
			this.@ԟ = this.GetString("Save");
			this.@Ԡ = this.GetString("SelectRoom");
			this.@ԡ = this.GetString("Send");
			this.@Ԣ = this.GetString("SpecifyUserName");
			this.@ԣ = this.GetString("Subject");
			this.@Ԥ = this.GetString("SupportBegin");
			this.@ԥ = this.GetString("SupportFinish");
			this.@Ԧ = this.GetString("SupportMailTemplate");
			this.@ԧ = this.GetString("SupportNoOpeartor");
			this.@Ԩ = this.GetString("SupportNoOperator");
			this.@ԩ = this.GetString("TargetUserAlreadyHere");
			this.@Ԫ = this.GetString("TargetBusyTryagain");
			this.@ԫ = this.GetString("My_USER_BLOCK");
			this.@Ԭ = this.GetString("TargetUserNotOnline");
			this.@ԭ = this.GetString("IsNotOnline");
			this.@Ԯ = this.GetString("TargetUserNotPtop");
			this.@ԯ = this.GetString("Underline");
			this.@԰ = this.GetString("WrongPassword");
			this.@Ա = this.GetString("UI_CONNECTION_Disconnect");
			this.@Բ = this.GetString("YouAreNotConnect");
			this.@Գ = this.GetString("YouAreNotModerator");
			this.@Դ = this.GetString("YouAreRejected");
			this.@Ե = this.GetString("YouCantGetin");
			this.@Զ = this.GetString("YouCantSpeak");
			this.@Է = this.GetString("MaxFileSizeMustBeLessThan");
		}
	}
}