using System;

namespace CuteChat
{
	[Serializable]
	public class SupportSession
	{
		public int SessionId;

		public int DepartmentId;

		public DateTime BeginTime;

		public string AgentUserId;

		public string CustomerId;

		public string DisplayName;

		public DateTime ActiveTime;

		public string Email;

		public string IPAddress;

		public string Culture;

		public string Platform;

		public string Browser;

		public string Referrer;

		public string Url;

		public int AgentRating;

		public string SessionData;

		public SupportSession()
		{
		}
	}
}