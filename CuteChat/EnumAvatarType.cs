using System;

namespace CuteChat
{
	public enum EnumAvatarType
	{
		Administrators,
		Managers,
		Users,
		Guests
	}
}