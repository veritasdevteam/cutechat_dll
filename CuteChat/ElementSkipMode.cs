using System;

namespace CuteChat
{
	public enum ElementSkipMode
	{
		SkipNone,
		SkipAll,
		SkipElement,
		SkipChildNodes
	}
}