using System;
using System.Collections;
using System.Text;

namespace CuteChat
{
	public class ChatSerialize
	{
		private const char @ӓ = '!';

		private const char @Ӕ = '=';

		private const char @ӕ = '-';

		private const char @Ӗ = ',';

		private const char @ӗ = ';';

		private static Hashtable @Ә;

		private static Hashtable @ә;

		private static Hashtable @Ӛ;

		private static Hashtable @ӛ;

		[ThreadStatic]
		private static ArrayList @Ӝ;

		static ChatSerialize()
		{
			ChatSerialize.@Ӛ = new Hashtable();
			ChatSerialize.@ӛ = new Hashtable();
			ChatSerialize.@Ә = new Hashtable()
			{
				{ typeof(Guid), "GUID" },
				{ typeof(byte), "B" },
				{ typeof(sbyte), "I1" },
				{ typeof(char), "c" },
				{ typeof(string), "S" },
				{ typeof(short), "I2" },
				{ typeof(int), "i" },
				{ typeof(long), "l" },
				{ typeof(float), "f" },
				{ typeof(double), "d" },
				{ typeof(ushort), "U2" },
				{ typeof(uint), "U4" },
				{ typeof(ulong), "U8" },
				{ typeof(decimal), "m" },
				{ typeof(bool), "b" },
				{ typeof(DateTime), "dt" },
				{ typeof(TimeSpan), "ts" }
			};
			ChatSerialize.@ә = new Hashtable();
			foreach (DictionaryEntry dictionaryEntry in ChatSerialize.@Ә)
			{
				ChatSerialize.@ә.Add(dictionaryEntry.Value, dictionaryEntry.Key);
			}
		}

		public ChatSerialize()
		{
		}

		private static string @ӝ(Type u0040ӓ)
		{
			string item = (string)ChatSerialize.@Ә[u0040ӓ];
			if (item != null)
			{
				return item;
			}
			if (!u0040ӓ.IsArray)
			{
				return null;
			}
			if (u0040ӓ.GetArrayRank() != 1)
			{
				return null;
			}
			Type elementType = u0040ӓ.GetElementType();
			if (elementType == typeof(byte))
			{
				return "bytes";
			}
			if (elementType == typeof(object))
			{
				return "objects";
			}
			string str = ChatSerialize.@ӟ(u0040ӓ.GetElementType());
			if (str == null)
			{
				return null;
			}
			return string.Concat("arr,", str);
		}

		private static string @ӟ(Type u0040ӓ)
		{
			if (u0040ӓ == null)
			{
				throw new ArgumentNullException("type");
			}
			object item = ChatSerialize.@Ӛ[u0040ӓ];
			if (item == null)
			{
				if (item != null)
				{
					return null;
				}
				string str = ChatSerialize.@ӝ(u0040ӓ);
				if (str != null)
				{
					string str1 = str;
					item = str1;
					ChatSerialize.@Ӛ[u0040ӓ] = str1;
				}
				else
				{
					Hashtable hashtables = ChatSerialize.@Ӛ;
					DBNull value = DBNull.Value;
					item = value;
					hashtables[u0040ӓ] = value;
				}
			}
			return item as string;
		}

		private static Type @Ӟ(string u0040ӓ)
		{
			if (u0040ӓ.StartsWith("arr,"))
			{
				return Type.GetType(string.Concat(ChatSerialize.@Ӡ(u0040ӓ.Remove(0, 4)).FullName, "[]"));
			}
			Type item = (Type)ChatSerialize.@ә[u0040ӓ];
			if (item != null)
			{
				return item;
			}
			if (u0040ӓ == "bytes")
			{
				return typeof(byte[]);
			}
			if (u0040ӓ != "objects")
			{
				throw new ArgumentOutOfRangeException("prefix", u0040ӓ, "Unknown prefix");
			}
			return typeof(object[]);
		}

		private static string @ӥ(string u0040ӓ, ref int u0040Ӕ)
		{
			string str;
			int num = u0040ӓ.IndexOf(';', u0040Ӕ);
			str = (num != u0040Ӕ ? u0040ӓ.Substring(u0040Ӕ, num - u0040Ӕ) : "");
			u0040Ӕ = num + 1;
			return str;
		}

		private static object @Ӥ(string u0040ӓ, ref int u0040Ӕ)
		{
			if (u0040ӓ[u0040Ӕ] == '!')
			{
				u0040Ӕ++;
				return null;
			}
			int num = u0040ӓ.IndexOf('-', u0040Ӕ);
			u0040Ӕ = num + 1;
			return ChatSerialize.@Ӧ(u0040ӓ.Substring(u0040Ӕ, num - u0040Ӕ), u0040ӓ, ref u0040Ӕ);
		}

		private static void @ӣ(StringBuilder u0040ӓ, object u0040Ӕ)
		{
			int length;
			long ticks;
			if (u0040Ӕ == null)
			{
				u0040ӓ.Append('!');
				return;
			}
			Type type = u0040Ӕ.GetType();
			string fullName = type.FullName;
			if (fullName == "System.Guid" || fullName == "System.Byte" || fullName == "System.SByte" || fullName == "System.Char" || fullName == "System.Int16" || fullName == "System.Int32" || fullName == "System.Int64" || fullName == "System.Single" || fullName == "System.UInt16" || fullName == "System.UInt32" || fullName == "System.UInt64" || fullName == "System.Decimal")
			{
				u0040ӓ.Append(u0040Ӕ.ToString());
				u0040ӓ.Append(';');
				return;
			}
			if (fullName == "System.Boolean")
			{
				u0040ӓ.Append(((bool)u0040Ӕ ? "1" : "0"));
				u0040ӓ.Append(';');
				return;
			}
			if (fullName == "System.String")
			{
				string str = (string)u0040Ӕ;
				length = str.Length;
				u0040ӓ.Append(length.ToString());
				u0040ӓ.Append('=');
				u0040ӓ.Append(str);
				u0040ӓ.Append(';');
				return;
			}
			if (fullName == "System.Double")
			{
				double num = (double)u0040Ӕ;
				if (double.IsNaN(num))
				{
					u0040ӓ.Append("NaN");
				}
				else if (double.IsNegativeInfinity(num))
				{
					u0040ӓ.Append("NI");
				}
				else if (!double.IsPositiveInfinity(num))
				{
					u0040ӓ.Append(num.ToString());
				}
				else
				{
					u0040ӓ.Append("PI");
				}
				u0040ӓ.Append(';');
				return;
			}
			if (fullName == "System.TimeSpan")
			{
				ticks = ((TimeSpan)u0040Ӕ).Ticks;
				u0040ӓ.Append(ticks.ToString());
				u0040ӓ.Append(';');
				return;
			}
			if (fullName == "System.DateTime")
			{
				ticks = ((DateTime)u0040Ӕ).Ticks;
				string str1 = ticks.ToString();
				if (str1.EndsWith("0000000"))
				{
					u0040ӓ.Append(str1.Substring(0, str1.Length - 7));
					u0040ӓ.Append("S");
				}
				else
				{
					u0040ӓ.Append(str1);
				}
				u0040ӓ.Append(';');
				return;
			}
			byte[] numArray = u0040Ӕ as byte[];
			if (numArray != null)
			{
				u0040ӓ.Append(Convert.ToBase64String(numArray));
				u0040ӓ.Append(';');
				return;
			}
			Array arrays = u0040Ӕ as Array;
			if (arrays == null)
			{
				throw new Exception(string.Concat("Unsupport type:", type.FullName));
			}
			if (arrays.Rank != 1)
			{
				throw new Exception("arr.Rank != 1");
			}
			if (type.GetElementType() != typeof(object))
			{
				length = arrays.Length;
				u0040ӓ.Append(length.ToString());
				u0040ӓ.Append('=');
				for (int i = 0; i < arrays.Length; i++)
				{
					if (i != 0)
					{
						u0040ӓ.Append(',');
					}
					ChatSerialize.@ӣ(u0040ӓ, arrays.GetValue(i));
				}
				return;
			}
			length = arrays.Length;
			u0040ӓ.Append(length.ToString());
			u0040ӓ.Append('=');
			object[] objArray = (object[])arrays;
			ChatSerialize.@ӡ(objArray);
			for (int j = 0; j < (int)objArray.Length; j++)
			{
				if (j != 0)
				{
					u0040ӓ.Append(',');
				}
				ChatSerialize.@Ӣ(u0040ӓ, objArray[j]);
			}
		}

		private static void @Ӣ(StringBuilder u0040ӓ, object u0040Ӕ)
		{
			if (u0040Ӕ == null)
			{
				u0040ӓ.Append('!');
				return;
			}
			string str = ChatSerialize.@ӟ(u0040Ӕ.GetType());
			if (str == null)
			{
				throw new ArgumentException(string.Concat("Unsupport type:", u0040Ӕ.GetType().FullName), "val");
			}
			u0040ӓ.Append(str);
			u0040ӓ.Append('-');
			ChatSerialize.@ӣ(u0040ӓ, u0040Ӕ);
		}

		private static object @ӧ(string u0040ӓ)
		{
			if (u0040ӓ == "NaN")
			{
				return double.NaN;
			}
			if (u0040ӓ == "NI")
			{
				return double.NegativeInfinity;
			}
			if (u0040ӓ == "PI")
			{
				return double.PositiveInfinity;
			}
			return double.Parse(u0040ӓ);
		}

		private static object @Ӧ(string u0040ӓ, string u0040Ӕ, ref int u0040ӕ)
		{
			if (u0040ӓ.Length == 1)
			{
				if (u0040ӓ == "B")
				{
					return byte.Parse(ChatSerialize.@ӥ(u0040Ӕ, ref u0040ӕ));
				}
				if (u0040ӓ == "i")
				{
					return int.Parse(ChatSerialize.@ӥ(u0040Ӕ, ref u0040ӕ));
				}
				if (u0040ӓ == "l")
				{
					return long.Parse(ChatSerialize.@ӥ(u0040Ӕ, ref u0040ӕ));
				}
				if (u0040ӓ == "f")
				{
					return float.Parse(ChatSerialize.@ӥ(u0040Ӕ, ref u0040ӕ));
				}
				if (u0040ӓ == "m")
				{
					return decimal.Parse(ChatSerialize.@ӥ(u0040Ӕ, ref u0040ӕ));
				}
				if (u0040ӓ == "c")
				{
					u0040ӕ += 2;
					return u0040Ӕ[u0040ӕ - 2];
				}
				if (u0040ӓ == "b")
				{
					u0040ӕ += 2;
					return u0040Ӕ[u0040ӕ - 2] == '1';
				}
				if (u0040ӓ == "S")
				{
					if (u0040Ӕ[u0040ӕ] == '!')
					{
						u0040ӕ++;
						return null;
					}
					int num = u0040Ӕ.IndexOf('=', u0040ӕ);
					int num1 = int.Parse(u0040Ӕ.Substring(u0040ӕ, num - u0040ӕ));
					u0040ӕ = num + 1;
					u0040ӕ = u0040ӕ + num1 + 1;
					return u0040Ӕ.Substring(u0040ӕ, num1);
				}
				if (u0040ӓ == "d")
				{
					return ChatSerialize.@ӧ(ChatSerialize.@ӥ(u0040Ӕ, ref u0040ӕ));
				}
			}
			if (u0040ӓ.Length == 2)
			{
				if (u0040ӓ == "I1")
				{
					return sbyte.Parse(ChatSerialize.@ӥ(u0040Ӕ, ref u0040ӕ));
				}
				if (u0040ӓ == "I2")
				{
					return short.Parse(ChatSerialize.@ӥ(u0040Ӕ, ref u0040ӕ));
				}
				if (u0040ӓ == "U2")
				{
					return ushort.Parse(ChatSerialize.@ӥ(u0040Ӕ, ref u0040ӕ));
				}
				if (u0040ӓ == "U4")
				{
					return uint.Parse(ChatSerialize.@ӥ(u0040Ӕ, ref u0040ӕ));
				}
				if (u0040ӓ == "U8")
				{
					return ulong.Parse(ChatSerialize.@ӥ(u0040Ӕ, ref u0040ӕ));
				}
				if (u0040ӓ == "ts")
				{
					return new TimeSpan(long.Parse(ChatSerialize.@ӥ(u0040Ӕ, ref u0040ӕ)));
				}
				if (u0040ӓ == "dt")
				{
					return ChatSerialize.@Ө(ChatSerialize.@ӥ(u0040Ӕ, ref u0040ӕ));
				}
			}
			if (u0040ӓ == "GUID")
			{
				return new Guid(ChatSerialize.@ӥ(u0040Ӕ, ref u0040ӕ));
			}
			if (u0040ӓ == "bytes")
			{
				return Convert.FromBase64String(ChatSerialize.@ӥ(u0040Ӕ, ref u0040ӕ));
			}
			if (u0040ӓ == "objects")
			{
				return ChatSerialize.@ө(u0040Ӕ, ref u0040ӕ);
			}
			if (!u0040ӓ.StartsWith("arr,"))
			{
				throw new ArgumentException("Unknown Expression Prefix", u0040ӓ);
			}
			return ChatSerialize.@Ӫ(u0040ӓ.Remove(0, 4), u0040Ӕ, ref u0040ӕ);
		}

		private static object @ө(string u0040ӓ, ref int u0040Ӕ)
		{
			int num = u0040ӓ.IndexOf('=', u0040Ӕ);
			int num1 = int.Parse(u0040ӓ.Substring(u0040Ӕ, num - u0040Ӕ));
			u0040Ӕ = num + 1;
			object[] objArray = new object[num1];
			for (int i = 0; i < num1; i++)
			{
				if (i != 0)
				{
					u0040Ӕ++;
				}
				objArray[i] = ChatSerialize.@Ӥ(u0040ӓ, ref u0040Ӕ);
			}
			return objArray;
		}

		private static object @Ө(string u0040ӓ)
		{
			u0040ӓ.Split(new char[] { '-' });
			string str = u0040ӓ;
			if (str.EndsWith("S"))
			{
				str = string.Concat(str.Substring(0, str.Length - 1), "0000000");
			}
			return new DateTime(long.Parse(str));
		}

		private static object @Ӫ(string u0040ӓ, string u0040Ӕ, ref int u0040ӕ)
		{
			int num = u0040Ӕ.IndexOf('=', u0040ӕ);
			int num1 = int.Parse(u0040Ӕ.Substring(u0040ӕ, num - u0040ӕ));
			u0040ӕ = num + 1;
			Array arrays = Array.CreateInstance(ChatSerialize.@Ӡ(u0040ӓ), num1);
			for (int i = 0; i < num1; i++)
			{
				if (i != 0)
				{
					u0040ӕ++;
				}
				arrays.SetValue(ChatSerialize.@Ӧ(u0040ӓ, u0040Ӕ, ref u0040ӕ), i);
			}
			return arrays;
		}

		private static void @ӡ(object[] u0040ӓ)
		{
			if (ChatSerialize.@Ӝ == null)
			{
				ChatSerialize.@Ӝ = new ArrayList();
			}
			else if (ChatSerialize.@Ӝ.Contains(u0040ӓ))
			{
				throw new ArgumentException("object[] recycled");
			}
			ChatSerialize.@Ӝ.Add(u0040ӓ);
		}

		private static Type @Ӡ(string u0040ӓ)
		{
			if (u0040ӓ == null)
			{
				throw new ArgumentNullException("prefix");
			}
			object item = ChatSerialize.@ӛ[u0040ӓ];
			Type type = item as Type;
			if (item == null)
			{
				if (item != null)
				{
					return null;
				}
				type = ChatSerialize.@Ӟ(u0040ӓ);
				if (type != null)
				{
					ChatSerialize.@ӛ[u0040ӓ] = type;
				}
				else
				{
					ChatSerialize.@ӛ[u0040ӓ] = DBNull.Value;
				}
			}
			return type;
		}

		public static object Deserialize(string exp)
		{
			if (exp == null)
			{
				return null;
			}
			int num = 0;
			return ChatSerialize.@Ӥ(exp, ref num);
		}

		public static string Serialize(object val)
		{
			if (val == null)
			{
				return null;
			}
			ChatSerialize.@Ӝ = null;
			StringBuilder stringBuilder = new StringBuilder();
			ChatSerialize.@Ӣ(stringBuilder, val);
			ChatSerialize.@Ӝ = null;
			return stringBuilder.ToString();
		}

		public static bool SupportType(Type type)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			return ChatSerialize.@ӟ(type) != null;
		}
	}
}