using System;
using System.Collections.Specialized;

namespace CuteChat
{
	public class ChatMessenger : ChatPlace
	{
		public override bool AllowAnonymous
		{
			get
			{
				return false;
			}
			set
			{
			}
		}

		public override string Location
		{
			get
			{
				return "Messenger";
			}
		}

		public override bool Locked
		{
			get
			{
				return false;
			}
			set
			{
			}
		}

		public override int MaxOnlineCount
		{
			get
			{
				return 2147483647;
			}
		}

		public override string Password
		{
			get
			{
				return null;
			}
			set
			{
			}
		}

		public override string PlaceTitle
		{
			get
			{
				return "Messenger";
			}
		}

		public ChatMessenger(ChatPortal portal) : base(portal, "Messenger")
		{
		}

		protected internal override void DispatchMessage(ChatConnection conn, NameValueCollection nvc, string[] args)
		{
			string str;
			string str1 = args[0];
			string str2 = args[1];
			string str3 = args[2];
			string str4 = args[3];
			if (!base.CheckMessage(conn, str1, ref str2, out str))
			{
				base.Manager.PushSTCMessage(conn, "SYS_ERROR_MESSAGE", null, new string[] { str });
				return;
			}
			bool flag = true;
			if (str3 == null || str3.Length == 0)
			{
				base.Manager.PushSTCMessage(conn, "SYS_ERROR_MESSAGE", null, new string[] { "INVALIDTARGET" });
				return;
			}
			ChatPlaceUser chatPlaceUser = base.FindUser(str3);
			if (chatPlaceUser != null)
			{
				if (chatPlaceUser.Connection is @ԫ)
				{
					flag = true;
				}
				else if (chatPlaceUser.IsVisibleConnection(conn))
				{
					flag = false;
				}
			}
			string str5 = "USER_MESSAGE";
			if (nvc != null && nvc["Emotion"] != null)
			{
				str5 = "USER_DOEMOTE";
			}
			string str6 = str5;
			NameValueCollection nameValueCollection = nvc;
			string[] objectGuid = new string[] { conn.PlaceUser.ObjectGuid, conn.PlaceUser.Identity.UniqueId, conn.PlaceUser.DisplayName, str1, str2, str3, str4, null, null };
			objectGuid[7] = (flag ? "1" : "0");
			DateTime universalTime = DateTime.Now.ToUniversalTime();
			objectGuid[8] = universalTime.Ticks.ToString();
			string msg = ChatUtility.JoinToMsg(str6, nameValueCollection, objectGuid);
			if (chatPlaceUser != null)
			{
				@ԫ connection = chatPlaceUser.Connection as @ԫ;
				if (connection != null)
				{
					connection.@Ӗ(conn.PlaceUser, str1, str2);
				}
				base.Manager.PushSTCMessage(chatPlaceUser.Connection, msg);
			}
			base.Manager.PushSTCMessage(conn, msg);
			base.Portal.DataManager.AsyncLogInstantMessage(conn.Identity, str3, str4, flag, str1, str2);
		}

		public string HandlePartial(ChatIdentity identity)
		{
			@ԫ connection;
			if (identity == null || identity.IsAnonymous)
			{
				return "NEEDLOGIN";
			}
			ChatPlaceUser placeUser = base.FindUser(identity.UniqueId);
			if (placeUser != null)
			{
				connection = placeUser.Connection as @ԫ;
				if (connection == null)
				{
					return "ONLINE";
				}
			}
			else
			{
				connection = new @ԫ(base.Portal, identity);
				base.Manager.@ӗ(connection);
				placeUser = connection.PlaceUser;
			}
			return connection.HandlePartial();
		}

		public override bool IsModerator(ChatIdentity identity)
		{
			return false;
		}

		protected internal override void LoadHistoryMessages(ChatConnection conn)
		{
			long ticks;
			DateTime dateTime = new DateTime(2000, 1, 1);
			string serverProperty = conn.PlaceUser.Info.GetServerProperty("InstantActivateTime");
			if (serverProperty != null)
			{
				dateTime = new DateTime(long.Parse(serverProperty));
			}
			base.Manager.PushSTCMessage(conn, "HISTORY_BEGIN", null, new string[0]);
			ChatMsgData[] chatMsgDataArray = base.Portal.DataManager.LoadInstantOfflineMessages(conn.Identity.UniqueId, dateTime);
			for (int i = 0; i < (int)chatMsgDataArray.Length; i++)
			{
				ChatMsgData chatMsgDatum = chatMsgDataArray[i];
				ChatManager manager = base.Manager;
				ChatConnection chatConnection = conn;
				string[] senderId = new string[] { "", chatMsgDatum.SenderId, chatMsgDatum.Sender, chatMsgDatum.Text, chatMsgDatum.Html, chatMsgDatum.TargetId, chatMsgDatum.Target, null, null };
				senderId[7] = (chatMsgDatum.Offline ? "1" : "0");
				ticks = chatMsgDatum.Time.ToUniversalTime().Ticks;
				senderId[8] = ticks.ToString();
				manager.PushSTCMessage(chatConnection, "USER_MESSAGE", null, senderId);
			}
			base.Manager.PushSTCMessage(conn, "HISTORY_END", null, new string[0]);
			if (!(conn is @ԫ))
			{
				IChatUserInfo info = conn.PlaceUser.Info;
				ticks = conn.ActivateTime.Ticks;
				info.SetServerProperty("InstantActivateTime", ticks.ToString());
				base.Portal.DataManager.UpdateUserInfo(conn.PlaceUser.Info);
			}
		}

		protected internal override void RemoveConnection(ChatConnection conn, string reason)
		{
			if (!(conn is @ԫ))
			{
				conn.PlaceUser.Info.SetServerProperty("InstantActivateTime", conn.ActivateTime.Ticks.ToString());
				base.Portal.DataManager.UpdateUserInfo(conn.PlaceUser.Info);
			}
			base.RemoveConnection(conn, reason);
		}

		public void SendOpenContact(ChatIdentity identity, string targetuserid)
		{
			ChatPlaceUser chatPlaceUser = base.FindUser(identity.UniqueId);
			if (chatPlaceUser == null)
			{
				return;
			}
			IChatUserInfo userInfo = base.Portal.DataManager.GetUserInfo(targetuserid);
			if (userInfo == null)
			{
				return;
			}
			base.Manager.PushSTCMessage(chatPlaceUser.Connection, "OPENCONTACT", null, new string[] { userInfo.UserId, userInfo.DisplayName });
		}
	}
}