using System;

namespace CuteChat
{
	public class ServerMessages
	{
		public const string InvalidArgument = "INVALIDARGUMENT";

		public const string InvalidIdentity = "INVALIDIDENTITY";

		public const string InvalidCommand = "INVALIDCOMMAND";

		public const string InvalidTarget = "INVALIDTARGET";

		public const string InvalidObject = "INVATEOBJECT";

		public const string InvalidStatus = "INVATESTATUS";

		public const string CommandOK = "COMMANDOK";

		public const string InvalidActionWhenAppearOffline = "You cannot send a message when your status is offline.";

		public ServerMessages()
		{
		}
	}
}