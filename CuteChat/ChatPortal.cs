using System;
using System.Collections;

namespace CuteChat
{
	public abstract class ChatPortal : IDisposable
	{
		private string @ӓ;

		private IChatPortalInfo @Ӕ;

		private ChatConnectionCollection @ӕ = new ChatConnectionCollection();

		private ChatDataManager @Ӗ;

		private ChatMessenger @ӗ;

		private Hashtable @Ә = new Hashtable(new CaseInsensitiveHashCodeProvider(), new CaseInsensitiveComparer());

		public ChatDataManager DataManager
		{
			get
			{
				if (this.@Ӗ == null)
				{
					this.@Ӗ = this.CreateDataManagerInstance();
				}
				return this.@Ӗ;
			}
		}

		public IChatPortalInfo Info
		{
			get
			{
				if (this.@Ӕ == null)
				{
					this.@Ӕ = this.DataManager.GetPortalInfo();
				}
				return this.@Ӕ;
			}
		}

		public bool IsMessengerStarted
		{
			get
			{
				return this.@ӗ != null;
			}
		}

		public ChatMessenger Messenger
		{
			get
			{
				if (this.@ӗ == null)
				{
					ChatMessenger chatMessenger = this.CreateMessengerInstance();
					chatMessenger.Init();
					this.@ӗ = chatMessenger;
					this.DataManager.AsyncLogEvent(this.@ӗ, null, "PLACEADDED", this.@ӗ.PlaceName);
				}
				return this.@ӗ;
			}
		}

		public string Name
		{
			get
			{
				return this.@ӓ;
			}
		}

		public ChatPortal(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			this.@ӓ = name;
		}

		private void @ә()
		{
			if (this.@ӗ != null)
			{
				this.DataManager.AsyncLogEvent(this.@ӗ, null, "PLACEREMOVED", this.@ӗ.PlaceName);
				this.@ӗ.Dispose();
				this.@ӗ = null;
			}
		}

		private void @ӛ(ChatChannel u0040ӓ)
		{
			this.@Ә.Remove(u0040ӓ.PlaceName);
			this.DataManager.AsyncLogEvent(u0040ӓ, null, "PLACEREMOVED", u0040ӓ.PlaceName);
			u0040ӓ.Dispose();
		}

		internal void @Ӛ(ChatPlace u0040ӓ)
		{
			if (u0040ӓ == this.@ӗ)
			{
				this.@ә();
				return;
			}
			ChatChannel chatChannel = u0040ӓ as ChatChannel;
			if (chatChannel == null)
			{
				throw new Exception(string.Concat("Unknown place:", u0040ӓ.PlaceName));
			}
			this.@ӛ(chatChannel);
		}

		protected internal virtual ChatConnection AcceptConnection(ChatIdentity identity)
		{
			ChatConnection chatConnection = this.CreateConnectionInstance(identity);
			this.@ӕ.Add(chatConnection);
			chatConnection.OnConnect();
			return chatConnection;
		}

		public void AddPlace(ChatPlace place)
		{
			string lower = place.PlaceName.ToLower();
			this.@Ә.Add(lower, place);
		}

		protected internal virtual void CloseConnection(ChatConnection conn, string reason)
		{
			this.@ӕ.Remove(conn.ObjectGuid);
			conn.OnDisconnect(reason);
		}

		public virtual ChatChannel CreateChannel(string name)
		{
			if (string.Compare(name, "SupportAgent", true) != 0)
			{
				return null;
			}
			return new SupportAgentChannel(this, "SupportAgent");
		}

		protected virtual ChatConnection CreateConnectionInstance(ChatIdentity identity)
		{
			return new ChatConnection(this, identity);
		}

		protected abstract ChatDataManager CreateDataManagerInstance();

		protected virtual ChatMessenger CreateMessengerInstance()
		{
			return new ChatMessenger(this);
		}

		public virtual void Dispose()
		{
			this.@ә();
			ChatChannel[] startedChannels = this.GetStartedChannels();
			for (int i = 0; i < (int)startedChannels.Length; i++)
			{
				this.@ӛ(startedChannels[i]);
			}
		}

		public ChatConnection FindConnection(ChatGuid guid)
		{
			return this.@ӕ[guid];
		}

		public ChatConnection[] FindConnection(ChatIdentity identity)
		{
			return this.@ӕ.Find(identity);
		}

		public ChatConnection[] GetAllConnections()
		{
			return this.@ӕ.ToArray();
		}

		public int GetConnectionCount()
		{
			return this.@ӕ.Count;
		}

		public ChatPlace GetPlace(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (string.Compare(name, "Messenger", true) == 0)
			{
				return this.Messenger;
			}
			string lower = name.ToLower();
			ChatChannel item = (ChatChannel)this.@Ә[lower];
			if (item == null)
			{
				item = this.CreateChannel(name);
				if (item == null)
				{
					return null;
				}
				item.Init();
				this.@Ә[lower] = item;
				this.DataManager.AsyncLogEvent(item, null, "PLACEADDED", item.PlaceName);
			}
			return item;
		}

		public string GetProperty(string name)
		{
			return this.Info.GetProperty(name);
		}

		public int GetProperty(string name, int defaultValue)
		{
			int num;
			string property = this.GetProperty(name);
			if (property == null || property == "")
			{
				return defaultValue;
			}
			try
			{
				num = int.Parse(property);
			}
			catch
			{
				num = defaultValue;
			}
			return num;
		}

		public ChatChannel[] GetStartedChannels()
		{
			ChatChannel[] chatChannelArray = new ChatChannel[this.@Ә.Count];
			this.@Ә.Values.CopyTo(chatChannelArray, 0);
			return chatChannelArray;
		}

		public bool IsPlaceStarted(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (string.Compare(name, "Messenger", true) == 0)
			{
				return this.@ӗ != null;
			}
			string lower = name.ToLower();
			if ((ChatChannel)this.@Ә[lower] != null)
			{
				return true;
			}
			return false;
		}

		public virtual void Load()
		{
		}

		public Hashtable LoadProperties()
		{
			return this.Info.LoadProperties();
		}

		public virtual void Maintain()
		{
			int i;
			if (this.IsMessengerStarted)
			{
				try
				{
					this.Messenger.Manager.Maintain();
					if (this.Messenger.IsExpired)
					{
						this.@ә();
					}
				}
				catch (Exception exception)
				{
					ChatSystem.Instance.LogException(exception);
				}
			}
			ChatChannel[] startedChannels = this.GetStartedChannels();
			for (i = 0; i < (int)startedChannels.Length; i++)
			{
				ChatChannel chatChannel = startedChannels[i];
				try
				{
					chatChannel.Manager.Maintain();
					if (chatChannel.IsExpired)
					{
						this.@ӛ(chatChannel);
					}
				}
				catch (Exception exception1)
				{
					ChatSystem.Instance.LogException(exception1);
				}
			}
			ChatConnection[] expiredConnections = this.@ӕ.GetExpiredConnections();
			for (i = 0; i < (int)expiredConnections.Length; i++)
			{
				this.CloseConnection(expiredConnections[i], "EXPIRED");
			}
			this.DataManager.Maintain();
		}

		public void ResetInfo()
		{
			this.@Ӕ = null;
		}

		public void SetProperty(string name, string value)
		{
			this.Info.SetProperty(name, value);
			this.DataManager.UpdatePortalInfo(this.Info);
		}
	}
}