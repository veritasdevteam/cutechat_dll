using System;

namespace CuteChat
{
	public interface IChatRule
	{
		string Category
		{
			get;
			set;
		}

		bool Disabled
		{
			get;
			set;
		}

		string Expression
		{
			get;
			set;
		}

		string Mode
		{
			get;
			set;
		}

		int RuleId
		{
			get;
		}

		int Sort
		{
			get;
			set;
		}
	}
}