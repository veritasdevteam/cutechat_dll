using System;
using System.ComponentModel;

namespace CuteChat
{
	public class ChatLicenceProvider : LicenseProvider
	{
		public ChatLicenceProvider()
		{
		}

		public override License GetLicense(LicenseContext context, Type type, object instance, bool allowExceptions)
		{
			return (new @Ԑ()
			{
				ProductType = EnumChatLicense.CuteChat
			}).GetLicense(context, type, instance, allowExceptions);
		}
	}
}