using System;
using System.Collections;
using System.Reflection;

namespace CuteChat
{
	public class StringTable
	{
		private ArrayList @ӓ = new ArrayList();

		private Hashtable @Ӕ = new Hashtable();

		public int this[string msg]
		{
			get
			{
				int item = (int)this.@Ӕ[msg];
				if (item == -1)
				{
					throw new Exception(string.Concat("item not found for '", msg, "'"));
				}
				return item;
			}
		}

		public string this[int id]
		{
			get
			{
				return (string)this.@ӓ[id];
			}
		}

		public StringTable()
		{
		}

		public int Add(string msg)
		{
			if (msg == null)
			{
				throw new ArgumentNullException("msg");
			}
			int count = this.@ӓ.Count + 1;
			this.@Ӕ.Add(msg, count);
			this.@ӓ.Add(msg);
			return count;
		}

		public string[] ToArray()
		{
			return (string[])this.@ӓ.ToArray(typeof(string));
		}
	}
}