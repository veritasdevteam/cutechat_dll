using System;

namespace CuteChat
{
	public class ChatPortalDataItem : IDisposable
	{
		private ChatPortal @ӓ;

		private DateTime @Ӕ = DateTime.Now;

		private ChatGuid @ӕ = new ChatGuid();

		public ChatGuid ObjectGuid
		{
			get
			{
				return this.@ӕ;
			}
		}

		public DateTime ObjectTime
		{
			get
			{
				return this.@Ӕ;
			}
		}

		public ChatPortal Portal
		{
			get
			{
				return this.@ӓ;
			}
		}

		public ChatPortalDataItem(ChatPortal portal)
		{
			if (portal == null)
			{
				throw new ArgumentNullException("portal");
			}
			this.@ӓ = portal;
		}

		public virtual void Dispose()
		{
		}
	}
}