using System;
using System.Collections;

namespace CuteChat
{
	[Serializable]
	public class ChatResponse
	{
		public ChatCookie Cookie;

		public int QueuePosition = -1;

		public string ReturnCode = "READY";

		public string ServerMessage = "";

		public ArrayList Messages = new ArrayList();

		public ChatResponse()
		{
		}
	}
}