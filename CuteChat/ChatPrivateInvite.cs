using System;
using System.Collections;

namespace CuteChat
{
	public class ChatPrivateInvite : ChatPlaceItem
	{
		private ChatIdentity @ӓ;

		private ArrayList @Ӕ = new ArrayList();

		private ArrayList @ӕ = new ArrayList();

		private ChatPrivateChannel @Ӗ;

		public int InviteCount
		{
			get
			{
				return this.@Ӕ.Count;
			}
		}

		public ChatIdentity Sender
		{
			get
			{
				return this.@ӓ;
			}
		}

		public ChatPrivateInvite(ChatPlace place, ChatIdentity sender) : base(place)
		{
			this.@ӓ = sender;
		}

		private void @ӗ(ChatIdentity u0040ӓ)
		{
			this.@ӕ.Remove(u0040ӓ);
			if (this.@ӕ.Count == 0)
			{
				base.Place.Manager.RemovePlaceItem(this, "CLEAR");
			}
			base.Place.Manager.UpdatePlaceItem(this);
		}

		public void AddInvite(ChatIdentity identity)
		{
			if (!this.@Ӕ.Contains(identity))
			{
				this.@Ӕ.Add(identity);
				this.@ӕ.Add(identity);
			}
		}

		protected override bool GetItemInfo(ChatConnection conn, string formsg, out string type, out string[] args)
		{
			type = "PRIVATEINVITE";
			string[] str = new string[] { base.ObjectGuid.ToString(), this.@Ӗ.PlaceName, this.@ӓ.UniqueId, this.@ӓ.DisplayName, null };
			str[4] = this.@Ӕ.Count.ToString();
			args = str;
			return true;
		}

		public override bool IsVisibleConnection(ChatConnection conn)
		{
			return this.@ӕ.Contains(conn.Identity);
		}

		public void OnAccept(ChatIdentity iden)
		{
			this.@ӗ(iden);
		}

		protected internal override void OnAdded()
		{
			if (this.@Ӗ == null)
			{
				this.@Ӗ = new ChatPrivateChannel(base.Portal, string.Concat("private-", base.ObjectGuid), this);
				base.Portal.AddPlace(this.@Ӗ);
			}
			base.OnAdded();
		}

		protected internal override void OnConnectionRemoved(ChatConnection conn)
		{
			base.OnConnectionRemoved(conn);
			this.@ӗ(conn.Identity);
		}

		public void OnReject(ChatIdentity iden)
		{
			this.@ӗ(iden);
		}

		public void SetPrivateChannel(ChatPrivateChannel channel)
		{
			this.@Ӗ = channel;
		}
	}
}