using System;
using System.Collections;

namespace CuteChat
{
	public abstract class ChatDataManager : ChatPortalDataItem
	{
		private Hashtable @ӓ = new Hashtable();

		private IChatPortalInfo @Ӕ;

		private Queue @ӕ = new Queue();

		public ChatDataManager(ChatPortal portal) : base(portal)
		{
		}

		public virtual void AddAgent(string department, string userid)
		{
			SupportDepartment[] departments = this.GetDepartments();
			for (int i = 0; i < (int)departments.Length; i++)
			{
				SupportDepartment supportDepartment = departments[i];
				if (string.Compare(supportDepartment.Name, department, true) == 0)
				{
					this.AddAgent(supportDepartment.DepartmentId, userid);
					this.OnDepartmentChanged();
					return;
				}
			}
		}

		public void AddAgent(int departmentid, string userid)
		{
			if (userid == null)
			{
				throw new ArgumentNullException("userid");
			}
			SupportDepartment supportDepartment = null;
			SupportDepartment[] departments = this.GetDepartments();
			int i = 0;
			while (i < (int)departments.Length)
			{
				SupportDepartment supportDepartment1 = departments[i];
				if (supportDepartment1.DepartmentId != departmentid)
				{
					i++;
				}
				else
				{
					supportDepartment = supportDepartment1;
					break;
				}
			}
			if (supportDepartment == null)
			{
				throw new ArgumentException("Department not exists");
			}
			SupportAgent[] agents = supportDepartment.Agents;
			for (i = 0; i < (int)agents.Length; i++)
			{
				if (string.Compare(userid, agents[i].UserId, true) == 0)
				{
					return;
				}
			}
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				chatDataProvider.AddAgent(departmentid, userid);
			}
			this.OnDepartmentChanged();
		}

		public virtual void AddContact(ChatIdentity identity, string userid)
		{
			IChatUserInfo userInfo = this.GetUserInfo(identity.UniqueId);
			IChatUserInfo[] contacts = this.GetContacts(identity);
			string[] userId = new string[1 + (contacts == null ? 0 : (int)contacts.Length)];
			for (int i = 0; i < (int)contacts.Length; i++)
			{
				IChatUserInfo chatUserInfo = contacts[i];
				if (string.Compare(chatUserInfo.UserId, userid, true) == 0)
				{
					return;
				}
				userId[i] = chatUserInfo.UserId;
			}
			userId[(int)contacts.Length] = userid;
			userInfo.BuildinContacts = ChatSerialize.Serialize(userId);
			this.UpdateUserInfo(userInfo);
			this.OnContactAdded(identity, userid);
		}

		public virtual void AddDepartment(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			SupportDepartment[] departments = this.GetDepartments();
			for (int i = 0; i < (int)departments.Length; i++)
			{
				if (string.Compare(name, departments[i].Name, true) == 0)
				{
					return;
				}
			}
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				chatDataProvider.AddDepartment(name);
			}
			this.OnDepartmentChanged();
		}

		public virtual void AddFeedback(SupportFeedback feedback)
		{
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				chatDataProvider.AddFeedback(feedback);
			}
		}

		public virtual void AddIgnore(ChatIdentity identity, string userid)
		{
			IChatUserInfo userInfo = this.GetUserInfo(identity.UniqueId);
			IChatUserInfo[] ignores = this.GetIgnores(identity);
			string[] userId = new string[1 + (ignores == null ? 0 : (int)ignores.Length)];
			for (int i = 0; i < (int)ignores.Length; i++)
			{
				IChatUserInfo chatUserInfo = ignores[i];
				if (string.Compare(chatUserInfo.UserId, userid, true) == 0)
				{
					return;
				}
				userId[i] = chatUserInfo.UserId;
			}
			userId[(int)ignores.Length] = userid;
			userInfo.BuildinIgnores = ChatSerialize.Serialize(userId);
			this.UpdateUserInfo(userInfo);
			this.OnIgnoreAdded(identity, userid);
		}

		public void AsyncLogEvent(ChatPlace place, ChatIdentity user, string category, string message)
		{
		}

		public virtual void AsyncLogInstantMessage(ChatIdentity sender, string targetid, string targetname, bool offline, string text, string html)
		{
			ChatDataManager.@Ӕ _u0040Ӕ = new ChatDataManager.@Ӕ()
			{
				sender = sender,
				targetid = targetid,
				targetname = targetname,
				offline = offline,
				text = text,
				html = html
			};
			this.PushHandler(_u0040Ӕ);
		}

		public virtual void AsyncLogMessage(ChatPlace place, ChatIdentity sender, ChatIdentity target, bool whisper, string text, string html)
		{
			ChatDataManager.@ӕ _u0040ӕ = new ChatDataManager.@ӕ()
			{
				place = place,
				sender = sender,
				target = target,
				whisper = whisper,
				text = text,
				html = html
			};
			this.PushHandler(_u0040ӕ);
		}

		public virtual void AsyncLogSupportMessage(ChatPlace place, SupportSession session, string type, ChatIdentity sender, ChatIdentity target, string text, string html)
		{
			ChatDataManager.@ӗ _u0040ӗ = new ChatDataManager.@ӗ()
			{
				place = place,
				session = session,
				type = type,
				sender = sender,
				target = target,
				text = text,
				html = html
			};
			this.PushHandler(_u0040ӗ);
		}

		public virtual bool CheckIP(string ipaddr)
		{
			bool flag;
			if (ipaddr == null)
			{
				throw new ArgumentNullException("ipaddr");
			}
			ICollection pRules = this.GetIPRules();
			if (pRules.Count == 0)
			{
				return true;
			}
			ArrayList arrayLists = new ArrayList(pRules);
			arrayLists.Sort(new ChatDataManager.@ӓ());
			IEnumerator enumerator = arrayLists.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					IChatRule current = (IChatRule)enumerator.Current;
					if (!IPChecker.TestIP(current.Expression, ipaddr))
					{
						continue;
					}
					if (current.Mode != "Deny")
					{
						flag = true;
						return flag;
					}
					else
					{
						flag = false;
						return flag;
					}
				}
				return true;
			}
			finally
			{
				IDisposable disposable = enumerator as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}
			return flag;
		}

		protected abstract IChatDataProvider CreateDataProvider();

		public virtual void CreateLobby(IChatLobby lobby)
		{
			if (lobby == null)
			{
				throw new ArgumentNullException("lobby");
			}
			if (lobby.LobbyId != -1)
			{
				throw new Exception("Object already be inserted");
			}
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				chatDataProvider.CreateLobby(lobby);
			}
		}

		public abstract IChatLobby CreateLobbyInstance();

		public virtual void CreateRule(IChatRule rule)
		{
			if (rule == null)
			{
				throw new ArgumentNullException("rule");
			}
			if (rule.RuleId != -1)
			{
				throw new Exception("Object already be inserted");
			}
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				chatDataProvider.CreateRule(rule);
			}
		}

		public abstract IChatRule CreateRuleInstance();

		public virtual void CreateSession(SupportSession session)
		{
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				chatDataProvider.CreateSession(session);
			}
		}

		public virtual void DeleteAgentSessions(DateTime start, DateTime endtime, string agentid, string departmentid, string customer, string email, string ipaddr, string culture)
		{
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				chatDataProvider.DeleteAgentSessions(start, endtime, agentid, departmentid, customer, email, ipaddr, culture);
			}
		}

		public virtual void DeleteFeedback(int feedbackid)
		{
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				chatDataProvider.DeleteFeedback(feedbackid);
			}
		}

		public virtual void DeleteInstantMessages(string ownerid, string targetid)
		{
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				chatDataProvider.DeleteInstantMessages(ownerid, targetid);
			}
		}

		public virtual void DeleteLobby(IChatLobby lobby)
		{
			if (lobby == null)
			{
				throw new ArgumentNullException("lobby");
			}
			if (lobby.LobbyId == -1)
			{
				throw new Exception("Object is not inserted");
			}
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				chatDataProvider.DeleteLobby(lobby);
			}
			ChatChannel[] startedChannels = base.Portal.GetStartedChannels();
			for (int i = 0; i < (int)startedChannels.Length; i++)
			{
				ChatLobbyChannel chatLobbyChannel = startedChannels[i] as ChatLobbyChannel;
				if (chatLobbyChannel != null && chatLobbyChannel.Lobby.LobbyId == lobby.LobbyId)
				{
					chatLobbyChannel.OnLobbyDeleted();
				}
			}
		}

		public void DeleteMessagesBeforeDays(int days)
		{
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				chatDataProvider.DeleteMessagesBeforeDays(days);
			}
		}

		public virtual void DeleteRule(IChatRule rule)
		{
			if (rule == null)
			{
				throw new ArgumentNullException("rule");
			}
			if (rule.RuleId == -1)
			{
				throw new Exception("Object is not inserted");
			}
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				chatDataProvider.DeleteRule(rule);
			}
		}

		public IChatRule[] GetBadWords()
		{
			return this.GetRules("BadWord");
		}

		public virtual IChatUserInfo[] GetContacts(ChatIdentity identity)
		{
			object obj = ChatSerialize.Deserialize(this.GetUserInfo(identity.UniqueId).BuildinContacts);
			if (obj == null)
			{
				return new IChatUserInfo[0];
			}
			ArrayList arrayLists = new ArrayList();
			string[] strArrays = (string[])obj;
			for (int i = 0; i < (int)strArrays.Length; i++)
			{
				string str = strArrays[i];
				arrayLists.Add(this.GetUserInfo(str));
			}
			return (IChatUserInfo[])arrayLists.ToArray(typeof(IChatUserInfo));
		}

		public virtual string GetCustomerData(string userid)
		{
			string customerData;
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				customerData = chatDataProvider.GetCustomerData(userid);
			}
			return customerData;
		}

		public virtual int GetCustomerSessionCount(string userid)
		{
			int customerSessionCount;
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				customerSessionCount = chatDataProvider.GetCustomerSessionCount(userid);
			}
			return customerSessionCount;
		}

		public virtual SupportDepartment[] GetDepartments()
		{
			SupportDepartment[] supportDepartmentArray;
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				supportDepartmentArray = chatDataProvider.LoadDepartments();
			}
			return supportDepartmentArray;
		}

		public virtual IChatUserInfo[] GetIgnores(ChatIdentity identity)
		{
			object obj = ChatSerialize.Deserialize(this.GetUserInfo(identity.UniqueId).BuildinIgnores);
			if (obj == null)
			{
				return new IChatUserInfo[0];
			}
			ArrayList arrayLists = new ArrayList();
			string[] strArrays = (string[])obj;
			for (int i = 0; i < (int)strArrays.Length; i++)
			{
				string str = strArrays[i];
				arrayLists.Add(this.GetUserInfo(str));
			}
			return (IChatUserInfo[])arrayLists.ToArray(typeof(IChatUserInfo));
		}

		public IChatRule[] GetIPRules()
		{
			return this.GetRules("IPRule");
		}

		public virtual SupportSession GetLastSession(string customerid)
		{
			SupportSession lastSession;
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				lastSession = chatDataProvider.GetLastSession(customerid);
			}
			return lastSession;
		}

		public virtual IChatLobby[] GetLobbies()
		{
			IChatLobby[] lobbies;
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				lobbies = chatDataProvider.GetLobbies();
			}
			return lobbies;
		}

		public virtual IChatLobby GetLobby(int lobbyid)
		{
			IChatLobby lobby;
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				lobby = chatDataProvider.GetLobby(lobbyid);
			}
			return lobby;
		}

		public abstract string GetMemberDisplayName(string userid);

		public abstract string GetMemberUserName(string userid);

		public ChatMsgData[] GetMessages(string location, string placename, int pagesize, int pageindex, out int totalcount)
		{
			ChatMsgData[] messages;
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				messages = chatDataProvider.GetMessages(location, placename, pagesize, pageindex, out totalcount);
			}
			return messages;
		}

		public virtual IChatPortalInfo GetPortalInfo()
		{
			if (this.@Ӕ == null)
			{
				using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
				{
					this.@Ӕ = chatDataProvider.GetPortalInfo(base.Portal.Name);
				}
			}
			return this.@Ӕ;
		}

		public virtual IChatRule GetRule(int ruleid)
		{
			IChatRule rule;
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				rule = chatDataProvider.GetRule(ruleid);
			}
			return rule;
		}

		public virtual IChatRule[] GetRules()
		{
			IChatRule[] rules;
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				rules = chatDataProvider.GetRules();
			}
			return rules;
		}

		public virtual IChatRule[] GetRules(string category)
		{
			ArrayList arrayLists = new ArrayList();
			arrayLists.AddRange(this.GetRules());
			ArrayList arrayLists1 = new ArrayList();
			foreach (IChatRule arrayList in arrayLists)
			{
				if (arrayList.Category != category)
				{
					continue;
				}
				arrayLists1.Add(arrayList);
			}
			return (IChatRule[])arrayLists1.ToArray(typeof(IChatRule));
		}

		public virtual string GetSessionData(string sessionid)
		{
			SupportSession supportSession = this.SelectSession(sessionid);
			if (supportSession == null)
			{
				return null;
			}
			return supportSession.SessionData;
		}

		public virtual IChatUserInfo GetUserInfo(string userid)
		{
			IChatUserInfo item = (IChatUserInfo)this.@ӓ[userid];
			if (item == null)
			{
				using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
				{
					item = chatDataProvider.GetUserInfo(userid);
				}
				if (item.UserName == null)
				{
					item.UserName = this.GetMemberUserName(userid);
				}
				if (item.DisplayName == null)
				{
					item.DisplayName = this.GetMemberDisplayName(userid);
				}
				this.@ӓ[userid] = item;
			}
			return item;
		}

		public abstract bool IsAdministratorId(string userid);

		public virtual bool IsAgent(string userid)
		{
			SupportDepartment[] departments = this.GetDepartments();
			for (int i = 0; i < (int)departments.Length; i++)
			{
				SupportAgent[] agents = departments[i].Agents;
				for (int j = 0; j < (int)agents.Length; j++)
				{
					if (string.Compare(userid, agents[j].UserId, true) == 0)
					{
						return true;
					}
				}
			}
			return false;
		}

		public virtual bool IsLobbyAdminId(IChatLobby lobby, string userid)
		{
			return false;
		}

		public abstract string IsMemberDisplayName(string dispname);

		public abstract bool IsMemberId(string userid);

		public virtual SupportSession[] LoadAgentSessions(string agentid)
		{
			SupportSession[] supportSessionArray;
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				supportSessionArray = chatDataProvider.LoadAgentSessions(agentid);
			}
			return supportSessionArray;
		}

		public virtual ChatMsgData[] LoadChannelHistoryMessages(string channelname, int maxcount, DateTime afterdate)
		{
			ChatMsgData[] chatMsgDataArray;
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				chatMsgDataArray = chatDataProvider.LoadChannelHistoryMessages(channelname, maxcount, afterdate);
			}
			return chatMsgDataArray;
		}

		public virtual SupportSession[] LoadCustomerSessions(string customerid)
		{
			SupportSession[] supportSessionArray;
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				supportSessionArray = chatDataProvider.LoadCustomerSessions(customerid);
			}
			return supportSessionArray;
		}

		public virtual SupportFeedback LoadFeedback(int feedbackid)
		{
			SupportFeedback supportFeedback;
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				supportFeedback = chatDataProvider.LoadFeedback(feedbackid);
			}
			return supportFeedback;
		}

		public virtual SupportFeedback[] LoadFeedbacks(bool history)
		{
			SupportFeedback[] supportFeedbackArray;
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				supportFeedbackArray = chatDataProvider.LoadFeedbacks(history);
			}
			return supportFeedbackArray;
		}

		public virtual ChatMsgData[] LoadInstantMessages(string ownerid, string targetid, bool offlineOnly, int maxid, int pagesize)
		{
			ChatMsgData[] chatMsgDataArray;
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				chatMsgDataArray = chatDataProvider.LoadInstantMessages(ownerid, targetid, offlineOnly, maxid, pagesize);
			}
			return chatMsgDataArray;
		}

		public virtual ChatMsgData[] LoadInstantOfflineMessages(string targetid, DateTime time)
		{
			ChatMsgData[] chatMsgDataArray;
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				chatMsgDataArray = chatDataProvider.LoadInstantOfflineMessages(targetid, time);
			}
			return chatMsgDataArray;
		}

		public virtual ChatMsgData[] LoadSessionMessages(int sessionid)
		{
			ChatMsgData[] chatMsgDataArray;
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				chatMsgDataArray = chatDataProvider.LoadSessionMessages(sessionid);
			}
			return chatMsgDataArray;
		}

		public virtual string LoadSetting(string name)
		{
			string str;
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				str = chatDataProvider.LoadSetting(name);
			}
			return str;
		}

		public virtual void Maintain()
		{
			if (this.@ӕ.Count != 0)
			{
				using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
				{
					while (this.@ӕ.Count != 0)
					{
						IAsyncDataHandler asyncDataHandler = (IAsyncDataHandler)this.@ӕ.Dequeue();
						try
						{
							asyncDataHandler.Process(chatDataProvider);
						}
						catch (Exception exception)
						{
							ChatSystem.Instance.LogException(exception);
						}
					}
				}
			}
		}

		public virtual void OnContactAdded(ChatIdentity identity, string userid)
		{
			ChatConnection[] chatConnectionArray = base.Portal.FindConnection(identity);
			for (int i = 0; i < (int)chatConnectionArray.Length; i++)
			{
				ChatConnection chatConnection = chatConnectionArray[i];
				if (chatConnection.PlaceUser != null)
				{
					chatConnection.PlaceUser.OnContactAdded(userid);
				}
			}
		}

		public virtual void OnContactRemoved(ChatIdentity identity, string userid)
		{
			ChatConnection[] chatConnectionArray = base.Portal.FindConnection(identity);
			for (int i = 0; i < (int)chatConnectionArray.Length; i++)
			{
				ChatConnection chatConnection = chatConnectionArray[i];
				if (chatConnection.PlaceUser != null)
				{
					chatConnection.PlaceUser.OnContactRemoved(userid);
				}
			}
		}

		public virtual void OnDepartmentChanged()
		{
			((SupportAgentChannel)base.Portal.GetPlace("SupportAgent")).SendDepartmentTree();
		}

		public virtual void OnIgnoreAdded(ChatIdentity identity, string userid)
		{
			ChatConnection[] chatConnectionArray = base.Portal.FindConnection(identity);
			for (int i = 0; i < (int)chatConnectionArray.Length; i++)
			{
				ChatConnection chatConnection = chatConnectionArray[i];
				if (chatConnection.PlaceUser != null)
				{
					chatConnection.PlaceUser.OnIgnoreAdded(userid);
				}
			}
		}

		public virtual void OnIgnoreRemoved(ChatIdentity identity, string userid)
		{
			ChatConnection[] chatConnectionArray = base.Portal.FindConnection(identity);
			for (int i = 0; i < (int)chatConnectionArray.Length; i++)
			{
				ChatConnection chatConnection = chatConnectionArray[i];
				if (chatConnection.PlaceUser != null)
				{
					chatConnection.PlaceUser.OnIgnoreRemoved(userid);
				}
			}
		}

		protected void PushHandler(IAsyncDataHandler handler)
		{
			this.@ӕ.Enqueue(handler);
		}

		public virtual void RemoveAgent(string department, string userid)
		{
			SupportDepartment[] departments = this.GetDepartments();
			for (int i = 0; i < (int)departments.Length; i++)
			{
				SupportDepartment supportDepartment = departments[i];
				if (string.Compare(supportDepartment.Name, department, true) == 0)
				{
					this.RemoveAgent(supportDepartment.DepartmentId, userid);
					this.OnDepartmentChanged();
					return;
				}
			}
		}

		public void RemoveAgent(int departmentid, string userid)
		{
			if (userid == null)
			{
				throw new ArgumentNullException("userid");
			}
			SupportDepartment supportDepartment = null;
			SupportDepartment[] departments = this.GetDepartments();
			int i = 0;
			while (i < (int)departments.Length)
			{
				SupportDepartment supportDepartment1 = departments[i];
				if (supportDepartment1.DepartmentId != departmentid)
				{
					i++;
				}
				else
				{
					supportDepartment = supportDepartment1;
					break;
				}
			}
			if (supportDepartment == null)
			{
				throw new ArgumentException("Department not exists");
			}
			SupportAgent[] agents = supportDepartment.Agents;
			for (i = 0; i < (int)agents.Length; i++)
			{
				if (string.Compare(userid, agents[i].UserId, true) == 0)
				{
					using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
					{
						chatDataProvider.RemoveAgent(departmentid, userid);
					}
					this.OnDepartmentChanged();
					return;
				}
			}
		}

		public virtual void RemoveContact(ChatIdentity identity, string userid)
		{
			IChatUserInfo userInfo = this.GetUserInfo(identity.UniqueId);
			string[] strArrays = (string[])ChatSerialize.Deserialize(userInfo.BuildinContacts);
			if (strArrays == null)
			{
				return;
			}
			for (int i = 0; i < (int)strArrays.Length; i++)
			{
				if (string.Compare(strArrays[i], userid, true) == 0)
				{
					string[] strArrays1 = new string[(int)strArrays.Length - 1];
					for (int j = 0; j < i; j++)
					{
						strArrays1[j] = strArrays[j];
					}
					for (int k = i + 1; k < (int)strArrays.Length; k++)
					{
						strArrays1[k - 1] = strArrays[k];
					}
					userInfo.BuildinContacts = ChatSerialize.Serialize(strArrays1);
					this.UpdateUserInfo(userInfo);
					this.OnContactRemoved(identity, userid);
					return;
				}
			}
		}

		public virtual void RemoveDepartment(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			SupportDepartment[] departments = this.GetDepartments();
			for (int i = 0; i < (int)departments.Length; i++)
			{
				SupportDepartment supportDepartment = departments[i];
				if (string.Compare(name, supportDepartment.Name, true) == 0)
				{
					using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
					{
						chatDataProvider.RemoveDepartment(supportDepartment.DepartmentId);
					}
					this.OnDepartmentChanged();
					return;
				}
			}
		}

		public virtual void RemoveIgnore(ChatIdentity identity, string userid)
		{
			IChatUserInfo userInfo = this.GetUserInfo(identity.UniqueId);
			string[] strArrays = (string[])ChatSerialize.Deserialize(userInfo.BuildinIgnores);
			if (strArrays == null)
			{
				return;
			}
			for (int i = 0; i < (int)strArrays.Length; i++)
			{
				if (string.Compare(strArrays[i], userid, true) == 0)
				{
					string[] strArrays1 = new string[(int)strArrays.Length - 1];
					for (int j = 0; j < i; j++)
					{
						strArrays1[j] = strArrays[j];
					}
					for (int k = i + 1; k < (int)strArrays.Length; k++)
					{
						strArrays1[k - 1] = strArrays[k];
					}
					userInfo.BuildinIgnores = ChatSerialize.Serialize(strArrays1);
					this.UpdateUserInfo(userInfo);
					this.OnIgnoreRemoved(identity, userid);
					return;
				}
			}
		}

		public virtual void RenameDepartment(string name, string newname)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			SupportDepartment[] departments = this.GetDepartments();
			for (int i = 0; i < (int)departments.Length; i++)
			{
				SupportDepartment supportDepartment = departments[i];
				if (string.Compare(name, supportDepartment.Name, true) == 0)
				{
					using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
					{
						chatDataProvider.RenameDepartment(supportDepartment.DepartmentId, newname);
					}
					this.OnDepartmentChanged();
					return;
				}
			}
		}

		public virtual void SaveSetting(string name, string data)
		{
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				chatDataProvider.SaveSetting(name, data);
			}
		}

		public virtual SupportSession[] SearchAgentSessions(DateTime start, DateTime endtime, string agentid, string departmentid, string customer, string email, string ipaddr, string culture)
		{
			SupportSession[] supportSessionArray;
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				supportSessionArray = chatDataProvider.SearchAgentSessions(start, endtime, agentid, departmentid, customer, email, ipaddr, culture);
			}
			return supportSessionArray;
		}

		public virtual SupportSession SelectSession(string sessionid)
		{
			SupportSession supportSession;
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				supportSession = chatDataProvider.SelectSession(sessionid);
			}
			return supportSession;
		}

		public virtual void SetCutomerData(string userid, string data)
		{
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				chatDataProvider.SetCustomerData(userid, data);
			}
		}

		public virtual void SetSessionData(string sessionid, string data)
		{
			SupportSession supportSession = this.SelectSession(sessionid);
			if (supportSession == null)
			{
				return;
			}
			supportSession.SessionData = data;
			this.UpdateSession(supportSession);
		}

		public virtual void UpdateFeedback(SupportFeedback feedback)
		{
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				chatDataProvider.UpdateFeedback(feedback);
			}
		}

		public virtual void UpdateLobby(IChatLobby lobby)
		{
			if (lobby == null)
			{
				throw new ArgumentNullException("lobby");
			}
			if (lobby.LobbyId == -1)
			{
				throw new Exception("Object is not inserted");
			}
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				chatDataProvider.UpdateLobby(lobby);
			}
			ChatChannel[] startedChannels = base.Portal.GetStartedChannels();
			for (int i = 0; i < (int)startedChannels.Length; i++)
			{
				ChatLobbyChannel chatLobbyChannel = startedChannels[i] as ChatLobbyChannel;
				if (chatLobbyChannel != null && chatLobbyChannel.Lobby.LobbyId == lobby.LobbyId)
				{
					chatLobbyChannel.OnLobbyUpdated(lobby);
				}
			}
		}

		public virtual void UpdatePortalInfo(IChatPortalInfo info)
		{
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				chatDataProvider.UpdatePortalInfo(info);
			}
		}

		public virtual void UpdateRule(IChatRule rule)
		{
			if (rule == null)
			{
				throw new ArgumentNullException("rule");
			}
			if (rule.RuleId == -1)
			{
				throw new Exception("Object is not inserted");
			}
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				chatDataProvider.UpdateRule(rule);
			}
		}

		public virtual void UpdateSession(SupportSession session)
		{
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				chatDataProvider.UpdateSession(session);
			}
		}

		public virtual void UpdateUserInfo(IChatUserInfo info)
		{
			if (info.DisplayName != null && info.DisplayName.Length > 40)
			{
				info.DisplayName = info.DisplayName.Substring(0, 40);
			}
			if (info.Description != null && info.Description.Length > 150)
			{
				info.Description = info.Description.Substring(0, 150);
			}
			using (IChatDataProvider chatDataProvider = this.CreateDataProvider())
			{
				chatDataProvider.UpdateUserInfo(info);
			}
		}

		private class @ӓ : IComparer
		{
			public @ӓ()
			{
			}

			public int Compare(object x, object y)
			{
				IChatRule chatRule = (IChatRule)x;
				IChatRule chatRule1 = (IChatRule)y;
				if (chatRule.Sort == chatRule1.Sort)
				{
					return 0;
				}
				return chatRule.Sort - chatRule1.Sort;
			}
		}

		private class @ӗ : IAsyncDataHandler
		{
			public ChatPlace place;

			public SupportSession session;

			public string type;

			public ChatIdentity sender;

			public ChatIdentity target;

			public string text;

			public string html;

			public @ӗ()
			{
			}

			public void Process(IChatDataProvider provider)
			{
				provider.LogSupportMessage(this.place, this.session, this.type, this.sender, this.target, this.text, this.html);
			}
		}

		private class @Ӗ : IAsyncDataHandler
		{
			public ChatPlace place;

			public ChatIdentity user;

			public string category;

			public string message;

			public @Ӗ()
			{
			}

			public void Process(IChatDataProvider provider)
			{
				provider.LogEvent(this.place, this.user, this.category, this.message);
			}
		}

		private class @ӕ : IAsyncDataHandler
		{
			public ChatPlace place;

			public ChatIdentity sender;

			public ChatIdentity target;

			public bool whisper;

			public string text;

			public string html;

			public @ӕ()
			{
			}

			public void Process(IChatDataProvider provider)
			{
				provider.LogMessage(this.place, this.sender, this.target, this.whisper, this.text, this.html);
			}
		}

		private class @Ӕ : IAsyncDataHandler
		{
			public ChatIdentity sender;

			public string targetname;

			public string targetid;

			public bool offline;

			public string text;

			public string html;

			public @Ӕ()
			{
			}

			public void Process(IChatDataProvider provider)
			{
				provider.LogInstantMessage(this.sender, this.targetid, this.targetname, this.offline, this.text, this.html);
			}
		}
	}
}