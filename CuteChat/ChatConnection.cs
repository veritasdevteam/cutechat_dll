using System;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;
using System.Threading;

namespace CuteChat
{
	public class ChatConnection : ChatPortalDataItem
	{
		private string @ӓ;

		private ChatIdentity @Ӕ;

		internal string @ӕ;

		private Guid @Ӗ = Guid.NewGuid();

		private CuteChat.Strings @ӗ;

		private DateTime @Ә = DateTime.Now;

		private DateTime @ә = DateTime.Now;

		private DateTime @Ӛ = DateTime.Now;

		internal int @ӛ = 1;

		private int @Ӝ;

		private ChatPlace @ӝ;

		private ChatPlaceUser @Ӟ;

		private ArrayList @ӟ = new ArrayList();

		private ArrayList @Ӡ = new ArrayList();

		private int @ӡ
		{
			get
			{
				if (this.@Ӝ == 0)
				{
					base.Portal.GetProperty("ChannelOnlineExpires");
					int num = 60;
					try
					{
						num = int.Parse(base.Portal.GetProperty("ChannelOnlineExpires"));
					}
					catch
					{
					}
					if (num < 30 || num > 600)
					{
						num = 60;
					}
					this.@Ӝ = num;
				}
				return this.@Ӝ;
			}
		}

		public DateTime ActionTime
		{
			get
			{
				return this.@Ӛ;
			}
		}

		public DateTime ActivateTime
		{
			get
			{
				return this.@ә;
			}
		}

		public string CloseReason
		{
			get
			{
				return this.@ӕ;
			}
		}

		public Guid ConnectionKey
		{
			get
			{
				return this.@Ӗ;
			}
		}

		public string Culture
		{
			get
			{
				return this.@ӓ;
			}
		}

		public ChatIdentity Identity
		{
			get
			{
				return this.@Ӕ;
			}
		}

		public virtual bool IsExpired
		{
			get
			{
				TimeSpan now = DateTime.Now - this.@ә;
				return now.TotalSeconds > (double)this.@ӡ;
			}
		}

		public ChatPlace Place
		{
			get
			{
				return this.@ӝ;
			}
		}

		public ChatPlaceUser PlaceUser
		{
			get
			{
				return this.@Ӟ;
			}
		}

		public DateTime StartTime
		{
			get
			{
				return this.@Ә;
			}
		}

		public CuteChat.Strings Strings
		{
			get
			{
				if (this.@ӗ == null)
				{
					this.@ӗ = ChatSystem.Instance.GetStrings(this.@ӓ);
				}
				return this.@ӗ;
			}
		}

		public ChatConnection(ChatPortal portal, ChatIdentity identity) : base(portal)
		{
			if (identity == null)
			{
				throw new ArgumentNullException("identity");
			}
			this.@Ӕ = identity;
			this.@ӓ = Thread.CurrentThread.CurrentUICulture.Name;
		}

		internal virtual void InitUser(ChatPlace place)
		{
			this.@Ӟ = place.CreatePlaceUser(this);
		}

		protected internal virtual void OnAttachPlace(ChatPlace place)
		{
			if (this.@ӝ != null)
			{
				throw new Exception("place already attached");
			}
			this.@ӝ = place;
		}

		protected internal virtual void OnConnect()
		{
		}

		protected internal virtual void OnDetachPlace(string reason)
		{
			ChatPlace chatPlace = this.@ӝ;
			this.@ӝ = null;
			this.@Ӟ = null;
		}

		protected internal virtual void OnDisconnect(string reason)
		{
		}

		protected internal virtual string[] PopCTSMessages()
		{
			string[] array = (string[])this.@Ӡ.ToArray(typeof(string));
			this.@Ӡ.Clear();
			return array;
		}

		protected internal virtual string[] PopSTCMessages()
		{
			string[] array = (string[])this.@ӟ.ToArray(typeof(string));
			this.@ӟ.Clear();
			return array;
		}

		protected internal virtual void PushCTSMessage(string message)
		{
			this.@Ӡ.Add(message);
		}

		protected internal virtual void PushSTCMessage(string msgid, NameValueCollection nvc, string[] args)
		{
			string msg = ChatUtility.JoinToMsg(msgid, nvc, args);
			this.@ӟ.Add(msg);
		}

		protected internal virtual void PushSTCMessage(string message)
		{
			this.@ӟ.Add(message);
		}

		public void SetActivate(bool hasAction)
		{
			this.@ә = DateTime.Now;
			if (hasAction)
			{
				this.@Ӛ = DateTime.Now;
			}
		}

		protected internal virtual void SwitchUser(ChatPlaceUser user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			this.@Ӟ = user;
		}
	}
}