using System;

namespace Xceed.Compression.CompressionEngine.Managed
{
	internal enum CompressionStrategy
	{
		Z_DEFAULT_STRATEGY = 0,
		Z_FILTERED = 1,
		Z_HUFFMAN_ONLY = 2
	}
}