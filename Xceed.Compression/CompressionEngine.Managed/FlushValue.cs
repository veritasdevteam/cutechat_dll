using System;

namespace Xceed.Compression.CompressionEngine.Managed
{
	internal enum FlushValue
	{
		Z_NO_FLUSH,
		Z_PARTIAL_FLUSH,
		Z_SYNC_FLUSH,
		Z_FULL_FLUSH,
		Z_FINISH
	}
}