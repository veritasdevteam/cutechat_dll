using System;

namespace Xceed.Compression.CompressionEngine.Managed
{
	internal enum DataTypeValue
	{
		Z_BINARY,
		Z_ASCII,
		Z_UNKNOWN
	}
}