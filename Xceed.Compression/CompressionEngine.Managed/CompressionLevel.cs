using System;

namespace Xceed.Compression.CompressionEngine.Managed
{
	internal enum CompressionLevel
	{
		Z_DEFAULT_COMPRESSION = -1,
		Z_NO_COMPRESSION = 0,
		Z_BEST_SPEED = 1,
		Z_BEST_COMPRESSION = 9
	}
}