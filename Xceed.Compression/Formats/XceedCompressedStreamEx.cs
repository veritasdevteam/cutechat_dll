using System;
using System.IO;
using Xceed.Compression;

namespace Xceed.Compression.Formats
{
	public class XceedCompressedStreamEx : XceedCompressedStream
	{
		public XceedCompressedStreamEx(Stream inner, CompressionMethod method, CompressionLevel level) : base(inner, method, level, false, 1)
		{
		}

		public XceedCompressedStreamEx(Stream inner) : base(inner, CompressionMethod.Deflated, CompressionLevel.Highest, false, 1)
		{
		}

		public XceedCompressedStreamEx(Stream inner, CompressionMethod method, CompressionLevel level, bool readHeader) : base(inner, method, level, readHeader, 1)
		{
		}

		public static new byte[] Compress(byte[] buffer)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			return XceedCompressedStreamEx.Compress(buffer, 0, (int)buffer.Length, CompressionMethod.Deflated, CompressionLevel.Highest);
		}

		public static new byte[] Compress(byte[] buffer, CompressionMethod method, CompressionLevel level)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			return XceedCompressedStreamEx.Compress(buffer, 0, (int)buffer.Length, method, level);
		}

		public static new byte[] Compress(byte[] buffer, int offset, int count, CompressionMethod method, CompressionLevel level)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (offset < 0)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("offset", offset, "The offset parameter cannot be less than zero.");
			}
			if (count < 0)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count parameter cannot be less than zero.");
			}
			if (count > (int)buffer.Length - offset)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count parameter exceeds the buffer's remaining length after offset.");
			}
			if (!Enum.IsDefined(typeof(CompressionMethod), method))
			{
				@Ӗ.ThrowArgumentOutOfRangeException("method", method, "An attempt was made to use an unknown compression method.");
			}
			MemoryStream memoryStream = new MemoryStream();
			using (XceedCompressedStreamEx xceedCompressedStreamEx = new XceedCompressedStreamEx(memoryStream, method, level))
			{
				xceedCompressedStreamEx.Write(buffer, offset, count);
			}
			return memoryStream.ToArray();
		}

		public static new byte[] Decompress(byte[] buffer)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			return XceedCompressedStreamEx.Decompress(buffer, 0, (int)buffer.Length);
		}

		public static new byte[] Decompress(byte[] buffer, int offset, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (offset < 0)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("offset", offset, "The offset parameter cannot be less than zero.");
			}
			if (count < 0)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count parameter cannot be less than zero.");
			}
			if (count > (int)buffer.Length - offset)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count parameter exceeds the buffer's remaining length after offset.");
			}
			byte[] array = new byte[0];
			using (MemoryStream memoryStream = new MemoryStream(buffer, offset, count))
			{
				using (MemoryStream memoryStream1 = new MemoryStream())
				{
					using (XceedCompressedStreamEx xceedCompressedStreamEx = new XceedCompressedStreamEx(memoryStream))
					{
						byte[] numArray = new byte[32786];
						int num = 0;
						while (true)
						{
							int num1 = xceedCompressedStreamEx.Read(numArray, 0, (int)numArray.Length);
							num = num1;
							if (num1 <= 0)
							{
								break;
							}
							memoryStream1.Write(numArray, 0, num);
						}
					}
					array = memoryStream1.ToArray();
				}
			}
			return array;
		}
	}
}