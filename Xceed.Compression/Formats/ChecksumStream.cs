using System;
using System.IO;

namespace Xceed.Compression.Formats
{
	public class ChecksumStream : Stream
	{
		private bool @ӓ;

		private Stream @Ӕ;

		private uint @ӕ;

		private uint @Ӗ;

		private ChecksumType @ӗ = ChecksumType.CRC32;

		private readonly static uint[] @Ә;

		private static byte[] @ә;

		public override bool CanRead
		{
			get
			{
				this.@Ӝ();
				return this.@Ӕ.CanRead;
			}
		}

		public override bool CanSeek
		{
			get
			{
				this.@Ӝ();
				return this.@Ӕ.CanSeek;
			}
		}

		public override bool CanWrite
		{
			get
			{
				this.@Ӝ();
				return this.@Ӕ.CanWrite;
			}
		}

		public int Checksum
		{
			get
			{
				return (int)this.@ӕ;
			}
		}

		public int ExpectedChecksum
		{
			get
			{
				return (int)this.@Ӗ;
			}
			set
			{
				this.@Ӝ();
				this.@Ӗ = (uint)value;
			}
		}

		public override long Length
		{
			get
			{
				this.@Ӝ();
				return this.@Ӕ.Length;
			}
		}

		public override long Position
		{
			get
			{
				this.@Ӝ();
				return this.@Ӕ.Position;
			}
			set
			{
				this.@Ӝ();
				this.@Ӕ.Position = value;
			}
		}

		static ChecksumStream()
		{
			ChecksumStream.@Ә = new uint[] { 0, 1996959894, 3993919788, 2567524794, 124634137, 1886057615, 3915621685, 2657392035, 249268274, 2044508324, 3772115230, 2547177864, 162941995, 2125561021, 3887607047, 2428444049, 498536548, 1789927666, 4089016648, 2227061214, 450548861, 1843258603, 4107580753, 2211677639, 325883990, 1684777152, 4251122042, 2321926636, 335633487, 1661365465, 4195302755, 2366115317, 997073096, 1281953886, 3579855332, 2724688242, 1006888145, 1258607687, 3524101629, 2768942443, 901097722, 1119000684, 3686517206, 2898065728, 853044451, 1172266101, 3705015759, 2882616665, 651767980, 1373503546, 3369554304, 3218104598, 565507253, 1454621731, 3485111705, 3099436303, 671266974, 1594198024, 3322730930, 2970347812, 795835527, 1483230225, 3244367275, 3060149565, 1994146192, 31158534, 2563907772, 4023717930, 1907459465, 112637215, 2680153253, 3904427059, 2013776290, 251722036, 2517215374, 3775830040, 2137656763, 141376813, 2439277719, 3865271297, 1802195444, 476864866, 2238001368, 4066508878, 1812370925, 453092731, 2181625025, 4111451223, 1706088902, 314042704, 2344532202, 4240017532, 1658658271, 366619977, 2362670323, 4224994405, 1303535960, 984961486, 2747007092, 3569037538, 1256170817, 1037604311, 2765210733, 3554079995, 1131014506, 879679996, 2909243462, 3663771856, 1141124467, 855842277, 2852801631, 3708648649, 1342533948, 654459306, 3188396048, 3373015174, 1466479909, 544179635, 3110523913, 3462522015, 1591671054, 702138776, 2966460450, 3352799412, 1504918807, 783551873, 3082640443, 3233442989, 3988292384, 2596254646, 62317068, 1957810842, 3939845945, 2647816111, 81470997, 1943803523, 3814918930, 2489596804, 225274430, 2053790376, 3826175755, 2466906013, 167816743, 2097651377, 4027552580, 2265490386, 503444072, 1762050814, 4150417245, 2154129355, 426522225, 1852507879, 4275313526, 2312317920, 282753626, 1742555852, 4189708143, 2394877945, 397917763, 1622183637, 3604390888, 2714866558, 953729732, 1340076626, 3518719985, 2797360999, 1068828381, 1219638859, 3624741850, 2936675148, 906185462, 1090812512, 3747672003, 2825379669, 829329135, 1181335161, 3412177804, 3160834842, 628085408, 1382605366, 3423369109, 3138078467, 570562233, 1426400815, 3317316542, 2998733608, 733239954, 1555261956, 3268935591, 3050360625, 752459403, 1541320221, 2607071920, 3965973030, 1969922972, 40735498, 2617837225, 3943577151, 1913087877, 83908371, 2512341634, 3803740692, 2075208622, 213261112, 2463272603, 3855990285, 2094854071, 198958881, 2262029012, 4057260610, 1759359992, 534414190, 2176718541, 4139329115, 1873836001, 414664567, 2282248934, 4279200368, 1711684554, 285281116, 2405801727, 4167216745, 1634467795, 376229701, 2685067896, 3608007406, 1308918612, 956543938, 2808555105, 3495958263, 1231636301, 1047427035, 2932959818, 3654703836, 1088359270, 936918000, 2847714899, 3736837829, 1202900863, 817233897, 3183342108, 3401237130, 1404277552, 615818150, 3134207493, 3453421203, 1423857449, 601450431, 3009837614, 3294710456, 1567103746, 711928724, 3020668471, 3272380065, 1510334235, 755167117 };
			ChecksumStream.@ә = new byte[] { 89, 85, 106, 79, 91, 93, 27, 59, 150, 206, 63, 24, 13, 104, 92, 11, 105, 44, 40, 56, 141, 255, 60, 40, 115, 105, 86, 49, 87, 44, 41, 55, 132, 248, 77, 54, 78, 89, 106, 61, 81, 111, 47, 50, 255, 246, 56, 95, 125, 110, 95, 59, 46, 93, 27, 79, 139, 245, 10, 95, 69, 104, 106, 8, 106, 44, 120, 62, 128, 253, 63, 24, 120, 88, 86, 62, 80, 44, 123, 62, 141, 242, 14, 43, 120, 45, 83, 12, 81, 107, 40, 63, 188, 200, 10, 97, 13, 118, 89, 79, 85, 89, 93, 106, 142, 253, 38, 45, 118, 84, 85, 59, 46, 20, 24, 116, 255, 191, 125, 111, 56, 45, 18, 79, 102, 105, 26, 8, 187, 141, 24, 46, 75, 89, 68, 12, 92, 107, 93, 20, 129, 200, 115 };
		}

		public ChecksumStream(Stream inner, ChecksumType checksumType) : this(inner, checksumType, 0)
		{
		}

		public ChecksumStream(Stream inner, ChecksumType checksumType, int expectedChecksum)
		{
			if (inner == null)
			{
				throw new ArgumentNullException("inner");
			}
			if (!Enum.IsDefined(typeof(ChecksumType), checksumType))
			{
				throw new ArgumentException("An unknown checksum type was specified.", "checksumType");
			}
			this.@Ӕ = inner;
			this.@ӗ = checksumType;
			this.@Ӗ = (uint)expectedChecksum;
		}

		public ChecksumStream(Stream inner, ChecksumType checksumType, int expectedChecksum, int initialChecksum) : this(inner, checksumType, expectedChecksum)
		{
			this.@ӕ = (uint)initialChecksum;
		}

		private ChecksumStream()
		{
		}

		internal static uint @ӛ(uint u0040ӓ, byte[] u0040Ӕ, uint u0040ӕ, uint u0040Ӗ)
		{
			uint num;
			uint num1;
			uint num2 = u0040ӓ & 65535;
			uint num3 = u0040ӓ >> 16 & 65535;
			if (u0040Ӕ == null)
			{
				return (uint)1;
			}
			while (u0040Ӗ > 0)
			{
				if (u0040Ӗ < 5552)
				{
					num = u0040Ӗ;
				}
				else
				{
					num = 5552;
				}
				uint num4 = num;
				u0040Ӗ -= num4;
				while (num4 >= 16)
				{
					uint num5 = u0040ӕ;
					u0040ӕ = num5 + 1;
					num2 += u0040Ӕ[num5];
					num3 += num2;
					uint num6 = u0040ӕ;
					u0040ӕ = num6 + 1;
					num2 += u0040Ӕ[num6];
					num3 += num2;
					uint num7 = u0040ӕ;
					u0040ӕ = num7 + 1;
					num2 += u0040Ӕ[num7];
					num3 += num2;
					uint num8 = u0040ӕ;
					u0040ӕ = num8 + 1;
					num2 += u0040Ӕ[num8];
					num3 += num2;
					uint num9 = u0040ӕ;
					u0040ӕ = num9 + 1;
					num2 += u0040Ӕ[num9];
					num3 += num2;
					uint num10 = u0040ӕ;
					u0040ӕ = num10 + 1;
					num2 += u0040Ӕ[num10];
					num3 += num2;
					uint num11 = u0040ӕ;
					u0040ӕ = num11 + 1;
					num2 += u0040Ӕ[num11];
					num3 += num2;
					uint num12 = u0040ӕ;
					u0040ӕ = num12 + 1;
					num2 += u0040Ӕ[num12];
					num3 += num2;
					uint num13 = u0040ӕ;
					u0040ӕ = num13 + 1;
					num2 += u0040Ӕ[num13];
					num3 += num2;
					uint num14 = u0040ӕ;
					u0040ӕ = num14 + 1;
					num2 += u0040Ӕ[num14];
					num3 += num2;
					uint num15 = u0040ӕ;
					u0040ӕ = num15 + 1;
					num2 += u0040Ӕ[num15];
					num3 += num2;
					uint num16 = u0040ӕ;
					u0040ӕ = num16 + 1;
					num2 += u0040Ӕ[num16];
					num3 += num2;
					uint num17 = u0040ӕ;
					u0040ӕ = num17 + 1;
					num2 += u0040Ӕ[num17];
					num3 += num2;
					uint num18 = u0040ӕ;
					u0040ӕ = num18 + 1;
					num2 += u0040Ӕ[num18];
					num3 += num2;
					uint num19 = u0040ӕ;
					u0040ӕ = num19 + 1;
					num2 += u0040Ӕ[num19];
					num3 += num2;
					uint num20 = u0040ӕ;
					u0040ӕ = num20 + 1;
					num2 += u0040Ӕ[num20];
					num3 += num2;
					num4 -= 16;
				}
				if (num4 != 0)
				{
					do
					{
						uint num21 = u0040ӕ;
						u0040ӕ = num21 + 1;
						num2 += u0040Ӕ[num21];
						num3 += num2;
						num1 = num4 - 1;
						num4 = num1;
					}
					while (num1 != 0);
				}
				else if (ChecksumStream.@ә[num4] % 16 == 0)
				{
					num2 += ChecksumStream.@ә[num4];
					num3 -= ChecksumStream.@ә[num4];
				}
				num2 %= 65521;
				num3 %= 65521;
			}
			return num3 << 16 | num2;
		}

		internal static uint @Ӛ(byte[] u0040ӓ, int u0040Ӕ, int u0040ӕ, uint u0040Ӗ)
		{
			if (u0040ӕ == 0)
			{
				return u0040Ӗ;
			}
			u0040Ӗ ^= -1;
			u0040Ӕ--;
			while (true)
			{
				int num = u0040ӕ - 1;
				u0040ӕ = num;
				if (num < 0)
				{
					break;
				}
				int num1 = u0040Ӕ + 1;
				u0040Ӕ = num1;
				u0040Ӗ = ChecksumStream.@Ә[(u0040Ӗ ^ u0040ӓ[num1]) & 255] ^ u0040Ӗ >> 8;
			}
			return u0040Ӗ ^ -1;
		}

		private void @Ӝ()
		{
			if (this.@ӓ)
			{
				throw new ObjectDisposedException("ChecksumStream", "Cannot access a stream once it has been closed.");
			}
		}

		public static int CalculateAdler32(byte[] buffer, int offset, int count, int previousAdler)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (offset < 0)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("offset", offset, "The offset cannot be less than zero.");
			}
			if (count < 0)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count cannot be less than zero.");
			}
			if (count > (int)buffer.Length - offset)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count exceeds the buffer's remaining length after offset.");
			}
			if (count == 0)
			{
				return previousAdler;
			}
			return (int)ChecksumStream.@ӛ((uint)previousAdler, buffer, (uint)offset, (uint)count);
		}

		public static int CalculateCrc32(byte[] buffer, int offset, int count, int previousCrc)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (offset < 0)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("offset", offset, "The offset cannot be less than zero.");
			}
			if (count < 0)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count cannot be less than zero.");
			}
			if (count > (int)buffer.Length - offset)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count exceeds the buffer's remaining length after offset.");
			}
			if (count == 0)
			{
				return previousCrc;
			}
			return (int)ChecksumStream.@Ӛ(buffer, offset, count, (uint)previousCrc);
		}

		public override void Close()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual new void Dispose(bool disposing)
		{
			if (!this.@ӓ)
			{
				if (disposing)
				{
					this.@Ӕ.Close();
				}
				this.@Ӕ = null;
				this.@ӓ = true;
			}
		}

		protected override void Finalize()
		{
			try
			{
				this.Dispose(false);
			}
			finally
			{
				base.Finalize();
			}
		}

		public override void Flush()
		{
			this.@Ӝ();
			this.@Ӕ.Flush();
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			this.@Ӝ();
			if (!this.CanRead)
			{
				throw new NotSupportedException("The ChecksumStream object does not support reading.");
			}
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (offset < 0)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("offset", offset, "The offset cannot be less than zero.");
			}
			if (count < 0)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count cannot be less than zero.");
			}
			if (count > (int)buffer.Length - offset)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count exceeds the buffer's remaining length after offset.");
			}
			int num = this.@Ӕ.Read(buffer, offset, count);
			if (num > 0)
			{
				if (this.@ӗ != ChecksumType.CRC32)
				{
					this.@ӕ = ChecksumStream.@ӛ(this.@ӕ, buffer, (uint)offset, (uint)num);
				}
				else
				{
					this.@ӕ = ChecksumStream.@Ӛ(buffer, offset, num, this.@ӕ);
				}
			}
			else if (this.@Ӗ != 0 && this.@ӕ != this.@Ӗ)
			{
				throw new IOException("The checksum for the data read from the stream does not match the expected checksum.");
			}
			return num;
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			this.@Ӝ();
			return this.@Ӕ.Seek(offset, origin);
		}

		public override void SetLength(long value)
		{
			this.@Ӝ();
			this.@Ӕ.SetLength(value);
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			this.@Ӝ();
			if (!this.CanWrite)
			{
				throw new NotSupportedException("The ChecksumStream object does not support writing.");
			}
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (offset < 0)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("offset", offset, "The offset cannot be less than zero.");
			}
			if (count < 0)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count cannot be less than zero.");
			}
			if (count > (int)buffer.Length - offset)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count exceeds the buffer's remaining length after offset.");
			}
			if (count > 0)
			{
				if (this.@ӗ != ChecksumType.CRC32)
				{
					this.@ӕ = ChecksumStream.@ӛ(this.@ӕ, buffer, (uint)offset, (uint)count);
				}
				else
				{
					this.@ӕ = ChecksumStream.@Ӛ(buffer, offset, count, this.@ӕ);
				}
			}
			this.@Ӕ.Write(buffer, offset, count);
		}
	}
}