using System;

namespace Xceed.Compression.Formats
{
	public enum ChecksumType
	{
		Adler32,
		CRC32
	}
}