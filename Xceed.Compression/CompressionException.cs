using System;
using System.IO;

namespace Xceed.Compression
{
	public class CompressionException : IOException
	{
		public CompressionException(string message) : base(message)
		{
		}

		public CompressionException(string message, Exception inner) : base(message, inner)
		{
		}
	}
}