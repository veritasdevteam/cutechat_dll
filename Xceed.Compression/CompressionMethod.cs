using System;

namespace Xceed.Compression
{
	public enum CompressionMethod
	{
		Stored = 0,
		Deflated = 8,
		Deflated64 = 9
	}
}