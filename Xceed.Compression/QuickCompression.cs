using System;

namespace Xceed.Compression
{
	public class QuickCompression
	{
		private QuickCompression()
		{
		}

		public static byte[] Compress(byte[] buffer)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			return QuickCompression.Compress(buffer, 0, (int)buffer.Length, CompressionMethod.Deflated, CompressionLevel.Normal);
		}

		public static byte[] Compress(byte[] buffer, CompressionMethod method, CompressionLevel level)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			return QuickCompression.Compress(buffer, 0, (int)buffer.Length, method, level);
		}

		public static byte[] Compress(byte[] buffer, int offset, int count, CompressionMethod method, CompressionLevel level)
		{
			byte[] numArray;
			byte[] numArray1;
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (offset < 0)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("offset", offset, "The offset parameter cannot be less than zero.");
			}
			if (count < 0)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count parameter cannot be less than zero.");
			}
			if (count > (int)buffer.Length - offset)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count parameter exceeds the buffer's remaining length after offset.");
			}
			if (!Enum.IsDefined(typeof(CompressionMethod), method))
			{
				@Ӗ.ThrowArgumentOutOfRangeException("method", method, "The compression method is not a valid CompressionMethod member.");
			}
			if (method == CompressionMethod.Stored)
			{
				byte[] numArray2 = new byte[count];
				Array.Copy(buffer, offset, numArray2, 0, count);
				return numArray2;
			}
			if (method == CompressionMethod.Deflated)
			{
				int num = (new @Ӡ((int)level)).Compress(buffer, offset, count, true, out numArray);
				if (num < (int)numArray.Length)
				{
					byte[] numArray3 = new byte[num];
					Buffer.BlockCopy(numArray, 0, numArray3, 0, num);
					numArray = numArray3;
				}
				return numArray;
			}
			if (method != CompressionMethod.Deflated64)
			{
				throw new CompressionInternalException();
			}
			int num1 = (new @Ӟ((int)level)).Compress(buffer, offset, count, true, out numArray1);
			if (num1 < (int)numArray1.Length)
			{
				byte[] numArray4 = new byte[num1];
				Buffer.BlockCopy(numArray1, 0, numArray4, 0, num1);
				numArray1 = numArray4;
			}
			return numArray1;
		}

		public static byte[] Decompress(byte[] buffer)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			return QuickCompression.Decompress(buffer, 0, (int)buffer.Length, CompressionMethod.Deflated);
		}

		public static byte[] Decompress(byte[] buffer, CompressionMethod method)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			return QuickCompression.Decompress(buffer, 0, (int)buffer.Length, method);
		}

		public static byte[] Decompress(byte[] buffer, int offset, int count, CompressionMethod method)
		{
			byte[] numArray;
			byte[] numArray1;
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (offset < 0)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("offset", offset, "The offset parameter cannot be less than zero.");
			}
			if (count < 0)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count parameter cannot be less than zero.");
			}
			if (count > (int)buffer.Length - offset)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count parameter exceeds the buffer's remaining length after offset.");
			}
			if (!Enum.IsDefined(typeof(CompressionMethod), method))
			{
				@Ӗ.ThrowArgumentOutOfRangeException("method", method, "The compression method is not a valid DecompressionMethod member.");
			}
			if (method == CompressionMethod.Stored)
			{
				byte[] numArray2 = new byte[count];
				Array.Copy(buffer, offset, numArray2, 0, count);
				return numArray2;
			}
			if (method == CompressionMethod.Deflated)
			{
				bool flag = true;
				int num = 0;
				int num1 = (new @ӡ()).Decompress(buffer, offset, count, ref flag, out numArray, out num);
				if (num1 < (int)numArray.Length)
				{
					byte[] numArray3 = new byte[num1];
					Buffer.BlockCopy(numArray, 0, numArray3, 0, num1);
					numArray = numArray3;
				}
				return numArray;
			}
			if (method != CompressionMethod.Deflated64)
			{
				throw new CompressionInternalException();
			}
			bool flag1 = true;
			int num2 = 0;
			int num3 = (new @ӟ()).Decompress(buffer, offset, count, ref flag1, out numArray1, out num2);
			if (num3 < (int)numArray1.Length)
			{
				byte[] numArray4 = new byte[num3];
				Buffer.BlockCopy(numArray1, 0, numArray4, 0, num3);
				numArray1 = numArray4;
			}
			return numArray1;
		}
	}
}