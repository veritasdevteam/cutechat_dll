using System;
using System.IO;
using Xceed.Compression.Formats;

internal class @Ց : @Վ
{
	private GZipCompressedStream @ӓ;

	public override bool CanRead
	{
		get
		{
			return true;
		}
	}

	public override bool CanWrite
	{
		get
		{
			return false;
		}
	}

	public @Ց(Stream inner)
	{
		if (inner == null)
		{
			throw new ArgumentNullException("inner");
		}
		this.@ӓ = new GZipCompressedStream(inner);
	}

	public override void Close()
	{
		this.@ӓ.Close();
	}

	public override void Flush()
	{
		this.@ӓ.Flush();
	}

	public override int Read(byte[] buffer, int offset, int count)
	{
		return this.@ӓ.Read(buffer, offset, count);
	}

	public override void Write(byte[] buffer, int offset, int count)
	{
		throw new InvalidOperationException();
	}
}