using CuteChat;
using System;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Threading;

internal class @ԟ
{
	private static object @ӓ;

	private static Queue @Ӕ;

	private IPEndPoint @ӕ;

	private Socket @Ӗ;

	static @ԟ()
	{
		@ԟ.@ӓ = new object();
		@ԟ.@Ӕ = new Queue();
	}

	public @ԟ()
	{
	}

	private void @ӗ()
	{
		this.@Ӗ = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
		this.@Ӗ.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendTimeout, 5000);
		this.@Ӗ.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 5000);
		this.@Ӗ.Blocking = true;
		this.@Ӗ.Connect(this.@ӕ);
	}

	public static @ԟ Allocate(IPEndPoint endpoint)
	{
		@ԟ _u0040ԟ;
		@ԟ _u0040ԟ1;
		Queue queues = @ԟ.@Ӕ;
		Monitor.Enter(queues);
		try
		{
			while (@ԟ.@Ӕ.Count > 0)
			{
				_u0040ԟ = (@ԟ)@ԟ.@Ӕ.Dequeue();
				if (!_u0040ԟ.@Ӗ.Connected)
				{
					continue;
				}
				_u0040ԟ1 = _u0040ԟ;
				return _u0040ԟ1;
			}
			_u0040ԟ = new @ԟ()
			{
				@ӕ = endpoint
			};
			try
			{
				_u0040ԟ.@ӗ();
			}
			catch (SocketException socketException)
			{
				if (!ClusterSupport.@Ӣ())
				{
					throw;
				}
				else
				{
					_u0040ԟ.@ӗ();
				}
			}
			return _u0040ԟ;
		}
		finally
		{
			Monitor.Exit(queues);
		}
		return _u0040ԟ1;
	}

	public void Close()
	{
		this.@Ӗ.Close();
	}

	public static void Release(@ԟ client)
	{
		Queue queues = @ԟ.@Ӕ;
		Monitor.Enter(queues);
		try
		{
			@ԟ.@Ӕ.Enqueue(client);
		}
		finally
		{
			Monitor.Exit(queues);
		}
	}

	public byte[] SendAndRead(byte[] sendbuffer)
	{
		byte[] bytes = BitConverter.GetBytes((int)sendbuffer.Length);
		this.@Ӗ.Send(bytes, 0, 4, SocketFlags.Partial);
		int num = 0;
		while (num < (int)sendbuffer.Length)
		{
			num += this.@Ӗ.Send(sendbuffer, num, (int)sendbuffer.Length - num, SocketFlags.Partial);
		}
		this.@Ӗ.Receive(bytes, 0, (int)bytes.Length, SocketFlags.Partial);
		byte[] numArray = new byte[BitConverter.ToInt32(bytes, 0)];
		int num1 = 0;
		while (num1 < (int)numArray.Length)
		{
			num1 += this.@Ӗ.Receive(numArray, num1, (int)numArray.Length - num1, SocketFlags.Partial);
		}
		return numArray;
	}
}