using System;
using System.IO;
using System.Text;

internal class @Ԝ
{
	private Stream @ӓ;

	public @Ԝ(Stream stream)
	{
		this.@ӓ = stream;
	}

	public void Write(bool val)
	{
		this.Write((val ? 1 : 0));
	}

	public void Write(int val)
	{
		byte[] bytes = BitConverter.GetBytes(val);
		this.@ӓ.Write(bytes, 0, (int)bytes.Length);
	}

	public void Write(byte[] val)
	{
		if (val == null)
		{
			throw new ArgumentNullException("val");
		}
		this.Write((int)val.Length);
		this.@ӓ.Write(val, 0, (int)val.Length);
	}

	public void Write(string val)
	{
		if (val == null)
		{
			this.Write(-1);
			return;
		}
		this.Write(val.Length);
		if (val.Length > 0)
		{
			this.Write(Encoding.UTF8.GetBytes(val));
		}
	}
}