using CuteChat;
using System;
using System.IO;
using System.Net.Sockets;

internal class @Ԡ
{
	private Socket @ӓ;

	private byte[] @Ӕ = new byte[4096];

	private int @ӕ;

	private MemoryStream @Ӗ;

	private byte[] @ӗ;

	private int @Ә;

	public @Ԡ()
	{
	}

	private void @ә()
	{
		this.@Ӗ = new MemoryStream();
		this.@ӕ = 0;
		this.@ӓ.BeginReceive(this.@Ӕ, 0, (int)this.@Ӕ.Length, SocketFlags.Partial, new AsyncCallback(this.@Ӛ), null);
	}

	private void @ӛ(IAsyncResult u0040ӓ)
	{
		try
		{
			this.@Ә += this.@ӓ.EndSend(u0040ӓ);
			if (this.@Ә >= (int)this.@ӗ.Length)
			{
				this.@ә();
			}
			else
			{
				this.@ӓ.BeginSend(this.@ӗ, this.@Ә, (int)this.@ӗ.Length - this.@Ә, SocketFlags.Partial, new AsyncCallback(this.@ӛ), null);
			}
		}
		catch (Exception exception)
		{
			ChatSystem.Instance.LogException(exception);
			this.@ӓ.Close();
		}
	}

	private void @Ӛ(IAsyncResult u0040ӓ)
	{
		try
		{
			int num = this.@ӓ.EndReceive(u0040ӓ);
			if (num > 0)
			{
				if (this.@ӕ != 0)
				{
					this.@Ӗ.Write(this.@Ӕ, 0, num);
				}
				else
				{
					this.@ӕ = BitConverter.ToInt32(this.@Ӕ, 0);
					this.@Ӗ.Write(this.@Ӕ, 4, num - 4);
				}
			}
			if (this.@Ӗ.Length >= (long)this.@ӕ)
			{
				this.@Ӗ.Seek((long)0, SeekOrigin.Begin);
				this.@ӗ = ClusterSupport.@ө(this.@Ӗ, "tcp");
				this.@ӓ.Send(BitConverter.GetBytes((int)this.@ӗ.Length), 0, 4, SocketFlags.Partial);
				this.@Ә = 0;
				this.@ӓ.BeginSend(this.@ӗ, this.@Ә, (int)this.@ӗ.Length - this.@Ә, SocketFlags.Partial, new AsyncCallback(this.@ӛ), null);
			}
			else
			{
				this.@ӓ.BeginReceive(this.@Ӕ, 0, (int)this.@Ӕ.Length, SocketFlags.None, new AsyncCallback(this.@Ӛ), null);
			}
		}
		catch (Exception exception)
		{
			ChatSystem.Instance.LogException(exception);
			this.@ӓ.Close();
		}
	}

	public static void HandleClient(Socket client)
	{
		client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendTimeout, 5000);
		client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 5000);
		client.Blocking = false;
		(new @Ԡ()
		{
			@ӓ = client
		}).@ә();
	}
}