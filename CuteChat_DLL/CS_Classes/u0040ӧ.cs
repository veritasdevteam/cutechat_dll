using System;
using System.IO;
using Xceed.Compression;

internal class @ӧ : Stream
{
	private bool @ӓ;

	private Stream @Ӕ;

	private int @ӕ;

	private byte[] @Ӗ;

	private int @ӗ;

	private bool @Ә;

	private bool @ә;

	public override bool CanRead
	{
		get
		{
			this.@Ӛ();
			return this.@Ӕ.CanRead;
		}
	}

	public override bool CanSeek
	{
		get
		{
			this.@Ӛ();
			return this.@Ӕ.CanSeek;
		}
	}

	public override bool CanWrite
	{
		get
		{
			this.@Ӛ();
			return this.@Ӕ.CanWrite;
		}
	}

	public Stream InnerStream
	{
		get
		{
			this.@Ӛ();
			return this.@Ӕ;
		}
	}

	public override long Length
	{
		get
		{
			this.@Ӛ();
			return this.@Ӕ.Length;
		}
	}

	public override long Position
	{
		get
		{
			this.@Ӛ();
			return this.@Ӕ.Position;
		}
		set
		{
			this.@Ӛ();
			this.@Ӕ.Position = value;
		}
	}

	public int ReservedFooter
	{
		get
		{
			this.@Ӛ();
			return this.@ӕ;
		}
		set
		{
			this.@Ӛ();
			
			if (this.@ӗ != 0)
			{
				throw new CompressionInternalException("An attempt was made to set the reserved footer twice.");
			}
			this.@ӕ = value;
			this.@Ӗ = new byte[value];
		}
	}

	public bool Transient
	{
		get
		{
			this.@Ӛ();
			return this.@Ә;
		}
		set
		{
			this.@Ӛ();
			this.@Ә = value;
		}
	}

	public @ӧ(Stream innerStream)
	{
		if (innerStream == null)
		{
			throw new ArgumentNullException("innerStream");
		}
		this.@Ӕ = innerStream;
	}

	private void @Ӛ()
	{
		if (this.@ӓ)
		{
			throw new ObjectDisposedException("TransientStream", "Cannot access a stream once it has been closed.");
		}
		if (this.@Ӕ == null)
		{
			throw new InvalidOperationException("An attempt was made to dispose of a non-initialized FormattedChecksumStream.");
		}
	}

	public override void Close()
	{
		this.Dispose(true);
		GC.SuppressFinalize(this);
	}

	protected virtual new void Dispose(bool disposing)
	{
		if (!this.@ӓ)
		{
			if (disposing && !this.@Ә)
			{
				this.@Ӕ.Close();
			}
			this.@Ӕ = null;
			this.@ӓ = true;
		}
	}



	public override void Flush()
	{
		this.@Ӛ();
		this.@Ӕ.Flush();
	}

	public int GetFooter(byte[] buffer, int index)
	{
		this.@Ӛ();
		if (buffer == null)
		{
			throw new ArgumentNullException("buffer");
		}
		
		if ((int)buffer.Length - index < this.ReservedFooter)
		{
			throw new ArgumentException("The buffer cannot hold all the buffered footer.", "buffer");
		}
		if (this.ReservedFooter > 0)
		{
			Array.Copy(this.@Ӗ, 0, buffer, index, this.ReservedFooter);
		}
		return this.ReservedFooter;
	}

	public override int Read(byte[] buffer, int offset, int count)
	{
		this.@Ӛ();
		if (this.@ә)
		{
			return 0;
		}
		int num = 0;
		if (this.@ӕ != 0)
		{
			byte[] numArray = buffer;
			int num1 = offset;
			int num2 = count;
			bool flag = false;
			if (count <= this.@ӕ)
			{
				numArray = new byte[this.@ӕ + count];
				num1 = 0;
				num2 = this.@ӕ + count;
				flag = true;
			}
			if (this.@ӗ > 0)
			{
				Array.Copy(this.@Ӗ, 0, numArray, num1, this.@ӗ);
				num1 += this.@ӗ;
				num2 -= this.@ӗ;
			}
			num = this.@ӗ;
			while (num2 > 0)
			{
				int num3 = this.@Ӕ.Read(numArray, num1, num2);
				if (num3 == 0)
				{
					break;
				}
				num += num3;
				num1 += num3;
				num2 -= num3;
			}
			if (num < this.@ӕ)
			{
				throw new IOException("There is not enough data in the stream.");
			}
			Array.Copy(numArray, num1 - this.@ӕ, this.@Ӗ, 0, this.@ӕ);
			this.@ӗ = this.@ӕ;
			num -= this.@ӕ;
			if (flag)
			{
				Array.Copy(numArray, 0, buffer, offset, num);
			}
		}
		else
		{
			num = this.@Ӕ.Read(buffer, offset, count);
		}
		if (num == 0)
		{
			this.@ә = true;
		}
		return num;
	}

	public override long Seek(long offset, SeekOrigin origin)
	{
		this.@Ӛ();
		return this.@Ӕ.Seek(offset, origin);
	}

	public override void SetLength(long value)
	{
		this.@Ӛ();
		this.@Ӕ.SetLength(value);
	}

	public override void Write(byte[] buffer, int offset, int count)
	{
		this.@Ӛ();
		this.@Ӕ.Write(buffer, offset, count);
	}
}