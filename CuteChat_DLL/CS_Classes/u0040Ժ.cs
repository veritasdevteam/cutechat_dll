using System;
using System.Collections;
using System.Reflection;
using System.Threading;

internal class @Ժ
{
	private readonly static int @ӓ;

	//private int @Ӕ = 10000;

	private long @ӕ;

	private bool @Ӗ;

	private ReaderWriterLock @ӗ = new ReaderWriterLock();

	private Hashtable @Ә = new Hashtable();

	private Timer @ә;

	private long @Ӛ;

	private long @ӛ;

	public int Count
	{
		get
		{
			int count;
			this.@ӗ.AcquireReaderLock(-1);
			try
			{
				count = this.@Ә.Count;
			}
			finally
			{
				this.@ӗ.ReleaseReaderLock();
			}
			return count;
		}
	}

	public bool DebugIsTimerStarted
	{
		get
		{
			return this.@Ӛ != (long)0;
		}
	}

	public long DegugTimerCount
	{
		get
		{
			return this.@ӛ;
		}
	}

	public object this[object key]
	{
		get
		{
			object obj;
			this.@ӗ.AcquireReaderLock(-1);
			try
			{
				@Ժ.@Ӕ item = (@Ժ.@Ӕ)this.@Ә[key];
				if (item == null)
				{
					return null;
				}
				else
				{
					obj = this.@Ӡ(item);
				}
			}
			finally
			{
				this.@ӗ.ReleaseReaderLock();
			}
			return obj;
		}
		set
		{
			this.@ӗ.AcquireWriterLock(-1);
			try
			{
				@Ժ.@Ӕ item = (@Ժ.@Ӕ)this.@Ә[key];
				if (item == null)
				{
					item = new @Ժ.@Ӕ(key, value);
					this.@Ә.Add(key, item);
					this.@Ӝ(item);
				}
				else
				{
					this.@ӡ(item, value);
				}
			}
			finally
			{
				this.@ӗ.ReleaseWriterLock();
			}
		}
	}

	static @Ժ()
	{
		@Ժ.@ӓ = 10;
	}

	public @Ժ()
	{
		TimeSpan timeSpan = TimeSpan.FromMilliseconds((double)5959);
		this.@ӕ = timeSpan.Ticks;
	}

	public @Ժ(TimeSpan timespan, bool autoReset) : this((int)timespan.TotalMilliseconds, autoReset)
	{
	}

	public @Ժ(int expiresMS, bool autoReset)
	{
		if (expiresMS <= 0)
		{
			throw new ArgumentOutOfRangeException("expiresMS", (object)expiresMS, "expiresMS must large then 0");
		}
		if (expiresMS < 1000)
		{
			throw new ArgumentException("expiresMS must large or equals then 1000");
		}
		//this.@Ӕ = expiresMS;
		this.@Ӗ = autoReset;
		//TimeSpan timeSpan = TimeSpan.FromMilliseconds((double)this.@Ӕ);
		//this.@ӕ = timeSpan.Ticks;
	}

	private static void @ӝ(object u0040ӓ)
	{
		@Ժ target;
		WeakReference weakReference = (WeakReference)u0040ӓ;
		if (!weakReference.IsAlive)
		{
			return;
		}
		try
		{
			target = (@Ժ)weakReference.Target;
		}
		catch
		{
			return;
		}
		target.@Ӟ();
	}

	private void @Ӝ(@Ժ.@Ӕ u0040ӓ)
	{
		long ticks = DateTime.Now.Ticks;
		u0040ӓ.ldt = ticks;
		if (this.@Ӛ == 0)
		{
			lock (this)
			{
				if (this.@ә != null)
				{
					//this.@ә.Change(this.@Ӕ + @Ժ.@ӓ, -1);
				}
				else
				{
					//this.@ә = new Timer(new TimerCallback(@Ժ.@ӝ), new WeakReference(this, true), this.@Ӕ + @Ժ.@ӓ, -1);
				}
			}
		}
	}

	private void @ӟ()
	{
		if (this.@Ә.Count == 0)
		{
			@Ժ _u0040Ժ = this;
			Monitor.Enter(_u0040Ժ);
			try
			{
				if (this.@ә != null)
				{
					this.@ә.Dispose();
					this.@ә = null;
				}
			}
			finally
			{
				Monitor.Exit(_u0040Ժ);
			}
		}
	}

	private void @Ӟ()
	{
		this.@ӗ.AcquireWriterLock(-1);
		this.@ӛ += (long)1;
		long ticks = DateTime.Now.Ticks;
		try
		{
			try
			{
				long num = 9223372036854775807L;
				long num1 = ticks - this.@ӕ;
				ArrayList arrayLists = new ArrayList();
				foreach (@Ժ.@Ӕ value in this.@Ә.Values)
				{
					if (value.ldt > num1)
					{
						num = Math.Min(value.ldt, num);
					}
					else
					{
						arrayLists.Add(value);
					}
				}
				int count = arrayLists.Count;
				if (count != 0)
				{
					for (int i = 0; i < count; i++)
					{
						this.@Ә.Remove(((@Ժ.@Ӕ)arrayLists[i]).key);
					}
				}
				if (num == 9223372036854775807L)
				{
					this.@Ӛ = (long)0;
				}
				else
				{
					this.@Ӛ = ticks;
					//this.@ә.Change(Math.Min(num + this.@ӕ - ticks, (long)this.@Ӕ) + (long)@Ժ.@ӓ, (long)-1);
				}
			}
			catch
			{
				this.@Ӛ = ticks;
				//this.@ә.Change(this.@Ӕ, -1);
				throw;
			}
		}
		finally
		{
			this.@ӟ();
			this.@ӗ.ReleaseWriterLock();
		}
	}

	private void @ӡ(@Ժ.@Ӕ u0040ӓ, object u0040Ӕ)
	{
		this.@Ӝ(u0040ӓ);
		u0040ӓ.val = u0040Ӕ;
	}

	private object @Ӡ(@Ժ.@Ӕ u0040ӓ)
	{
		if (this.@Ӗ)
		{
			this.@Ӝ(u0040ӓ);
		}
		return u0040ӓ.val;
	}

	public void Clear()
	{
		this.@ӗ.AcquireWriterLock(-1);
		try
		{
			this.@Ә.Clear();
			this.@ӟ();
		}
		finally
		{
			this.@ӗ.ReleaseWriterLock();
		}
	}

	public @Ժ.Iterator GetIterator(object key)
	{
		if (key == null)
		{
			throw new ArgumentNullException("key");
		}
		return new @Ժ.Iterator(this, key);
	}

	public void Remove(object key)
	{
		this.@ӗ.AcquireReaderLock(-1);
		try
		{
			if (this.@Ә.ContainsKey(key))
			{
				this.@ӗ.UpgradeToWriterLock(-1);
				this.@Ә.Remove(key);
				this.@ӟ();
			}
		}
		finally
		{
			this.@ӗ.ReleaseReaderLock();
		}
	}

	private class @Ӕ
	{
		public object key;

		public object val;

		public long ldt;

		public @Ӕ(object _key, object _val)
		{
			this.key = _key;
			this.val = _val;
			this.ldt = DateTime.Now.Ticks;
		}
	}

	public class Iterator : IDisposable
	{
		private @Ժ @ӓ;

		private object @Ӕ;

		private @Ժ.@Ӕ @ӕ;

		private IDisposable @Ӗ;

		public bool IsNotSet
		{
			get
			{
				return this.@ӕ == null;
			}
		}

		public object Key
		{
			get
			{
				return this.@Ӕ;
			}
		}

		public object Value
		{
			get
			{
				if (this.@ӕ == null)
				{
					return null;
				}
				return this.@ӓ.@Ӡ(this.@ӕ);
			}
			set
			{
				this.@ӓ[this.Key] = value;
				if (this.@ӕ == null)
				{
					this.@ӗ();
				}
			}
		}

		internal Iterator(@Ժ cache, object key)
		{
			this.@ӓ = cache;
			this.@Ӕ = key;
			try
			{
				this.@ӗ();
			}
			catch
			{
				if (this.@Ӗ != null)
				{
					this.@Ӗ.Dispose();
				}
				throw;
			}
		}

		private void @ӗ()
		{
			this.@ӓ.@ӗ.AcquireReaderLock(-1);
			try
			{
				this.@ӕ = (@Ժ.@Ӕ)this.@ӓ.@Ә[this.@Ӕ];
			}
			finally
			{
				this.@ӓ.@ӗ.ReleaseReaderLock();
			}
		}

		public void Dispose()
		{
			if (this.@Ӗ != null)
			{
				IDisposable disposable = this.@Ӗ;
				this.@Ӗ = null;
				disposable.Dispose();
			}
		}

		public void Remove()
		{
			if (this.@ӕ != null)
			{
				this.@ӓ.Remove(this.Key);
				this.@ӗ();
			}
		}
	}
}