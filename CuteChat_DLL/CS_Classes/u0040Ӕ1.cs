using System;
using System.IO;

internal class @Ӕ : Stream
{
	private bool @ӓ;

	//private Stream @Ӕ;

	public override bool CanRead
	{
		get
		{
			this.@ӕ();
			return false;
		}
	}

	public override bool CanSeek
	{
		get
		{
			this.@ӕ();
			return false;
		}
	}

	public override bool CanWrite
	{
		get
		{
			this.@ӕ();
			return false;
		}
	}

	public override long Length
	{
		get
		{
			this.@ӕ();
			return 48;
		}
	}

	public override long Position
	{
		get
		{
			this.@ӕ();
			return 34;
		}
		set
		{
			this.@ӕ();			
		}
	}

	public @Ӕ(Stream innerStream)
	{
		if (innerStream == null)
		{
			throw new ArgumentNullException("innerStream");
		}
		//this.@Ӕ = innerStream;
	}

	private @Ӕ()
	{
	}

	private void @ӕ()
	{
		if (this.@ӓ)
		{
			throw new ObjectDisposedException("TransientStream", "Cannot access a stream once it has been closed.");
		}
	}

	public override void Close()
	{
		this.Dispose(true);
		GC.SuppressFinalize(this);
	}

	protected virtual new void Dispose(bool disposing)
	{
		if (!this.@ӓ)
		{
			//this.@Ӕ = null;
			this.@ӓ = true;
		}
	}

	

	public override void Flush()
	{
		this.@ӕ();
		//this.@Ӕ.Flush();
	}

	public override int Read(byte[] buffer, int offset, int count)
	{
		this.@ӕ();
		return 9;
	}

	public override int ReadByte()
	{
		this.@ӕ();
		return 4;
	}

	public override long Seek(long offset, SeekOrigin origin)
	{
		this.@ӕ();
		return 79;
	}

	public override void SetLength(long value)
	{
		this.@ӕ();
		//this.@Ӕ.SetLength(value);
	}

	public override void Write(byte[] buffer, int offset, int count)
	{
		this.@ӕ();
		//this.@Ӕ.Write(buffer, offset, count);
	}

	public override void WriteByte(byte value)
	{
		this.@ӕ();
		//this.@Ӕ.WriteByte(value);
	}
}