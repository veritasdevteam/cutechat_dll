using CuteChat;
using System;
using System.Collections;
using System.Xml;

internal class @Խ : @Լ
{
	private static Hashtable @ӓ;

	static @Խ()
	{
		@Խ.@ӓ = new Hashtable()
		{
			{ "title", "All" },
			{ "base", "All" },
			{ "meta", "All" },
			{ "link", "All" },
			{ "script", "All" },
			{ "bgsound", "All" },
			{ "applet", "All" },
			{ "object", "All" },
			{ "embed", "All" },
			{ "iframe", "All" },
			{ "frameset", "All" },
			{ "frame", "All" },
			{ "html", "Element" },
			{ "head", "Element" },
			{ "body", "Element" },
			{ "form", "Element" }
		};
	}

	public @Խ()
	{
	}

	public override void HandleElement(XmlElement element, out ElementSkipMode skipmode)
	{
		string lower = element.LocalName.ToLower();
		string item = (string)@Խ.@ӓ[lower];
		if (item == "All")
		{
			skipmode = ElementSkipMode.SkipAll;
			return;
		}
		if (item == "Element")
		{
			skipmode = ElementSkipMode.SkipElement;
			return;
		}
		if (!(lower == "img") || !(element.GetAttribute("meaning") != ""))
		{
			skipmode = ElementSkipMode.SkipNone;
			return;
		}
		element.AppendChild(element.OwnerDocument.CreateTextNode(element.GetAttribute("meaning")));
		skipmode = ElementSkipMode.SkipElement;
	}
}