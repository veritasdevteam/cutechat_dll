using System;
using System.IO;
using Xceed.Compression;
using Xceed.Compression.Formats;

internal class @Ր : MemoryStream
{
	private Stream @ӓ;

	public override bool CanRead
	{
		get
		{
			return false;
		}
	}

	public override bool CanWrite
	{
		get
		{
			return true;
		}
	}

	public @Ր(Stream inner)
	{
		if (inner == null)
		{
			throw new ArgumentNullException("inner");
		}
		this.@ӓ = inner;
	}

	public void Finish()
	{
		this.Flush();
	}

	public override void Flush()
	{
		this.@ӓ.Flush();
	}

	public override int Read(byte[] buffer, int offset, int count)
	{
		throw new InvalidOperationException();
	}

	public override void Write(byte[] buffer, int offset, int count)
	{
		byte[] numArray = ZLibCompressedStream.Compress(buffer, 0, count, CompressionMethod.Deflated, CompressionLevel.Highest);
		this.@ӓ.Write(numArray, 0, (int)numArray.Length);
	}
}