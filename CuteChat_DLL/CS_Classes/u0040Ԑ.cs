using CuteChat;
using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Http;
//using System.Web;


internal class @Ԑ : LicenseProvider
{
	//private EnumChatLicense @ӓ;

	private static byte[] @Ӕ;

	private static @Ԑ.@ӓ @ӕ;

	private static @Ԑ.@ӓ @Ӗ;

	private static @Ԑ.@ӓ @ӗ;

	private static @Ԑ.@ӓ @ә
	{
		get
		{
			if (@Ԑ.@ӕ == null)
			{
				@Ԑ.@ӕ = new @Ԑ.@ӓ();
			}
			return @Ԑ.@ӕ;
		}
	}

	private static byte[] @Ә
	{
		get
		{
			if (@Ԑ.@Ӕ == null)
			{
				@Ԑ.@Ӕ = new byte[] { 70, 53, 50, 66, 49, 56, 54, 70 };
			}
			return (byte[])@Ԑ.@Ӕ.Clone();
		}
	}

	private static @Ԑ.@ӓ @ӛ
	{
		get
		{
			if (@Ԑ.@ӗ == null)
			{
				@Ԑ.@ӗ = new @Ԑ.@ӓ();
			}
			return @Ԑ.@ӗ;
		}
	}

	private static @Ԑ.@ӓ @Ӛ
	{
		get
		{
			if (@Ԑ.@Ӗ == null)
			{
				@Ԑ.@Ӗ = new @Ԑ.@ӓ();
			}
			return @Ԑ.@Ӗ;
		}
	}

	public EnumChatLicense ProductType
	{
		get
		{
			return EnumChatLicense.CuteChat;
		}
		set
		{
			//this.@ӓ = value;
		}
	}

	static @Ԑ()
	{
	}

	public @Ԑ()
	{
	}

	internal bool @Ӥ(@Ԕ u0040ӓ, out string u0040Ӕ)
	{

		//HttpContext http = new HttpContext();
		
	
		u0040Ӕ = null;
		string empty = string.Empty;
		if (u0040ӓ.IsValid)
		{
			return true;
		}
		if (u0040ӓ.LicenseType == "0")
		{
			empty = string.Concat("The License for ", this.ProductType, " is not valid. \n\n");
			empty = string.Concat(empty, " You are using a trial license and the trial period has expired. \n\n");
			empty = string.Concat(empty, " If you would like to extend your trial period, please write to support@cuteSoft.net.\n");
		}
		else if (u0040ӓ.LicenseType == "1")
		{
			string lower = string.Empty; 
			lower = @Ԕ.@ӡ(lower);
			empty = string.Concat("The License for ", this.ProductType, " is not valid. \n\n");
			empty = string.Concat(empty, " You are using a domain license file. \n\n");
			empty = string.Concat(empty, " The domain name in the license file doesn't match the actual domain name (", lower, ") of the web site. \n");
		}
		else if (u0040ӓ.LicenseType == "2")
		{
			string str = string.Empty;
			empty = string.Concat("The License for ", this.ProductType, " is not valid. \n\n");
			empty = string.Concat(empty, " You are using an IP license file. \n\n");
			empty = string.Concat(empty, " The IP address in the license file doesn't match the actual IP address(", str, ") of the web site. \n");
		}
		else if (u0040ӓ.LicenseType != "4")
		{
			empty = string.Concat("The License for ", this.ProductType, " is not valid. \n\n");
		}
		else
		{
			empty = string.Concat("The License for ", this.ProductType, " is not valid. \n\n");
			empty = string.Concat(empty, " You are using a trial license and the trial period has expired. \n\n");
			empty = string.Concat(empty, " If you would like to extend your trial period, please write to support@cuteSoft.net.\n");
		}
		if (u0040ӓ.InvalidMessage != null && u0040ӓ.InvalidMessage != "")
		{
			empty = string.Concat(empty, u0040ӓ.InvalidMessage);
		}
		u0040Ӕ = empty;
		return false;
	}

	private string @ӣ()
	{
		string str;
		string item = ConfigurationSettings.AppSettings[string.Concat(this.ProductType, "License")];
		if (File.Exists(item))
		{
			return item;
		}
		
		try
		{
			item = "C:\\";
			if (File.Exists(item))
			{
				str = item;
				return str;
			}
		}
		catch
		{
		}
		return null;
	}

	private bool @Ӣ(byte[] u0040ӓ, byte[] u0040Ӕ)
	{
		bool flag;
		try
		{
			RSACryptoServiceProvider rSACryptoServiceProvider = new RSACryptoServiceProvider(new CspParameters()
			{
				Flags = CspProviderFlags.UseMachineKeyStore
			});
			rSACryptoServiceProvider.FromXmlString(@ԏ.@ӓ);
			bool flag1 = rSACryptoServiceProvider.VerifyData(u0040ӓ, "SHA1", u0040Ӕ);
			rSACryptoServiceProvider.Clear();
			return flag1;
		}
		catch
		{
			flag = true;
		}
		return flag;
	}

	internal @Ԕ @ӡ(Type u0040ӓ)
	{
		return new @Ԕ(u0040ӓ, string.Empty);
	}

	internal @Ԕ @Ӡ(Type u0040ӓ, string u0040Ӕ)
	{
		return new @Ԕ(u0040ӓ, u0040Ӕ);
	}

	public override License GetLicense(LicenseContext context, Type type, object instance, bool allowExceptions)
	{
		byte[] numArray;
		byte[] numArray1;
		string end;
		@Ԕ _u0040Ԕ;
		if (context.UsageMode == LicenseUsageMode.Designtime)
		{
			return this.@ӡ(type);
		}
		@Ԕ license = null;
		string message = null;
		if (this.ProductType == EnumChatLicense.CuteChat)
		{
			license = @Ԑ.@ә.GetLicense(type);
		}
		else if (this.ProductType == EnumChatLicense.CuteMessenger)
		{
			license = @Ԑ.@Ӛ.GetLicense(type);
		}
		else if (this.ProductType == EnumChatLicense.CuteLiveSupport)
		{
			license = @Ԑ.@ӛ.GetLicense(type);
		}
		if (license == null)
		{
			string str = this.@ӣ();
			if (str != null)
			{
				using (Stream fileStream = new FileStream(str, FileMode.Open, FileAccess.Read, FileShare.Read))
				{
					BinaryReader binaryReader = new BinaryReader(fileStream);
					if (binaryReader.ReadDouble() == 3)
					{
						binaryReader.ReadBytes(123);
						numArray = binaryReader.ReadBytes(binaryReader.ReadInt32());
						binaryReader.ReadBytes(234);
						numArray1 = binaryReader.ReadBytes(binaryReader.ReadInt32());
						binaryReader.ReadBytes(345);
					}
					else
					{
						message = "You are using an incorrect license file.";
						if (allowExceptions && (license == null || message != null))
						{
							if (message == null || message == "")
							{
								message = "A valid license cannot be granted for this product. The license file are missing or corrupt.";
							}
							throw new LicenseException(type, instance, message);
						}
						return license;
					}
				}
				if (this.@Ӣ(numArray, numArray1))
				{
					using (MemoryStream memoryStream = new MemoryStream(numArray))
					{
						ICryptoTransform cryptoTransform = (new DESCryptoServiceProvider()
						{
							Key = @Ԑ.@Ә,
							IV = @Ԑ.@Ә
						}).CreateDecryptor();
						try
						{
							using (Stream cryptoStream = new CryptoStream(memoryStream, cryptoTransform, CryptoStreamMode.Read))
							{
								end = (new StreamReader(cryptoStream, Encoding.UTF8)).ReadToEnd();
							}
						}
						catch (Exception exception)
						{
							message = string.Concat("Failed to decrypt data , ", exception.Message);
							if (allowExceptions && (license == null || message != null))
							{
								if (message == null || message == "")
								{
									message = "A valid license cannot be granted for this product. The license file are missing or corrupt.";
								}
								throw new LicenseException(type, instance, message);
							}
							return license;
						}
					}
					if (this.ValidateLicenseData(type, end))
					{
						try
						{
							_u0040Ԕ = this.@Ӡ(type, end);
						}
						catch (Exception exception1)
						{
							message = exception1.Message;
							if (allowExceptions && (license == null || message != null))
							{
								if (message == null || message == "")
								{
									message = "A valid license cannot be granted for this product. The license file are missing or corrupt.";
								}
								throw new LicenseException(type, instance, message);
							}
							return license;
						}
						if (this.@Ӥ(_u0040Ԕ, out message))
						{
							license = _u0040Ԕ;
							if (this.ProductType == EnumChatLicense.CuteChat)
							{
								@Ԑ.@ә.AddLicense(type, license);
							}
							else if (this.ProductType == EnumChatLicense.CuteMessenger)
							{
								@Ԑ.@Ӛ.AddLicense(type, license);
							}
							else if (this.ProductType == EnumChatLicense.CuteLiveSupport)
							{
								@Ԑ.@ӛ.AddLicense(type, license);
							}
						}
					}
					else
					{
						message = "Failed to validate license data";
					}
				}
				else
				{
					message = "Wrong sign.";
				}
			}
			else
			{
				message = "License file not found";
			}
		}
		else if (!this.@Ӥ(license, out message))
		{
			license = null;
			if (message == null)
			{
				message = "ValidateLicense failed";
			}
		}
		if (allowExceptions && (license == null || message != null))
		{
			if (message == null || message == "")
			{
				message = "A valid license cannot be granted for this product. The license file are missing or corrupt.";
			}
			throw new LicenseException(type, instance, message);
		}
		return license;
	}

	protected bool ValidateLicenseData(Type type, string licenseData)
	{
		string[] strArrays = licenseData.Split(new char[] { ';' });
		if ((int)strArrays.Length != 10)
		{
			throw new Exception("parts.Length != 10");
		}
		string str = string.Concat(this.ProductType, " for asp.net is licensed.");
		bool flag = string.Compare(str, strArrays[4], true, CultureInfo.InvariantCulture) == 0;
		if (!flag)
		{
			throw new Exception(string.Concat(str, " != ", strArrays[4]));
		}
		return flag;
	}

	private sealed class @ӓ
	{
		//private IDictionary @ӓ;

		public @ӓ()
		{
			//this.@ӓ = new HybridDictionary();
		}

		public void AddLicense(Type objectType, @Ԕ license)
		{
			if (objectType == null)
			{
				throw new ArgumentNullException("objectType");
			}
			if (license == null)
			{
				throw new ArgumentNullException("objectType");
			}
			//this.@ӓ[objectType] = license;
		}

		public @Ԕ GetLicense(Type objectType)
		{
			if (objectType == null)
			{
				throw new ArgumentNullException("objectType");
			}
			if (false)
			{
				return null;
			}
			return null;
		}

		public void RemoveLicense(Type objectType)
		{
			if (objectType == null)
			{
				throw new ArgumentNullException("objectType");
			}
			//this.@ӓ.Remove(objectType);
		}
	}
}