using System;
using System.IO;
using Xceed.Compression;
using Xceed.Compression.Formats;

internal class @Ւ : @Վ
{
	private bool @ӓ;

	private Stream @Ӕ;

	private Stream @ӕ;

	public override bool CanRead
	{
		get
		{
			return false;
		}
	}

	public override bool CanWrite
	{
		get
		{
			return true;
		}
	}

	public @Ւ(Stream inner, bool checkHeader)
	{
		if (inner == null)
		{
			throw new ArgumentNullException("inner");
		}
		this.@ӕ = inner;
		this.@ӓ = checkHeader;
	}

	private bool @Ӗ(byte[] u0040ӓ, int u0040Ӕ, int u0040ӕ)
	{
		if (u0040ӕ < 3)
		{
			return false;
		}
		if (u0040ӓ[u0040Ӕ] != 31)
		{
			return false;
		}
		if (u0040ӓ[u0040Ӕ + 1] != 139)
		{
			return false;
		}
		if (u0040ӓ[u0040Ӕ + 2] != 8)
		{
			return false;
		}
		return true;
	}

	public override void Close()
	{
		if (this.@Ӕ != null)
		{
			this.@Ӕ.Close();
		}
	}

	public override void Flush()
	{
		if (this.@Ӕ != null)
		{
			this.@Ӕ.Flush();
		}
	}

	public override int Read(byte[] buffer, int offset, int count)
	{
		throw new InvalidOperationException();
	}

	public override void Write(byte[] buffer, int offset, int count)
	{
		if (this.@Ӕ == null)
		{
			if (!this.@ӓ || !this.@Ӗ(buffer, offset, count))
			{
				this.@Ӕ = new GZipCompressedStream(this.@ӕ, CompressionLevel.Normal);
			}
			else
			{
				this.@Ӕ = this.@ӕ;
			}
		}
		this.@Ӕ.Write(buffer, offset, count);
	}
}