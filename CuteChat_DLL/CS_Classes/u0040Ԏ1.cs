using CuteChat;
using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Web;

internal class @Ԏ
{
	private Hashtable @ӓ = Hashtable.Synchronized(new Hashtable());

	public @Ԏ()
	{
	}

	private static void @Ӗ(byte[] u0040ӓ)
	{
		int length = (int)u0040ӓ.Length;
		for (int i = 0; i < length; i++)
		{
			u0040ӓ[i] = (byte)(255 - u0040ӓ[i]);
		}
	}

	private @Ԕ @ӕ(EnumChatLicense u0040ӓ)
	{
		LicenseProvider chatLicenceProvider;
		if (u0040ӓ == EnumChatLicense.CuteChat)
		{
			chatLicenceProvider = new ChatLicenceProvider();
		}
		else if (u0040ӓ != EnumChatLicense.CuteMessenger)
		{
			if (u0040ӓ != EnumChatLicense.CuteLiveSupport)
			{
				throw new ArgumentException(string.Concat("Unknown license type : ", u0040ӓ));
			}
			chatLicenceProvider = new LiveSupportLicenceProvider();
		}
		else
		{
			chatLicenceProvider = new MessengerLicenceProvider();
		}
		LicenseContext licenseContext = new LicenseContext();
		if (licenseContext.UsageMode != LicenseUsageMode.Runtime)
		{
			throw new Exception("unknown LicenseContext");
		}
		return (@Ԕ)chatLicenceProvider.GetLicense(licenseContext, typeof(@Ԕ), null, true);
	}

	private void @Ӕ(@Ԕ u0040ӓ)
	{
		if (u0040ӓ == null)
		{
			throw new Exception("License File Does Not Exist!");
		}
		if (!u0040ӓ.IsValid)
		{
			throw new Exception(string.Concat("License File is Invalid: ", u0040ӓ.InvalidMessage));
		}
	}

	public @Ԕ CheckGeneralLicense()
	{
		@Ԕ cachedLicense = this.GetCachedLicense(EnumChatLicense.CuteChat);
		this.@Ӕ(cachedLicense);
		return cachedLicense;
	}

	protected @Ԕ CheckLicense(EnumChatLicense licenseType)
	{
		return this.@ӕ(licenseType);
	}

	public @Ԕ CheckMessengerLicense()
	{
		@Ԕ cachedLicense = this.GetCachedLicense(EnumChatLicense.CuteMessenger);
		this.@Ӕ(cachedLicense);
		return cachedLicense;
	}

	public @Ԕ CheckSupportLicense()
	{
		@Ԕ cachedLicense = this.GetCachedLicense(EnumChatLicense.CuteLiveSupport);
		this.@Ӕ(cachedLicense);
		return cachedLicense;
	}

	public @Ԕ GetCachedLicense(EnumChatLicense licenseType)
	{
		string dictKey = this.GetDictKey(licenseType);
		@Ԕ item = (@Ԕ)this.@ӓ[dictKey];
		if (item == null)
		{
			item = this.CheckLicense(licenseType);
			this.@ӓ[dictKey] = item;
		}
		return item;
	}

	protected string GetDictKey(EnumChatLicense licenseType)
	{
		//HttpContext current = HttpContext.Current;
		
		string item = string.Empty;
		string str = string.Empty;
		if (item == null)
		{
			throw new Exception("Failed to get the LOCAL_ADDR");
		}
		if (str == null)
		{
			throw new Exception("Failed to get the SERVER_NAME");
		}
		return string.Concat(new object[] { licenseType, ":", item, ":", str });
	}
}