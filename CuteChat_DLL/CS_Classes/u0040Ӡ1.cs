using System;
using Xceed.Compression;
using Xceed.Compression.CompressionEngine.Managed;

internal class @Ӡ : Compressor
{
	private @ӳ @ӓ;

	private byte[] @Ӕ;

	private bool @ӕ;

	public virtual int MaxWindowBits
	{
		get
		{
			return 15;
		}
	}

	public virtual int Method
	{
		get
		{
			return 8;
		}
	}

	public @Ӡ(int compressionLevel) : this(compressionLevel, false)
	{
	}

	public @Ӡ(int compressionLevel, bool useZlibHeaders)
	{
		int num;
		try
		{
			this.@ӓ = new @ӳ();
			num = (useZlibHeaders ? this.MaxWindowBits : -this.MaxWindowBits);
			if (@Ӵ.@ә(this.@ӓ, compressionLevel, this.Method, num, 8, CompressionStrategy.Z_DEFAULT_STRATEGY) != ReturnCode.Z_OK)
			{
				throw new CompressionException("Unable to initialize the managed deflate compression engine.");
			}
		}
		catch (CompressionException compressionException)
		{
			throw;
		}
		catch (Exception exception)
		{
			//if (!@ӕ.IsPublicException(exception))
			//{
			//	throw new CompressionInternalException(exception);
			//}
			//throw;
		}
	}

	public override int Compress(byte[] buffer, int offset, int count, bool endOfData, out byte[] compressed)
	{
		int num;
		ReturnCode i;
		if (buffer == null)
		{
			throw new ArgumentNullException("buffer", "The buffer parameter cannot be null.");
		}
		if (offset < 0)
		{
			@Ӗ.ThrowArgumentOutOfRangeException("offset", offset, "The offset parameter must be zero.");
		}
		if (count < 0)
		{
			@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count parameter cannot be less than zero.");
		}
		if (count > (int)buffer.Length - offset)
		{
			@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count parameter exceeds the buffer's remaining length after offset.");
		}
		try
		{
			int length = 0;
			if (count != 0 || !this.@ӕ || endOfData)
			{
				if (count <= 0)
				{
					length = 65536;
					this.@ӓ.@ӓ = null;
					this.@ӓ.@Ӕ = 0;
					this.@ӓ.@ӕ = 0;
				}
				else
				{
					length = count + count / 9 + 12;
					this.@ӓ.@ӓ = buffer;
					this.@ӓ.@Ӕ = (uint)offset;
					this.@ӓ.@ӕ = (uint)count;
				}
				if (this.@Ӕ == null || (int)this.@Ӕ.Length < length)
				{
					this.@Ӕ = new byte[length];
				}
				else
				{
					length = (int)this.@Ӕ.Length;
				}
				this.@ӓ.@ӗ = this.@Ӕ;
				this.@ӓ.@Ә = 0;
				this.@ӓ.@ә = (uint)length;
				ulong num1 = this.@ӓ.@Ӛ;
				if (!endOfData)
				{
					FlushValue flushValue = FlushValue.Z_NO_FLUSH;
					this.@ӕ = false;
					if (count == 0)
					{
						this.@ӕ = true;
						flushValue = FlushValue.Z_SYNC_FLUSH;
					}
					ReturnCode returnCode = @Ӵ.@Ӕ(this.@ӓ, flushValue);
					while (this.@ӓ.@ә == 0 && this.@ӓ.@ӕ != 0)
					{
						length += 65536;
						byte[] numArray = new byte[length];
						this.@Ӕ.CopyTo(numArray, 0);
						this.@Ӕ = numArray;
						this.@ӓ.@ӗ = this.@Ӕ;
						this.@ӓ.@Ә = (uint)(this.@ӓ.@Ӛ - num1);
						//this.@ӓ.@ә = length - (uint)(this.@ӓ.@Ӛ - num1);
						returnCode = @Ӵ.@Ӕ(this.@ӓ, flushValue);
					}
					if (returnCode != ReturnCode.Z_OK)
					{
						throw new CompressionException("Expected end of stream not found");
					}
					length = (int)(this.@ӓ.@Ӛ - num1);
				}
				else
				{
					for (i = @Ӵ.@Ӕ(this.@ӓ, FlushValue.Z_FINISH); i == ReturnCode.Z_OK; i = @Ӵ.@Ӕ(this.@ӓ, FlushValue.Z_FINISH))
					{
						length += 65536;
						byte[] numArray1 = new byte[length];
						this.@Ӕ.CopyTo(numArray1, 0);
						this.@Ӕ = numArray1;
						this.@ӓ.@ӗ = this.@Ӕ;
						this.@ӓ.@Ә = (uint)(this.@ӓ.@Ӛ - num1);
						//this.@ӓ.@ә = length - (uint)(this.@ӓ.@Ӛ - num1);
					}
					if (i != ReturnCode.Z_STREAM_END)
					{
						throw new CompressionException("Expected end of stream not found");
					}
					length = (int)(this.@ӓ.@Ӛ - num1);
				}
				compressed = this.@Ӕ;
				num = length;
			}
			else
			{
				compressed = new byte[0];
				num = 0;
			}
		}
		catch (CompressionException compressionException)
		{
			throw;
		}
		catch (Exception exception)
		{
			
		}

		compressed = new byte[10];
		return 0;
	}
}