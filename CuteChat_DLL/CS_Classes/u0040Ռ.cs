using CuteChat.Configuration;
using System;
using System.Configuration;
using System.IO;
using System.Security;
using System.Xml;

internal class @Ռ
{
	public @Ռ Parent;

	private string @ӓ;

	private DateTime @Ӕ;

	private Config @ӕ;

	public @Ռ(string filename)
	{
		this.@ӓ = filename;
	}

	private static XmlDocument @ӗ()
	{
		return new ConfigXmlDocument();
	}

	private Config @Ӗ(out bool u0040ӓ)
	{
		XmlDocument xmlDocument;
		bool flag = false;
		Config config = null;
		if (this.Parent != null)
		{
			config = this.Parent.@Ӗ(out flag);
		}
		if (!File.Exists(this.@ӓ))
		{
			if (this.@ӕ == null)
			{
				u0040ӓ = flag;
			}
			else
			{
				u0040ӓ = true;
				this.@ӕ = null;
			}
			return config;
		}
		DateTime dateTime = @Ռ.@ә(this.@ӓ);
		if (!flag && dateTime == this.@Ӕ)
		{
			u0040ӓ = false;
			return this.@ӕ;
		}
		u0040ӓ = true;
		try
		{
			xmlDocument = @Ռ.@ӗ();
		}
		catch (SecurityException securityException)
		{
			xmlDocument = new XmlDocument();
		}
		xmlDocument.Load(this.@ӓ);
		this.@ӕ = new Config(config, xmlDocument.DocumentElement);
		this.@Ӕ = dateTime;
		return this.@ӕ;
	}

	private static DateTime @ә(string u0040ӓ)
	{
		return File.GetLastWriteTime(u0040ӓ);
	}

	private static bool @Ә(string u0040ӓ)
	{
		return File.Exists(u0040ӓ);
	}

	public Config GetConfig()
	{
		bool flag;
		return this.@Ӗ(out flag);
	}
}