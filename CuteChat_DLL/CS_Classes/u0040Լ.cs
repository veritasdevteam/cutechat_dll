using CuteChat;
using System;
using System.Xml;

internal class @Լ
{
	public @Լ()
	{
	}

	public virtual void HandleAttribute(XmlAttribute node, out bool bskip)
	{
		bskip = false;
	}

	public virtual void HandleElement(XmlElement element, out ElementSkipMode skipmode)
	{
		skipmode = ElementSkipMode.SkipNone;
	}

	public virtual string HandleText(string text)
	{
		return text;
	}
}