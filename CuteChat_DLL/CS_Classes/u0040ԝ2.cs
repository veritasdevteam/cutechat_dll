using System;
using System.IO;
using System.Text;

internal class @ԝ
{
	private Stream @ӓ;

	public @ԝ(Stream stream)
	{
		this.@ӓ = stream;
	}

	public bool ReadBoolean()
	{
		return this.ReadInt32() == 1;
	}

	public byte[] ReadBytes()
	{
		int num = this.ReadInt32();
		byte[] numArray = new byte[num];
		if (num != this.@ӓ.Read(numArray, 0, (int)numArray.Length))
		{
			throw new Exception("Unexpect stream end?");
		}
		return numArray;
	}

	public byte[] ReadBytes(int len)
	{
		if (this.ReadInt32() != len)
		{
			throw new Exception("Invalid len!");
		}
		byte[] numArray = new byte[len];
		if (len != this.@ӓ.Read(numArray, 0, (int)numArray.Length))
		{
			throw new Exception("Unexpect stream end?");
		}
		return numArray;
	}

	public int ReadInt32()
	{
		byte[] numArray = new byte[4];
		this.@ӓ.Read(numArray, 0, (int)numArray.Length);
		return BitConverter.ToInt32(numArray, 0);
	}

	public string ReadString()
	{
		int num = this.ReadInt32();
		if (num == -1)
		{
			return null;
		}
		if (num == 0)
		{
			return "";
		}
		byte[] numArray = this.ReadBytes();
		return Encoding.UTF8.GetString(numArray, 0, (int)numArray.Length);
	}
}