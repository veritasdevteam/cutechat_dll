using CuteChat;
using System;
using System.Threading;

internal class @Ԍ : IAsyncResult
{
	private ChatPortal @ӓ;

	private ChatResponse @Ӕ;

	private object @ӕ;

	private bool @Ӗ;

	private bool @ӗ;

	private AsyncCallback @Ә;

	private AsyncCallback @ә;

	private Exception @Ӛ;

	public object AsyncState
	{
		get
		{
			return this.@ӕ;
		}
	}

	public WaitHandle AsyncWaitHandle
	{
		get
		{
			throw new NotImplementedException();
		}
	}

	public AsyncCallback Callback
	{
		get
		{
			return this.@Ә;
		}
		set
		{
			this.@Ә = value;
		}
	}

	public AsyncCallback CompleteCallback
	{
		get
		{
			return this.@ә;
		}
		set
		{
			this.@ә = value;
		}
	}

	public bool CompletedSynchronously
	{
		get
		{
			return this.@ӗ;
		}
	}

	public Exception Error
	{
		get
		{
			return this.@Ӛ;
		}
		set
		{
			this.@Ӛ = value;
		}
	}

	public bool IsCompleted
	{
		get
		{
			return this.@Ӗ;
		}
	}

	public ChatPortal Portal
	{
		get
		{
			return this.@ӓ;
		}
	}

	public ChatResponse Response
	{
		get
		{
			return this.@Ӕ;
		}
	}

	internal @Ԍ(ChatPortal portal, ChatResponse res, object state)
	{
		this.@ӓ = portal;
		this.@Ӕ = res;
		this.@ӕ = state;
		this.@Ӗ = true;
		this.@ӗ = true;
	}

	internal void @ӛ()
	{
		this.@Ӗ = true;
	}

	internal void @Ӝ()
	{
		this.@Ӗ = false;
		this.@ӗ = false;
	}
}