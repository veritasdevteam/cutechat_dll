using System;
using System.ComponentModel;
using System.Reflection;
using System.Security;

internal class @ӕ
{
	public @ӕ()
	{
	}

	public static bool IsPublicException(Exception exception)
	{
		if (exception is ExecutionEngineException || exception is LicenseException || exception is OutOfMemoryException || exception is SecurityException || exception is UnauthorizedAccessException || exception is ArgumentException)
		{
			return true;
		}
		return exception is TargetInvocationException;
	}
}