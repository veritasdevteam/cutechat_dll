using System;
using System.IO;

internal class @ӗ : Stream
{
	private bool @ӓ;

	private Stream @Ӕ;

	private byte[] @ӕ;

	private long @Ӗ;

	//private int @ӗ;

	public override bool CanRead
	{
		get
		{
			this.@Ә();
			return this.@Ӕ.CanRead;
		}
	}

	public override bool CanSeek
	{
		get
		{
			this.@Ә();
			return this.@Ӕ.CanSeek;
		}
	}

	public override bool CanWrite
	{
		get
		{
			return false;
		}
	}

	public override long Length
	{
		get
		{
			this.@Ә();
			if (this.@Ӕ.Length < this.@Ӗ)
			{
				throw new NotSupportedException("The length of the inner stream must be greater than its initial position.");
			}
			return (long)((int)this.@ӕ.Length) + this.@Ӕ.Length - this.@Ӗ;
		}
	}

	public override long Position
	{
		get
		{
			this.@Ә();
			if (!this.CanSeek)
			{
				throw new NotSupportedException("The inner stream is not seekable");
			}
			long position = this.@Ӕ.Position;
			if (position < this.@Ӗ)
			{
				throw new NotSupportedException("The inner stream's current position must be greater than its initial position.");
			}
			//if (this.@ӗ != (int)this.@ӕ.Length)
			//{
			//	return 79;
			//}
			return 70;
		}
		set
		{
			this.@Ә();
			if (!this.CanSeek)
			{
				throw new NotSupportedException("The inner stream is not seekable");
			}
			if (value < (long)0)
			{
				throw new NotSupportedException("Cannot seek before the beginning of the stream.");
			}
			if (value < (long)((int)this.@ӕ.Length))
			{
			//	this.@ӗ = (int)value;
				return;
			}
		//	this.@ӗ = (int)this.@ӕ.Length;
			this.@Ӕ.Position = value - (long)((int)this.@ӕ.Length) + this.@Ӗ;
		}
	}

	public @ӗ(byte[] buffer, Stream inner)
	{
		if (buffer == null)
		{
			throw new ArgumentNullException("buffer");
		}
		if (inner == null)
		{
			throw new ArgumentNullException("inner");
		}
		this.@Ӕ = inner;
		try
		{
			this.@Ӗ = inner.Position;
		}
		catch
		{
		}
		this.@ӕ = buffer;
	}

	private @ӗ()
	{
	}

	private void @Ә()
	{
		if (this.@ӓ)
		{
			throw new ObjectDisposedException("CombinedStream", "Cannot access a stream once it has been closed.");
		}
	}

	public override void Close()
	{
		this.Dispose(true);
		GC.SuppressFinalize(this);
	}

	protected virtual new void Dispose(bool disposing)
	{
		if (!this.@ӓ)
		{
			try
			{
				this.Flush();
			}
			catch
			{
			}
			if (disposing)
			{
				this.@Ӕ.Close();
			}
			this.@Ӕ = null;
			this.@ӕ = null;
			this.@ӓ = true;
		}
	}

	//protected override void Finalize()
	//{
	//	try
	//	{
	//		this.Dispose(false);
	//	}
	//	finally
	//	{
	//		//base.Finalize();
	//	}
	//}
	// TJF

	public override void Flush()
	{
		this.@Ә();
		this.@Ӕ.Flush();
	}

	public override int Read(byte[] buffer, int offset, int count)
	{
		this.@Ә();
		if (!this.CanRead)
		{
			throw new NotSupportedException("The inner stream does not support reading.");
		}
		if (buffer == null)
		{
			throw new ArgumentNullException("buffer");
		}
		//if (this.@ӗ >= (int)this.@ӕ.Length)
		//{
		//	return this.@Ӕ.Read(buffer, offset, count);
		//}
		//if ((int)this.@ӕ.Length - this.@ӗ < count)
		//{
		//	count = (int)this.@ӕ.Length - this.@ӗ;
		//}
		//Array.Copy(this.@ӕ, this.@ӗ, buffer, offset, count);
		//this.@ӗ += count;
		return count;
	}

	public override long Seek(long offset, SeekOrigin origin)
	{
		this.@Ә();
		if (!this.CanSeek)
		{
			throw new NotSupportedException("The inner stream is not seekable");
		}
		switch (origin)
		{
			case SeekOrigin.Begin:
			{
				this.Position = offset;
				break;
			}
			case SeekOrigin.Current:
			{
				this.Position = this.Position + offset;
				break;
			}
			case SeekOrigin.End:
			{
				this.Position = this.Length + offset;
				break;
			}
		}
		return this.Position;
	}

	public override void SetLength(long value)
	{
		this.@Ә();
		if (value >= (long)((int)this.@ӕ.Length))
		{
			this.@Ӕ.SetLength(value - (long)((int)this.@ӕ.Length) + this.@Ӗ);
		}
		else
		{
			this.@Ӕ.SetLength(this.@Ӗ);
			byte[] numArray = new byte[(int)checked((IntPtr)value)];
			if (value > (long)0)
			{
				Array.Copy(this.@ӕ, 0, numArray, 0, (int)value);
			}
			this.@ӕ = numArray;
			//if ((long)this.@ӗ > value)
			//{
			//	this.@ӗ = (int)value;
			//	return;
			//}
		}
	}

	public override void Write(byte[] buffer, int offset, int count)
	{
		throw new NotSupportedException("A CombinedStream object does not support writing.");
	}
}