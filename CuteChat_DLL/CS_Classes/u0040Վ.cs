using System;
using System.IO;

internal abstract class @Վ : Stream
{
	public override bool CanSeek
	{
		get
		{
			return false;
		}
	}

	public override long Length
	{
		get
		{
			throw new InvalidOperationException();
		}
	}

	public override long Position
	{
		get
		{
			throw new InvalidOperationException();
		}
		set
		{
			throw new InvalidOperationException();
		}
	}

	protected @Վ()
	{
	}

	public override long Seek(long offset, SeekOrigin origin)
	{
		throw new InvalidOperationException();
	}

	public override void SetLength(long value)
	{
		throw new InvalidOperationException();
	}
}