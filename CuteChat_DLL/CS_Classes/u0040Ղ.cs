using CuteChat;
using System;
using System.Collections;

internal class @Ղ : IComparer
{
	private int @ӓ;

	public static IComparer SortByTimeAsc
	{
		get
		{
			return new @Ղ(1);
		}
	}

	public static IComparer SortByTimeDesc
	{
		get
		{
			return new @Ղ(-1);
		}
	}

	public @Ղ(int order)
	{
		this.@ӓ = order;
	}

	public int Compare(object x, object y)
	{
		int num = this.@ӓ;
		DateTime objectTime = ((ChatPortalDataItem)x).ObjectTime;
		return num * objectTime.CompareTo(((ChatPortalDataItem)y).ObjectTime);
	}
}