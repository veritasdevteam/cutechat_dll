using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

internal class @ԛ
{
	public @ԛ()
	{
	}

	public static object Deserialize(Stream stream)
	{
		@ԝ _u0040ԝ = new @ԝ(stream);
		if (_u0040ԝ.ReadString() != "SERIALIZE")
		{
			throw new Exception("Invalid data (serialize)!");
		}
		if (!_u0040ԝ.ReadBoolean())
		{
			return null;
		}
		string str = _u0040ԝ.ReadString();
		if (str != "PREFIX_SERIALIZE")
		{
			throw new Exception(string.Concat("Invalid data (prefix):", str, "!"));
		}
		int num = _u0040ԝ.ReadInt32();
		byte[] numArray = _u0040ԝ.ReadBytes();
		if ((int)numArray.Length != num)
		{
			throw new Exception(string.Concat(new object[] { "Invalid data length!", (int)numArray.Length, "/", num }));
		}
		string str1 = _u0040ԝ.ReadString();
		if (str1 != "SUFFIX_SERIALIZE")
		{
			throw new Exception(string.Concat("Invalid data (suffix):", str1, "!", Convert.ToBase64String(numArray)));
		}
		return (new BinaryFormatter()).Deserialize(new MemoryStream(numArray));
	}

	public static void Serialize(Stream stream, object graph)
	{
		@Ԝ _u0040Ԝ = new @Ԝ(stream);
		_u0040Ԝ.Write("SERIALIZE");
		if (graph == null)
		{
			_u0040Ԝ.Write(false);
			return;
		}
		_u0040Ԝ.Write(true);
		_u0040Ԝ.Write("PREFIX_SERIALIZE");
		MemoryStream memoryStream = new MemoryStream();
		(new BinaryFormatter()).Serialize(memoryStream, graph);
		byte[] array = memoryStream.ToArray();
		_u0040Ԝ.Write((int)array.Length);
		_u0040Ԝ.Write(array);
		_u0040Ԝ.Write("SUFFIX_SERIALIZE");
	}
}