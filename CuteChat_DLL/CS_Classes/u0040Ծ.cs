using CuteChat;
using System;
using System.Collections;
using System.IO;
using System.Web;
using System.Xml;

internal class @Ծ : StringWriter
{
	private ArrayList @ӓ;

	public ArrayList Sinks
	{
		get
		{
			if (this.@ӓ == null)
			{
				this.@ӓ = new ArrayList();
			}
			return this.@ӓ;
		}
	}

	public @Ծ()
	{
	}

	internal string @Ӕ(string u0040ӓ)
	{
		if (u0040ӓ == null || u0040ӓ == "")
		{
			return "";
		}
		XmlDocument xmlDocument = new XmlDocument();
		xmlDocument.LoadXml("<xml xmlns:v='urn:schemas-microsoft-com:vml' xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:V='urn:schemas-microsoft-com:vml' xmlns:O='urn:schemas-microsoft-com:office:office'></xml>");
		SafeHtmlParser.ParseHtml(xmlDocument.DocumentElement, u0040ӓ);
		this.WriteNodes(xmlDocument.DocumentElement.ChildNodes);
		return xmlDocument.InnerText;
	}

	public void WriteAttribute(XmlAttribute attr)
	{
		bool flag;
		foreach (object sink in this.Sinks)
		{
			((@Լ)sink).HandleAttribute(attr, out flag);
			if (!flag)
			{
				continue;
			}
			return;
		}
		string lower = attr.Name.ToLower();
		if (lower.StartsWith("on"))
		{
			return;
		}
		if (lower == "id" || lower == "name")
		{
			return;
		}
		string str = attr.Value.ToLower();
		if (str.IndexOf("script:") != -1)
		{
			return;
		}
		if (lower == "style" && str.IndexOf("expression") != -1)
		{
			return;
		}
		this.Write(attr.OuterXml);
	}

	public void WriteElement(XmlElement element)
	{
		ElementSkipMode elementSkipMode = ElementSkipMode.SkipNone;
		foreach (object sink in this.Sinks)
		{
			((@Լ)sink).HandleElement(element, out elementSkipMode);
			switch (elementSkipMode)
			{
				case ElementSkipMode.SkipAll:
				{
					return;
				}
				case ElementSkipMode.SkipElement:
				{
					this.WriteNodes(element.ChildNodes);
					return;
				}
				case ElementSkipMode.SkipChildNodes:
				{
					while (element.ChildNodes.Count > 0)
					{
						element.RemoveChild(element.ChildNodes[0]);
					}
					continue;
				}
				default:
				{
					continue;
				}
			}
		}
		string lower = element.Name.ToLower();
		this.Write("<");
		this.Write(element.Name);
		this.Write(" ");
		if (lower != null && (lower == "a" || lower == "area"))
		{
			this.Write(" target='_blank' ");
		}
		foreach (XmlAttribute attribute in element.Attributes)
		{
			this.Write(" ");
			this.WriteAttribute(attribute);
		}
		if (lower == "br" || lower == "hr")
		{
			this.Write(" />");
			return;
		}
		this.Write(">");
		this.WriteNodes(element.ChildNodes);
		this.Write("</");
		this.Write(element.Name);
		this.Write(">");
	}

	public void WriteNode(XmlNode node)
	{
		switch (node.NodeType)
		{
			case XmlNodeType.Element:
			{
				this.WriteElement((XmlElement)node);
				return;
			}
			case XmlNodeType.Attribute:
			{
				this.Write("[");
				this.Write(node.NodeType.ToString());
				this.Write("]");
				return;
			}
			case XmlNodeType.Text:
			{
				this.WriteText(node.InnerText);
				return;
			}
			case XmlNodeType.CDATA:
			{
				this.WriteText(node.InnerText);
				return;
			}
			default:
			{
				this.Write("[");
				this.Write(node.NodeType.ToString());
				this.Write("]");
				return;
			}
		}
	}

	public void WriteNodes(XmlNodeList nodes)
	{
		foreach (XmlNode node in nodes)
		{
			this.WriteNode(node);
		}
	}

	public void WriteText(string text)
	{
		foreach (object sink in this.Sinks)
		{
			text = ((@Լ)sink).HandleText(text);
			if (text != null)
			{
				continue;
			}
			return;
		}
		this.Write(HttpUtility.HtmlEncode(text));
	}
}