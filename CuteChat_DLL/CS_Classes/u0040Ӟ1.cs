using System;

internal class @Ӟ : @Ӡ
{
	public override int MaxWindowBits
	{
		get
		{
			return 16;
		}
	}

	public override int Method
	{
		get
		{
			return 9;
		}
	}

	public @Ӟ(int compressionLevel) : base(compressionLevel)
	{
	}

	public @Ӟ(int compressionLevel, bool useZlibHeaders) : base(compressionLevel, useZlibHeaders)
	{
	}
}