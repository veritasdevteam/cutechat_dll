using System;
using System.Collections;
using System.Text;

internal class @Պ
{
	public @Պ()
	{
	}

	public static string Join(ICollection collection)
	{
		return @Պ.Join(collection, ",", null, null, null);
	}

	public static string Join(ICollection collection, string joiner)
	{
		return @Պ.Join(collection, joiner, null, null, null);
	}

	public static string Join(ICollection collection, string joiner, string left, string right, string defaultStr)
	{
		if (collection == null)
		{
			return null;
		}
		return @Պ.Join(collection.GetEnumerator(), joiner, left, right, defaultStr);
	}

	public static string Join(IEnumerable enumerable)
	{
		return @Պ.Join(enumerable, ",", null, null, null);
	}

	public static string Join(IEnumerable enumerable, string joiner)
	{
		return @Պ.Join(enumerable, joiner, null, null, null);
	}

	public static string Join(IEnumerable enumerable, string joiner, string left, string right, string defaultStr)
	{
		if (enumerable == null)
		{
			return null;
		}
		return @Պ.Join(enumerable.GetEnumerator(), joiner, left, right, defaultStr);
	}

	public static string Join(IEnumerator enumerator)
	{
		return @Պ.Join(enumerator, ",", null, null, null);
	}

	public static string Join(IEnumerator enumerator, string joiner)
	{
		return @Պ.Join(enumerator, joiner, null, null, null);
	}

	public static string Join(IEnumerator enumerator, string joiner, string left, string right, string defaultStr)
	{
		object obj;
		object obj1;
		if (enumerator == null)
		{
			return null;
		}
		StringBuilder stringBuilder = new StringBuilder();
		if (!enumerator.MoveNext())
		{
			return string.Empty;
		}
		stringBuilder.Append(left);
		object current = enumerator.Current;
		StringBuilder stringBuilder1 = stringBuilder;
		if (current == null)
		{
			obj = defaultStr;
		}
		else
		{
			obj = current;
		}
		stringBuilder1.Append(obj);
		stringBuilder.Append(right);
		while (enumerator.MoveNext())
		{
			stringBuilder.Append(joiner);
			stringBuilder.Append(left);
			current = enumerator.Current;
			StringBuilder stringBuilder2 = stringBuilder;
			if (current == null)
			{
				obj1 = defaultStr;
			}
			else
			{
				obj1 = current;
			}
			stringBuilder2.Append(obj1);
			stringBuilder.Append(right);
		}
		return stringBuilder.ToString();
	}

	public static string Join(object[] objs)
	{
		return @Պ.Join(objs, ",", null, null, null);
	}

	public static string Join(object[] objs, string joiner)
	{
		return @Պ.Join(objs, joiner, null, null, null);
	}

	public static string Join(object[] objs, string joiner, string left, string right, string defaultStr)
	{
		object obj;
		object obj1;
		if (objs == null)
		{
			return null;
		}
		if (objs.Length == 0)
		{
			return string.Empty;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(left);
		object obj2 = objs[0];
		StringBuilder stringBuilder1 = stringBuilder;
		if (obj2 == null)
		{
			obj = defaultStr;
		}
		else
		{
			obj = obj2;
		}
		stringBuilder1.Append(obj);
		stringBuilder.Append(right);
		int length = (int)objs.Length;
		for (int i = 1; i < length; i++)
		{
			stringBuilder.Append(joiner);
			stringBuilder.Append(left);
			obj2 = objs[i];
			StringBuilder stringBuilder2 = stringBuilder;
			if (obj2 == null)
			{
				obj1 = defaultStr;
			}
			else
			{
				obj1 = obj2;
			}
			stringBuilder2.Append(obj1);
			stringBuilder.Append(right);
		}
		return stringBuilder.ToString();
	}
}