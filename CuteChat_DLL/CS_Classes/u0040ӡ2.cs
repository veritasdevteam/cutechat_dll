using System;
using Xceed.Compression;
using Xceed.Compression.CompressionEngine.Managed;

internal class @ӡ : Decompressor
{
	private @ӳ @ӓ;

	private byte[] @Ӕ;

	public virtual int MaxWindowBits
	{
		get
		{
			return 15;
		}
	}

	public virtual int Method
	{
		get
		{
			return 8;
		}
	}

	public @ӡ() : this(false)
	{
	}

	public @ӡ(bool useZlibHeaders)
	{
		int num;
		try
		{
			this.@ӓ = new @ӳ();
			num = (useZlibHeaders ? this.MaxWindowBits : -this.MaxWindowBits);
			if (@Ӵ.@Ӝ(ref this.@ӓ, this.Method, num) != ReturnCode.Z_OK)
			{
				throw new CompressionException("Unable to initialize the managed deflate decompression engine.");
			}
		}
		catch (CompressionException compressionException)
		{
			throw;
		}
		catch (Exception exception)
		{
			if (!@ӕ.IsPublicException(exception))
			{
				throw new CompressionInternalException(exception);
			}
			throw;
		}
	}

	public override int Decompress(byte[] buffer, int offset, int count, ref bool endOfData, out byte[] decompressed, out int remaining)
	{
		int num;
		if (buffer == null)
		{
			throw new ArgumentNullException("buffer", "The buffer parameter cannot be null.");
		}
		if (offset < 0)
		{
			@Ӗ.ThrowArgumentOutOfRangeException("offset", offset, "The offset parameter must be zero.");
		}
		if (count < 0)
		{
			@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count parameter cannot be less than zero.");
		}
		if (count > (int)buffer.Length - offset)
		{
			@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count parameter exceeds the buffer's remaining length after offset.");
		}
		if (count == 0 && this.@ӓ.@Ӗ == 0)
		{
			endOfData = true;
			decompressed = new byte[0];
			remaining = 0;
			return 0;
		}
		try
		{
			int length = count * 2;
			this.@ӓ.@ӓ = buffer;
			this.@ӓ.@Ӕ = (uint)offset;
			this.@ӓ.@ӕ = (uint)count;
			if (count == 0)
			{
				length = 65536;
			}
			if (this.@Ӕ == null || (int)this.@Ӕ.Length < length)
			{
				this.@Ӕ = new byte[length];
			}
			else
			{
				length = (int)this.@Ӕ.Length;
			}
			this.@ӓ.@ӗ = this.@Ӕ;
			this.@ӓ.@Ә = 0;
			this.@ӓ.@ә = (uint)length;
			ulong num1 = this.@ӓ.@Ӛ;
			if (!endOfData)
			{
				FlushValue flushValue = FlushValue.Z_NO_FLUSH;
				if (count == 0)
				{
					flushValue = FlushValue.Z_SYNC_FLUSH;
				}
				ReturnCode returnCode = @Ӵ.@ӗ(ref this.@ӓ, flushValue);
				while (this.@ӓ.@ә == 0 && this.@ӓ.@ӕ != 0)
				{
					length = length + (length < 268435456 ? length : 134217728);
					byte[] numArray = new byte[length];
					this.@Ӕ.CopyTo(numArray, 0);
					this.@Ӕ = numArray;
					this.@ӓ.@ӗ = this.@Ӕ;
					this.@ӓ.@Ә = (uint)(this.@ӓ.@Ӛ - num1);
					//this.@ӓ.@ә = length - (uint)(this.@ӓ.@Ӛ - num1);
					returnCode = @Ӵ.@ӗ(ref this.@ӓ, flushValue);
				}
				if (returnCode != ReturnCode.Z_OK)
				{
					if (returnCode != ReturnCode.Z_STREAM_END)
					{
						throw new CompressionException("Expected end of stream not found");
					}
					length = (int)(this.@ӓ.@Ӛ - num1);
					endOfData = true;
				}
				else
				{
					length = (int)(this.@ӓ.@Ӛ - num1);
				}
			}
			else
			{
				byte[] numArray1 = new byte[count + 1];
				if (count > 0)
				{
					Array.Copy(buffer, offset, numArray1, 0, count);
				}
				this.@ӓ.@ӓ = numArray1;
				this.@ӓ.@Ӕ = 0;
				this.@ӓ.@ӕ = (uint)(count + 1);
				ReturnCode returnCode1 = @Ӵ.@ӗ(ref this.@ӓ, FlushValue.Z_FINISH);
				while (returnCode1 == ReturnCode.Z_OK || returnCode1 == ReturnCode.Z_BUF_ERROR)
				{
					length = length + (length < 268435456 ? length : 134217728);
					byte[] numArray2 = new byte[length];
					this.@Ӕ.CopyTo(numArray2, 0);
					this.@Ӕ = numArray2;
					this.@ӓ.@ӗ = this.@Ӕ;
					this.@ӓ.@Ә = (uint)(this.@ӓ.@Ӛ - num1);
					//this.@ӓ.@ә = length - (uint)(this.@ӓ.@Ӛ - num1);
					ulong num2 = this.@ӓ.@Ӛ;
					returnCode1 = @Ӵ.@ӗ(ref this.@ӓ, FlushValue.Z_FINISH);
					if (num2 != this.@ӓ.@Ӛ)
					{
						continue;
					}
					returnCode1 = ReturnCode.Z_DATA_ERROR;
				}
				this.@ӓ.@ӕ--;
				if (returnCode1 != ReturnCode.Z_STREAM_END)
				{
					throw new CompressionException("Expected end of stream not found");
				}
				length = (int)(this.@ӓ.@Ӛ - num1);
			}
			decompressed = this.@Ӕ;
			remaining = (int)this.@ӓ.@ӕ;
			num = length;
		}
		catch (CompressionException compressionException)
		{
			throw;
		}
		catch (Exception exception)
		{
			if (!@ӕ.IsPublicException(exception))
			{
				throw new CompressionInternalException(exception);
			}
			throw;
		}
		return num;
	}
}