using System;
using System.IO;
using Xceed.Compression.Formats;

internal class @Տ : @Վ
{
	private Stream @ӓ;

	public override bool CanRead
	{
		get
		{
			return false;
		}
	}

	public override bool CanWrite
	{
		get
		{
			return true;
		}
	}

	public @Տ(Stream inner)
	{
		if (inner == null)
		{
			throw new ArgumentNullException("inner");
		}
		this.@ӓ = inner;
	}

	public override void Flush()
	{
		this.@ӓ.Flush();
	}

	public override int Read(byte[] buffer, int offset, int count)
	{
		throw new InvalidOperationException();
	}

	public override void Write(byte[] buffer, int offset, int count)
	{
		byte[] numArray = ZLibCompressedStream.Decompress(buffer, 0, count);
		this.@ӓ.Write(numArray, 0, (int)numArray.Length);
	}
}