using CuteChat;
using System;
using System.Collections;
using System.Text;

internal class @ԫ : ChatConnection
{
	private ChatCookie @ӓ = new ChatCookie();

	private int @Ӕ = -1;

	private ArrayList @ӕ = new ArrayList();

	public ChatCookie Cookie
	{
		get
		{
			return this.@ӓ;
		}
	}

	public @ԫ(ChatPortal portal, ChatIdentity identity) : base(portal, identity)
	{
	}

	internal void @Ӗ(ChatPlaceUser u0040ӓ, string u0040Ӕ, string u0040ӕ)
	{
		string str = ChatUtility.EncodeJScriptString(u0040ӓ.Identity.UniqueId);
		string str1 = ChatUtility.EncodeJScriptString(u0040ӓ.DisplayName);
		string str2 = ChatUtility.EncodeJScriptString(u0040Ӕ);
		string str3 = ChatUtility.EncodeJScriptString(u0040ӕ);
		string str4 = string.Concat(new string[] { "[", str, ",", str1, ",", str2, ",", str3, "]" });
		this.@ӕ.Add(str4);
	}

	public string HandlePartial()
	{
		base.SetActivate(false);
		DateTime dateTime = new DateTime(2000, 1, 1);
		string serverProperty = base.PlaceUser.Info.GetServerProperty("InstantActivateTime");
		if (serverProperty != null)
		{
			dateTime = new DateTime(long.Parse(serverProperty));
		}
		if (this.@Ӕ == -1)
		{
			this.@Ӕ = (int)base.Portal.DataManager.LoadInstantOfflineMessages(base.Identity.UniqueId, dateTime).Length;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append("{OfflineMsgCount:").Append(this.@Ӕ).Append(",Messages:[");
		for (int i = 0; i < this.@ӕ.Count; i++)
		{
			if (i > 0)
			{
				stringBuilder.Append(",");
			}
			stringBuilder.Append(this.@ӕ[i]);
		}
		stringBuilder.Append("]}");
		return string.Concat("ACTIVE:", stringBuilder.ToString());
	}

	protected internal override void PushSTCMessage(string message)
	{
	}
}