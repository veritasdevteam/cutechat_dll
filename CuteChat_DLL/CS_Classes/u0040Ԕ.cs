using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Net;
using System.Web;

internal class @Ԕ : License
{
	private Type @ӓ;

	private string @Ӕ;

	private bool @ӕ = true;

	private string @Ӗ;

	private string @ӗ;

	private string @Ә;

	private string @ә;

	private string @Ӛ;

	private string @ӛ;

	private string @Ӝ;

	private string @ӝ;

	private string @Ӟ;

	private DateTime @ӟ;

	private string @Ӡ = "Unknown reason";

	public string Country
	{
		get
		{
			return this.@Ӗ;
		}
	}

	public DateTime Expiresdatetime
	{
		get
		{
			return this.@ӟ;
		}
	}

	public string InvalidMessage
	{
		get
		{
			return this.@Ӡ;
		}
	}

	public bool IsValid
	{
		get
		{
			return this.@ӕ;
		}
	}

	public Type LicensedType
	{
		get
		{
			return this.@ӓ;
		}
	}

	public override string LicenseKey
	{
		get
		{
			return this.@Ӕ;
		}
	}

	public string LicenseType
	{
		get
		{
			return this.@Ӝ;
		}
	}

	public string Locale
	{
		get
		{
			return this.@ӗ;
		}
	}

	public string OrderID
	{
		get
		{
			return this.@ә;
		}
	}

	public string Organization
	{
		get
		{
			return this.@Ә;
		}
	}

	public string ProductID
	{
		get
		{
			return this.@Ӛ;
		}
	}

	public string ProductVersion
	{
		get
		{
			return this.@ӛ;
		}
	}

	public string Remoteaddress
	{
		get
		{
			return this.@Ӟ;
		}
	}

	public string Remotehost
	{
		get
		{
			return this.@ӝ;
		}
	}

	public @Ԕ(Type type, string key)
	{
		this.@ӓ = type;
		this.@Ӕ = key;
		if (key.Length == 0)
		{
			this.@Ӡ = " no key ? ";
			this.@ӕ = false;
			return;
		}
		try
		{
			string[] strArrays = key.Split(new char[] { ';' });
			this.@Ӗ = strArrays[0];
			this.@ӗ = strArrays[1];
			this.@Ә = strArrays[2];
			this.@ә = strArrays[3];
			this.@Ӛ = strArrays[4];
			this.@ӛ = strArrays[5];
			this.@Ӝ = strArrays[6];
			this.@ӝ = strArrays[7];
			this.@Ӟ = strArrays[8];
			string str = strArrays[9];
			try
			{
				this.@ӟ = @Ԕ.ParseDate(str);
			}
			catch
			{
				this.@ӟ = @Ԕ.ParseDate("09/09/2003");
			}
			this.@ӝ = this.@ӝ.ToLower();
			string lower = string.Empty;
			lower = @Ԕ.@ӡ(lower);
			string lower1 = string.Empty;
			IPAddress[] addressList = Dns.GetHostByName(Dns.GetHostName()).AddressList;
			string empty = string.Empty;
			for (int i = 0; i < (int)addressList.Length; i++)
			{
				empty = string.Concat(empty, addressList[i].ToString(), ";");
			}
			if (this.@Ӝ == "0")
			{
				this.@ӕ = (this.@ӟ <= DateTime.Now ? false : string.Compare(lower, "localhost", true, CultureInfo.InvariantCulture) == 0);
				if (!this.@ӕ)
				{
					this.@Ӡ = "Trial license expired!";
				}
			}
			else if (this.@Ӝ == "1")
			{
				this.@ӕ = (string.Compare(lower, "localhost", true, CultureInfo.InvariantCulture) == 0 ? true : this.@ӝ.IndexOf(lower) != -1);
				if (!this.@ӕ)
				{
					this.@Ӡ = "Domain not match!";
				}
			}
			else if (this.@Ӝ == "2")
			{
				this.@ӕ = (string.Compare(lower, "localhost", true, CultureInfo.InvariantCulture) == 0 ? true : this.@Ӟ.IndexOf(lower1) != -1);
				if (!this.@ӕ)
				{
					this.@Ӡ = "IP not match!";
				}
			}
			else if (this.@Ӝ == "3")
			{
				this.@ӕ = (string.Compare(lower, "localhost", true, CultureInfo.InvariantCulture) == 0 ? true : string.Compare(lower, this.@ӝ.Trim(), true, CultureInfo.InvariantCulture) == 0);
				if (!this.@ӕ)
				{
					this.@Ӡ = "Small business license: Domain not match!";
				}
			}
			else if (this.@Ӝ == "4")
			{
				this.@ӕ = this.@ӟ > DateTime.Now;
				if (!this.@ӕ)
				{
					this.@Ӡ = "Trial license license expired! ";
				}
			}
			else if (this.@Ӝ != "5")
			{
				this.@Ӡ = string.Concat("wrong license type ", this.@Ӝ);
				this.@ӕ = false;
			}
			else
			{
				this.@ӕ = true;
			}
			if (this.@ӛ != "510406")
			{
				this.@Ӡ = "Product version not match";
				this.@ӕ = false;
			}
		}
		catch (Exception exception)
		{
			this.@Ӡ = string.Concat("exception:", exception.Message);
			this.@ӕ = false;
		}
	}

	internal static string @ӡ(string u0040ӓ)
	{
		u0040ӓ = u0040ӓ.ToLower();
		string[] strArrays = u0040ӓ.Split(new char[] { '.' });
		if ((int)strArrays.Length < 3)
		{
			return u0040ӓ;
		}
		string[] strArrays1 = new string[] { "com", "net", "gov", "edu", "org", "biz", "name", "info", "cc", "tv" };
		for (int i = 0; i < (int)strArrays1.Length; i++)
		{
			string str = strArrays1[i];
			if (u0040ӓ.EndsWith(string.Concat(".", str)))
			{
				return string.Concat(strArrays[(int)strArrays.Length - 2], ".", str);
			}
			if (strArrays[(int)strArrays.Length - 2] == str)
			{
				return string.Concat(new string[] { strArrays[(int)strArrays.Length - 3], ".", str, ".", strArrays[(int)strArrays.Length - 1] });
			}
		}
		if (!u0040ӓ.StartsWith("www."))
		{
			return u0040ӓ;
		}
		return u0040ӓ.Remove(0, 4);
	}

	public override void Dispose()
	{
	}

	public static DateTime ParseDate(string str)
	{
		string[] strArrays = str.Split(new char[] { '/' });
		int num = int.Parse(strArrays[0].TrimStart(new char[] { '0' }));
		int num1 = int.Parse(strArrays[1].TrimStart(new char[] { '0' }));
		return new DateTime(int.Parse(strArrays[2].TrimStart(new char[] { '0' })), num1, num);
	}
}