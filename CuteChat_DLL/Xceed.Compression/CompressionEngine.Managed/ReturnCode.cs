using System;

namespace Xceed.Compression.CompressionEngine.Managed
{
	internal enum ReturnCode
	{
		Z_INTERNAL_ERROR = -7,
		Z_VERSION_ERROR = -6,
		Z_BUF_ERROR = -5,
		Z_MEM_ERROR = -4,
		Z_DATA_ERROR = -3,
		Z_STREAM_ERROR = -2,
		Z_ERRNO = -1,
		Z_OK = 0,
		Z_STREAM_END = 1,
		Z_NEED_DICT = 2
	}
}