using System;

namespace Xceed.Compression
{
	public abstract class Compressor
	{
		protected Compressor()
		{
		}

		public abstract int Compress(byte[] buffer, int offset, int count, bool endOfData, out byte[] compressed);
	}
}