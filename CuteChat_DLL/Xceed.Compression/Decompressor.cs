using System;

namespace Xceed.Compression
{
	public abstract class Decompressor
	{
		protected Decompressor()
		{
		}

		public abstract int Decompress(byte[] buffer, int offset, int count, ref bool endOfData, out byte[] decompressed, out int remaining);

        internal bool IsPublicException(Exception exception)
        {
            throw new NotImplementedException();
        }
    }
}