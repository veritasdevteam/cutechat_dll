using System;
using System.IO;

namespace Xceed.Compression
{
	public class CompressedStream : Stream
	{
		private Stream @ӓ;

		private Compressor @Ӕ;

		private Decompressor @ӕ;

		private bool @Ӗ;

		private CompressionMethod @ӗ;

		private CompressionLevel @Ә;

		private MemoryStream @ә = new MemoryStream();

		private bool @Ӛ;

		private const int @ӛ = 32768;

		private bool @Ӝ;

		private bool @ӝ;

		private byte[] @Ӟ = new byte[0];

		private bool @ӟ;

		private bool @Ӡ;

		private bool @ӡ;

		public override bool CanRead
		{
			get
			{
				this.@Ӣ();
				if (this.@ӝ)
				{
					return this.@ӓ.CanRead;
				}
				if (!this.@ӓ.CanRead)
				{
					return false;
				}
				if (this.@Ӗ)
				{
					return true;
				}
				return this.@ӕ != null;
			}
		}

		public override bool CanSeek
		{
			get
			{
				this.@Ӣ();
				if (!this.@ӝ)
				{
					return false;
				}
				return this.@ӓ.CanSeek;
			}
		}

		public override bool CanWrite
		{
			get
			{
				this.@Ӣ();
				if (this.@ӝ)
				{
					return this.@ӓ.CanWrite;
				}
				if (!this.@ӓ.CanWrite)
				{
					return false;
				}
				if (this.@Ӗ)
				{
					return true;
				}
				return this.@Ӕ != null;
			}
		}

		public Stream InnerStream
		{
			get
			{
				return this.@ӓ;
			}
		}

		public override long Length
		{
			get
			{
				this.@Ӣ();
				return this.@ӓ.Length;
			}
		}

		public override long Position
		{
			get
			{
				this.@Ӣ();
				return this.@ӓ.Position;
			}
			set
			{
				this.@Ӣ();
				if (!this.@ӝ)
				{
					throw new NotSupportedException("A CompressedStream object is not seekable.");
				}
				this.@ӓ.Position = value;
			}
		}

		public bool Transient
		{
			get
			{
				this.@Ӣ();
				return this.@Ӝ;
			}
			set
			{
				this.@Ӣ();
				this.@Ӝ = value;
			}
		}

		public CompressedStream(Stream inner) : this(inner, CompressionMethod.Deflated, CompressionLevel.Highest)
		{
		}

		public CompressedStream(Stream inner, CompressionMethod method, CompressionLevel level) : this(inner, null, null)
		{
			if (method > (CompressionMethod.Deflated | CompressionMethod.Deflated64))
			{
				this.@ӡ = true;
				method -= (CompressionMethod)128;
			}
			bool flag = this.@ӡ;
			if (level > (CompressionLevel.Lowest | CompressionLevel.Normal | CompressionLevel.Highest))
			{
				flag = true;
				level -= (CompressionLevel)128;
			}
			
			this.@ӗ = method;
			this.@Ә = level;
			this.@Ӗ = true;
			if (!flag && (this.@ӗ == CompressionMethod.Stored || level == CompressionLevel.None))
			{
				this.@ӝ = true;
			}
		}

		public CompressedStream(Stream inner, Compressor compressor) : this(inner, compressor, null)
		{
		}

		public CompressedStream(Stream inner, Decompressor decompressor) : this(inner, null, decompressor)
		{
		}

		public CompressedStream(Stream inner, Compressor compressor, Decompressor decompressor)
		{
			if (inner == null)
			{
				throw new ArgumentNullException("inner", "The inner stream parameter cannot be null.");
			}
			try
			{
				this.@ӓ = inner;
				this.@Ӕ = compressor;
				this.@ӕ = decompressor;
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				if (!@ӕ.IsPublicException(exception))
				{
					throw new CompressionInternalException("An unexpected internal failure prevented the CompressedStream object from being initialized properly.", exception);
				}
				throw;
			}
		}

		private CompressedStream()
		{
		}

		private void @Ӣ()
		{
			if (this.@ӟ)
			{
				throw new ObjectDisposedException("CompressedStream", "Cannot access a stream once it has been closed.");
			}
		}

		public override void Close()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual new void Dispose(bool disposing)
		{
			if (!this.@ӟ)
			{
				try
				{
					if (this.@Ӡ)
					{
						this.@Ӛ = true;
						this.Flush();
					}
				}
				catch (NotSupportedException notSupportedException)
				{
				}
				catch (Exception exception1)
				{
					Exception exception = exception1;
					if (disposing)
					{
						if (!@ӕ.IsPublicException(exception))
						{
							throw new CompressionInternalException(exception);
						}
						throw;
					}
				}
				if (disposing)
				{
					if (!this.@Ӝ)
					{
						this.@ӓ.Close();
					}
					IDisposable disposable = this.@Ӕ as IDisposable;
					if (disposable != null)
					{
						disposable.Dispose();
					}
					disposable = this.@ӕ as IDisposable;
					if (disposable != null)
					{
						disposable.Dispose();
					}
					this.@Ӕ = null;
					this.@ӕ = null;
					if (this.@ә != null)
					{
						this.@ә.Close();
						this.@ә = null;
					}
				}
				this.@ӓ = null;
				this.@ӟ = true;
			}
		}		

		public override void Flush()
		{
			this.@Ӣ();
			if (!this.@ӝ && this.@Ӡ && this.CanWrite)
			{
				this.Write(new byte[0], 0, 0);
			}
			this.@ӓ.Flush();
		}

		public Stream GetRemainingStream()
		{
			this.@Ӣ();
			if (!this.Transient)
			{
				throw new InvalidOperationException("An attempt was made to access the remaining data while the CompressedStream is not transient.");
			}
			if (!this.@Ӛ)
			{
				throw new InvalidOperationException("An attempt was made to access the remaining stream before the end of the CompressedStream was reached.");
			}
			if (this.@Ӟ == null)
			{
				throw new NotSupportedException("GetRemainingStream can only be called once.");
			}
			@ӗ _u0040ӗ = new @ӗ(this.@Ӟ, this.@ӓ);
			this.@Ӟ = null;
			return _u0040ӗ;
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			int num;
			int num1;
			this.@Ӣ();
			if (this.@ӝ)
			{
				return this.@ӓ.Read(buffer, offset, count);
			}
			if (!this.CanRead)
			{
				throw new NotSupportedException("This CompressedStream object does not support reading or decompressing.");
			}
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer", "The buffer parameter cannot be null.");
			}
			
			try
			{
				if (this.@ӕ == null)
				{
					CompressionMethod compressionMethod = this.@ӗ;
					if (compressionMethod == CompressionMethod.Deflated)
					{
						this.@ӕ = new @ӡ(this.@ӡ);
					}
					else
					{
						if (compressionMethod != CompressionMethod.Deflated64)
						{
							throw new CompressionInternalException("Unknown CompressionMethod value");
						}
						this.@ӕ = new @ӟ(this.@ӡ);
					}
				}
				try
				{
					num = this.@ә.Read(buffer, offset, count);
				}
				catch (IOException oException)
				{
					throw new CompressionInternalException(oException);
				}
				while (num == 0 && count > 0 && !this.@Ӛ)
				{
					this.@ә.SetLength((long)0);
					if (!this.@Ӛ)
					{
						byte[] numArray = new byte[32768];
						int num2 = this.@ӓ.Read(numArray, 0, (int)numArray.Length);
						this.@Ӛ = num2 == 0;
						byte[] numArray1 = null;
						int num3 = 0;
						int num4 = this.@ӕ.Decompress(numArray, 0, num2, ref this.@Ӛ, out numArray1, out num3);
						if (num3 > 0)
						{
							this.@Ӟ = new byte[num3];
							Array.Copy(numArray, num2 - num3, this.@Ӟ, 0, num3);
						}
						try
						{
							long position = this.@ә.Position;
							if (position != 0)
							{
								this.@ә.Write(numArray1, 0, num4);
								this.@ә.Position = position;
							}
							else
							{
								this.@ә = new MemoryStream(numArray1, 0, num4);
							}
						}
						catch (IOException oException1)
						{
							throw new CompressionInternalException(oException1);
						}
					}
					try
					{
						num = this.@ә.Read(buffer, offset, count);
					}
					catch (IOException oException2)
					{
						throw new CompressionInternalException(oException2);
					}
				}
				num1 = num;
			}
			catch (IOException oException3)
			{
				throw;
			}
			catch (Exception exception)
			{
				if (!@ӕ.IsPublicException(exception))
				{
					throw new CompressionInternalException(exception);
				}
				throw;
			}
			return num1;
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			this.@Ӣ();
			if (!this.@ӝ)
			{
				throw new NotSupportedException("A CompressedStream object is not seekable.");
			}
			return this.@ӓ.Seek(offset, origin);
		}

		public override void SetLength(long value)
		{
			this.@Ӣ();
			if (!this.@ӝ)
			{
				throw new NotSupportedException("A CompressedStream object is not seekable.");
			}
			this.@ӓ.SetLength(value);
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			this.@Ӣ();
			if (this.@ӝ)
			{
				this.@ӓ.Write(buffer, offset, count);
				return;
			}
			if (!this.CanWrite)
			{
				throw new NotSupportedException("This CompressedStream object does not support writing or compressing.");
			}
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer", "The buffer parameter cannot be null.");
			}
			
			try
			{
				if (!this.@Ӡ)
				{
					this.@Ӡ = count > 0;
				}
				if (this.@Ӡ)
				{
					if (this.@Ӕ == null)
					{
						CompressionMethod compressionMethod = this.@ӗ;
						if (compressionMethod == CompressionMethod.Deflated)
						{
							this.@Ӕ = new @Ӡ((int)this.@Ә, this.@ӡ);
						}
						else
						{
							if (compressionMethod != CompressionMethod.Deflated64)
							{
								throw new CompressionInternalException("Unknown CompressionMethod value");
							}
							this.@Ӕ = new @Ӟ((int)this.@Ә, this.@ӡ);
						}
					}
					byte[] numArray = null;
					int num = this.@Ӕ.Compress(buffer, offset, count, this.@Ӛ, out numArray);
					if (num > 0)
					{
						this.@ӓ.Write(numArray, 0, num);
					}
				}
			}
			catch (IOException oException)
			{
				throw;
			}
			catch (NotSupportedException notSupportedException)
			{
				throw new NotSupportedException("This CompressedStream object does not support writing or compressing.", notSupportedException);
			}
			catch (Exception exception)
			{
				if (!@ӕ.IsPublicException(exception))
				{
					throw new CompressionInternalException(exception);
				}
				throw;
			}
		}
	}
}