using System;

namespace Xceed.Compression
{
	public enum CompressionLevel
	{
		None = 0,
		Lowest = 1,
		Normal = 6,
		Highest = 9
	}
}