using System;

namespace Xceed.Compression
{
	public class CompressionInternalException : CompressionException
	{
		private const string @ӓ = "An unexpected internal failure occurred in the compression engine.";

		public CompressionInternalException() : base("An unexpected internal failure occurred in the compression engine.")
		{
		}

		public CompressionInternalException(Exception inner) : base("An unexpected internal failure occurred in the compression engine.", inner)
		{
		}

		public CompressionInternalException(string message) : base(message)
		{
		}

		public CompressionInternalException(string message, Exception inner) : base(message, inner)
		{
		}
	}
}