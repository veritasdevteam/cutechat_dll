using System;
using System.IO;
using Xceed.Compression;

namespace Xceed.Compression.Formats
{
	public class ZLibCompressedStream : FormattedCompressedStream
	{
		private CompressionMethod @ӓ = CompressionMethod.Deflated;

		private CompressionLevel @Ӕ = CompressionLevel.Highest;

		public ZLibCompressedStream(Stream inner, CompressionMethod method, CompressionLevel level) : base(inner)
		{
			if (inner == null)
			{
				throw new ArgumentNullException("inner");
			}
			if (!Enum.IsDefined(typeof(CompressionMethod), method))
			{
				throw new ArgumentException("An attempt was made to use an unknown compression method.", "method");
			}
			if (!Enum.IsDefined(typeof(CompressionLevel), level))
			{
				throw new ArgumentException("An attempt was made to use an unknown compression level.", "level");
			}
			this.@ӓ = method;
			this.@Ӕ = level;
		}

		public ZLibCompressedStream(Stream inner) : this(inner, CompressionMethod.Deflated, CompressionLevel.Highest)
		{
		}

		public ZLibCompressedStream(Stream inner, CompressionMethod method, CompressionLevel level, bool readHeader) : base(inner)
		{
			if (inner == null)
			{
				throw new ArgumentNullException("inner");
			}
			if (!Enum.IsDefined(typeof(CompressionMethod), method))
			{
				throw new ArgumentException("An attempt was made to use an unknown compression method.", "method");
			}
			if (!Enum.IsDefined(typeof(CompressionLevel), level))
			{
				throw new ArgumentException("An attempt was made to use an unknown compression level.", "level");
			}
			if (readHeader && !inner.CanRead)
			{
				throw new ArgumentException("An attempt was made to read the header while the inner stream was not readable.", "inner");
			}
			this.@ӓ = method;
			this.@Ӕ = level;
			if (readHeader)
			{
				base.@Ӟ();
			}
		}

		public static byte[] Compress(byte[] buffer)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			return ZLibCompressedStream.Compress(buffer, 0, (int)buffer.Length, CompressionMethod.Deflated, CompressionLevel.Highest);
		}

		public static byte[] Compress(byte[] buffer, CompressionMethod method, CompressionLevel level)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			return ZLibCompressedStream.Compress(buffer, 0, (int)buffer.Length, method, level);
		}

		public static byte[] Compress(byte[] buffer, int offset, int count, CompressionMethod method, CompressionLevel level)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (offset < 0)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("offset", offset, "The offset parameter cannot be less than zero.");
			}
			if (count < 0)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count parameter cannot be less than zero.");
			}
			if (count > (int)buffer.Length - offset)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count parameter exceeds the buffer's remaining length after offset.");
			}
			if (!Enum.IsDefined(typeof(CompressionMethod), method))
			{
				throw new ArgumentException("An attempt was made to use an unknown compression method.", "method");
			}
			MemoryStream memoryStream = new MemoryStream();
			using (ZLibCompressedStream zLibCompressedStream = new ZLibCompressedStream(memoryStream, method, level))
			{
				zLibCompressedStream.Write(buffer, offset, count);
			}
			return memoryStream.ToArray();
		}

		public static byte[] Decompress(byte[] buffer)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			return ZLibCompressedStream.Decompress(buffer, 0, (int)buffer.Length);
		}

		public static byte[] Decompress(byte[] buffer, int offset, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (offset < 0)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("offset", offset, "The offset parameter cannot be less than zero.");
			}
			if (count < 0)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count parameter cannot be less than zero.");
			}
			if (count > (int)buffer.Length - offset)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count parameter exceeds the buffer's remaining length after offset.");
			}
			byte[] array = new byte[0];
			using (MemoryStream memoryStream = new MemoryStream(buffer, offset, count))
			{
				using (MemoryStream memoryStream1 = new MemoryStream())
				{
					using (ZLibCompressedStream zLibCompressedStream = new ZLibCompressedStream(memoryStream))
					{
						byte[] numArray = new byte[32768];
						int num = 0;
						while (true)
						{
							int num1 = zLibCompressedStream.Read(numArray, 0, (int)numArray.Length);
							num = num1;
							if (num1 <= 0)
							{
								break;
							}
							memoryStream1.Write(numArray, 0, num);
						}
					}
					array = memoryStream1.ToArray();
				}
			}
			return array;
		}

		protected override void ReadFooter(Stream stream)
		{
		}

		protected override Stream ReadHeader(Stream stream)
		{
			return stream;
			
		}

		protected override void WriteFooter(Stream stream)
		{
		}

		protected override Stream WriteHeader(Stream stream)
		{
			return stream;			
		}
	}
}