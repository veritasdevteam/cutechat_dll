using System;
using System.IO;
using Xceed.Compression;

namespace Xceed.Compression.Formats
{
	public class XceedCompressedStream : FormattedCompressedStream
	{
		private CompressionMethod @ӓ = CompressionMethod.Deflated;

		private CompressionLevel @Ӕ = CompressionLevel.Highest;

		private int @ӕ;

		public XceedCompressedStream(Stream inner, CompressionMethod method, CompressionLevel level) : base(inner)
		{
			if (inner == null)
			{
				throw new ArgumentNullException("inner");
			}
			if (!Enum.IsDefined(typeof(CompressionMethod), method))
			{
				throw new ArgumentException("An attempt was made to use an unknown compression method.", "method");
			}
			if (!Enum.IsDefined(typeof(CompressionLevel), level))
			{
				throw new ArgumentException("An attempt was made to use an unknown compression level.", "level");
			}
			this.@ӓ = method;
			this.@Ӕ = level;
		}

		public XceedCompressedStream(Stream inner) : this(inner, CompressionMethod.Deflated, CompressionLevel.Highest)
		{
		}

		public XceedCompressedStream(Stream inner, CompressionMethod method, CompressionLevel level, bool readHeader) : base(inner)
		{
			if (inner == null)
			{
				throw new ArgumentNullException("inner");
			}
			if (!Enum.IsDefined(typeof(CompressionMethod), method))
			{
				throw new ArgumentException("An attempt was made to use an unknown compression method.", "method");
			}
			if (!Enum.IsDefined(typeof(CompressionLevel), level))
			{
				throw new ArgumentException("An attempt was made to use an unknown compression level.", "level");
			}
			if (readHeader && !inner.CanRead)
			{
				throw new ArgumentException("An attempt was made to read the header while the inner stream was not readable.", "inner");
			}
			this.@ӓ = method;
			this.@Ӕ = level;
			if (readHeader)
			{
				base.@Ӟ();
			}
		}

		internal XceedCompressedStream(Stream inner, CompressionMethod method, CompressionLevel level, bool readHeader, int initialAdler32) : this(inner, method, level, readHeader)
		{
			this.@ӕ = initialAdler32;
		}

		public static byte[] Compress(byte[] buffer)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			return XceedCompressedStream.Compress(buffer, 0, (int)buffer.Length, CompressionMethod.Deflated, CompressionLevel.Highest);
		}

		public static byte[] Compress(byte[] buffer, CompressionMethod method, CompressionLevel level)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			return XceedCompressedStream.Compress(buffer, 0, (int)buffer.Length, method, level);
		}

		public static byte[] Compress(byte[] buffer, int offset, int count, CompressionMethod method, CompressionLevel level)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (offset < 0)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("offset", offset, "The offset parameter cannot be less than zero.");
			}
			if (count < 0)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count parameter cannot be less than zero.");
			}
			if (count > (int)buffer.Length - offset)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count parameter exceeds the buffer's remaining length after offset.");
			}
			if (!Enum.IsDefined(typeof(CompressionMethod), method))
			{
				@Ӗ.ThrowArgumentOutOfRangeException("method", method, "An attempt was made to use an unknown compression method.");
			}
			MemoryStream memoryStream = new MemoryStream();
			using (XceedCompressedStream xceedCompressedStream = new XceedCompressedStream(memoryStream, method, level))
			{
				xceedCompressedStream.Write(buffer, offset, count);
			}
			return memoryStream.ToArray();
		}

		public static byte[] Decompress(byte[] buffer)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			return XceedCompressedStream.Decompress(buffer, 0, (int)buffer.Length);
		}

		public static byte[] Decompress(byte[] buffer, int offset, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (offset < 0)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("offset", offset, "The offset parameter cannot be less than zero.");
			}
			if (count < 0)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count parameter cannot be less than zero.");
			}
			if (count > (int)buffer.Length - offset)
			{
				@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count parameter exceeds the buffer's remaining length after offset.");
			}
			byte[] array = new byte[0];
			using (MemoryStream memoryStream = new MemoryStream(buffer, offset, count))
			{
				using (MemoryStream memoryStream1 = new MemoryStream())
				{
					using (XceedCompressedStream xceedCompressedStream = new XceedCompressedStream(memoryStream))
					{
						byte[] numArray = new byte[32786];
						int num = 0;
						while (true)
						{
							int num1 = xceedCompressedStream.Read(numArray, 0, (int)numArray.Length);
							num = num1;
							if (num1 <= 0)
							{
								break;
							}
							memoryStream1.Write(numArray, 0, num);
						}
					}
					array = memoryStream1.ToArray();
				}
			}
			return array;
		}

		protected override void ReadFooter(Stream stream)
		{
			if (base.@ә > 0)
			{
				byte[] numArray = new byte[4];
				base.@ӝ(numArray, 0);
				if (BitConverter.ToUInt32(numArray, 0) != (base.@Ә as ChecksumStream).Checksum)
				{
					throw new IOException("The calculated checksum differs from the stored checksum.");
				}
			}
		}

		protected override Stream ReadHeader(Stream stream)
		{
			int num = -1;
			try
			{
				num = stream.ReadByte();
			}
			catch
			{
			}
			if (num == -1)
			{
				return new @Ӕ(stream);
			}
			base.@Ӝ(4);
			if ((num & 1) != 0)
			{
				throw new NotSupportedException("An attempt was made to decompress an encrypted stream.");
			}
			if ((num & 14) != 0)
			{
				throw new NotSupportedException("The XceedCompressedStream does not support having data written in its reserved bits.");
			}
			CompressedStream compressedStream = null;
			switch (num >> 4)
			{
				case 0:
				{
					compressedStream = new CompressedStream(stream, CompressionMethod.Deflated, this.@Ӕ);
					break;
				}
				case 1:
				{
					compressedStream = new CompressedStream(stream, CompressionMethod.Stored, this.@Ӕ);
					break;
				}
				case 2:
				{
					throw new NotSupportedException("An attempt was made to use an unsupported compression method. XceedCompressedStream does not support the BurrowsWheeler compression method.");
				}
				case 3:
				{
					compressedStream = new CompressedStream(stream, CompressionMethod.Deflated64, this.@Ӕ);
					break;
				}
				default:
				{
					throw new NotSupportedException("An attempt was made to use an unknown compression method.");
				}
			}
			compressedStream.Transient = true;
			return new ChecksumStream(compressedStream, ChecksumType.Adler32, 0, this.@ӕ);
		}

		protected override void WriteFooter(Stream stream)
		{
			ChecksumStream checksumStream = base.@Ә as ChecksumStream;
			stream.Write(BitConverter.GetBytes((uint)checksumStream.Checksum), 0, 4);
		}

		protected override Stream WriteHeader(Stream stream)
		{
			int num = 0;
			int num1 = 0;
			CompressionMethod compressionMethod = this.@ӓ;
			if (compressionMethod == CompressionMethod.Stored)
			{
				num1 = 1;
			}
			else if (compressionMethod == CompressionMethod.Deflated)
			{
				num1 = 0;
			}
			else
			{
				if (compressionMethod != CompressionMethod.Deflated64)
				{
					throw new NotSupportedException("An attempt was made to use an unsupported compression method. The XceedCompressedStream only supports the Deflate, Deflate64 and Stored compression methods.");
				}
				num1 = 3;
			}
			CompressedStream compressedStream = new CompressedStream(stream, this.@ӓ, this.@Ӕ);
			num = num | num1 << 4;
			stream.WriteByte((byte)(num & 255));
			compressedStream.Transient = true;
			return new ChecksumStream(compressedStream, ChecksumType.Adler32, 0, this.@ӕ);
		}
	}
}