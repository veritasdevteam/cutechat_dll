using System;
using System.IO;

namespace Xceed.Compression.Formats
{
	public abstract class FormattedCompressedStream : Stream
	{
		private bool @ӓ;

		private @ӧ @Ӕ;

		private Stream @ӕ;

		private bool @Ӗ = true;

		private bool @ӗ = true;

		internal int @ә
		{
			get
			{
				return this.@Ӕ.ReservedFooter;
			}
		}

		internal Stream @Ә
		{
			get
			{
				return this.@ӕ;
			}
		}

		public override bool CanRead
		{
			get
			{
				this.@Ӡ();
				if (this.@ӕ != null)
				{
					return this.@ӕ.CanRead;
				}
				return this.@Ӕ.CanRead;
			}
		}

		public override bool CanSeek
		{
			get
			{
				this.@Ӡ();
				if (this.@ӕ != null)
				{
					return this.@ӕ.CanSeek;
				}
				return this.@Ӕ.CanSeek;
			}
		}

		public override bool CanWrite
		{
			get
			{
				this.@Ӡ();
				if (this.@ӕ != null)
				{
					return this.@ӕ.CanWrite;
				}
				return this.@Ӕ.CanWrite;
			}
		}

		public override long Length
		{
			get
			{
				this.@Ӡ();
				if (this.@ӕ != null)
				{
					return this.@ӕ.Length;
				}
				return this.@Ӕ.Length;
			}
		}

		public override long Position
		{
			get
			{
				this.@Ӡ();
				if (this.@ӕ != null)
				{
					return this.@ӕ.Position;
				}
				return this.@Ӕ.Position;
			}
			set
			{
				this.@Ӡ();
				this.@ӕ.Position = value;
			}
		}

		public bool Transient
		{
			get
			{
				this.@Ӡ();
				return this.@Ӕ.Transient;
			}
			set
			{
				this.@Ӡ();
				this.@Ӕ.Transient = value;
			}
		}

		internal FormattedCompressedStream(Stream inner)
		{
			if (inner == null)
			{
				throw new ArgumentNullException("inner");
			}
			this.@Ӕ = new @ӧ(inner);
			if (inner.CanRead && !inner.CanWrite)
			{
				this.@Ӟ();
			}
		}

		internal int @ӝ(byte[] u0040ӓ, int u0040Ӕ)
		{
			return this.@Ӕ.GetFooter(u0040ӓ, u0040Ӕ);
		}

		internal void @Ӝ(int u0040ӓ)
		{
			this.@Ӕ.ReservedFooter = u0040ӓ;
		}

		internal void @ӟ()
		{
			if (!this.@Ӗ)
			{
				throw new InvalidOperationException("An attempt was made to both read and write on a FormattedCompressedStream.");
			}
			if (this.@ӗ)
			{
				this.@ӗ = false;
				this.@ӕ = this.WriteHeader(this.@Ӕ);
			}
			if (this.@ӕ == null)
			{
				throw new NullReferenceException("The compressed stream was not provided by the formatted stream.");
			}
		}

		internal void @Ӟ()
		{
			if (!this.@ӗ)
			{
				throw new InvalidOperationException("An attempt was made to both read and write on a FormattedCompressedStream.");
			}
			if (this.@Ӗ)
			{
				this.@Ӗ = false;
				this.@ӕ = this.ReadHeader(this.@Ӕ);
			}
			if (this.@ӕ == null)
			{
				throw new NullReferenceException("The compressed stream was not provided by the formatted stream.");
			}
		}

		private void @Ӡ()
		{
			if (this.@ӓ)
			{
				throw new ObjectDisposedException("FormattedCompressedStream", "Cannot access a stream once it has been closed.");
			}
		}

		public override void Close()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual new void Dispose(bool disposing)
		{
			if (!this.@ӓ)
			{
				if (disposing)
				{
					if (this.@ӕ != null)
					{
						this.@ӕ.Close();
					}
					try
					{
						if (!this.@ӗ)
						{
							this.WriteFooter(this.@Ӕ);
						}
					}
					finally
					{
						this.@Ӕ.Close();
					}
				}
				this.@ӕ = null;
				this.@Ӕ = null;
				this.@ӓ = true;
			}
		}	

		public override void Flush()
		{
			this.@Ӡ();
			if (this.@ӕ != null)
			{
				this.@ӕ.Flush();
			}
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			this.@Ӡ();
			this.@Ӟ();
			int num = this.@ӕ.Read(buffer, offset, count);
			if (num == 0)
			{
				this.ReadFooter(this.@Ӕ);
			}
			return num;
		}

		protected abstract void ReadFooter(Stream stream);

		protected abstract Stream ReadHeader(Stream stream);

		public override long Seek(long offset, SeekOrigin origin)
		{
			this.@Ӡ();
			if (this.@ӕ == null)
			{
				return (long)0;
			}
			return this.@ӕ.Seek(offset, origin);
		}

		public override void SetLength(long value)
		{
			this.@Ӡ();
			if (this.@ӕ != null)
			{
				this.@ӕ.SetLength(value);
			}
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			this.@Ӡ();
			this.@ӟ();
			this.@ӕ.Write(buffer, offset, count);
		}

		protected abstract void WriteFooter(Stream stream);

		protected abstract Stream WriteHeader(Stream stream);
	}
}