using System;

namespace Xceed.Compression.Formats
{
	public class GZipHeader
	{
		private bool @ӓ;

		private bool @Ӕ;

		private DateTime @ӕ = DateTime.Now;

		private byte[] @Ӗ = new byte[0];

		private string @ӗ = string.Empty;

		private string @Ә = string.Empty;

		private bool @ә;

		public string Comment
		{
			get
			{
				return this.@Ә;
			}
			set
			{
				this.@ӛ();
				if (value == null)
				{
					this.@Ә = string.Empty;
					return;
				}
				this.@Ә = value;
			}
		}

		public byte[] ExtraHeader
		{
			get
			{
				return this.@Ӗ;
			}
			set
			{
				this.@ӛ();
				if (value != null)
				{
					this.@Ӗ = value;
					return;
				}
				this.@Ӗ = new byte[0];
			}
		}

		public string FileName
		{
			get
			{
				return this.@ӗ;
			}
			set
			{
				this.@ӛ();
				if (value == null)
				{
					this.@ӗ = string.Empty;
					return;
				}
				this.@ӗ = value;
			}
		}

		public bool HasHeaderChecksum
		{
			get
			{
				return this.@Ӕ;
			}
			set
			{
				this.@ӛ();
				this.@Ӕ = value;
			}
		}

		public bool IsTextFile
		{
			get
			{
				return this.@ӓ;
			}
			set
			{
				this.@ӛ();
				this.@ӓ = value;
			}
		}

		public DateTime LastWriteDateTime
		{
			get
			{
				return this.@ӕ;
			}
			set
			{
				this.@ӛ();
				this.@ӕ = value;
			}
		}

		public GZipHeader()
		{
		}

		public GZipHeader(bool isTextFile, bool hasHeaderChecksum, DateTime lastWriteDateTime, byte[] extraHeader, string fileName, string comment)
		{
			this.@ӓ = isTextFile;
			this.@Ӕ = hasHeaderChecksum;
			this.@ӕ = lastWriteDateTime;
			if (extraHeader != null)
			{
				this.@Ӗ = extraHeader;
			}
			if (fileName != null)
			{
				this.@ӗ = fileName;
			}
			if (comment != null)
			{
				this.@Ә = comment;
			}
		}

		public GZipHeader(GZipHeader template) : this(template.IsTextFile, template.HasHeaderChecksum, template.LastWriteDateTime, template.ExtraHeader, template.FileName, template.Comment)
		{
		}

		private void @ӛ()
		{
			if (this.@ә)
			{
				throw new InvalidOperationException("An attempt was made to modify the header. Once data is written to the GZipCompressedStream, the header cannot be modified.");
			}
		}

		internal void @Ӛ()
		{
			this.@ә = true;
		}
	}
}