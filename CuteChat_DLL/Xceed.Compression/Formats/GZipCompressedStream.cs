using System;
using System.IO;
using System.Text;
using Xceed.Compression;

namespace Xceed.Compression.Formats
{
	public class GZipCompressedStream : FormattedCompressedStream
	{
		private const byte @ӓ = 1;

		private const byte @Ӕ = 2;

		private const byte @ӕ = 4;

		private const byte @Ӗ = 8;

		private const byte @ӗ = 16;

		private GZipHeader @Ә = new GZipHeader();

		private CompressionLevel @ә = CompressionLevel.Highest;

		private long @Ӛ;

		public GZipHeader Header
		{
			get
			{
				return this.@Ә;
			}
		}

		public GZipCompressedStream(Stream inner) : base(inner)
		{
			if (inner == null)
			{
				throw new ArgumentNullException("inner");
			}
		}

		public GZipCompressedStream(Stream inner, CompressionLevel level) : base(inner)
		{
			if (inner == null)
			{
				throw new ArgumentNullException("inner");
			}
			if (!Enum.IsDefined(typeof(CompressionLevel), level))
			{
				throw new ArgumentException("An attempt was made to use an unknown compression level.", "level");
			}
			this.@ә = level;
		}

		public GZipCompressedStream(Stream inner, GZipHeader header, CompressionLevel level) : base(inner)
		{
			if (inner == null)
			{
				throw new ArgumentNullException("inner");
			}
			if (!inner.CanWrite)
			{
				throw new ArgumentException("The inner stream must be writable when using this constructor.", "inner");
			}
			if (header == null)
			{
				throw new ArgumentNullException("header");
			}
			if (!Enum.IsDefined(typeof(CompressionLevel), level))
			{
				throw new ArgumentException("An attempt was made to use an unknown compression level.", "level");
			}
			this.@Ә = header;
			this.@ә = level;
			base.@ӟ();
		}

		public GZipCompressedStream(Stream inner, CompressionLevel level, bool readHeader) : base(inner)
		{
			if (inner == null)
			{
				throw new ArgumentNullException("inner");
			}
			if (!Enum.IsDefined(typeof(CompressionLevel), level))
			{
				throw new ArgumentException("An attempt was made to use an unknown compression level.", "level");
			}
			if (readHeader && !inner.CanRead)
			{
				throw new ArgumentException("An attempt was made to read the header while the inner stream was not readable.", "inner");
			}
			this.@ә = level;
			if (readHeader)
			{
				base.@Ӟ();
			}
		}

		public static byte[] Compress(byte[] buffer)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			return GZipCompressedStream.Compress(buffer, 0, (int)buffer.Length, CompressionLevel.Highest);
		}

		public static byte[] Compress(byte[] buffer, CompressionLevel level)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			return GZipCompressedStream.Compress(buffer, 0, (int)buffer.Length, level);
		}

		public static byte[] Compress(byte[] buffer, int offset, int count, CompressionLevel level)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			//if (offset < 0)
			//{
			//	@Ӗ.ThrowArgumentOutOfRangeException("offset", offset, "The offset parameter cannot be less than zero.");
			//}
			//if (count < 0)
			//{
			//	@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count parameter cannot be less than zero.");
			//}
			//if (count > (int)buffer.Length - offset)
			//{
			//	@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count parameter exceeds the buffer's remaining length after offset.");
			//}
			MemoryStream memoryStream = new MemoryStream();
			using (GZipCompressedStream gZipCompressedStream = new GZipCompressedStream(memoryStream, level))
			{
				gZipCompressedStream.Write(buffer, offset, count);
				gZipCompressedStream.Transient = true;
			}
			return memoryStream.ToArray();
		}

		public static byte[] Decompress(byte[] buffer)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			return GZipCompressedStream.Decompress(buffer, 0, (int)buffer.Length);
		}

		public static byte[] Decompress(byte[] buffer, int offset, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			//if (offset < 0)
			//{
			//	@Ӗ.ThrowArgumentOutOfRangeException("offset", offset, "The offset parameter cannot be less than zero.");
			//}
			//if (count < 0)
			//{
			//	@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count parameter cannot be less than zero.");
			//}
			//if (count > (int)buffer.Length - offset)
			//{
			//	@Ӗ.ThrowArgumentOutOfRangeException("count", count, "The count parameter exceeds the buffer's remaining length after offset.");
			//}
			byte[] array = new byte[0];
			using (MemoryStream memoryStream = new MemoryStream(buffer, offset, count))
			{
				using (MemoryStream memoryStream1 = new MemoryStream())
				{
					using (GZipCompressedStream gZipCompressedStream = new GZipCompressedStream(memoryStream))
					{
						byte[] numArray = new byte[32768];
						int num = 0;
						while (true)
						{
							int num1 = gZipCompressedStream.Read(numArray, 0, (int)numArray.Length);
							num = num1;
							if (num1 <= 0)
							{
								break;
							}
							memoryStream1.Write(numArray, 0, num);
						}
					}
					array = memoryStream1.ToArray();
				}
			}
			return array;
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			int num = base.Read(buffer, offset, count);
			this.@Ӛ += (long)num;
			return num;
		}

		protected override void ReadFooter(Stream stream)
		{
			if (base.@ә > 0)
			{
				byte[] numArray = new byte[8];
				base.@ӝ(numArray, 0);
				if (BitConverter.ToUInt32(numArray, 0) != (base.@Ә as ChecksumStream).Checksum)
				{
					throw new IOException("The calculated checksum differs from the stored checksum.");
				}
				uint num = BitConverter.ToUInt32(numArray, 4);
				//if ((uint)(this.@Ӛ & (ulong)-1) != num)
				//{
				//	throw new IOException("The calculated uncompressed size differs from the stored uncompressed size.");
				//}
			}
		}

		protected override Stream ReadHeader(Stream stream)
		{
			Stream _u0040Ӕ;
			byte[] numArray = null;
			bool flag = false;
			bool flag1 = false;
			int num = 0;
			BinaryReader binaryReader = new BinaryReader(new @Ӕ(stream));
			try
			{
				numArray = binaryReader.ReadBytes(10);
				if (numArray == null || numArray.Length == 0)
				{
					_u0040Ӕ = new @Ӕ(stream);
				}
				else
				{
					if ((int)numArray.Length != 10)
					{
						throw new IOException("There is not enough data in the stream.");
					}
					base.@Ӝ(8);
					if (numArray[0] != 31 || numArray[1] != 139)
					{
						throw new IOException("The compressed data contains an invalid header signature.");
					}
					if (numArray[2] != 8)
					{
						throw new NotSupportedException("An attempt was made to use an unsupported compression method. Only Deflate is supported by the GZipCompressedStream.");
					}
					this.@Ә.IsTextFile = (numArray[3] & 1) != 0;
					this.@Ә.HasHeaderChecksum = (numArray[3] & 2) != 0;
					bool flag2 = (numArray[3] & 4) != 0;
					flag = (numArray[3] & 8) != 0;
					flag1 = (numArray[3] & 16) != 0;
					long num1 = (long)((ulong)BitConverter.ToUInt32(numArray, 4) * (long)10000000 + 116444736000000000L);
					this.@Ә.LastWriteDateTime = DateTime.FromFileTime(num1).ToLocalTime();
					if (this.@Ә.HasHeaderChecksum)
					{
						num = ChecksumStream.CalculateCrc32(numArray, 0, (int)numArray.Length, num);
					}
					this.@Ә.ExtraHeader = null;
					if (flag2)
					{
						ushort num2 = binaryReader.ReadUInt16();
						if (num2 > 0)
						{
							this.@Ә.ExtraHeader = binaryReader.ReadBytes((int)num2);
						}
						if (this.@Ә.HasHeaderChecksum)
						{
							byte[] bytes = BitConverter.GetBytes(num2);
							num = ChecksumStream.CalculateCrc32(bytes, 0, (int)bytes.Length, num);
							if (num2 > 0)
							{
								num = ChecksumStream.CalculateCrc32(this.@Ә.ExtraHeader, 0, (int)num2, num);
							}
						}
					}
					this.@Ә.FileName = string.Empty;
					if (flag)
					{
						using (MemoryStream memoryStream = new MemoryStream())
						{
							byte num3 = 0;
							while (true)
							{
								byte num4 = binaryReader.ReadByte();
								num3 = num4;
								if (num4 == 0)
								{
									break;
								}
								memoryStream.WriteByte(num3);
							}
							byte[] array = memoryStream.ToArray();
							byte[] numArray1 = new byte[(int)array.Length + 1];
							array.CopyTo(numArray1, 0);
							if (this.@Ә.HasHeaderChecksum)
							{
								num = ChecksumStream.CalculateCrc32(numArray1, 0, (int)numArray1.Length, num);
							}
							this.@Ә.FileName = Encoding.Default.GetString(array, 0, (int)array.Length);
						}
					}
					this.@Ә.Comment = string.Empty;
					if (flag1)
					{
						using (MemoryStream memoryStream1 = new MemoryStream())
						{
							byte num5 = 0;
							while (true)
							{
								byte num6 = binaryReader.ReadByte();
								num5 = num6;
								if (num6 == 0)
								{
									break;
								}
								memoryStream1.WriteByte(num5);
							}
							byte[] array1 = memoryStream1.ToArray();
							byte[] numArray2 = new byte[(int)array1.Length + 1];
							array1.CopyTo(numArray2, 0);
							if (this.@Ә.HasHeaderChecksum)
							{
								num = ChecksumStream.CalculateCrc32(numArray2, 0, (int)numArray2.Length, num);
							}
							this.@Ә.Comment = Encoding.Default.GetString(array1, 0, (int)array1.Length);
						}
					}
					if (this.@Ә.HasHeaderChecksum)
					{
						ushort num7 = (ushort)(num >> 16 ^ num & 65535);
						if (binaryReader.ReadUInt16() != num7)
						{
							throw new IOException("The compressed data contains an invalid header checksum.");
						}
					}
					this.@Ә.@Ӛ();
					return stream;
				}
			}
			finally
			{
				binaryReader.Close();
			}
			return _u0040Ӕ;
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			base.Write(buffer, offset, count);
			this.@Ӛ += (long)count;
		}

		protected override void WriteFooter(Stream stream)
		{
			ChecksumStream checksumStream = base.@Ә as ChecksumStream;
			stream.Write(BitConverter.GetBytes((uint)checksumStream.Checksum), 0, 4);
			//stream.Write(BitConverter.GetBytes((uint)(this.@Ӛ & (ulong)-1)), 0, 4);
		}

		protected override Stream WriteHeader(Stream stream)
		{
			byte[] numArray = new byte[10];
			bool flag = false;
			bool flag1 = false;
			bool flag2 = false;
			int num = 0;
			BinaryWriter binaryWriter = new BinaryWriter(new @Ӕ(stream));
			try
			{
				numArray[0] = 31;
				numArray[1] = 139;
				numArray[2] = 8;
				numArray[3] = 0;
				if (this.@Ә.IsTextFile)
				{
					ref byte numPointer = ref numArray[3];
					numPointer = (byte)(numPointer | 1);
				}
				if (this.@Ә.HasHeaderChecksum)
				{
					ref byte numPointer1 = ref numArray[3];
					numPointer1 = (byte)(numPointer1 | 2);
				}
				if (this.@Ә.ExtraHeader.Length != 0)
				{
					flag = true;
					ref byte numPointer2 = ref numArray[3];
					numPointer2 = (byte)(numPointer2 | 4);
				}
				if (this.@Ә.FileName.Length > 0)
				{
					flag1 = true;
					ref byte numPointer3 = ref numArray[3];
					numPointer3 = (byte)(numPointer3 | 8);
				}
				if (this.@Ә.Comment.Length > 0)
				{
					flag2 = true;
					ref byte numPointer4 = ref numArray[3];
					numPointer4 = (byte)(numPointer4 | 16);
				}
				DateTime universalTime = this.@Ә.LastWriteDateTime.ToUniversalTime();
				BitConverter.GetBytes((uint)((universalTime.ToFileTime() - 116444736000000000L) / (long)10000000)).CopyTo(numArray, 4);
				numArray[9] = 255;
				binaryWriter.Write(numArray);
				if (this.@Ә.HasHeaderChecksum)
				{
					num = ChecksumStream.CalculateCrc32(numArray, 0, (int)numArray.Length, num);
				}
				if (flag)
				{
					ushort length = (ushort)((int)this.@Ә.ExtraHeader.Length);
					if (this.@Ә.HasHeaderChecksum)
					{
						byte[] bytes = BitConverter.GetBytes(length);
						num = ChecksumStream.CalculateCrc32(bytes, 0, (int)bytes.Length, num);
						num = ChecksumStream.CalculateCrc32(this.@Ә.ExtraHeader, 0, (int)length, num);
					}
					binaryWriter.Write(length);
					binaryWriter.Write(this.@Ә.ExtraHeader);
				}
				if (flag1)
				{
					byte[] bytes1 = Encoding.Default.GetBytes(this.@Ә.FileName);
					byte[] numArray1 = new byte[(int)bytes1.Length + 1];
					((Array)bytes1).CopyTo(numArray1, 0);
					if (this.@Ә.HasHeaderChecksum)
					{
						num = ChecksumStream.CalculateCrc32(numArray1, 0, (int)numArray1.Length, num);
					}
					binaryWriter.Write(numArray1);
				}
				if (flag2)
				{
					byte[] bytes2 = Encoding.Default.GetBytes(this.@Ә.Comment);
					byte[] numArray2 = new byte[(int)bytes2.Length + 1];
					((Array)bytes2).CopyTo(numArray2, 0);
					if (this.@Ә.HasHeaderChecksum)
					{
						num = ChecksumStream.CalculateCrc32(numArray2, 0, (int)numArray2.Length, num);
					}
					binaryWriter.Write(numArray2);
				}
				if (this.@Ә.HasHeaderChecksum)
				{
					ushort num1 = (ushort)(num >> 16 ^ num & 65535);
					binaryWriter.Write(num1);
				}
			}
			finally
			{
				binaryWriter.Close();
			}
			this.@Ә.@Ӛ();
			
			return stream;
				
		}
	}
}