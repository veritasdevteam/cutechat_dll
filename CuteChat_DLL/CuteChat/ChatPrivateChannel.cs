using System;

namespace CuteChat
{
	public class ChatPrivateChannel : ChatChannel
	{
		private ChatPrivateInvite @ӓ;

		public ChatPrivateInvite Invite
		{
			get
			{
				return this.@ӓ;
			}
		}

		public override string Location
		{
			get
			{
				return "Private";
			}
		}

		public override string PlaceTitle
		{
			get
			{
				return this.@ӓ.Sender.DisplayName;
			}
		}

		public ChatPrivateChannel(ChatPortal portal, string name, ChatPrivateInvite invite) : base(portal, name)
		{
			this.@ӓ = invite;
		}

		public override bool IsModerator(ChatIdentity identity)
		{
			return this.@ӓ.Sender == identity;
		}

		protected internal override void LoadHistoryMessages(ChatConnection conn)
		{
		}
	}
}