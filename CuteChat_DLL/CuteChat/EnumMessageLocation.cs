using System;

namespace CuteChat
{
	public class EnumMessageLocation
	{
		public const string Global = "Global";

		public const string Messenger = "Messenger";

		public const string Lobby = "Lobby";

		public EnumMessageLocation()
		{
		}
	}
}