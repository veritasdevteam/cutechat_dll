using System;
using System.Reflection;

namespace CuteChat
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
	public class ChatServerTypeAttribute : Attribute
	{
		public ChatServerTypeAttribute()
		{
		}

		public static void ValidateMethod(MethodInfo method)
		{
			if (method == null)
			{
				throw new ArgumentNullException("method");
			}
			if (Attribute.IsDefined(method, typeof(ChatServerTypeAttribute)))
			{
				return;
			}
			if (!Attribute.IsDefined(method.DeclaringType, typeof(ChatServerTypeAttribute)))
			{
				throw new ArgumentException(string.Concat("Method or class must be declared as [ChatServerTypeAttribute] : ", method.Name));
			}
		}
	}
}