using System;
using System.ComponentModel;

namespace CuteChat
{
	public class MessengerLicenceProvider : LicenseProvider
	{
		public MessengerLicenceProvider()
		{
		}

		public override License GetLicense(LicenseContext context, Type type, object instance, bool allowExceptions)
		{
			return (new @Ԑ()
			{
				ProductType = EnumChatLicense.CuteMessenger
			}).GetLicense(context, type, instance, allowExceptions);
		}
	}
}