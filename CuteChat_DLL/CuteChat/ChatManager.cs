using System;
using System.Collections;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Web;

namespace CuteChat
{
	public class ChatManager : ChatPortalDataItem
	{
		//private ChatPlace @ӓ;

		private @Ԏ @Ӕ = new @Ԏ();

		private @Ԕ @ӕ;

		private ArrayList @Ӗ;

		public ChatPlace Place
		{
			get
			{
				return null;
			}
		}

		public ChatManager(ChatPlace place) : base(place.Portal)
		{
			//this.@ӓ = place;
		}

		internal void @ӗ(@ԫ u0040ӓ)
		{
			u0040ӓ.InitUser(this.Place);
			u0040ӓ.PlaceUser.InitConnect(false, false);
			this.AddPlaceConnection(u0040ӓ);
			u0040ӓ.PlaceUser.OnlineStatus = "PARTIAL";
			this.AddPlaceItem(u0040ӓ.PlaceUser);
		}

		private bool @ә()
		{
			ChatPlaceUser[] allUsers = this.Place.GetAllUsers();
			for (int i = 0; i < (int)allUsers.Length; i++)
			{
				ChatPlaceUser chatPlaceUser = allUsers[i];
				if (this.Place.IsModerator(chatPlaceUser.Identity))
				{
					return true;
				}
			}
			return false;
		}

		private void @Ә(ChatIdentity u0040ӓ, ChatResponse u0040Ӕ)
		{
			if (this.Place.IsKicked(u0040ӓ) && !this.Place.IsModerator(u0040ӓ))
			{
				u0040Ӕ.ReturnCode = "KICK";
				return;
			}
			if (this.Place.IsKickedIP(u0040ӓ.IPAddress) && !this.Place.IsModerator(u0040ӓ))
			{
				u0040Ӕ.ReturnCode = "KICK";
				return;
			}
			if (!base.Portal.DataManager.CheckIP(u0040ӓ.IPAddress))
			{
				u0040Ӕ.ReturnCode = "KICK";
				return;
			}
			if (u0040ӓ.IsAnonymous && !this.Place.AllowAnonymous)
			{
				u0040Ӕ.ReturnCode = "NEEDLOGIN";
				return;
			}
			string password = this.Place.Password;
			if (password != null && password.Length != 0 && password != u0040Ӕ.Cookie.Password && !this.Place.IsModerator(u0040ӓ))
			{
				u0040Ӕ.ReturnCode = "NEEDPASSWORD";
				return;
			}
			try
			{
				this.ValidateIdentity(u0040ӓ);
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				u0040Ӕ.ReturnCode = "ERROR";
				u0040Ӕ.ServerMessage = exception.Message;
			}
		}

		public virtual void AddPlaceConnection(ChatConnection conn)
		{
			conn.OnAttachPlace(this.Place);
			this.Place.AcceptConnection(conn);
			this.Place.SendUpdatedMessage(conn);
			conn.PlaceUser.OnSelfAdded();
			ChatPlaceItem[] allItems = this.Place.GetAllItems();
			Array.Sort((Array)allItems, @Ղ.SortByTimeAsc);
			ChatPlaceItem[] chatPlaceItemArray = allItems;
			for (int i = 0; i < (int)chatPlaceItemArray.Length; i++)
			{
				chatPlaceItemArray[i].OnConnectionAdded(conn);
			}
			if (conn.PlaceUser.Moderated)
			{
				this.SendInitializeMessages(conn);
			}
			base.Portal.DataManager.AsyncLogEvent(this.Place, conn.Identity, "CONNECTION", "READY");
		}

		public virtual void AddPlaceItem(ChatPlaceItem item)
		{
			this.Place.AddItem(item);
			item.OnAdded();
			base.Portal.DataManager.AsyncLogEvent(this.Place, null, "PLACEITEMADDED", item.ToString());
		}

		public IAsyncResult BeginSyncData(ChatIdentity identity, ChatCookie cookie, string[] ctsmessages, NameValueCollection nvc, AsyncCallback cb, object state)
		{
			if (identity == null)
			{
				throw new ArgumentNullException("identity");
			}
			ChatSystem.Instance.@Ӧ();
			ChatResponse chatResponse = new ChatResponse()
			{
				Cookie = cookie,
				ReturnCode = "READY"
			};
			@Ԍ _u0040Ԍ = new @Ԍ(base.Portal, chatResponse, state)
			{
				Callback = cb
			};
			if (cookie.ConnectionId != null)
			{
				ChatConnection chatConnection = base.Portal.FindConnection(chatResponse.Cookie.ConnectionId);
				if (chatConnection == null)
				{
					chatResponse.ReturnCode = "NOCONNECTION";
					chatResponse.ServerMessage = string.Concat("conn-", chatResponse.Cookie.ConnectionId);
				}
				else if (identity == chatConnection.Identity)
				{
					this.PreSyncData(chatConnection, chatResponse, ctsmessages);
					if (chatResponse.ReturnCode == "READY")
					{
						bool flag = false;
						if (nvc != null && nvc["HASMESSAGE"] == "1")
						{
							flag = true;
						}
						if (!flag && this.ShouldQueueSync(chatConnection.PlaceUser) && ChatSystem.Instance.@ӧ(_u0040Ԍ))
						{
							ChatManager.@ӓ _u0040ӓ = new ChatManager.@ӓ()
							{
								_this = this,
								res = chatResponse,
								conn = chatConnection
							};
							_u0040Ԍ.@Ӝ();
							_u0040Ԍ.CompleteCallback = new AsyncCallback(_u0040ӓ.CompleteCallback);
							return _u0040Ԍ;
						}
						this.PostSyncData(chatConnection, chatResponse);
					}
				}
				else
				{
					chatResponse.ReturnCode = "ERROR";
					chatResponse.ServerMessage = "INVALIDIDENTITY";
				}
			}
			else
			{
				chatResponse.ReturnCode = "ERROR";
				chatResponse.ServerMessage = "Argument null:ConnectionId";
			}
			if (_u0040Ԍ.Callback != null)
			{
				_u0040Ԍ.Callback(_u0040Ԍ);
			}
			return _u0040Ԍ;
		}

		protected virtual void CheckConnection(ChatConnection conn, ChatResponse res)
		{
			if (conn.Place != null)
			{
				if (conn.ConnectionKey == res.Cookie.ConnectionKey)
				{
					return;
				}
				res.ReturnCode = "NOCONNECTION";
				res.ServerMessage = "key";
				return;
			}
			if (conn.@ӕ == "IDLE")
			{
				res.ReturnCode = "REMOVED";
				return;
			}
			if (conn.@ӕ == "KICK")
			{
				res.ReturnCode = "KICK";
				return;
			}
			res.ReturnCode = "REJECTED";
		}

		public virtual void ClosePlaceConnection(ChatConnection conn, string reason)
		{
			conn.@ӕ = reason;
			if (reason != "OVERRIDE")
			{
				this.RemovePlaceItem(conn.PlaceUser, reason);
			}
			ChatPlaceItem[] allItems = this.Place.GetAllItems();
			Array.Sort((Array)allItems, @Ղ.SortByTimeDesc);
			ChatPlaceItem[] chatPlaceItemArray = allItems;
			for (int i = 0; i < (int)chatPlaceItemArray.Length; i++)
			{
				chatPlaceItemArray[i].OnConnectionRemoved(conn);
			}
			this.Place.RemoveConnection(conn, reason);
			conn.OnDetachPlace(reason);
			base.Portal.DataManager.AsyncLogEvent(this.Place, conn.Identity, "CONNECTION", reason);
		}

		public ChatResponse Connect(ChatIdentity identity, ChatCookie cookie, NameValueCollection nvc)
		{
			ChatResponse chatResponse = this.InternalConnect(identity, cookie, nvc);
			if (chatResponse.ReturnCode != "READY")
			{
				base.Portal.DataManager.AsyncLogEvent(this.Place, identity, "CONNECTION", chatResponse.ReturnCode);
			}
			return chatResponse;
		}

		protected virtual bool ConnectRequireModerate(ChatConnection conn)
		{
			if (this.Place.Locked && !this.Place.IsModerator(conn.Identity))
			{
				return true;
			}
			return false;
		}

		public virtual ChatResponse Disconnect(ChatIdentity identity, ChatCookie cookie)
		{
			if (identity == null)
			{
				throw new ArgumentNullException("identity");
			}
			ChatResponse chatResponse = new ChatResponse()
			{
				Cookie = cookie,
				ReturnCode = "READY"
			};
			if (cookie.ConnectionId == null)
			{
				chatResponse.ReturnCode = "ERROR";
				chatResponse.ServerMessage = "Argument null:ConnectionId";
				return null;
			}
			ChatConnection chatConnection = base.Portal.FindConnection(cookie.ConnectionId);
			if (chatConnection == null)
			{
				chatResponse.ReturnCode = "NOCONNECTION";
				return chatResponse;
			}
			this.RemoveQueueConnection(chatConnection);
			if (this.Place.FindConnection(chatConnection.ObjectGuid) == null)
			{
				chatResponse.ReturnCode = "READY";
				return chatResponse;
			}
			this.ClosePlaceConnection(chatConnection, "BYCLIENT");
			base.Portal.CloseConnection(chatConnection, "BYCLIENT");
			chatResponse.ReturnCode = "READY";
			return chatResponse;
		}

		public ChatResponse EndSyncData(IAsyncResult ar)
		{
			@Ԍ _u0040Ԍ = (@Ԍ)ar;
			if (!_u0040Ԍ.CompletedSynchronously)
			{
				Trace.WriteLine("EndSyncData", "what?");
			}
			if (!_u0040Ԍ.IsCompleted)
			{
				Exception exception = new Exception("SyncData not completed!");
				ChatSystem.Instance.LogException(exception);
				throw exception;
			}
			if (_u0040Ԍ.Error != null)
			{
				ChatSystem.Instance.LogException(_u0040Ԍ.Error);
				throw new Exception("Error at PostSyncData", _u0040Ԍ.Error);
			}
			return _u0040Ԍ.Response;
		}

		protected virtual ChatConnection FindQueueConnection(ChatIdentity identity)
		{
			ChatConnection chatConnection;
			if (this.@Ӗ == null)
			{
				return null;
			}
			if (this.@Ӗ.Count == 0)
			{
				return null;
			}
			IEnumerator enumerator = this.@Ӗ.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					ChatConnection current = (ChatConnection)enumerator.Current;
					if (current.Identity != identity)
					{
						continue;
					}
					chatConnection = current;
					return chatConnection;
				}
				return null;
			}
			finally
			{
				IDisposable disposable = enumerator as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}
			return chatConnection;
		}

		public ChatConnection[] GetQueueConnections()
		{
			if (this.@Ӗ == null)
			{
				return new ChatConnection[0];
			}
			return (ChatConnection[])this.@Ӗ.ToArray(typeof(ChatConnection));
		}

		public virtual bool HandleCTSAddon(ChatConnection conn, NameValueCollection nvc, params string[] args)
		{
			conn.PlaceUser.PushSTCMessageToRelativeConnections(ChatUtility.JoinToMsg("ADDON", nvc, args));
			return true;
		}

		public virtual bool HandleCTSCustom(ChatConnection conn, NameValueCollection nvc, params string[] args)
		{
			return false;
		}

		public virtual bool HandleCTSMessage(ChatConnection conn, string msgid, NameValueCollection nvc, string[] args)
		{
			if (args == null)
			{
				args = new string[0];
			}
			if (msgid == "CUSTOM")
			{
				return this.HandleCTSCustom(conn, nvc, args);
			}
			if (msgid == "ADDON")
			{
				return this.HandleCTSAddon(conn, nvc, args);
			}
			if (msgid == "USER_TYPING")
			{
				return this.HandleCTSUserTyping(conn, nvc, args);
			}
			if (msgid == "USER_MESSAGE")
			{
				return this.HandleCTSUserMessage(conn, nvc, args);
			}
			if (msgid == "USER_COMMAND")
			{
				return this.HandleCTSUserCommand(conn, nvc, args);
			}
			if (msgid == "USER_BROADCAST")
			{
				return this.HandleCTSUserBroadCast(conn, nvc, args);
			}
			if (msgid != "MODERATOR_COMMAND")
			{
				return false;
			}
			if (conn.PlaceUser.IsModerator)
			{
				return this.HandleCTSModeratorCommand(conn, nvc, args);
			}
			this.PushSTCMessage(conn, "SYS_ERROR_MESSAGE", null, new string[] { "INVALIDIDENTITY" });
			return true;
		}

		public virtual bool HandleCTSModeratorCommand(ChatConnection conn, NameValueCollection nvc, params string[] args)
		{
			string str = args[0];
			if (str == "SETALLOWANONYMOUS")
			{
				this.Place.AllowAnonymous = args[1] == "1";
				this.Place.SendUpdatedMessage();
				return true;
			}
			if (str == "SETPASSWORD")
			{
				this.Place.Password = args[1];
				return true;
			}
			if (str == "SETMAXONLINECOUNT")
			{
				this.Place.MaxOnlineCount = int.Parse(args[1]);
				return true;
			}
			if (str == "SETLOCKCHANNEL")
			{
				this.Place.Locked = args[1] == "1";
				this.Place.SendUpdatedMessage();
				return true;
			}
			if (str == "ACCEPTUSER" || str == "REJECTUSER")
			{
				ChatPlaceUser chatPlaceUser = this.Place.FindUser(args[1]);
				if (chatPlaceUser != null)
				{
					this.ModerateUser(chatPlaceUser.Identity, str == "ACCEPTUSER");
					return true;
				}
				this.PushSTCMessage(conn, "SYS_ERROR_MESSAGE", null, new string[] { "INVALIDTARGET" });
				return true;
			}
			if (str == "KICKUSER")
			{
				ChatPlaceUser chatPlaceUser1 = this.Place.FindUser(args[1]);
				if (chatPlaceUser1 == null)
				{
					this.PushSTCMessage(conn, "SYS_ERROR_MESSAGE", null, new string[] { "INVALIDTARGET" });
					return true;
				}
				if (!this.Place.IsModerator(chatPlaceUser1.Identity))
				{
					this.KickUser(chatPlaceUser1.Connection);
					return true;
				}
				this.PushSTCMessage(conn, "SYS_ERROR_MESSAGE", null, new string[] { "INVALIDTARGET" });
				return true;
			}
			if (str == "UNKICKUSERS")
			{
				this.Place.ClearKickedUsers();
				this.PushSTCMessage(conn, "SYS_INFO_MESSAGE", null, new string[] { "UI_MSG_UnKickUsers" });
				return true;
			}
			if (str == "KICKIP")
			{
				ChatPlaceUser chatPlaceUser2 = this.Place.FindUser(args[1]);
				if (chatPlaceUser2 != null)
				{
					this.KickIP(chatPlaceUser2.Identity.IPAddress);
					return true;
				}
				this.PushSTCMessage(conn, "SYS_ERROR_MESSAGE", null, new string[] { "INVALIDTARGET" });
				return true;
			}
			if (str == "UNKICKIPS")
			{
				this.Place.ClearKickedIPs();
				this.PushSTCMessage(conn, "SYS_INFO_MESSAGE", null, new string[] { "UI_MSG_UnBanIPs" });
				return true;
			}
			if (str == "SETUSERLEVEL")
			{
				ChatPlaceUser chatPlaceUser3 = this.Place.FindUser(args[1]);
				if (chatPlaceUser3 != null)
				{
					chatPlaceUser3.Level = args[2];
					this.UpdatePlaceItem(chatPlaceUser3);
					return true;
				}
				this.PushSTCMessage(conn, "SYS_ERROR_MESSAGE", null, new string[] { "INVALIDTARGET" });
				return true;
			}
			if (str == "ADDMODERATOR")
			{
				string str1 = base.Portal.DataManager.IsMemberDisplayName(args[1]);
				if (str1 != null)
				{
					this.Place.AddModerator(str1);
					return true;
				}
				this.PushSTCMessage(conn, "SYS_ERROR_MESSAGE", null, new string[] { "INVALIDTARGET" });
				return true;
			}
			if (str != "REMOVEMODERATOR")
			{
				return false;
			}
			string str2 = base.Portal.DataManager.IsMemberDisplayName(args[1]);
			if (str2 != null)
			{
				this.Place.RemoveModerator(str2);
				return true;
			}
			this.PushSTCMessage(conn, "SYS_ERROR_MESSAGE", null, new string[] { "INVALIDTARGET" });
			return true;
		}

		public virtual bool HandleCTSUserBroadCast(ChatConnection conn, NameValueCollection nvc, params string[] args)
		{
			string str = args[0];
			args[0] = conn.PlaceUser.Identity.UniqueId;
			if (str == null || str == "")
			{
				conn.PlaceUser.PushSTCMessageToRelativeConnections("USER_BROADCAST", null, args);
			}
			else
			{
				ChatPlaceUser chatPlaceUser = this.Place.FindUser(str);
				this.PushSTCMessage(chatPlaceUser.Connection, "USER_BROADCAST", null, args);
			}
			return true;
		}

		public virtual bool HandleCTSUserCommand(ChatConnection conn, NameValueCollection nvc, params string[] args)
		{
			int i;
			bool flag;
			ChatConnection[] allConnections;
			string str = args[0];
			if (str == "LOADHISTORY")
			{
				if (conn.PlaceUser.Moderated)
				{
					this.Place.LoadHistoryMessages(conn);
				}
				return true;
			}
			if (str == "ADDCONTACT")
			{
				base.Portal.DataManager.AddContact(conn.Identity, args[1]);
				return true;
			}
			if (str == "REMOVECONTACT")
			{
				base.Portal.DataManager.RemoveContact(conn.Identity, args[1]);
				return true;
			}
			if (str == "ADDIGNORE")
			{
				base.Portal.DataManager.AddIgnore(conn.Identity, args[1]);
				return true;
			}
			if (str == "REMOVEIGNORE")
			{
				base.Portal.DataManager.RemoveIgnore(conn.Identity, args[1]);
				return true;
			}
			if (str == "INVITEINTOPRIVATE")
			{
				ChatPrivateChannel place = this.Place as ChatPrivateChannel;
				if (place == null)
				{
					this.PushSTCMessage(conn, "SYS_ERROR_MESSAGE", null, new string[] { "INVALIDCOMMAND" });
					return true;
				}
				string str1 = args[1];
				ChatPlaceUser[] allUsers = place.GetAllUsers();
				for (i = 0; i < (int)allUsers.Length; i++)
				{
					if (string.Compare(allUsers[i].DisplayName, str1, true) == 0)
					{
						return true;
					}
				}
				ArrayList arrayLists = new ArrayList();
				if (base.Portal.IsMessengerStarted)
				{
					arrayLists.Add(base.Portal.Messenger);
				}
				arrayLists.AddRange(base.Portal.GetStartedChannels());
				IEnumerator enumerator = arrayLists.GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						ChatPlace current = (ChatPlace)enumerator.Current;
						allUsers = current.GetAllUsers();
						i = 0;
						while (i < (int)allUsers.Length)
						{
							ChatPlaceUser chatPlaceUser = allUsers[i];
							if (string.Compare(chatPlaceUser.DisplayName, str1, true) != 0)
							{
								i++;
							}
							else
							{
								ChatPrivateInvite chatPrivateInvite = new ChatPrivateInvite(current, conn.Identity);
								chatPrivateInvite.SetPrivateChannel(place);
								chatPrivateInvite.AddInvite(chatPlaceUser.Identity);
								current.Manager.AddPlaceItem(chatPrivateInvite);
								flag = true;
								return flag;
							}
						}
					}
					this.PushSTCMessage(conn, "SYS_ERROR_MESSAGE", null, new string[] { "INVALIDTARGET" });
					return true;
				}
				finally
				{
					IDisposable disposable = enumerator as IDisposable;
					if (disposable != null)
					{
						disposable.Dispose();
					}
				}
				return flag;
			}
			if (str == "PRIVATEINVITE")
			{
				ChatPrivateInvite chatPrivateInvite1 = new ChatPrivateInvite(this.Place, conn.Identity);
				for (int j = 1; j < (int)args.Length; j++)
				{
					ChatPlaceUser chatPlaceUser1 = this.Place.FindUser(args[j]);
					if (chatPlaceUser1 != null)
					{
						chatPrivateInvite1.AddInvite(chatPlaceUser1.Identity);
					}
				}
				if (chatPrivateInvite1.InviteCount == 0)
				{
					this.PushSTCMessage(conn, "SYS_ERROR_MESSAGE", null, new string[] { "INVALIDTARGET" });
				}
				else
				{
					chatPrivateInvite1.AddInvite(conn.Identity);
					this.AddPlaceItem(chatPrivateInvite1);
				}
				return true;
			}
			if (str == "ACCEPTPRIVATE" || str == "REJECTPRIVATE")
			{
				ChatPrivateInvite chatPrivateInvite2 = this.Place.FindItem(new ChatGuid(args[1])) as ChatPrivateInvite;
				if (chatPrivateInvite2 == null)
				{
					this.PushSTCMessage(conn, "SYS_ERROR_MESSAGE", null, new string[] { "INVATEOBJECT" });
					return true;
				}
				if (str != "ACCEPTPRIVATE")
				{
					chatPrivateInvite2.OnReject(conn.Identity);
					ChatPlaceUser chatPlaceUser2 = this.Place.FindUser(chatPrivateInvite2.Sender.UniqueId);
					if (chatPlaceUser2 != null && chatPlaceUser2.Identity != conn.Identity)
					{
						this.PushSTCMessage(chatPlaceUser2.Connection, "SYS_ALERT_MESSAGE", null, new string[] { string.Concat(conn.Identity.DisplayName, " has rejected your invitation") });
					}
				}
				else
				{
					chatPrivateInvite2.OnAccept(conn.Identity);
				}
				return true;
			}
			if (str == "DISPLAYNAME")
			{
				if (base.Portal.GetProperty("AllowChangeName") == "False")
				{
					this.PushSTCMessage(conn, "SYS_ERROR_MESSAGE", null, new string[] { "INVALIDCOMMAND" });
					return true;
				}
				string displayName = args[1].Trim();
				if (displayName.Length == 0)
				{
					displayName = conn.Identity.DisplayName;
				}
				allConnections = this.Place.GetAllConnections();
				for (i = 0; i < (int)allConnections.Length; i++)
				{
					if (string.Compare(displayName, allConnections[i].PlaceUser.DisplayName, true) == 0)
					{
						this.PushSTCMessage(conn, "SYS_ERROR_MESSAGE", null, new string[] { "INVALIDARGUMENT", "name" });
						return true;
					}
				}
				conn.PlaceUser.PushSTCMessageToRelativeConnections("USER_RENAME", null, new string[] { conn.PlaceUser.ObjectGuid, displayName, conn.PlaceUser.DisplayName });
				conn.PlaceUser.DisplayName = displayName;
				this.UpdatePlaceItem(conn.PlaceUser);
				return true;
			}
			if (str == "ONLINESTATUS")
			{
				if (string.Compare("APPEAROFFLINE", args[1], true) != 0)
				{
					conn.PlaceUser.AppearOffline = false;
					conn.PlaceUser.OnlineStatus = args[1];
				}
				else
				{
					conn.PlaceUser.AppearOffline = true;
					conn.PlaceUser.OnlineStatus = "APPEAROFFLINE";
				}
				this.UpdatePlaceItem(conn.PlaceUser);
				return true;
			}
			if (str == "DESCRIPTION")
			{
				IChatUserInfo userInfo = base.Portal.DataManager.GetUserInfo(conn.Identity.UniqueId);
				userInfo.Description = args[1];
				base.Portal.DataManager.UpdateUserInfo(userInfo);
				allConnections = base.Portal.FindConnection(conn.Identity);
				for (i = 0; i < (int)allConnections.Length; i++)
				{
					ChatConnection chatConnection = allConnections[i];
					if (chatConnection.PlaceUser != null)
					{
						this.UpdatePlaceItem(chatConnection.PlaceUser);
					}
				}
				return true;
			}
			if (str == "PRIVATEPROPERTY")
			{
				IChatUserInfo chatUserInfo = base.Portal.DataManager.GetUserInfo(conn.Identity.UniqueId);
				chatUserInfo.SetPrivateProperty(args[1], args[2]);
				base.Portal.DataManager.UpdateUserInfo(chatUserInfo);
				allConnections = base.Portal.FindConnection(conn.Identity);
				for (i = 0; i < (int)allConnections.Length; i++)
				{
					ChatConnection chatConnection1 = allConnections[i];
					if (chatConnection1.PlaceUser != null)
					{
						this.UpdatePlaceItem(chatConnection1.PlaceUser);
					}
				}
				return true;
			}
			if (str != "PUBLICPROPERTY")
			{
				return false;
			}
			IChatUserInfo userInfo1 = base.Portal.DataManager.GetUserInfo(conn.Identity.UniqueId);
			userInfo1.SetPublicProperty(args[1], args[2]);
			base.Portal.DataManager.UpdateUserInfo(userInfo1);
			allConnections = base.Portal.FindConnection(conn.Identity);
			for (i = 0; i < (int)allConnections.Length; i++)
			{
				ChatConnection chatConnection2 = allConnections[i];
				if (chatConnection2.PlaceUser != null)
				{
					this.UpdatePlaceItem(chatConnection2.PlaceUser);
				}
			}
			return true;
		}

		public virtual bool HandleCTSUserMessage(ChatConnection conn, NameValueCollection nvc, params string[] args)
		{
			if (conn.PlaceUser.Level == "Silence")
			{
				this.PushSTCMessage(conn, "SYS_INFO_MESSAGE", null, new string[] { "INVATESTATUS" });
				return true;
			}
			string str = args[0];
			string str1 = args[1];
			str = ChatUtility.StripScript(str);
			str1 = ChatUtility.StripScript(str1);
			args[0] = str;
			args[1] = str1;
			this.Place.DispatchMessage(conn, nvc, args);
			conn.PlaceUser.OnSendMessage();
			return true;
		}

		public virtual bool HandleCTSUserTyping(ChatConnection conn, NameValueCollection nvc, params string[] args)
		{
			if (args != null && args.Length != 0)
			{
				string str = args[0];
				if (str != null && str != "")
				{
					ChatPlaceUser chatPlaceUser = this.Place.FindItem(new ChatGuid(str)) as ChatPlaceUser;
					if (chatPlaceUser != null)
					{
						conn.PlaceUser.SetTyping();
						this.PushSTCMessage(chatPlaceUser.Connection, "USER_TYPING", null, new string[] { conn.PlaceUser.ObjectGuid });
					}
					return true;
				}
			}
			if (!conn.PlaceUser.SetTyping())
			{
				return true;
			}
			conn.PlaceUser.PushSTCMessageToRelativeConnections("USER_TYPING", null, new string[] { conn.PlaceUser.ObjectGuid });
			return true;
		}

		protected virtual bool IgnorePlaceQueue(ChatIdentity identity)
		{
			return false;
		}

		protected virtual ChatResponse InternalConnect(ChatIdentity identity, ChatCookie cookie, NameValueCollection nvc)
		{
			ChatConnection chatConnection;
			ChatResponse chatResponse;
			if (identity == null)
			{
				throw new ArgumentNullException("identity");
			}
			bool flag = this.Place.IsModerator(identity);
			ChatResponse message = new ChatResponse()
			{
				Cookie = cookie,
				ReturnCode = "READY"
			};
			if (this.@ӕ == null)
			{
				try
				{
					if (this.Place is ChatMessenger)
					{
						this.@ӕ = this.@Ӕ.CheckMessengerLicense();
					}
					else if (this.Place is SupportAgentChannel)
					{
						this.@ӕ = this.@Ӕ.CheckSupportLicense();
					}
					else if (this.Place.Location == "Support")
					{
						this.@ӕ = this.@Ӕ.CheckSupportLicense();
					}
					else if (this.Place.Location != "Private")
					{
						this.@ӕ = this.@Ӕ.CheckGeneralLicense();
					}
					else
					{
						try
						{
							this.@ӕ = this.@Ӕ.CheckGeneralLicense();
						}
						catch
						{
							try
							{
								this.@ӕ = this.@Ӕ.CheckMessengerLicense();
							}
							catch
							{
								throw new Exception("Messenger or CuteChat license file not found!");
							}
						}
					}
					goto Label0;
				}
				catch (Exception exception1)
				{
					Exception exception = exception1;
					ChatSystem.Instance.LogException(exception);
					message.ReturnCode = "ERROR";
					message.ServerMessage = exception.Message;
					chatResponse = message;
				}
				return chatResponse;
			}
		Label0:
			this.@Ә(identity, message);
			if (message.ReturnCode != "READY")
			{
				return message;
			}
			IChatUserInfo userInfo = base.Portal.DataManager.GetUserInfo(identity.UniqueId);
			if (userInfo.DisplayName == null || userInfo.DisplayName == "")
			{
				userInfo.DisplayName = this.Place.GetDisplayName(identity);
				base.Portal.DataManager.UpdateUserInfo(userInfo);
			}
			ChatPlaceUser chatPlaceUser = this.Place.FindUser(identity.UniqueId);
			if (chatPlaceUser == null)
			{
				if (!flag)
				{
					if (ChatSystem.Instance.GetSystemConnectionCount() >= 1200000)
					{
						message.ReturnCode = "LIMITED";
						return message;
					}
					if (base.Portal.GetConnectionCount() >= 1200000)
					{
						message.ReturnCode = "LIMITED";
						return message;
					}
				}
				chatConnection = this.FindQueueConnection(identity);
				if (chatConnection != null && base.Portal.FindConnection(chatConnection.ObjectGuid) == null)
				{
					this.RemoveQueueConnection(chatConnection);
					chatConnection = null;
				}
				if (this.Place.Locked && !flag && !this.@ә())
				{
					message.ReturnCode = "LOCKED";
					return message;
				}
			}
			else
			{
				if (chatPlaceUser.Connection is @ԫ)
				{
					chatPlaceUser.OnlineStatus = "ONLINE";
					this.UpdatePlaceItem(chatPlaceUser);
				}
				else if ((DateTime.Now - chatPlaceUser.Connection.ActivateTime).Seconds < 10)
				{
					message.ReturnCode = "IDEXISTS";
					return message;
				}
				this.ClosePlaceConnection(chatPlaceUser.Connection, "OVERRIDE");
				chatConnection = null;
			}
			if (chatConnection == null)
			{
				chatConnection = base.Portal.AcceptConnection(identity);
			}
			message.Cookie.ConnectionId = chatConnection.ObjectGuid;
			message.Cookie.ConnectionKey = chatConnection.ConnectionKey;
			bool flag1 = false;
			string item = null;
			if (nvc != null)
			{
				if (nvc["AppearOffline"] == "1")
				{
					flag1 = true;
				}
				item = nvc["OnlineStatus"];
			}
			if (chatPlaceUser == null)
			{
				if (!flag && !this.IgnorePlaceQueue(identity))
				{
					int num = this.QueueConnection(chatConnection);
					if (num != -1)
					{
						message.ReturnCode = "INQUEUE";
						message.QueuePosition = num;
						return message;
					}
				}
				this.RemoveQueueConnection(chatConnection);
				chatConnection.InitUser(this.Place);
				bool flag2 = this.ConnectRequireModerate(chatConnection);
				chatConnection.PlaceUser.InitConnect(flag1, flag2);
				if (!flag1 && item != null)
				{
					chatConnection.PlaceUser.OnlineStatus = item;
				}
				this.AddPlaceConnection(chatConnection);
				this.AddPlaceItem(chatConnection.PlaceUser);
			}
			else
			{
				this.SwitchConnectionUser(chatConnection, chatPlaceUser);
				this.AddPlaceConnection(chatConnection);
				if (flag1)
				{
					chatPlaceUser.AppearOffline = true;
					this.UpdatePlaceItem(chatPlaceUser);
				}
				else if (item != null)
				{
					chatPlaceUser.OnlineStatus = item;
					this.UpdatePlaceItem(chatPlaceUser);
				}
			}
			message.ReturnCode = "READY";
			return message;
		}

		public virtual void KickIP(string ipaddr)
		{
			this.Place.AddKickedIP(ipaddr);
			ChatConnection[] allConnections = this.Place.GetAllConnections();
			for (int i = 0; i < (int)allConnections.Length; i++)
			{
				ChatConnection chatConnection = allConnections[i];
				if (chatConnection.Identity.IPAddress == ipaddr && !this.Place.IsModerator(chatConnection.Identity))
				{
					this.ClosePlaceConnection(chatConnection, "KICK");
				}
			}
		}

		public virtual void KickUser(ChatConnection conn)
		{
			this.Place.AddKickedUser(conn.Identity);
			this.ClosePlaceConnection(conn, "KICK");
		}

		public virtual void Maintain()
		{
			int i;
			int maxIdleMinute = this.Place.MaxIdleMinute;
			if (maxIdleMinute <= 0 || maxIdleMinute > 20000)
			{
				maxIdleMinute = 20000;
			}
			DateTime now = DateTime.Now - TimeSpan.FromMinutes((double)maxIdleMinute);
			int num = 0;
			ChatConnection[] allConnections = this.Place.GetAllConnections();
			for (i = 0; i < (int)allConnections.Length; i++)
			{
				ChatConnection chatConnection = allConnections[i];
				if (!chatConnection.IsExpired)
				{
					num++;
					string[] strArrays = chatConnection.PopCTSMessages();
					if (strArrays.Length != 0)
					{
						string[] strArrays1 = strArrays;
						for (int j = 0; j < (int)strArrays1.Length; j++)
						{
							string str = strArrays1[j];
							if (str != null && str.Length != 0)
							{
								this.SafeHandleCTSMessage(chatConnection, str);
							}
						}
					}
					else if (chatConnection.ActionTime < now)
					{
						bool flag = true;
						if (this.Place is SupportSessionChannel && base.Portal.DataManager.IsAgent(chatConnection.Identity.UniqueId))
						{
							flag = false;
						}
						if (flag)
						{
							this.ClosePlaceConnection(chatConnection, "IDLE");
						}
					}
				}
				else
				{
					this.ClosePlaceConnection(chatConnection, "EXPIRED");
				}
			}
			ChatPlaceItem[] allItems = this.Place.GetAllItems();
			for (i = 0; i < (int)allItems.Length; i++)
			{
				allItems[i].Maintain();
			}
			this.Place.Maintain();
			if (this.@Ӗ != null)
			{
				object[] array = this.@Ӗ.ToArray();
				for (i = 0; i < (int)array.Length; i++)
				{
					ChatConnection chatConnection1 = (ChatConnection)array[i];
					if (chatConnection1.IsExpired)
					{
						this.@Ӗ.Remove(chatConnection1);
					}
				}
				if (this.@Ӗ.Count == 0)
				{
					this.@Ӗ = null;
				}
			}
			if (num == 0 && this.Place.IsExpired)
			{
				base.Portal.@Ӛ(this.Place);
			}
		}

		public virtual void ModerateUser(ChatIdentity identity, bool allow)
		{
			ChatPlaceUser chatPlaceUser = this.Place.FindUser(identity.UniqueId);
			if (chatPlaceUser == null)
			{
				return;
			}
			if (chatPlaceUser.Moderated)
			{
				if (!allow)
				{
					this.ClosePlaceConnection(chatPlaceUser.Connection, "REJECTED");
				}
				return;
			}
			if (!allow)
			{
				this.ClosePlaceConnection(chatPlaceUser.Connection, "REJECTED");
				return;
			}
			chatPlaceUser.Moderated = true;
			this.SendInitializeMessages(chatPlaceUser.Connection);
			this.UpdatePlaceItem(chatPlaceUser);
		}

		protected internal virtual void PostSyncData(ChatConnection conn, ChatResponse res)
		{
			if (conn.Place == null || conn.PlaceUser == null)
			{
				res.ReturnCode = "NOCONNECTION";
				res.ServerMessage = (conn.Place == null ? "place" : "user");
				return;
			}
			ArrayList placeUser = null;
			if (res.Cookie.ResponseId < conn.PlaceUser.@Ә)
			{
				placeUser = conn.PlaceUser.@ә;
			}
			if (placeUser == null)
			{
				placeUser = new ArrayList();
			}
			placeUser.AddRange(conn.PopSTCMessages());
			conn.PlaceUser.@Ә++;
			res.ReturnCode = "READY";
			res.Cookie.ResponseId = conn.PlaceUser.@Ә;
			res.Messages = placeUser;
			conn.PlaceUser.@ә = placeUser;
			res.ReturnCode = "READY";
		}

		protected internal virtual void PreSyncData(ChatConnection conn, ChatResponse res, string[] ctsmessages)
		{
			this.@Ә(conn.Identity, res);
			if (res.ReturnCode != "READY")
			{
				return;
			}
			this.CheckConnection(conn, res);
			if (res.ReturnCode != "READY")
			{
				return;
			}
			conn.SetActivate(ctsmessages.Length != 0);
			this.Place.SetActivate();
			conn.@ӛ++;
			string[] strArrays = ctsmessages;
			for (int i = 0; i < (int)strArrays.Length; i++)
			{
				this.SafeHandleCTSMessage(conn, strArrays[i]);
			}
		}

		public void PushSTCMessage(ChatConnection conn, string msgid, NameValueCollection nvc, params string[] args)
		{
			conn.PushSTCMessage(msgid, nvc, args);
		}

		public virtual void PushSTCMessage(ChatConnection conn, string message)
		{
			conn.PushSTCMessage(message);
		}

		protected virtual int QueueConnection(ChatConnection conn)
		{
			int connectionCount = this.Place.GetConnectionCount();
			int maxOnlineCount = this.Place.MaxOnlineCount;
			int count = 0;
			if (this.@Ӗ != null)
			{
				count = this.@Ӗ.Count;
			}
			if (connectionCount + count < maxOnlineCount)
			{
				return -1;
			}
			if (this.@Ӗ == null)
			{
				this.@Ӗ = new ArrayList();
			}
			int num = this.@Ӗ.IndexOf(conn);
			if (num == -1)
			{
				num = count;
				this.@Ӗ.Add(conn);
			}
			return num + connectionCount - maxOnlineCount;
		}

		public virtual void RemovePlaceItem(ChatPlaceItem item, string reason)
		{
			this.Place.RemoveItem(item);
			item.OnRemoved(reason);
			base.Portal.DataManager.AsyncLogEvent(this.Place, null, "PLACEITEMREMOVED", item.ToString());
		}

		protected virtual void RemoveQueueConnection(ChatConnection conn)
		{
			if (this.@Ӗ != null)
			{
				this.@Ӗ.Remove(conn);
			}
		}

		public void SafeHandleCTSMessage(ChatConnection conn, string message)
		{
			string str;
			NameValueCollection nameValueCollection;
			string[] strArrays;
			try
			{
				ChatUtility.SplitMsg(message, out str, out nameValueCollection, out strArrays);
				base.Portal.DataManager.AsyncLogEvent(this.Place, conn.Identity, "CTSMESSAGE", message);
				if (!this.Place.PreProcessCTSMessage(conn, str, strArrays, nameValueCollection))
				{
					if (!this.HandleCTSMessage(conn, str, nameValueCollection, strArrays))
					{
						this.PushSTCMessage(conn, "SYS_ERROR_MESSAGE", null, new string[] { "INVALIDCOMMAND", message });
						return;
					}
				}
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				this.PushSTCMessage(conn, "SYS_DEBUG_MESSAGE", null, new string[] { "CTSMessageError", message, exception.Message });
				ChatSystem.Instance.LogException(exception);
			}
		}

		protected virtual void SendInitializeMessages(ChatConnection conn)
		{
			string property = base.Portal.GetProperty("Announcement");
			if (property != null && property != "")
			{
				this.PushSTCMessage(conn, "SYS_INFO_MESSAGE", null, new string[] { property });
			}
			this.Place.SendInitializeMessages(conn);
		}

		protected virtual bool ShouldQueueSync(ChatPlaceUser user)
		{
			if (user.IsModerator)
			{
				return false;
			}
			return true;
		}

		public virtual void SwitchConnectionUser(ChatConnection conn, ChatPlaceUser placeuser)
		{
			placeuser.SwitchConnection(conn);
			conn.SwitchUser(placeuser);
			base.Portal.DataManager.AsyncLogEvent(this.Place, conn.Identity, "SWITCHCONNECTIONUSER", string.Concat(conn.ObjectGuid, ":", placeuser.ObjectGuid));
		}

		public virtual void UpdatePlaceConnection(ChatConnection conn)
		{
			ChatPlaceItem[] allItems = this.Place.GetAllItems();
			for (int i = 0; i < (int)allItems.Length; i++)
			{
				allItems[i].OnConnectionUpdated(conn);
			}
		}

		public virtual void UpdatePlaceItem(ChatPlaceItem item)
		{
			item.OnUpdated();
		}

		protected virtual void ValidateIdentity(ChatIdentity identity)
		{
			this.Place.ValidateIdentity(identity);
		}

		private class @ӓ
		{
			public ChatManager _this;

			public ChatConnection conn;

			public ChatResponse res;

			public @ӓ()
			{
			}

			public void CompleteCallback(IAsyncResult result)
			{
				@Ԍ _u0040Ԍ = (@Ԍ)result;
				try
				{
					this._this.PostSyncData(this.conn, this.res);
				}
				catch (Exception exception1)
				{
					Exception exception = exception1;
					ChatSystem.Instance.LogException(exception);
					_u0040Ԍ.Error = exception;
				}
			}
		}
	}
}