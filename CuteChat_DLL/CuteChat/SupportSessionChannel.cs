using System;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using System.Web;
using System.Xml;

namespace CuteChat
{
	public class SupportSessionChannel : ChatChannel
	{
		private ChatIdentity @ӓ;

		private SupportCustomerItem @Ӕ;

		private SupportSession @ӕ;

		public ChatIdentity CustomerIdentity
		{
			get
			{
				return this.@ӓ;
			}
		}

		public override bool IsExpired
		{
			get
			{
				return (DateTime.Now - base.ActivateTime).TotalSeconds > 30;
			}
		}

		public override string Location
		{
			get
			{
				return "Support";
			}
		}

		public override string PlaceTitle
		{
			get
			{
				return this.@Ӕ.DisplayName;
			}
		}

		public SupportSessionChannel(ChatPortal portal, string name, SupportCustomerItem customeritem) : base(portal, name)
		{
			this.@Ӕ = customeritem;
			this.@ӓ = customeritem.Identity;
			this.AutoAwayMinute = 3;
		}

		private void @Ӗ()
		{
			if (this.@ӕ != null)
			{
				return;
			}
			SupportSession supportSession = new SupportSession()
			{
				BeginTime = DateTime.Now,
				ActiveTime = DateTime.Now,
				CustomerId = this.@Ӕ.Identity.UniqueId,
				DisplayName = this.@Ӕ.DisplayName,
				Email = this.@Ӕ.Email,
				Browser = this.@Ӕ.Browser,
				IPAddress = this.@Ӕ.IPAddress,
				Platform = this.@Ӕ.Platform,
				Culture = this.@Ӕ.Culture,
				AgentUserId = this.@Ӕ.Agent.UniqueId,
				Url = this.@Ӕ.Url,
				Referrer = this.@Ӕ.Referrer,
				DepartmentId = 0
			};
			SupportDepartment[] departments = base.Portal.DataManager.GetDepartments();
			for (int i = 0; i < (int)departments.Length; i++)
			{
				SupportDepartment supportDepartment = departments[i];
				if (string.Compare(supportDepartment.Name, this.@Ӕ.Department, true) == 0)
				{
					supportSession.DepartmentId = supportDepartment.DepartmentId;
				}
			}
			XmlDocument xmlDocument = new XmlDocument();
			xmlDocument.LoadXml("<CustomerData />");
			xmlDocument.DocumentElement.SetAttribute("Question", this.@Ӕ.Question);
			xmlDocument.DocumentElement.SetAttribute("CustomData", this.@Ӕ.CustomData);
			xmlDocument.DocumentElement.SetAttribute("InitFlag", "1");
			supportSession.SessionData = xmlDocument.InnerXml;
			base.Portal.DataManager.CreateSession(supportSession);
			this.@ӕ = supportSession;
		}

		protected internal override void AddItem(ChatPlaceItem item)
		{
			base.AddItem(item);
			if (this.@ӕ != null)
			{
				return;
			}
			if (item is ChatPlaceUser && (int)base.GetAllUsers().Length >= 2)
			{
				this.@Ӗ();
			}
		}

		protected override ChatManager CreateManagerInstance()
		{
			return new SupportChannelManager(this);
		}

		public override void Dump(XmlWriter writer)
		{
			this.@Ӗ();
			writer.WriteAttributeString("department", this.@Ӕ.Department);
			writer.WriteAttributeString("agentid", this.@Ӕ.Agent.UniqueId);
			writer.WriteAttributeString("agentname", this.@Ӕ.Agent.DisplayName);
			writer.WriteAttributeString("customerip", this.@ӓ.IPAddress);
			writer.WriteAttributeString("customerid", this.@ӓ.UniqueId);
			writer.WriteAttributeString("customername", this.@ӓ.DisplayName);
			writer.WriteAttributeString("sessionbegin", this.@ӕ.BeginTime.ToString("yyyy-MM-dd HH:mm:ss"));
			writer.WriteAttributeString("sessionactive", this.@ӕ.ActiveTime.ToString("yyyy-MM-dd HH:mm:ss"));
			TimeSpan now = DateTime.Now - this.@ӕ.BeginTime;
			double totalSeconds = now.TotalSeconds;
			writer.WriteAttributeString("sessionspan", totalSeconds.ToString("0"));
			now = DateTime.Now - this.@ӕ.ActiveTime;
			totalSeconds = now.TotalSeconds;
			writer.WriteAttributeString("sessionstop", totalSeconds.ToString("0"));
			writer.WriteAttributeString("sessionbrowser", this.@ӕ.Browser);
			writer.WriteAttributeString("sessionplatform", this.@ӕ.Platform);
			writer.WriteAttributeString("sessionculture", this.@ӕ.Culture);
			writer.WriteAttributeString("sessionrating", this.@ӕ.AgentRating.ToString());
			base.Dump(writer);
		}

		public override bool IsModerator(ChatIdentity identity)
		{
			return base.Portal.DataManager.IsAgent(identity.UniqueId);
		}

		protected override void LogMessage(ChatConnection conn, ChatIdentity targetidentity, bool whisper, string text, string html)
		{
			this.@Ӗ();
			this.@ӕ.ActiveTime = DateTime.Now;
			base.Portal.DataManager.UpdateSession(this.@ӕ);
			base.Portal.DataManager.AsyncLogSupportMessage(this, this.@ӕ, "USER", conn.Identity, targetidentity, text, html);
		}

		public override bool PreProcessCTSMessage(ChatConnection conn, string msgid, string[] args, NameValueCollection nvc)
		{
			ChatConnection[] allConnections;
			int i;
			if (msgid == "LSAGENT_COMMAND" && args[0] == "SENDFILE")
			{
				this.SaveFile(conn, args[1], args[2]);
				return true;
			}
			if (msgid == "LSCUSTOMER_COMMAND")
			{
				string str = args[0];
				if (str == "SENDTOEMAIL")
				{
					this.SendToEmail(conn, args[1]);
					return true;
				}
				if (str == "SETAGENTRATING")
				{
					this.SetAgentRating(conn, args[1]);
					return true;
				}
				if (str == "COBROWSE_START")
				{
					allConnections = base.GetAllConnections();
					for (i = 0; i < (int)allConnections.Length; i++)
					{
						ChatConnection chatConnection = allConnections[i];
						base.Manager.PushSTCMessage(chatConnection, "COBROWSE_START", null, args);
					}
					return true;
				}
				if (str == "COBROWSE_STOP")
				{
					allConnections = base.GetAllConnections();
					for (i = 0; i < (int)allConnections.Length; i++)
					{
						ChatConnection chatConnection1 = allConnections[i];
						base.Manager.PushSTCMessage(chatConnection1, "COBROWSE_STOP", null, args);
					}
					return true;
				}
				if (str == "COBROWSE_UPDATE")
				{
					allConnections = base.GetAllConnections();
					for (i = 0; i < (int)allConnections.Length; i++)
					{
						ChatConnection chatConnection2 = allConnections[i];
						base.Manager.PushSTCMessage(chatConnection2, "COBROWSE_UPDATE", null, args);
					}
					return true;
				}
			}
			if (msgid == "LSAGENT_COMMAND")
			{
				if (!base.Portal.DataManager.IsAgent(conn.Identity.UniqueId))
				{
					return false;
				}
				string str1 = args[0];
				if (str1 == "COBROWSE_START")
				{
					allConnections = base.GetAllConnections();
					for (i = 0; i < (int)allConnections.Length; i++)
					{
						ChatConnection chatConnection3 = allConnections[i];
						base.Manager.PushSTCMessage(chatConnection3, "COBROWSE_START", null, args);
					}
					return true;
				}
				if (str1 == "COBROWSE_STOP")
				{
					allConnections = base.GetAllConnections();
					for (i = 0; i < (int)allConnections.Length; i++)
					{
						ChatConnection chatConnection4 = allConnections[i];
						base.Manager.PushSTCMessage(chatConnection4, "COBROWSE_STOP", null, args);
					}
					return true;
				}
				if (str1 == "COBROWSE_UPDATE")
				{
					allConnections = base.GetAllConnections();
					for (i = 0; i < (int)allConnections.Length; i++)
					{
						ChatConnection chatConnection5 = allConnections[i];
						base.Manager.PushSTCMessage(chatConnection5, "COBROWSE_UPDATE", null, args);
					}
					return true;
				}
			}
			return base.PreProcessCTSMessage(conn, msgid, args, nvc);
		}

		public void RestoreSession(SupportSession session)
		{
			this.@ӕ = session;
		}

		public void SaveFile(ChatConnection conn, string filename, string base64data)
		{
			byte[] numArray = Convert.FromBase64String(base64data);
			string mimeType = @Հ.GetMimeType(filename);
			string str = DateTime.Now.ToString("yyyy-MM-dd");
			string str1 = Guid.NewGuid().ToString();
			string str2 = string.Concat(new string[] { "CuteChatSendFiles/", str, "/", str1, ".licx" });
			string str3 = string.Empty; // Path.Combine(HttpRuntime.AppDomainAppPath, str2);
			string directoryName = Path.GetDirectoryName(str3);
			if (!Directory.Exists(directoryName))
			{
				Directory.CreateDirectory(directoryName);
			}
			using (FileStream fileStream = new FileStream(str3, FileMode.Create, FileAccess.Write))
			{
				fileStream.Write(numArray, 0, (int)numArray.Length);
			}
			string str4 = string.Concat(new string[] { "DownloadFile.ashx?date=", str, "&guid=", str1, "&filename=", HttpUtility.UrlEncode(filename), "&mime=", mimeType });
			this.SendFileMessage(conn, null, filename, (long)((int)numArray.Length), str4, mimeType);
		}

		public void SendToEmail(ChatConnection conn, string email)
		{
			if (this.@ӕ == null)
			{
				return;
			}
			StringBuilder stringBuilder = new StringBuilder();
			ChatMsgData[] chatMsgDataArray = base.Portal.DataManager.LoadSessionMessages(this.@ӕ.SessionId);
			Array.Reverse((Array)chatMsgDataArray);
			ChatMsgData[] chatMsgDataArray1 = chatMsgDataArray;
			for (int i = 0; i < (int)chatMsgDataArray1.Length; i++)
			{
				ChatMsgData chatMsgDatum = chatMsgDataArray1[i];
				stringBuilder.Append(chatMsgDatum.Sender).Append(" : ").Append(chatMsgDatum.Text);
				stringBuilder.Append("\r\n");
			}
			DateTime now = DateTime.Now;
			string str = string.Concat(now.ToString("MM-dd HH:mm "), this.@Ӕ.DisplayName, " : LiveSupport messages history with ", this.@Ӕ.Agent.DisplayName);
			ChatSystem.Instance.SendMail(str, stringBuilder.ToString(), email);
		}

		public void SetAgentRating(ChatConnection conn, string ratingstr)
		{
			int num = int.Parse(ratingstr);
			if (num < 0 || num > 5)
			{
				return;
			}
			this.@Ӗ();
			if (this.@ӕ.AgentRating == num)
			{
				return;
			}
			this.@ӕ.AgentRating = num;
			base.Portal.DataManager.UpdateSession(this.@ӕ);
			ChatConnection[] allConnections = base.GetAllConnections();
			for (int i = 0; i < (int)allConnections.Length; i++)
			{
				ChatConnection chatConnection = allConnections[i];
				base.Manager.PushSTCMessage(chatConnection, "LSAGENTRATING", null, new string[] { ratingstr });
			}
		}

		public void SetOwnerAgent(ChatIdentity agentid)
		{
			this.@Ӕ.Agent = agentid;
			this.@Ӗ();
			this.@ӕ.AgentUserId = agentid.UniqueId;
			base.Portal.DataManager.UpdateSession(this.@ӕ);
		}

		protected internal override void ValidateIdentity(ChatIdentity identity)
		{
			base.ValidateIdentity(identity);
			if (identity.IsAnonymous && base.FindUser(identity.UniqueId) != null)
			{
				return;
			}
			if (this.@ӓ == identity)
			{
				return;
			}
			if (!base.Portal.DataManager.IsAgent(identity.UniqueId))
			{
				throw new Exception("Require Agent Or Customer");
			}
		}
	}
}