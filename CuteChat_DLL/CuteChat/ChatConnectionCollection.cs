using System;
using System.Collections;
using System.Reflection;

namespace CuteChat
{
	public class ChatConnectionCollection
	{
		private Hashtable @ӓ = new Hashtable();

		public int Count
		{
			get
			{
				return this.@ӓ.Count;
			}
		}

		public ChatConnection this[ChatGuid guid]
		{
			get
			{
				return (ChatConnection)this.@ӓ[guid];
			}
		}

		public ChatConnectionCollection()
		{
		}

		public void Add(ChatConnection conn)
		{
			if (conn == null)
			{
				throw new ArgumentNullException("conn");
			}
			this.@ӓ.Add(conn.ObjectGuid, conn);
		}

		public ChatConnection[] Find(ChatIdentity identity)
		{
			if (identity == null)
			{
				throw new ArgumentNullException("identity");
			}
			ArrayList arrayLists = new ArrayList();
			foreach (ChatConnection value in this.@ӓ.Values)
			{
				if (value.Identity != identity)
				{
					continue;
				}
				arrayLists.Add(value);
			}
			return (ChatConnection[])arrayLists.ToArray(typeof(ChatConnection));
		}

		public ChatConnection[] GetExpiredConnections()
		{
			ArrayList arrayLists = new ArrayList();
			foreach (ChatConnection value in this.@ӓ.Values)
			{
				if (!value.IsExpired)
				{
					continue;
				}
				arrayLists.Add(value);
			}
			return (ChatConnection[])arrayLists.ToArray(typeof(ChatConnection));
		}

		public void Remove(ChatGuid guid)
		{
			this.@ӓ.Remove(guid);
		}

		public ChatConnection[] ToArray()
		{
			ChatConnection[] chatConnectionArray = new ChatConnection[this.@ӓ.Count];
			this.@ӓ.Values.CopyTo(chatConnectionArray, 0);
			return chatConnectionArray;
		}
	}
}