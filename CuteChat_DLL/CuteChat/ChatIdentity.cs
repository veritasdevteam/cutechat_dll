using System;

namespace CuteChat
{
	[Serializable]
	public abstract class ChatIdentity : IComparable
	{
		public abstract string DisplayName
		{
			get;
		}

		public abstract string IPAddress
		{
			get;
		}

		public abstract bool IsAnonymous
		{
			get;
		}

		public abstract string UniqueId
		{
			get;
		}

		protected ChatIdentity()
		{
		}

		public int CompareTo(object obj)
		{
			if (this == obj)
			{
				return 0;
			}
			ChatIdentity chatIdentity = obj as ChatIdentity;
			if (chatIdentity == null)
			{
				return -1;
			}
			return string.Compare(this.UniqueId, chatIdentity.UniqueId, true);
		}

		public override bool Equals(object obj)
		{
			ChatIdentity chatIdentity = obj as ChatIdentity;
			if (chatIdentity == null)
			{
				return false;
			}
			return this.UniqueIdEquals(chatIdentity.UniqueId);
		}

		public override int GetHashCode()
		{
			return this.UniqueId.GetHashCode();
		}

		public static bool operator ==(ChatIdentity iden1, ChatIdentity iden2)
		{
			if ((object)iden1 == (object)iden2)
			{
				return true;
			}
			if (iden1 == null)
			{
				return false;
			}
			if (iden2 == null)
			{
				return false;
			}
			return iden1.UniqueIdEquals(iden2.UniqueId);
		}

		public static bool operator !=(ChatIdentity iden1, ChatIdentity iden2)
		{
			return !(iden1 == iden2);
		}

		public override string ToString()
		{
			return this.DisplayName;
		}

		public bool UniqueIdEquals(string userid)
		{
			return string.Compare(this.UniqueId, userid, true) == 0;
		}
	}
}