using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;

namespace CuteChat
{
	public class ClusterSupport
	{
		public const int DEFAULT_TCP_PORT = 5123;

		private static bool @ӓ;

		private static bool @Ӕ;

		private static bool @ӕ;

		private static bool @Ӗ;

		private static string @ӗ;

		private static int @Ә;

		private static string @ә;

		private static string @Ӛ;

		private static string @ӛ;

		private static string @Ӝ;

		private static IPAddress @ӝ;

		private static Socket @Ӟ;

		private static @ԡ @ӟ;

		public static bool IsClusterClient
		{
			get
			{
				ClusterSupport.@Ӡ();
				return ClusterSupport.@ӕ;
			}
		}

		public static bool IsClusterServer
		{
			get
			{
				ClusterSupport.@Ӡ();
				return ClusterSupport.@Ӕ;
			}
		}

		public static string Password
		{
			get
			{
				ClusterSupport.@Ӡ();
				return ClusterSupport.@ӛ;
			}
		}

		static ClusterSupport()
		{
			ClusterSupport.@ӓ = false;
			ClusterSupport.@Ӕ = false;
			ClusterSupport.@ӕ = false;
			ClusterSupport.@Ӗ = false;
			ClusterSupport.@ӗ = "http";
			ClusterSupport.@Ә = 5123;
			ClusterSupport.@Ӝ = null;
		}

		public ClusterSupport()
		{
		}

		private static void @ӥ()
		{
			ClusterSupport.@ӟ = new @ԡ(ClusterSupport.@Ӝ);
			ClusterSupport.@ӟ.OnData += new @Ԣ(ClusterSupport.@Ӧ);
			ClusterSupport.@Ӕ = ClusterSupport.@ӟ.Initialize();
			ClusterSupport.@ӕ = !ClusterSupport.@Ӕ;
		}

		private static void @Ӥ(IAsyncResult u0040ӓ)
		{
			Socket socket = null;
			Exception exception = null;
			try
			{
				socket = ClusterSupport.@Ӟ.EndAccept(u0040ӓ);
			}
			catch (Exception exception1)
			{
				exception = exception1;
			}
			ClusterSupport.@Ӟ.BeginAccept(new AsyncCallback(ClusterSupport.@Ӥ), null);
			if (exception == null)
			{
				@Ԡ.HandleClient(socket);
				return;
			}
			ChatSystem.Instance.LogException(exception);
		}

		private static void @ӣ()
		{
			Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			try
			{
				if (!ClusterSupport.@Ӗ)
				{
					socket.Bind(new IPEndPoint(IPAddress.Any, ClusterSupport.@Ә));
				}
				else
				{
					socket.Bind(new IPEndPoint(IPAddress.Loopback, ClusterSupport.@Ә));
				}
			}
			catch (SecurityException securityException)
			{
				throw new Exception("Unable top use 'local' mode for web gardens. Because the asp.net trust level is not High or Full !");
			}
			catch (SocketException socketException)
			{
				ClusterSupport.@ӕ = true;
				return;
			}
			socket.Listen(100);
			ClusterSupport.@Ӟ = socket;
			ClusterSupport.@Ӟ.BeginAccept(new AsyncCallback(ClusterSupport.@Ӥ), null);
			ClusterSupport.@Ӕ = true;
		}

		internal static bool @Ӣ()
		{
			if (ClusterSupport.@ә != "server" && ClusterSupport.@ә != "local")
			{
				return false;
			}
			ClusterSupport.@ӣ();
			return ClusterSupport.@Ӕ;
		}

		internal static void @ӧ(string u0040ӓ, string[] u0040Ӕ, byte[] u0040ӕ, ChatIdentity u0040Ӗ, out string u0040ӗ, out string[] u0040Ә, out byte[] u0040ә)
		{
			byte[] array;
			byte[] numArray;
			ClusterSupport.@Ӡ();
			if (!ClusterSupport.@ӕ)
			{
				throw new Exception("Invalid Mode.");
			}
			using (MemoryStream memoryStream = new MemoryStream())
			{
				@Ԝ _u0040Ԝ = new @Ԝ(memoryStream);
				_u0040Ԝ.Write(ClusterSupport.@ӛ);
				_u0040Ԝ.Write("COMMAND");
				_u0040Ԝ.Write(u0040ӓ);
				if (u0040Ӕ == null)
				{
					_u0040Ԝ.Write(-1);
				}
				else if (u0040Ӕ.Length != 0)
				{
					_u0040Ԝ.Write((int)u0040Ӕ.Length);
					string[] strArrays = u0040Ӕ;
					for (int i = 0; i < (int)strArrays.Length; i++)
					{
						_u0040Ԝ.Write(strArrays[i]);
					}
				}
				else
				{
					_u0040Ԝ.Write(0);
				}
				if (u0040ӕ == null)
				{
					_u0040Ԝ.Write(-1);
				}
				else if (u0040ӕ.Length != 0)
				{
					_u0040Ԝ.Write((int)u0040ӕ.Length);
					_u0040Ԝ.Write(u0040ӕ);
				}
				else
				{
					_u0040Ԝ.Write(0);
				}
				if (u0040Ӗ != null)
				{
					_u0040Ԝ.Write(true);
					_u0040Ԝ.Write(u0040Ӗ.UniqueId);
					_u0040Ԝ.Write(u0040Ӗ.DisplayName);
					_u0040Ԝ.Write(u0040Ӗ.IsAnonymous);
					_u0040Ԝ.Write(u0040Ӗ.IPAddress);
				}
				else
				{
					_u0040Ԝ.Write(false);
				}
				array = memoryStream.ToArray();
			}
			u0040ӗ = null;
			u0040Ә = null;
			u0040ә = null;
			if (ClusterSupport.@ӗ != "lfc")
			{
				@ԟ _u0040ԟ = null;
				if (ClusterSupport.@ӗ == "tcp")
				{
					try
					{
						_u0040ԟ = @ԟ.Allocate(new IPEndPoint(ClusterSupport.@ӝ, ClusterSupport.@Ә));
					}
					catch (SocketException socketException)
					{
					}
				}
				if (_u0040ԟ == null)
				{
					Guid guid = Guid.NewGuid();
					string str = string.Concat("--------", guid.ToString());
					HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(ClusterSupport.@Ӛ);
					httpWebRequest.KeepAlive = false;
					httpWebRequest.Method = "POST";
					httpWebRequest.Headers.Add("CuteChat.NLB", "1");
					httpWebRequest.ContentType = string.Concat("multipart/form-data; boundary=", str);
					StringBuilder stringBuilder = new StringBuilder();
					stringBuilder.Append("--");
					stringBuilder.Append(str);
					stringBuilder.Append("\r\n");
					stringBuilder.Append("Content-Disposition: form-data; name=\"CuteChatNLBRequest\"; filename=\"C:\\CuteChatNLBRequest.dat");
					stringBuilder.Append("\"");
					stringBuilder.Append("\r\n");
					stringBuilder.Append("\r\n");
					byte[] bytes = Encoding.UTF8.GetBytes(stringBuilder.ToString());
					stringBuilder = new StringBuilder();
					stringBuilder.Append("\r\n");
					stringBuilder.Append("--");
					stringBuilder.Append(str);
					stringBuilder.Append("\r\n");
					byte[] bytes1 = Encoding.UTF8.GetBytes(stringBuilder.ToString());
					using (Stream requestStream = httpWebRequest.GetRequestStream())
					{
						requestStream.Write(bytes, 0, (int)bytes.Length);
						requestStream.Write(array, 0, (int)array.Length);
						requestStream.Write(bytes1, 0, (int)bytes1.Length);
					}
					using (HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse())
					{
						using (Stream responseStream = response.GetResponseStream())
						{
							numArray = new byte[29];
							int num = 0;
							while (num < (int)numArray.Length)
							{
								int num1 = responseStream.Read(numArray, num, (int)numArray.Length - num);
								num += num1;
								if (num1 != 0)
								{
									continue;
								}
								throw new Exception("Unable to download data");
							}
						}
					}
				}
				else
				{
					try
					{
						try
						{
							numArray = _u0040ԟ.SendAndRead(array);
						}
						catch (SocketException socketException2)
						{
							SocketException socketException1 = socketException2;
							_u0040ԟ.Close();
							@ԟ.Release(_u0040ԟ);
							ChatSystem.Instance.LogException(socketException1);
							_u0040ԟ = @ԟ.Allocate(new IPEndPoint(ClusterSupport.@ӝ, ClusterSupport.@Ә));
							numArray = _u0040ԟ.SendAndRead(array);
						}
					}
					catch (Exception exception1)
					{
						Exception exception = exception1;
						_u0040ԟ.Close();
						ChatSystem.Instance.LogException(exception);
						throw;
					}
					@ԟ.Release(_u0040ԟ);
				}
			}
			else
			{
				numArray = ClusterSupport.@ӟ.Send(array);
			}
			using (MemoryStream memoryStream1 = new MemoryStream(numArray))
			{
				@ԝ _u0040ԝ = new @ԝ(memoryStream1);
				if (_u0040ԝ.ReadString() == "exception")
				{
					throw new Exception(_u0040ԝ.ReadString());
				}
				u0040ӗ = _u0040ԝ.ReadString();
				int num2 = _u0040ԝ.ReadInt32();
				if (num2 > -1)
				{
					if (num2 > 64)
					{
						throw new Exception("Invalid info count");
					}
					u0040Ә = new string[num2];
					for (int j = 0; j < num2; j++)
					{
						u0040Ә[j] = _u0040ԝ.ReadString();
					}
				}
				int num3 = _u0040ԝ.ReadInt32();
				if (num3 > -1)
				{
					if (num3 != 0)
					{
						u0040ә = _u0040ԝ.ReadBytes(num3);
					}
					else
					{
						u0040ә = new byte[0];
						return;
					}
				}
			}
		}

		private static byte[] @Ӧ(@ԡ u0040ӓ, byte[] u0040Ӕ)
		{
			return ClusterSupport.@ө(new MemoryStream(u0040Ӕ), "lfc");
		}

		internal static byte[] @ө(Stream u0040ӓ, string u0040Ӕ)
		{
			string str;
			string[] strArrays;
			byte[] numArray;
			byte[] array;
			ClusterSupport.@Ӡ();
			string[] strArrays1 = null;
			byte[] numArray1 = null;
			@ԝ _u0040ԝ = new @ԝ(u0040ӓ);
			string str1 = _u0040ԝ.ReadString();
			_u0040ԝ.ReadString();
			string str2 = _u0040ԝ.ReadString();
			int num = _u0040ԝ.ReadInt32();
			if (num > -1)
			{
				if (num > 64)
				{
					throw new Exception("Invalid info count");
				}
				strArrays1 = new string[num];
				for (int i = 0; i < num; i++)
				{
					strArrays1[i] = _u0040ԝ.ReadString();
				}
			}
			int num1 = _u0040ԝ.ReadInt32();
			if (num1 > -1)
			{
				numArray1 = (num1 != 0 ? _u0040ԝ.ReadBytes(num1) : new byte[0]);
			}
			ChatIdentity nLBChatIdentity = null;
			if (_u0040ԝ.ReadBoolean())
			{
				string str3 = _u0040ԝ.ReadString();
				string str4 = _u0040ԝ.ReadString();
				bool flag = _u0040ԝ.ReadBoolean();
				nLBChatIdentity = new NLBChatIdentity(str4, flag, str3, _u0040ԝ.ReadString());
			}
			MemoryStream memoryStream = new MemoryStream();
			@Ԝ _u0040Ԝ = new @Ԝ(memoryStream);
			try
			{
				if (ClusterSupport.Password != null && ClusterSupport.Password != "" && ClusterSupport.Password != str1)
				{
					throw new Exception("Invalid NLB Password.");
				}
				if (!ClusterSupport.IsClusterServer)
				{
					if (u0040Ӕ == "tcp")
					{
						throw new Exception("Unknown exception! not tcp server ?");
					}
					if (ClusterSupport.@ә != "server")
					{
						throw new Exception(string.Concat("Not a CuteChat server:", ClusterSupport.@ә));
					}
				}
				ClusterSupport.@Ө(str2, strArrays1, numArray1, nLBChatIdentity, out str, out strArrays, out numArray);
				goto Label0;
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				ChatSystem.Instance.LogException(exception);
				_u0040Ԝ.Write("exception");
				_u0040Ԝ.Write(string.Concat(exception.ToString(), "\r\n--------------------------------"));
				array = memoryStream.ToArray();
			}
			return array;
		Label0:
			_u0040Ԝ.Write("ok");
			_u0040Ԝ.Write(str);
			if (strArrays == null)
			{
				_u0040Ԝ.Write(-1);
			}
			else if (strArrays.Length != 0)
			{
				_u0040Ԝ.Write((int)strArrays.Length);
				string[] strArrays2 = strArrays;
				for (int j = 0; j < (int)strArrays2.Length; j++)
				{
					_u0040Ԝ.Write(strArrays2[j]);
				}
			}
			else
			{
				_u0040Ԝ.Write(0);
			}
			if (numArray == null)
			{
				_u0040Ԝ.Write(-1);
			}
			else if (numArray.Length != 0)
			{
				_u0040Ԝ.Write((int)numArray.Length);
				_u0040Ԝ.Write(numArray);
			}
			else
			{
				_u0040Ԝ.Write(0);
			}
			return memoryStream.ToArray();
		}

		internal static void @Ө(string u0040ӓ, string[] u0040Ӕ, byte[] u0040ӕ, ChatIdentity u0040Ӗ, out string u0040ӗ, out string[] u0040Ә, out byte[] u0040ә)
		{
			MethodInfo methodInfo;
			ChatPortal chatPortal = null;
			ClusterSupport.@Ӡ();
			if (!ClusterSupport.@Ӕ)
			{
				throw new Exception("Invalid Mode.");
			}
			if (u0040ӓ == null)
			{
				throw new ArgumentNullException("command");
			}
			if (u0040ӓ.StartsWith("!"))
			{
				if (u0040ӓ == "!ExecuteMethod")
				{
					object[] objArray = new object[0];
					using (MemoryStream memoryStream = new MemoryStream(u0040ӕ))
					{
						methodInfo = (MethodInfo)@ԛ.Deserialize(memoryStream);
						ChatServerTypeAttribute.ValidateMethod(methodInfo);
						if (memoryStream.Position < memoryStream.Length)
						{
							objArray = (object[])@ԛ.Deserialize(memoryStream);
						}
					}
					object obj = methodInfo.Invoke(null, objArray);
					u0040ӗ = null;
					u0040Ә = null;
					u0040ә = null;
					if (obj != null)
					{
						using (MemoryStream memoryStream1 = new MemoryStream())
						{
							@ԛ.Serialize(memoryStream1, obj);
							u0040ә = memoryStream1.ToArray();
						}
					}
					return;
				}
				if (u0040ӓ == "!Request")
				{
					string str = u0040Ӕ[0];
					string str1 = u0040Ӕ[1];
					MemoryStream memoryStream2 = new MemoryStream(u0040ӕ);
					ChatCookie chatCookie = (ChatCookie)@ԛ.Deserialize(memoryStream2);
					string[] strArrays = (string[])@ԛ.Deserialize(memoryStream2);
					NameValueCollection nameValueCollection = (NameValueCollection)@ԛ.Deserialize(memoryStream2);
					ChatResponse chatResponse = null;
					try
					{
						lock (ChatSystem.Instance.GetCurrentPortal())
						{
							ChatPlace place = chatPortal.GetPlace(str1);
							if (place == null)
							{
								chatResponse = new ChatResponse()
								{
									Cookie = chatCookie,
									ReturnCode = "NOPLACE"
								};
								ChatChannel[] startedChannels = chatPortal.GetStartedChannels();
								for (int i = 0; i < (int)startedChannels.Length; i++)
								{
									ChatPlace chatPlace = startedChannels[i];
									ChatResponse chatResponse1 = chatResponse;
									chatResponse1.ServerMessage = string.Concat(chatResponse1.ServerMessage, ",", chatPlace.PlaceName);
								}
							}
							else if (str == "CONNECT")
							{
								chatResponse = place.Manager.Connect(u0040Ӗ, chatCookie, nameValueCollection);
							}
							else if (str != "SYNCDATA")
							{
								if (str != "DISCONNECT")
								{
									throw new Exception(string.Concat("Unknown method : ", str));
								}
								chatResponse = place.Manager.Disconnect(u0040Ӗ, chatCookie);
							}
							else
							{
								IAsyncResult asyncResult = place.Manager.BeginSyncData(u0040Ӗ, chatCookie, strArrays, nameValueCollection, null, null);
								chatResponse = place.Manager.EndSyncData(asyncResult);
							}
						}
					}
					catch (Exception exception1)
					{
						Exception exception = exception1;
						ChatSystem.Instance.LogException(exception);
						chatResponse = new ChatResponse()
						{
							Cookie = chatCookie,
							ReturnCode = "ERROR",
							ServerMessage = exception.Message
						};
					}
					u0040ӗ = null;
					u0040Ә = null;
					memoryStream2 = new MemoryStream();
					@ԛ.Serialize(memoryStream2, chatResponse);
					u0040ә = memoryStream2.ToArray();
					return;
				}
			}
			throw new NotImplementedException(string.Concat("Cluster Support - Unknown command : ", u0040ӓ));
		}

		private static void @ӡ()
		{
			string item = ConfigurationSettings.AppSettings["CuteChat.NLB"];
			if (item == null || item == "")
			{
				return;
			}
			try
			{
				NameValueCollection nameValueCollection = new NameValueCollection();
				string[] strArrays = item.Split(new char[] { ';' });
				for (int i = 0; i < (int)strArrays.Length; i++)
				{
					string str = strArrays[i].Trim();
					if (str.Length != 0)
					{
						string[] strArrays1 = str.Split(new char[] { '=' }, 2);
						if ((int)strArrays1.Length == 1)
						{
							throw new Exception(string.Concat("Invalid item : ", str));
						}
						nameValueCollection[strArrays1[0].ToLower()] = strArrays1[1];
					}
				}
				string item1 = nameValueCollection["mode"];
				if (item1 == null || item1 == "")
				{
					throw new Exception("Mode not declared");
				}
				ClusterSupport.@ә = item1.ToLower();
				string str1 = nameValueCollection["port"];
				if (str1 != null && str1 != "")
				{
					ClusterSupport.@ӗ = "tcp";
					ClusterSupport.@Ә = int.Parse(nameValueCollection["port"]);
				}
				ClusterSupport.@ӛ = nameValueCollection["password"];
				if (string.Compare(item1, "server", true) != 0)
				{
					if (string.Compare(item1, "client", true) != 0)
					{
						if (string.Compare(item1, "local", true) != 0)
						{
							throw new Exception("Mode must be 'server' or 'client' or 'local' ");
						}
						ClusterSupport.@Ӗ = true;
						string item2 = nameValueCollection["directory"];
						if (item2 == null || item2 == "")
						{
							ClusterSupport.@ӗ = "tcp";
							ClusterSupport.@ӝ = IPAddress.Loopback;
							ClusterSupport.@ӣ();
						}
						else
						{
							ClusterSupport.@ӗ = "lfc";
							ClusterSupport.@ӥ();
						}
					}
					else
					{
						ClusterSupport.@ӕ = true;
						ClusterSupport.@Ӛ = nameValueCollection["url"];
						if (ClusterSupport.@Ӛ == null || ClusterSupport.@Ӛ == "")
						{
							throw new Exception("Url not declared");
						}
						string str2 = nameValueCollection["tcpserver"];
						if (str2 != null && str2 != "")
						{
							string[] strArrays2 = str2.Split(new char[] { ':' });
							ClusterSupport.@ӝ = Dns.Resolve(strArrays2[0]).AddressList[0];
							if ((int)strArrays2.Length > 1)
							{
								if (ClusterSupport.@ӗ == "tcp")
								{
									throw new Exception("Do not specify port when the url already contains port");
								}
								ClusterSupport.@Ә = int.Parse(strArrays2[1]);
							}
							ClusterSupport.@ӗ = "tcp";
						}
					}
				}
				else if (ClusterSupport.@ӗ != "tcp")
				{
					ClusterSupport.@Ӕ = true;
				}
				else if (!ClusterSupport.@Ӣ())
				{
					ClusterSupport.@ӝ = IPAddress.Loopback;
				}
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				throw new Exception(string.Concat("Invalid CuteChat.NLB configuration : ", exception.Message), exception);
			}
		}

		internal static void @Ӡ()
		{
			// 
			// Current member / type: System.Void CuteChat.ClusterSupport::@Ӡ()
			// File path: C:\Users\ThomasFroggatt\Desktop\TEMP\CuteChat.dll
			// 
			// Product version: 2019.1.118.0
			// Exception in: System.Void @Ӡ()
			// 
			// Object reference not set to an instance of an object.
			//    at ..() in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Steps\RebuildLockStatements.cs:line 93
			//    at ..( ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Steps\RebuildLockStatements.cs:line 24
			//    at ..Visit(ICodeNode ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Ast\BaseCodeVisitor.cs:line 69
			//    at ..(DecompilationContext ,  ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Steps\RebuildLockStatements.cs:line 19
			//    at ..(MethodBody ,  , ILanguage ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Decompiler\DecompilationPipeline.cs:line 88
			//    at ..(MethodBody , ILanguage ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Decompiler\DecompilationPipeline.cs:line 70
			//    at Telerik.JustDecompiler.Decompiler.Extensions.( , ILanguage , MethodBody , DecompilationContext& ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Decompiler\Extensions.cs:line 95
			//    at Telerik.JustDecompiler.Decompiler.Extensions.(MethodBody , ILanguage , DecompilationContext& ,  ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Decompiler\Extensions.cs:line 58
			//    at ..(ILanguage , MethodDefinition ,  ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Decompiler\WriterContextServices\BaseWriterContextService.cs:line 117
			// 
			// mailto: JustDecompilePublicFeedback@telerik.com

		}

		public static ChatResponse Client_Request(string method, string placename, ChatIdentity identity, ChatCookie cookie, string[] args, NameValueCollection nvc)
		{
			string str;
			string[] strArrays;
			byte[] numArray;
			ClusterSupport.@Ӡ();
			MemoryStream memoryStream = new MemoryStream();
			@Ԝ _u0040Ԝ = new @Ԝ(memoryStream);
			@ԛ.Serialize(memoryStream, cookie);
			@ԛ.Serialize(memoryStream, args);
			@ԛ.Serialize(memoryStream, nvc);
			ClusterSupport.@ӧ("!Request", new string[] { method, placename }, memoryStream.ToArray(), identity, out str, out strArrays, out numArray);
			return (ChatResponse)@ԛ.Deserialize(new MemoryStream(numArray));
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static object ExecuteCurrentMethod(params object[] args)
		{
			ClusterSupport.@Ӡ();
			return ClusterSupport.ExecuteMethod((new StackFrame(1)).GetMethod() as MethodInfo, args);
		}

		public static object ExecuteMethod(MethodInfo method, params object[] args)
		{
			string str;
			string[] strArrays;
			byte[] numArray;
			if (method == null)
			{
				throw new ArgumentNullException("method");
			}
			if (!method.IsStatic)
			{
				throw new ArgumentException(string.Concat("method is not static : ", method.Name));
			}
			ClusterSupport.@Ӡ();
			ChatServerTypeAttribute.ValidateMethod(method);
			ChatIdentity chatIdentity = null;
			byte[] array = null;
			using (MemoryStream memoryStream = new MemoryStream())
			{
				@ԛ.Serialize(memoryStream, method);
				if (args != null && args.Length != 0)
				{
					@ԛ.Serialize(memoryStream, args);
				}
				array = memoryStream.ToArray();
			}
			ClusterSupport.@ӧ("!ExecuteMethod", null, array, chatIdentity, out str, out strArrays, out numArray);
			if (numArray == null)
			{
				return null;
			}
			return @ԛ.Deserialize(new MemoryStream(numArray));
		}
	}
}