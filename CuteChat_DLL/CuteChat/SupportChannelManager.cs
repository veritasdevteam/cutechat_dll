using System;

namespace CuteChat
{
	public class SupportChannelManager : ChatManager
	{
		public SupportChannelManager(ChatPlace place) : base(place)
		{
		}

		public override void AddPlaceConnection(ChatConnection conn)
		{
			if (base.Portal.DataManager.IsAgent(conn.Identity.UniqueId))
			{
				conn.PlaceUser.SetInfoProperty("IsAgent", "1");
			}
			base.AddPlaceConnection(conn);
		}

		protected override bool IgnorePlaceQueue(ChatIdentity identity)
		{
			if (base.Portal.DataManager.IsAgent(identity.UniqueId))
			{
				return true;
			}
			return base.IgnorePlaceQueue(identity);
		}

		protected internal override void PostSyncData(ChatConnection conn, ChatResponse res)
		{
			base.PostSyncData(conn, res);
			if (res.ReturnCode == "READY")
			{
				SupportSessionChannel place = base.Place as SupportSessionChannel;
				if (place != null && place.CustomerIdentity == conn.Identity)
				{
					//((SupportAgentChannel)base.Portal.GetPlace("SupportAgent")).@Ӗ(conn.Identity);
				}
			}
		}

		protected override bool ShouldQueueSync(ChatPlaceUser user)
		{
			string runtimeProperty = user.GetRuntimeProperty("IsAgent");
			if (runtimeProperty == null)
			{
				runtimeProperty = (base.Portal.DataManager.IsAgent(user.Identity.UniqueId) ? "1" : "0");
				user.SetRuntimeProperty("IsAgent", runtimeProperty);
			}
			if (runtimeProperty == "1")
			{
				return false;
			}
			return base.ShouldQueueSync(user);
		}
	}
}