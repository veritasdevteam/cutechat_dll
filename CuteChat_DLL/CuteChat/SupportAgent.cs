using System;

namespace CuteChat
{
	[Serializable]
	public class SupportAgent
	{
		private string _id;

		private SupportDepartment _depart;

		public SupportDepartment Department
		{
			get
			{
				return this._depart;
			}
			set
			{
				this._depart = value;
			}
		}

		public string UserId
		{
			get
			{
				return this._id;
			}
			set
			{
				this._id = value;
			}
		}

		public SupportAgent()
		{
		}
	}
}