using System;

namespace CuteChat
{
	[Serializable]
	public class ChatMsgData
	{
		private int _msgid;

		private string _location;

		private string _placename;

		private DateTime _time;

		private string _sender;

		private string _senderid;

		private string _target;

		private string _targetid;

		private string _text;

		private string _html;

		private bool _offline;

		private bool _whisper;

		private string _claimNo { get; set; }

		public string ClaimNo
		{
			get
			{
				return this._claimNo;
			}
			set
			{
				this._claimNo = value;
			}
		}


		public string Html
		{
			get
			{
				return this._html;
			}
			set
			{
				this._html = value;
			}
		}

		public string Location
		{
			get
			{
				return this._location;
			}
			set
			{
				this._location = value;
			}
		}

		public int MessageId
		{
			get
			{
				return this._msgid;
			}
			set
			{
				this._msgid = value;
			}
		}

		public bool Offline
		{
			get
			{
				return this._offline;
			}
			set
			{
				this._offline = value;
			}
		}

		public string PlaceName
		{
			get
			{
				return this._placename;
			}
			set
			{
				this._placename = value;
			}
		}

		public string Sender
		{
			get
			{
				return this._sender;
			}
			set
			{
				this._sender = value;
			}
		}

		public string SenderId
		{
			get
			{
				return this._senderid;
			}
			set
			{
				this._senderid = value;
			}
		}

		public string Target
		{
			get
			{
				return this._target;
			}
			set
			{
				this._target = value;
			}
		}

		public string TargetId
		{
			get
			{
				return this._targetid;
			}
			set
			{
				this._targetid = value;
			}
		}

		public string Text
		{
			get
			{
				return this._text;
			}
			set
			{
				this._text = value;
			}
		}

		public DateTime Time
		{
			get
			{
				return this._time;
			}
			set
			{
				this._time = value;
			}
		}

		public bool Whisper
		{
			get
			{
				return this._whisper;
			}
			set
			{
				this._whisper = value;
			}
		}

		public ChatMsgData()
		{
		}
	}
}