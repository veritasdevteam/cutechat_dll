using System;
using System.Text;
using System.Xml;

namespace CuteChat
{
	public class SafeHtmlParser
	{
		private const string @ӓ = ",LINK,META,BASE,BGSOUND,BR,HR,INPUT,PARAM,IMG,AREA,";

		private const string @Ӕ = ",IFRAME,A,P,DIV,OBJECT,ADDRESS,BIG,BLOCKQUOTE,B,CAPTION,CENTER,CITE,CODE,\r\n\t\t\t\t\t\t\t\t\t,DD,DFN,DIR,DL,DT,EM,FONT,FORM,H1,H2,H3,H4,H5,H6,HEAD,HTML,I,LI,MAP,MENU,OL,OPTION,\r\n\t\t\t\t\t\t\t\t\t,PRE,SELECT,SMALL,STRIKE,STRONG,SUB,SUP,TABLE,TD,TEXTAREA,TH,TITLE,TR,TT,U,";

		private XmlDocument @ӕ;

		private string @Ӗ;

		private string @ӗ;

		private int @Ә;

		private int @ә;

		private static char[] @Ӛ;

		static SafeHtmlParser()
		{
			SafeHtmlParser.@Ӛ = new char[] { '&', ';' };
		}

		private SafeHtmlParser()
		{
		}

		private XmlElement @ӛ(string u0040ӓ)
		{
			XmlElement xmlElement;
			u0040ӓ = u0040ӓ.ToLower();
			try
			{
				if (u0040ӓ.IndexOf(":") != -1)
				{
					string[] strArrays = u0040ӓ.Split(new char[] { ':' }, 2);
					string str = strArrays[0];
					string str1 = strArrays[1];
					if (str1.IndexOf(":") != -1)
					{
						str1 = str1.Replace(':', '-');
					}
					if (str1 == "")
					{
						this.@ӕ.CreateElement(str);
					}
					xmlElement = (str != "" ? this.@ӕ.CreateElement(str, str1, string.Concat("http://unknownprefix/", str)) : this.@ӕ.CreateElement(str1));
				}
				else
				{
					xmlElement = this.@ӕ.CreateElement(u0040ӓ);
				}
			}
			catch
			{
				xmlElement = this.@ӕ.CreateElement("error");
			}
			return xmlElement;
		}

		private void @ӝ(string u0040ӓ)
		{
			if (this.@ә + 1 > this.@Ә)
			{
				return;
			}
			if (this.@Ӗ[this.@ә] != '<' || this.@Ӗ[this.@ә + 1] != '/')
			{
				return;
			}
			int num = this.@ә;
			this.@ә += 2;
			string str = this.@ӯ();
			if (str != null)
			{
				this.@ә += str.Length;
				this.@Ӭ();
				if (this.@ә < this.@Ә && this.@Ӗ[this.@ә] == '>')
				{
					this.@ә++;
				}
				if (string.Compare(str, u0040ӓ, true) == 0)
				{
					return;
				}
			}
			this.@ә = num;
		}

		private void @Ӝ()
		{
			if (this.@ә + 1 > this.@Ә)
			{
				return;
			}
			if (this.@Ӗ[this.@ә] != '<' || this.@Ӗ[this.@ә + 1] != '/')
			{
				return;
			}
			this.@ә += 2;
			string str = this.@ӯ();
			if (str != null)
			{
				this.@ә += str.Length;
				this.@Ӭ();
				if (this.@ә < this.@Ә && this.@Ӗ[this.@ә] == '>')
				{
					this.@ә++;
				}
			}
		}

		private void @ӟ(ref XmlElement u0040ӓ)
		{
			string str;
			int num = this.@Ә;
			this.@ә++;
			string upper = u0040ӓ.LocalName.ToUpper();
			if (this.@ә < num)
			{
				char chr = this.@Ӗ[this.@ә];
				if (chr == '!')
				{
					this.@ә++;
					this.@ӣ(u0040ӓ);
					return;
				}
				if (chr == '?')
				{
					this.@ә++;
					this.@Ӥ(u0040ӓ);
					return;
				}
				string str1 = this.@ӯ();
				if (str1 != null)
				{
					bool flag = false;
					string upper1 = str1.ToUpper();
					if (upper1 != null)
					{
						if (upper1 == "TR")
						{
							if (!(upper == "TABLE") && !(upper == "TBODY") && !(upper == "THEAD") && !(upper == "TFOOT"))
							{
								flag = true;
							}
						}
						else if (upper1 != "TD" && upper1 != "TH")
						{
							if (upper1 == "LI")
							{
								if (!(upper == "UL") && !(upper == "OL") && !(upper == "DIR") && !(upper == "MENU"))
								{
									flag = true;
								}
							}
						}
						else if (!(upper == "TR") && !(upper == "TABLE") && !(upper == "TBODY") && !(upper == "THEAD") && !(upper == "TFOOT"))
						{
							flag = true;
						}
					}
					if (flag)
					{
						this.@ә--;
						u0040ӓ = (XmlElement)u0040ӓ.ParentNode;
						return;
					}
					this.@ә += str1.Length;
					XmlElement xmlElement = this.@ӛ(str1);
					string upper2 = xmlElement.LocalName.ToUpper();
					u0040ӓ.AppendChild(xmlElement);
					this.@Ӡ(xmlElement);
					if (this.@ә >= num)
					{
						return;
					}
					chr = this.@Ӗ[this.@ә];
					if (chr == '/')
					{
						this.@ә++;
						if (this.@ә < num && this.@Ӗ[this.@ә] == '>')
						{
							this.@ә++;
						}
						return;
					}
					if (chr == '>')
					{
						this.@ә++;
					}
					if (this.@ә >= num)
					{
						return;
					}
					if (",LINK,META,BASE,BGSOUND,BR,HR,INPUT,PARAM,IMG,AREA,".IndexOf(string.Concat(",", upper2, ",")) != -1)
					{
						return;
					}
					if (upper2 != "SCRIPT")
					{
						this.@Ӟ(ref xmlElement);
						if (",IFRAME,A,P,DIV,OBJECT,ADDRESS,BIG,BLOCKQUOTE,B,CAPTION,CENTER,CITE,CODE,\r\n\t\t\t\t\t\t\t\t\t,DD,DFN,DIR,DL,DT,EM,FONT,FORM,H1,H2,H3,H4,H5,H6,HEAD,HTML,I,LI,MAP,MENU,OL,OPTION,\r\n\t\t\t\t\t\t\t\t\t,PRE,SELECT,SMALL,STRIKE,STRONG,SUB,SUP,TABLE,TD,TEXTAREA,TH,TITLE,TR,TT,U,".IndexOf(string.Concat(",", upper2, ",")) != -1 && xmlElement.ChildNodes.Count == 0)
						{
							xmlElement.AppendChild(this.@ӕ.CreateTextNode(""));
						}
					}
					else
					{
						int num1 = this.@ӗ.IndexOf("</script", this.@ә);
						if (num1 == -1)
						{
							str = this.@ӥ();
						}
						else if (num1 != this.@ә)
						{
							str = this.@Ӗ.Substring(this.@ә, num1 - this.@ә);
							this.@ә = num1;
						}
						else
						{
							str = "";
						}
						str = str.Replace("/*<![CDATA[*/", "").Replace("/*]]>*/", "").Trim().Replace("]]>", "]]&gt;");
						xmlElement.AppendChild(this.@ӕ.CreateTextNode("/*"));
						xmlElement.AppendChild(this.@ӕ.CreateCDataSection(string.Concat("*/\r\n", str, "\r\n/*")));
						xmlElement.AppendChild(this.@ӕ.CreateTextNode("*/"));
					}
					this.@ӝ(str1);
					return;
				}
			}
			u0040ӓ.AppendChild(this.@ӕ.CreateTextNode("<"));
		}

		private void @Ӟ(ref XmlElement u0040ӓ)
		{
			int num = this.@Ә;
			while (this.@ә < num)
			{
				if (this.@Ӗ[this.@ә] != '<')
				{
					string str = this.@Ӭ();
					string str1 = this.@Ӱ();
					if (str1 != null)
					{
						this.@ә += str1.Length;
						u0040ӓ.AppendChild(this.@ӕ.CreateTextNode(string.Concat(str, SafeHtmlParser.@ӱ(str1))));
					}
					else
					{
						if (str != null)
						{
							continue;
						}
						this.@ә++;
					}
				}
				else
				{
					if (this.@ә + 1 < num && this.@Ӗ[this.@ә + 1] == '/')
					{
						return;
					}
					this.@ӟ(ref u0040ӓ);
				}
			}
		}

		private string @ӥ()
		{
			if (this.@ә >= this.@Ә)
			{
				return "";
			}
			string str = this.@Ӗ.Substring(this.@ә);
			this.@ә = this.@Ә;
			return str;
		}

		private void @Ӥ(XmlElement u0040ӓ)
		{
			int num = this.@Ә;
			string str = this.@ӯ();
			if (str != null)
			{
				this.@ә += str.Length;
				return;
			}
			u0040ӓ.AppendChild(this.@ӕ.CreateTextNode("<?"));
		}

		private void @ӣ(XmlElement u0040ӓ)
		{
			int num = this.@Ә;
			if (string.Compare(this.@Ӗ, this.@ә, "[CDATA[", 0, 7, true) == 0)
			{
				this.@ә += 7;
				u0040ӓ.AppendChild(this.@ӕ.CreateCDataSection(this.@Ӫ("]]>")));
				return;
			}
			if (string.Compare(this.@Ӗ, this.@ә, "--", 0, "--".Length, true) != 0)
			{
				string str = this.@ӯ();
				if (str == null)
				{
					u0040ӓ.AppendChild(this.@ӕ.CreateTextNode("<!"));
					return;
				}
				this.@ә += str.Length;
				try
				{
					u0040ӓ.AppendChild(this.@ӕ.CreateDocumentType(str, null, null, this.@ө('>')));
				}
				catch
				{
				}
				return;
			}
			this.@ә += 2;
			string str1 = this.@ӧ("-->");
			if (str1.IndexOf("--") != -1)
			{
				str1 = str1.Replace("--", "- -");
			}
			if (str1.StartsWith("-") || str1.EndsWith("-"))
			{
				str1 = string.Concat(" ", str1, " ");
			}
			u0040ӓ.AppendChild(this.@ӕ.CreateComment(str1));
			this.@ә += 2;
		}

		private string @Ӣ()
		{
			int num = this.@ә;
			while (this.@ә < this.@Ә)
			{
				char chr = this.@Ӗ[this.@ә];
				if (chr == '>' || char.IsWhiteSpace(chr))
				{
					break;
				}
				this.@ә++;
			}
			if (num == this.@ә)
			{
				return null;
			}
			return this.@Ӗ.Substring(num, this.@ә - num);
		}

		private string @ӧ(string u0040ӓ)
		{
			if (this.@ә >= this.@Ә)
			{
				return "";
			}
			int num = this.@Ӗ.IndexOf(u0040ӓ, this.@ә);
			if (num == -1)
			{
				return this.@ӫ();
			}
			string str = this.@Ӗ.Substring(this.@ә, num - this.@ә);
			this.@ә = num + 1;
			return str;
		}

		private string @Ӧ(char u0040ӓ)
		{
			if (this.@ә >= this.@Ә)
			{
				return "";
			}
			int num = this.@Ӗ.IndexOf(u0040ӓ, this.@ә);
			if (num == -1)
			{
				return this.@ӥ();
			}
			string str = this.@Ӗ.Substring(this.@ә, num - this.@ә);
			this.@ә = num + 1;
			return str;
		}

		private string @ө(char u0040ӓ)
		{
			if (this.@ә >= this.@Ә)
			{
				return "";
			}
			int num = this.@Ӗ.IndexOf(u0040ӓ, this.@ә);
			if (num == -1)
			{
				return this.@ӫ();
			}
			int num1 = this.@Ӗ.IndexOf('<', this.@ә, num - this.@ә);
			if (num1 != -1)
			{
				num = num1;
			}
			string str = this.@Ӗ.Substring(this.@ә, num - this.@ә);
			this.@ә = num + 1;
			return str;
		}

		private string @Ө(params char[] u0040ӓ)
		{
			if (this.@ә >= this.@Ә)
			{
				return "";
			}
			int num = this.@Ӗ.IndexOfAny(u0040ӓ, this.@ә);
			if (num == -1)
			{
				return this.@ӫ();
			}
			int num1 = this.@Ӗ.IndexOf('<', this.@ә, num - this.@ә);
			if (num1 != -1)
			{
				num = num1;
			}
			string str = this.@Ӗ.Substring(this.@ә, num - this.@ә);
			this.@ә = num + 1;
			return str;
		}

		private string @ӫ()
		{
			if (this.@ә >= this.@Ә)
			{
				return "";
			}
			if (this.@Ӗ[this.@ә] == '<')
			{
				return "";
			}
			int num = this.@Ӗ.IndexOf('<', this.@ә);
			if (num == -1)
			{
				string str = this.@Ӗ.Substring(this.@ә);
				this.@ә = this.@Ә;
				return str;
			}
			string str1 = this.@Ӗ.Substring(this.@ә, num - this.@ә);
			this.@ә = num;
			return str1;
		}

		private string @Ӫ(string u0040ӓ)
		{
			if (this.@ә >= this.@Ә)
			{
				return "";
			}
			int num = this.@Ӗ.IndexOf(u0040ӓ, this.@ә);
			if (num == -1)
			{
				return this.@ӫ();
			}
			int num1 = this.@Ӗ.IndexOf('<', this.@ә, num - this.@ә);
			if (num1 != -1)
			{
				num = num1;
			}
			string str = this.@Ӗ.Substring(this.@ә, num - this.@ә);
			this.@ә = num + u0040ӓ.Length;
			return str;
		}

		private static string @ӱ(string u0040ӓ)
		{
			if (u0040ӓ == null)
			{
				return null;
			}
			if (u0040ӓ == "")
			{
				return "";
			}
			StringBuilder stringBuilder = new StringBuilder();
			int length = u0040ӓ.Length;
			int num = 0;
			int num1 = u0040ӓ.IndexOf('&');
			while (num1 != -1)
			{
				int num2 = u0040ӓ.IndexOfAny(SafeHtmlParser.@Ӛ, num1 + 1);
				if (num2 == -1)
				{
					break;
				}
				if (u0040ӓ[num2] != '&')
				{
					if (num != num1)
					{
						stringBuilder.Append(u0040ӓ, num, num1 - num);
					}
					stringBuilder.Append(SafeHtmlParser.@Ӳ(u0040ӓ.Substring(num1, num2 + 1 - num1)));
					num = num2 + 1;
					if (num >= length)
					{
						break;
					}
					num1 = u0040ӓ.IndexOf('&', num);
				}
				else
				{
					stringBuilder.Append(u0040ӓ, num, num2 - num);
					num1 = num2;
				}
			}
			if (num < length)
			{
				stringBuilder.Append(u0040ӓ, num, length - num);
			}
			return stringBuilder.ToString();
		}

		private string @Ӱ()
		{
			StringBuilder stringBuilder = new StringBuilder();
			int num = this.@ә;
			stringBuilder.Append(this.@ӫ());
			this.@ә = num;
			if (stringBuilder.Length == 0)
			{
				return null;
			}
			return stringBuilder.ToString();
		}

		private string @ӯ()
		{
			int i;
			int num = this.@Ә;
			if (this.@ә >= num)
			{
				return null;
			}
			char chr = this.@Ӗ[this.@ә];
			if ((chr < 'a' || chr > 'z') && (chr < 'A' || chr > 'Z'))
			{
				return null;
			}
			for (i = this.@ә + 1; i < num; i++)
			{
				chr = this.@Ӗ[i];
				if ((chr < 'a' || chr > 'z') && (chr < 'A' || chr > 'Z') && (chr < '0' || chr > '9') && chr != '\u005F' && chr != '-' && chr != '.' && chr != ':')
				{
					break;
				}
			}
			return this.@Ӗ.Substring(this.@ә, i - this.@ә);
		}

		private string @Ӯ(out string u0040ӓ)
		{
			int num = this.@ә;
			u0040ӓ = this.@Ӭ();
			this.@ә = num;
			return this.@ӯ();
		}

		private static string @Ӳ(string u0040ӓ)
		{
			string lower = u0040ӓ.ToLower();
			if (lower != null)
			{
				if (lower == "&quot;")
				{
					return new string('\"', 1);
				}
				if (lower == "&amp;")
				{
					return new string('&', 1);
				}
				if (lower == "&lt;")
				{
					return new string('<', 1);
				}
				if (lower == "&gt;")
				{
					return new string('>', 1);
				}
				if (lower == "&nbsp;")
				{
					return new string('\u00A0', 1);
				}
			}
			if (u0040ӓ[1] != '#')
			{
				return u0040ӓ;
			}
			if (u0040ӓ.Length == 3)
			{
				return "&#;";
			}
			try
			{
				char chr = (char)short.Parse(u0040ӓ.Substring(2, u0040ӓ.Length - 3));
				lower = chr.ToString();
			}
			catch
			{
				lower = u0040ӓ;
			}
			return lower;
		}

		private string @ӭ()
		{
			int num = this.@Ә;
			int num1 = this.@ә;
			while (num1 < num && char.IsWhiteSpace(this.@Ӗ[num1]))
			{
				num1++;
			}
			if (num1 == this.@ә)
			{
				return null;
			}
			return this.@Ӗ.Substring(this.@ә, num1 - this.@ә);
		}

		private string @Ӭ()
		{
			string str = this.@ӭ();
			if (str == null)
			{
				return null;
			}
			this.@ә += str.Length;
			return str;
		}

		private void @ӡ(XmlElement u0040ӓ, string u0040Ӕ, string u0040ӕ)
		{
			u0040Ӕ = u0040Ӕ.ToLower();
			if (u0040Ӕ.IndexOf(':') == -1)
			{
				u0040ӓ.SetAttribute(u0040Ӕ, u0040ӕ);
				return;
			}
			string[] strArrays = u0040Ӕ.Split(new char[] { ':' }, 2);
			string str = strArrays[0];
			string str1 = strArrays[1];
			if (str == "")
			{
				u0040ӓ.SetAttribute(str1, u0040ӕ);
				return;
			}
			if (str1 == "")
			{
				u0040ӓ.SetAttribute(str, u0040ӕ);
				return;
			}
			u0040ӓ.SetAttribute(str1, u0040ӕ);
		}

		private void @Ӡ(XmlElement u0040ӓ)
		{
			string str;
			while (this.@ә < this.@Ә)
			{
				this.@Ӭ();
				string str1 = this.@ӯ();
				if (str1 == null)
				{
					return;
				}
				this.@ә += str1.Length;
				this.@Ӭ();
				if (this.@ә >= this.@Ә)
				{
					this.@ӡ(u0040ӓ, str1, str1);
					return;
				}
				if (this.@Ӗ[this.@ә] != '=')
				{
					continue;
				}
				this.@ә++;
				this.@Ӭ();
				if (this.@ә >= this.@Ә)
				{
					this.@ӡ(u0040ӓ, str1, str1);
					return;
				}
				char chr = this.@Ӗ[this.@ә];
				if (chr == '\"' || chr == '\'')
				{
					this.@ә++;
					str = this.@Ө(new char[] { '>', chr });
					if (this.@ә < this.@Ә && this.@Ӗ[this.@ә] == chr)
					{
						this.@ә++;
					}
				}
				else if (chr != '>')
				{
					str = this.@Ӣ();
				}
				else
				{
					str = null;
				}
				str = (str != null ? SafeHtmlParser.@ӱ(str) : "");
				this.@ӡ(u0040ӓ, str1, str);
			}
		}

		public static void ParseHtml(XmlElement element, string xmlstring)
		{
			if (element == null)
			{
				throw new ArgumentNullException("element");
			}
			if (xmlstring == null)
			{
				throw new ArgumentNullException("xmlstring");
			}
			SafeHtmlParser safeHtmlParser = new SafeHtmlParser()
			{
				@ӕ = element.OwnerDocument,
				@Ӗ = xmlstring,
				@ӗ = xmlstring.ToLower(),
				@Ә = xmlstring.Length,
				@ә = 0
			};
			while (true)
			{
				XmlElement xmlElement = element;
				safeHtmlParser.@Ӟ(ref xmlElement);
				if (safeHtmlParser.@ә >= safeHtmlParser.@Ә)
				{
					break;
				}
				safeHtmlParser.@Ӝ();
			}
		}
	}
}