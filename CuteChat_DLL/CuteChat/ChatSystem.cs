using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Net.Mail;
using System.Xml;

namespace CuteChat
{
	public abstract class ChatSystem : IDisposable
	{
		private static string @ӓ;

		private static ChatSystem @Ӕ;

		private Hashtable @ӕ = new Hashtable();

		private int @Ӗ;

		private DateTime @ӗ = DateTime.Now;

		private int @Ә;

		private DateTime @ә = DateTime.Now;

		private int @Ӛ;

		private int @ӛ;

		private Queue @Ӝ = new Queue();

		private int @ӝ;

		private Hashtable @Ӟ = new Hashtable();

		private DataTable @ӟ;

		private bool @Ӡ;

		private bool @ӡ;

		public abstract string DefaultMailAddress
		{
			get;
		}

		public static bool HasStarted
		{
			get
			{
				return ChatSystem.@Ӕ != null;
			}
		}

		public static ChatSystem Instance
		{
			get
			{
				ChatSystem chatSystem = ChatSystem.@Ӕ;
				if (chatSystem == null)
				{
					throw new Exception("ChatSystem not installed");
				}
				return chatSystem;
			}
		}

		public static string PhysicalDataDirectory
		{
			get
			{
				return ChatSystem.@ӓ;
			}
			set
			{
				ChatSystem.@ӓ = value;
			}
		}

		static ChatSystem()
		{
		}

		protected ChatSystem()
		{
		}

		private int @ӥ()
		{
			this.@ә = DateTime.Now;
			this.@Ӛ = 0;
			return 0;
		}

		private void @Ӥ(object u0040ӓ)
		{
			if (this.@Ӡ || this.@ӡ)
			{
				return;
			}
			try
			{
				while (!this.@Ӡ && !this.@ӡ)
				{
					Thread.Sleep(10);
					this.Maintain();
					this.@Ө();
				}
			}
			catch (ThreadAbortException threadAbortException)
			{
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				try
				{
					this.LogException(exception);
				}
				catch
				{
				}
			}
			ThreadPool.QueueUserWorkItem(new WaitCallback(this.@Ӥ), null);
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		private void @ӣ()
		{
			AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(ChatSystem.@Ӣ);
		}

		private static void @Ӣ(object u0040ӓ, UnhandledExceptionEventArgs u0040Ӕ)
		{
			if (ChatSystem.@Ӕ != null)
			{
				Exception exceptionObject = u0040Ӕ.ExceptionObject as Exception;
				if (exceptionObject != null)
				{
					ChatSystem.@Ӕ.LogException(new ApplicationException(string.Concat("Crit Error! ", exceptionObject.Message), exceptionObject));
				}
			}
		}

		internal bool @ӧ(@Ԍ u0040ӓ)
		{
			// 
			// Current member / type: System.Boolean CuteChat.ChatSystem::@ӧ(@Ԍ)
			// File path: C:\Users\ThomasFroggatt\Desktop\TEMP\CuteChat.dll
			// 
			// Product version: 2019.1.118.0
			// Exception in: System.Boolean @ӧ(u0040Ԍ)
			// 
			// Object reference not set to an instance of an object.
			//    at ..() in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Steps\RebuildLockStatements.cs:line 93
			//    at ..( ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Steps\RebuildLockStatements.cs:line 24
			//    at ..Visit(ICodeNode ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Ast\BaseCodeVisitor.cs:line 69
			//    at ..(DecompilationContext ,  ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Steps\RebuildLockStatements.cs:line 19
			//    at ..(MethodBody ,  , ILanguage ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Decompiler\DecompilationPipeline.cs:line 88
			//    at ..(MethodBody , ILanguage ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Decompiler\DecompilationPipeline.cs:line 70
			//    at Telerik.JustDecompiler.Decompiler.Extensions.( , ILanguage , MethodBody , DecompilationContext& ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Decompiler\Extensions.cs:line 95
			//    at Telerik.JustDecompiler.Decompiler.Extensions.(MethodBody , ILanguage , DecompilationContext& ,  ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Decompiler\Extensions.cs:line 58
			//    at ..(ILanguage , MethodDefinition ,  ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Decompiler\WriterContextServices\BaseWriterContextService.cs:line 117
			// 
			// mailto: JustDecompilePublicFeedback@telerik.com
			return true;
		}

		internal void @Ӧ()
		{
			this.@Ә++;
			this.@Ӛ++;
		}

		private void @ө(object u0040ӓ)
		{
			@Ԍ _u0040Ԍ = (@Ԍ)u0040ӓ;
			try
			{
				ChatPortal portal = _u0040Ԍ.Portal;
				Monitor.Enter(portal);
				try
				{
					if (_u0040Ԍ.CompleteCallback != null)
					{
						_u0040Ԍ.CompleteCallback(_u0040Ԍ);
					}
					_u0040Ԍ.@ӛ();
					if (_u0040Ԍ.Callback != null)
					{
						_u0040Ԍ.Callback(_u0040Ԍ);
					}
				}
				finally
				{
					Monitor.Exit(portal);
				}
			}
			catch (Exception exception)
			{
				this.LogException(exception);
			}
		}

		private void @Ө()
		{
			Queue queues = this.@Ӝ;
			Monitor.Enter(queues);
			try
			{
				int num = this.@ӥ();
				int count = this.@Ӝ.Count;
				if (num > 0)
				{
					count -= num;
				}
				if (count > 0)
				{
					for (int i = 0; i < count; i++)
					{
						@Ԍ _u0040Ԍ = (@Ԍ)this.@Ӝ.Dequeue();
						ThreadPool.QueueUserWorkItem(new WaitCallback(this.@ө), _u0040Ԍ);
					}
				}
			}
			finally
			{
				Monitor.Exit(queues);
			}
		}

		public static string CombineApplicationPath(string subpath)
		{
			string applicationBase;
			if (subpath == null)
			{
				throw new ArgumentNullException("subpath");
			}
			if (ChatSystem.@ӓ == null)
			{
				string item = ConfigurationSettings.AppSettings["CuteChat.DataDirectory"];
				if (item == null || item == "")
				{
					item = "~/CuteSoft_Client/CuteChat/";
				}
				if (item.StartsWith("/"))
				{
					item = item.Remove(0, 1);
				}
				if (item.StartsWith("~/"))
				{
					item = item.Remove(0, 2);
				}
				if (item.EndsWith("/"))
				{
					item = item.Substring(0, item.Length - 1);
				}
				try
				{
					applicationBase = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
				}
				catch
				{
					//applicationBase = HttpRuntime.AppDomainAppPath;
				}
				//ChatSystem.@ӓ = Path.Combine(applicationBase, item);
			}
			if (subpath == "")
			{
				return ChatSystem.@ӓ;
			}
			return Path.Combine(ChatSystem.@ӓ, subpath);
		}

		protected abstract ChatPortal CreatePortalInstance(string name);

		[Conditional("DEBUG")]
		public virtual void DebugMessage(string message, string category)
		{
		}

		public virtual void Dispose()
		{
			this.@Ӡ = true;
			foreach (object value in this.@ӕ.Values)
			{
				((ChatPortal)value).Dispose();
			}
			this.@ӕ.Clear();
			this.@ӡ = true;
		}

		protected virtual void DoStart()
		{
			try
			{
				this.@ӣ();
			}
			catch (Exception exception)
			{
			}
			ThreadPool.QueueUserWorkItem(new WaitCallback(this.@Ӥ), null);
		}

		public void Dump(XmlWriter writer)
		{
			writer.WriteAttributeString("connections", this.@ӝ.ToString());
			Queue queues = this.@Ӝ;
			Monitor.Enter(queues);
			try
			{
				writer.WriteAttributeString("queue", this.@Ӝ.Count.ToString());
			}
			finally
			{
				Monitor.Exit(queues);
			}
			writer.WriteAttributeString("_sysstart", this.@ӗ.ToString("yyyy-MM-dd HH:mm:ss"));
			writer.WriteAttributeString("_syscount", this.@Ә.ToString());
			writer.WriteAttributeString("_asynctime", this.@Ӗ.ToString());
			TimeSpan now = DateTime.Now - this.@ӗ;
			int totalSeconds = (int)((double)this.@Ә / now.TotalSeconds);
			writer.WriteAttributeString("_syssps", totalSeconds.ToString());
			TimeSpan timeSpan = DateTime.Now - this.@ә;
			if (timeSpan.TotalSeconds > 1)
			{
				double num = (double)this.@Ә / timeSpan.TotalSeconds;
				writer.WriteAttributeString("_currentsps", num.ToString("###,##0"));
				writer.WriteAttributeString("_limitspan", timeSpan.TotalSeconds.ToString("###,##0"));
				writer.WriteAttributeString("_synccount", this.@Ӛ.ToString());
				writer.WriteAttributeString("_lastcalccount", this.@ӛ.ToString());
			}
		}

		protected virtual object EnterIdentity()
		{
			return null;
		}

		public abstract ChatIdentity GetCurrentIdentity();

		public ChatPortal GetCurrentPortal()
		{
			if (ClusterSupport.IsClusterClient)
			{
				throw new Exception("Unable to get portal instance because current application is not cutechat server.");
			}
			string currentPortalName = this.GetCurrentPortalName();
			if (currentPortalName == null)
			{
				throw new Exception("Unable o get current chat portal name");
			}
			return this.GetOrCreatePortal(currentPortalName);
		}

		public abstract string GetCurrentPortalName();

		public ChatPortal GetOrCreatePortal(string name)
		{
			// 
			// Current member / type: CuteChat.ChatPortal CuteChat.ChatSystem::GetOrCreatePortal(System.String)
			// File path: C:\Users\ThomasFroggatt\Desktop\TEMP\CuteChat.dll
			// 
			// Product version: 2019.1.118.0
			// Exception in: CuteChat.ChatPortal GetOrCreatePortal(System.String)
			// 
			// Object reference not set to an instance of an object.
			//    at ..() in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Steps\RebuildLockStatements.cs:line 93
			//    at ..( ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Steps\RebuildLockStatements.cs:line 24
			//    at ..Visit(ICodeNode ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Ast\BaseCodeVisitor.cs:line 69
			//    at ..(DecompilationContext ,  ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Steps\RebuildLockStatements.cs:line 19
			//    at ..(MethodBody ,  , ILanguage ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Decompiler\DecompilationPipeline.cs:line 88
			//    at ..(MethodBody , ILanguage ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Decompiler\DecompilationPipeline.cs:line 70
			//    at Telerik.JustDecompiler.Decompiler.Extensions.( , ILanguage , MethodBody , DecompilationContext& ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Decompiler\Extensions.cs:line 95
			//    at Telerik.JustDecompiler.Decompiler.Extensions.(MethodBody , ILanguage , DecompilationContext& ,  ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Decompiler\Extensions.cs:line 58
			//    at ..(ILanguage , MethodDefinition ,  ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Decompiler\WriterContextServices\BaseWriterContextService.cs:line 117
			// 
			// mailto: JustDecompilePublicFeedback@telerik.com
			ChatPortal chat = null;

			return chat;
		}

		public ChatPortal[] GetStartedPortals()
		{
			ChatPortal[] chatPortalArray;
			Hashtable hashtables = this.@ӕ;
			Monitor.Enter(hashtables);
			try
			{
				chatPortalArray = new ChatPortal[this.@ӕ.Count];
				this.@ӕ.Values.CopyTo(chatPortalArray, 0);
			}
			finally
			{
				Monitor.Exit(hashtables);
			}
			return chatPortalArray;
		}

		public Strings GetStrings(string culture)
		{
			DataTable dataTable = this.@ӟ;
			if (dataTable == null)
			{
				dataTable = new DataTable("strings");
				dataTable.Columns.Add("culture", typeof(string));
				dataTable.Columns.Add("name", typeof(string));
				dataTable.Columns.Add("value", typeof(string));
				string[] files = Directory.GetFiles(ChatSystem.CombineApplicationPath("Languages"), "*.xml");
				for (int i = 0; i < (int)files.Length; i++)
				{
					string str = files[i];
					string lower = Path.GetFileNameWithoutExtension(str).ToLower();
					string str1 = (lower == "en-us" ? "" : lower);
					XmlDocument xmlDocument = new XmlDocument();
					xmlDocument.Load(str);
					foreach (XmlElement xmlElement in xmlDocument.DocumentElement.SelectNodes("resource"))
					{
						dataTable.Rows.Add(new object[] { str1, xmlElement.GetAttribute("name"), xmlElement.InnerText });
					}
				}
				dataTable.AcceptChanges();
				this.@ӟ = dataTable;
			}
			if (culture == null || culture == "")
			{
				culture = "en-us";
			}
			Strings item = (Strings)this.@Ӟ[culture.ToLower()];
			if (item == null)
			{
				item = new StringsContainer(culture, dataTable);
				this.@Ӟ[culture.ToLower()] = item;
			}
			return item;
		}

		public int GetSystemConnectionCount()
		{
			return this.@ӝ;
		}

		protected virtual void LeaveIdentity(object state)
		{
		}

		public virtual void LogException(Exception error)
		{
		}

		protected virtual void Maintain()
		{
			object obj = this.EnterIdentity();
			try
			{
				int connectionCount = 0;
				ChatPortal[] startedPortals = this.GetStartedPortals();
				for (int i = 0; i < (int)startedPortals.Length; i++)
				{
					ChatPortal chatPortal = startedPortals[i];
					try
					{
						ChatPortal chatPortal1 = chatPortal;
						Monitor.Enter(chatPortal1);
						try
						{
							chatPortal.Maintain();
							connectionCount += chatPortal.GetConnectionCount();
						}
						finally
						{
							Monitor.Exit(chatPortal1);
						}
					}
					catch (Exception exception)
					{
						this.LogException(exception);
					}
				}
				this.@ӝ = connectionCount;
			}
			finally
			{
				this.LeaveIdentity(obj);
			}
		}

		public void SendMail(string title, string content)
		{
			this.SendMail(title, content, this.DefaultMailAddress);
		}

		public void SendMail(string title, string content, string to)
		{
			MailMessage mailMessage = new MailMessage()
			{
				//To = to,
				Subject = title,
				Body = content
			};
			this.SendMail(mailMessage);
		}

		public abstract void SendMail(MailMessage msg);

		public static void Start(ChatSystem sys)
		{
			if (sys == null)
			{
				throw new ArgumentNullException("sys");
			}
			if (ChatSystem.@Ӕ != null)
			{
				throw new Exception("ChatSystem already started");
			}
			ChatSystem.@Ӕ = sys;
			try
			{
				sys.DoStart();
			}
			catch
			{
				ChatSystem.@Ӕ = null;
				sys.Dispose();
				throw;
			}
		}

		public static void Stop()
		{
			if (ChatSystem.@Ӕ == null)
			{
				throw new Exception("ChatSystem is not started");
			}
			ChatSystem.@Ӕ.Dispose();
			ChatSystem.@Ӕ = null;
		}
	}
}