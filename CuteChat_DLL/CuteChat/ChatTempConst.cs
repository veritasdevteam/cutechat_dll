using System;
using System.Runtime.CompilerServices;

namespace CuteChat
{
	public class ChatTempConst
	{
		public const string MessengerPlaceName = "Messenger";

		public const string SupportAgentPlaceName = "SupportAgent";

		public const string SupportPlacePrefix = "SupportSession:";

		public const int ConnectionExpireInSeconds = 60;

		public const int PlaceExpireInSeconds = 300;

		public const int RejectSameIdentityInSeconds = 10;

		public const int ChannelMaxOnlineCount = 400;

		public const int TypingTimeExpires = 5;

		public const int MaintainSleepMiliseconds = 10;

		public const int MaxPortalConnections = 1200000;

		public const int MaxSystemConnections = 1200000;

		[DecimalConstant(0, 0, 0, 0, 100)]
		public readonly static decimal MaxCpuUsage;

		public const int BestSystemConnections = 600;

		public const double ClientSyncInterval = 1;

		public const bool EnableLogEvent = false;

		public const bool EnableLogMessage = true;

		public const bool HandleCTSAtSyncData = true;

		static ChatTempConst()
		{
			ChatTempConst.MaxCpuUsage = new decimal(100);
		}

		public ChatTempConst()
		{
		}
	}
}