using System;
using System.Collections;
using System.Collections.Specialized;

namespace CuteChat
{
	public class ChatModerateItem : ChatPlaceItem
	{
		public ChatPlaceUser User;

		public string MsgId;

		public string[] Args;

		public NameValueCollection Nvc
		{
			get
			{
				return this.infoProps;
			}
			set
			{
				this.infoProps = value;
			}
		}

		public ChatModerateItem(ChatPlace place) : base(place)
		{
		}

		protected override bool GetItemInfo(ChatConnection conn, string formsg, out string type, out string[] args)
		{
			type = "MODERATORITEM";
			ArrayList arrayLists = new ArrayList();
			arrayLists.Add(this.User.ObjectGuid.ToString());
			arrayLists.Add(this.User.Identity.UniqueId);
			arrayLists.Add(this.User.DisplayName);
			arrayLists.Add(this.MsgId);
			arrayLists.AddRange(this.Args);
			args = (string[])arrayLists.ToArray(typeof(string));
			return true;
		}

		public override bool IsVisibleConnection(ChatConnection conn)
		{
			return base.Place.IsModerator(conn.Identity);
		}
	}
}