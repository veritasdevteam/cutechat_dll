using System;
using System.Collections;
using System.Xml;

namespace CuteChat
{
	public class ChatPlaceUser : ChatPlaceItem
	{
		private ChatConnection @ӓ;

		private string @Ӕ;

		private string @ӕ = "ONLINE";

		private string @Ӗ = "Normal";

		private string @ӗ = "BYCLIENT";

		internal int @Ә;

		internal ArrayList @ә;

		private bool @Ӛ = true;

		private bool @ӛ;

		private int @Ӝ;

		private DateTime @ӝ = DateTime.MinValue;

		private DateTime @Ӟ = DateTime.MinValue;

		private IChatUserInfo[] @ӟ;

		private IChatUserInfo[] @Ӡ;

		private IChatUserInfo @ӡ;

		public bool AppearOffline
		{
			get
			{
				return this.@ӛ;
			}
			set
			{
				this.@ӛ = value;
			}
		}

		public ChatConnection Connection
		{
			get
			{
				return this.@ӓ;
			}
		}

		public string DisplayName
		{
			get
			{
				if (base.Place is ChatMessenger)
				{
					return this.Info.DisplayName;
				}
				if (this.@Ӕ != null)
				{
					return this.@Ӕ;
				}
				if (this.@Ӕ == null || this.@Ӕ == "")
				{
					this.@Ӕ = base.Place.GetDisplayName(this.Identity);
				}
				return this.@Ӕ;
			}
			set
			{
				if (value != null)
				{
					value = value.Trim();
					if (value.Length == 0)
					{
						value = this.Identity.DisplayName;
					}
				}
				else
				{
					value = this.Identity.DisplayName;
				}
				if (!(base.Place is ChatMessenger))
				{
					this.@Ӕ = value;
					return;
				}
				this.Info.DisplayName = value;
				base.Portal.DataManager.UpdateUserInfo(this.Info);
			}
		}

		public ChatIdentity Identity
		{
			get
			{
				return this.@ӓ.Identity;
			}
		}

		public IChatUserInfo Info
		{
			get
			{
				if (this.@ӡ == null)
				{
					this.@ӡ = base.Portal.DataManager.GetUserInfo(this.Identity.UniqueId);
				}
				return this.@ӡ;
			}
		}

		public bool IsModerator
		{
			get
			{
				return base.Place.IsModerator(this.Identity);
			}
		}

		public string Level
		{
			get
			{
				return this.@Ӗ;
			}
			set
			{
				this.@Ӗ = value;
			}
		}

		public int MessageCount
		{
			get
			{
				return this.@Ӝ;
			}
		}

		public DateTime MessageTime
		{
			get
			{
				return this.@ӝ;
			}
		}

		public bool Moderated
		{
			get
			{
				return this.@Ӛ;
			}
			set
			{
				this.@Ӛ = value;
			}
		}

		public string OnlineStatus
		{
			get
			{
				return this.@ӕ;
			}
			set
			{
				this.@ӕ = value;
			}
		}

		public DateTime TypingTime
		{
			get
			{
				return this.@Ӟ;
			}
		}

		public ChatPlaceUser(ChatPlace place, ChatConnection conn) : base(place)
		{
			if (conn == null)
			{
				throw new ArgumentNullException("conn");
			}
			this.@ӓ = conn;
		}

		public override void Dump(XmlWriter writer)
		{
			writer.WriteAttributeString("name", this.DisplayName);
			if (this.Connection != null)
			{
				DateTime activateTime = this.Connection.ActivateTime;
				writer.WriteAttributeString("active", activateTime.ToString("HH:mm:ss"));
			}
			writer.WriteAttributeString("offline", (this.AppearOffline ? "1" : "0"));
			writer.WriteAttributeString("admin", (this.IsModerator ? "1" : "0"));
			if (this.@Ӝ > 0)
			{
				writer.WriteAttributeString("msgs", this.@Ӝ.ToString());
				writer.WriteAttributeString("msgtime", this.@Ӟ.ToString("HH:mm:ss"));
			}
			if (this.Connection != null)
			{
				writer.WriteAttributeString("connguid", this.Connection.ObjectGuid);
			}
		}

		protected override bool GetItemInfo(ChatConnection conn, string formsg, out string type, out string[] args)
		{
			type = "USER";
			string privateProperties = null;
			if (formsg == "MYINFO_UPDATED")
			{
				privateProperties = this.Info.PrivateProperties;
			}
			string pAddress = "";
			if (conn != null && conn.PlaceUser != null && conn.PlaceUser.IsModerator)
			{
				pAddress = this.Identity.IPAddress;
			}
			string[] uniqueId = new string[] { this.Identity.UniqueId, this.Identity.DisplayName, this.DisplayName, this.Info.Description, null, null, null, null, null, null, null, null, null, null };
			uniqueId[4] = (this.Identity.IsAnonymous ? "1" : "0");
			uniqueId[5] = (this.IsModerator ? "1" : "0");
			uniqueId[6] = (this.Moderated ? "1" : "0");
			uniqueId[7] = this.OnlineStatus;
			uniqueId[8] = this.@ӗ;
			uniqueId[9] = this.Info.PublicProperties;
			uniqueId[10] = privateProperties;
			uniqueId[11] = pAddress;
			uniqueId[12] = (this.AppearOffline ? "1" : "0");
			uniqueId[13] = this.Level;
			args = uniqueId;
			return true;
		}

		public void InitConnect(bool asoffline, bool requireModerate)
		{
			this.@ӛ = asoffline;
			if (asoffline)
			{
				this.OnlineStatus = "APPEAROFFLINE";
			}
			if (requireModerate)
			{
				this.@Ӛ = false;
			}
		}

		public override bool IsVisibleConnection(ChatConnection conn)
		{
			int i;
			if (conn.Identity == this.Identity)
			{
				return true;
			}
			if (!(base.Place is ChatMessenger))
			{
				if (!this.Moderated)
				{
					if (conn.PlaceUser.IsModerator)
					{
						return true;
					}
					return false;
				}
				if (this.AppearOffline)
				{
					if (conn.PlaceUser.IsModerator)
					{
						return true;
					}
					return false;
				}
				if (!conn.PlaceUser.Moderated)
				{
					return false;
				}
				return true;
			}
			if (this.AppearOffline)
			{
				return false;
			}
			IChatUserInfo[] placeUser = this.@Ӡ;
			for (i = 0; i < (int)placeUser.Length; i++)
			{
				if (string.Compare(placeUser[i].UserId, conn.Identity.UniqueId, true) == 0)
				{
					return false;
				}
			}
			if (conn.PlaceUser.@ӟ == null)
			{
				return false;
			}
			placeUser = conn.PlaceUser.@ӟ;
			for (i = 0; i < (int)placeUser.Length; i++)
			{
				if (string.Compare(placeUser[i].UserId, this.Identity.UniqueId, true) == 0)
				{
					return true;
				}
			}
			return false;
		}

		protected internal virtual void OnContactAdded(string userid)
		{
			IChatUserInfo userInfo = base.Portal.DataManager.GetUserInfo(userid);
			this.RenderContactInfo(userInfo, "CONTACT_ADDED");
			this.@ӟ = base.Portal.DataManager.GetContacts(this.Identity);
			base.Place.Manager.UpdatePlaceItem(this);
		}

		protected internal virtual void OnContactRemoved(string userid)
		{
			IChatUserInfo userInfo = base.Portal.DataManager.GetUserInfo(userid);
			this.RenderContactInfo(userInfo, "CONTACT_REMOVED");
			this.@ӟ = base.Portal.DataManager.GetContacts(this.Identity);
			base.Place.Manager.UpdatePlaceItem(this);
		}

		protected internal virtual void OnIgnoreAdded(string userid)
		{
			IChatUserInfo userInfo = base.Portal.DataManager.GetUserInfo(userid);
			this.RenderIgnoreInfo(userInfo, "IGNORE_ADDED");
			this.@Ӡ = base.Portal.DataManager.GetIgnores(this.Identity);
			base.Place.Manager.UpdatePlaceItem(this);
		}

		protected internal virtual void OnIgnoreRemoved(string userid)
		{
			IChatUserInfo userInfo = base.Portal.DataManager.GetUserInfo(userid);
			this.RenderIgnoreInfo(userInfo, "IGNORE_REMOVED");
			this.@Ӡ = base.Portal.DataManager.GetIgnores(this.Identity);
			base.Place.Manager.UpdatePlaceItem(this);
		}

		protected override void OnRemoveForConnection(ChatConnection conn, string reason)
		{
			this.@ӗ = reason;
			base.OnRemoveForConnection(conn, reason);
		}

		protected internal virtual void OnSelfAdded()
		{
			int i;
			this.RenderItemInfo(this.Connection, "MYINFO_UPDATED");
			this.@ӟ = base.Portal.DataManager.GetContacts(this.Identity);
			this.@Ӡ = base.Portal.DataManager.GetIgnores(this.Identity);
			IChatUserInfo[] chatUserInfoArray = this.@ӟ;
			for (i = 0; i < (int)chatUserInfoArray.Length; i++)
			{
				this.RenderContactInfo(chatUserInfoArray[i], "CONTACT_UPDATED");
			}
			chatUserInfoArray = this.@Ӡ;
			for (i = 0; i < (int)chatUserInfoArray.Length; i++)
			{
				this.RenderIgnoreInfo(chatUserInfoArray[i], "IGNORE_UPDATED");
			}
		}

		public void OnSendMessage()
		{
			this.@Ӝ++;
			this.@ӝ = DateTime.Now;
		}

		protected internal override void OnUpdated()
		{
			base.OnUpdated();
			base.Place.Manager.UpdatePlaceConnection(this.Connection);
		}

		protected override void OnUpdateForConnection(ChatConnection conn)
		{
			if (conn.Identity == this.Identity)
			{
				this.RenderItemInfo(conn, "MYINFO_UPDATED");
			}
			base.OnUpdateForConnection(conn);
		}

		protected void RenderContactInfo(IChatUserInfo info, string msgid)
		{
			string[] displayName = new string[] { info.DisplayName, info.UserId, info.UserName, info.Description, info.PublicProperties };
			base.Place.Manager.PushSTCMessage(this.Connection, msgid, null, displayName);
		}

		protected void RenderIgnoreInfo(IChatUserInfo info, string msgid)
		{
			string[] displayName = new string[] { info.DisplayName, info.UserId, info.UserName, info.Description, info.PublicProperties };
			base.Place.Manager.PushSTCMessage(this.Connection, msgid, null, displayName);
		}

		public bool SetTyping()
		{
			if ((DateTime.Now - this.@Ӟ).TotalSeconds <= 5)
			{
				return false;
			}
			this.@Ӟ = DateTime.Now;
			return true;
		}

		protected internal virtual void SwitchConnection(ChatConnection conn)
		{
			if (conn == null)
			{
				throw new ArgumentNullException("conn");
			}
			this.@ӓ = conn;
		}
	}
}