using System;
using System.Collections;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Web;

namespace CuteChat
{
	[ChatServerType]
	public class ChatApi
	{
		public static string CurrentCulture
		{
			get
			{
				string name;
				string currentPortalProperty = ChatApi.GetCurrentPortalProperty("CustomCulture");
				if (currentPortalProperty != null && currentPortalProperty != "")
				{
					return currentPortalProperty;
				}
				string str = ChatApi.GetCurrentPortalProperty("CultureType");
				if (str == null || str == "" || str == "Client")
				{
					//HttpContext current = HttpContext.Current;
					//if (current != null)
					//{
					//	try
					//	{
					//		name = (new CultureInfo(current.Request.UserLanguages[0])).Name;
					//	}
					//	catch
					//	{
					//		return CultureInfo.CurrentUICulture.ToString();
					//	}
					//	return name;
					//}
				}
				return CultureInfo.CurrentUICulture.ToString();
			}
		}

		public static CuteChat.Strings Strings
		{
			get
			{
				return ChatApi.GetStrings(ChatApi.CurrentCulture);
			}
		}

		public ChatApi()
		{
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static IChatRule AddBadWord(string exp, string mode)
		{
			ChatPortal chatPortal = null;
			IChatRule chatRule;
			if (ClusterSupport.IsClusterClient)
			{
				return (IChatRule)ClusterSupport.ExecuteCurrentMethod(new object[] { exp, mode });
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				IChatRule chatRule1 = chatPortal.DataManager.CreateRuleInstance();
				chatRule1.Category = "BadWord";
				chatRule1.Expression = exp;
				chatRule1.Mode = mode;
				chatPortal.DataManager.CreateRule(chatRule1);
				chatRule = chatRule1;
			}
			return chatRule;
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static IChatRule AddIPRule(string exp, int sort, string mode)
		{
			ChatPortal chatPortal = null;
			IChatRule chatRule;
			if (ClusterSupport.IsClusterClient)
			{
				return (IChatRule)ClusterSupport.ExecuteCurrentMethod(new object[] { exp, sort, mode });
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				IChatRule chatRule1 = chatPortal.DataManager.CreateRuleInstance();
				chatRule1.Category = "IPRule";
				chatRule1.Expression = exp;
				chatRule1.Mode = mode;
				chatPortal.DataManager.CreateRule(chatRule1);
				chatRule = chatRule1;
			}
			return chatRule;
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		[Obsolete("This method is not compatible with NLB mode", false)]
		public static void CreateLobby(IChatLobby lobby)
		{
			ChatPortal chatPortal = null;
			if (ClusterSupport.IsClusterClient)
			{
				ClusterSupport.ExecuteCurrentMethod(new object[] { lobby });
				return;
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				chatPortal.DataManager.CreateLobby(lobby);
			}
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static IChatLobby CreateLobbyInstance()
		{
			ChatPortal chatPortal = null;
			IChatLobby chatLobby;
			if (ClusterSupport.IsClusterClient)
			{
				return (IChatLobby)ClusterSupport.ExecuteCurrentMethod(new object[0]);
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				chatLobby = chatPortal.DataManager.CreateLobbyInstance();
			}
			return chatLobby;
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static void DeleteMessagesBeforeDays(int days)
		{
			ChatPortal chatPortal = null;
			if (ClusterSupport.IsClusterClient)
			{
				ClusterSupport.ExecuteCurrentMethod(new object[] { days });
				return;
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				chatPortal.DataManager.DeleteMessagesBeforeDays(days);
			}
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static void ExecuteOpenContact(ChatIdentity identity, string contact)
		{
			ChatPortal chatPortal = null;
			if (ClusterSupport.IsClusterClient)
			{
				ClusterSupport.ExecuteCurrentMethod(new object[] { identity, contact });
				return;
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				((ChatMessenger)chatPortal.GetPlace("Messenger")).SendOpenContact(identity, contact);
			}
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static string ExecutePartialMessenger(ChatIdentity identity)
		{
			ChatPortal chatPortal = null;
			string str;
			if (ClusterSupport.IsClusterClient)
			{
				return (string)ClusterSupport.ExecuteCurrentMethod(new object[] { identity });
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				str = ((ChatMessenger)chatPortal.GetPlace("Messenger")).HandlePartial(identity);
			}
			return str;
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static IChatRule GetBadWord(int ruleid)
		{
			ChatPortal chatPortal = null;
			IChatRule rule;
			if (ClusterSupport.IsClusterClient)
			{
				return (IChatRule)ClusterSupport.ExecuteCurrentMethod(new object[] { ruleid });
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				rule = chatPortal.DataManager.GetRule(ruleid);
			}
			return rule;
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static IChatRule[] GetBadWords()
		{
			if (!ClusterSupport.IsClusterClient)
			{
				return ChatApi.GetRules("BadWord");
			}
			return (IChatRule[])ClusterSupport.ExecuteCurrentMethod(new object[0]);
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static IChatUserInfo GetChatUserInfo(string uniqueid)
		{
			ChatPortal chatPortal = null;
			IChatUserInfo userInfo;
			if (ClusterSupport.IsClusterClient)
			{
				return (IChatUserInfo)ClusterSupport.ExecuteCurrentMethod(new object[] { uniqueid });
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				userInfo = chatPortal.DataManager.GetUserInfo(uniqueid);
			}
			return userInfo;
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static string GetConfig(string name)
		{
			ChatPortal chatPortal = null;
			string property;
			if (ClusterSupport.IsClusterClient)
			{
				return (string)ClusterSupport.ExecuteCurrentMethod(new object[] { name });
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				property = chatPortal.GetProperty(name);
			}
			return property;
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static string GetCurrentPortalProperty(string propname)
		{
			ChatPortal chatPortal = null;
			string property;
			if (ClusterSupport.IsClusterClient)
			{
				return (string)ClusterSupport.ExecuteCurrentMethod(new object[] { propname });
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				property = chatPortal.GetProperty(propname);
			}
			return property;
		}

		public static string[] GetDepartments()
		{
			ChatPortal chatPortal = null;
			string[] strArrays;
			if (ClusterSupport.IsClusterClient)
			{
				return (string[])ClusterSupport.ExecuteCurrentMethod(new object[0]);
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				SupportDepartment[] departments = chatPortal.DataManager.GetDepartments();
				string[] name = new string[(int)departments.Length];
				for (int i = 0; i < (int)departments.Length; i++)
				{
					name[i] = departments[i].Name;
				}
				strArrays = name;
			}
			return strArrays;
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static IChatRule GetIPRule(int ruleid)
		{
			ChatPortal chatPortal = null;
			IChatRule rule;
			if (ClusterSupport.IsClusterClient)
			{
				return (IChatRule)ClusterSupport.ExecuteCurrentMethod(new object[] { ruleid });
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				rule = chatPortal.DataManager.GetRule(ruleid);
			}
			return rule;
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static IChatRule[] GetIPRules()
		{
			if (!ClusterSupport.IsClusterClient)
			{
				return ChatApi.GetRules("IPRule");
			}
			return (IChatRule[])ClusterSupport.ExecuteCurrentMethod(new object[0]);
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static IChatLobby GetLobby(int lobbyid)
		{
			ChatPortal chatPortal = null;
			IChatLobby lobby;
			if (ClusterSupport.IsClusterClient)
			{
				return (IChatLobby)ClusterSupport.ExecuteCurrentMethod(new object[] { lobbyid });
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				lobby = chatPortal.DataManager.GetLobby(lobbyid);
			}
			return lobby;
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static IChatLobby[] GetLobbyArray()
		{
			ChatPortal chatPortal = null;
			IChatLobby[] lobbies;
			if (ClusterSupport.IsClusterClient)
			{
				return (IChatLobby[])ClusterSupport.ExecuteCurrentMethod(new object[0]);
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				lobbies = chatPortal.DataManager.GetLobbies();
			}
			return lobbies;
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static LobbyInfo[] GetLobbyInfoArray()
		{
			IChatLobby[] lobbies;
			ChatPortal chatPortal = null;
			if (ClusterSupport.IsClusterClient)
			{
				return (LobbyInfo[])ClusterSupport.ExecuteCurrentMethod(new object[0]);
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				lobbies = chatPortal.DataManager.GetLobbies();
			}
			LobbyInfo[] lobbyInfo = new LobbyInfo[(int)lobbies.Length];
			for (int i = 0; i < (int)lobbies.Length; i++)
			{
				lobbyInfo[i] = new LobbyInfo(lobbies[i]);
				lobbyInfo[i].LoadPortalData(chatPortal);
			}
			return lobbyInfo;
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static ChatMsgData[] GetMessages(string location, string placename, int pagesize, int pageindex, out int totalcount)
		{
			ChatPortal chatPortal = null;
			ChatMsgData[] messages;
			if (!ClusterSupport.IsClusterClient)
			{
				lock (ChatSystem.Instance.GetCurrentPortal())
				{
					messages = chatPortal.DataManager.GetMessages(location, placename, pagesize, pageindex, out totalcount);
				}
				return messages;
			}
			totalcount = 0;
			return (ChatMsgData[])ClusterSupport.ExecuteCurrentMethod(new object[] { location, placename, pagesize, pageindex, totalcount });
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static IChatRule[] GetRules(string category)
		{
			ChatPortal chatPortal = null;
			IChatRule[] rules;
			if (ClusterSupport.IsClusterClient)
			{
				return (IChatRule[])ClusterSupport.ExecuteCurrentMethod(new object[] { category });
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				rules = chatPortal.DataManager.GetRules(category);
			}
			return rules;
		}

		public static CuteChat.Strings GetStrings(string culture)
		{
			return ChatSystem.Instance.GetStrings(culture);
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static string GetUserDisplayName(string userid)
		{
			ChatPortal chatPortal = null;
			string displayName;
			if (ClusterSupport.IsClusterClient)
			{
				return (string)ClusterSupport.ExecuteCurrentMethod(new object[] { userid });
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				IChatUserInfo userInfo = chatPortal.DataManager.GetUserInfo(userid);
				if (userInfo != null)
				{
					displayName = userInfo.DisplayName;
				}
				else
				{
					displayName = null;
				}
			}
			return displayName;
		}

		public static bool HasOnlineAgents()
		{
			ChatPortal chatPortal = null;
			bool flag;
			if (ClusterSupport.IsClusterClient)
			{
				return (bool)ClusterSupport.ExecuteCurrentMethod(new object[0]);
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				if (chatPortal.IsPlaceStarted("SupportAgent"))
				{
					ChatPlace place = chatPortal.GetPlace("SupportAgent");
					if (place.GetConnectionCount() != 0)
					{
						ChatPlaceUser[] allUsers = place.GetAllUsers();
						int num = 0;
						while (num < (int)allUsers.Length)
						{
							if (allUsers[num].AppearOffline)
							{
								num++;
							}
							else
							{
								flag = true;
								return flag;
							}
						}
						return false;
					}
					else
					{
						flag = false;
					}
				}
				else
				{
					flag = false;
				}
			}
			return flag;
		}

		public static bool HasOnlineDepartmentAgents(string department)
		{
			ChatPortal chatPortal = null;
			bool flag;
			if (ClusterSupport.IsClusterClient)
			{
				return (bool)ClusterSupport.ExecuteCurrentMethod(new object[] { department });
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				if (chatPortal.IsPlaceStarted("SupportAgent"))
				{
					ChatPlace place = chatPortal.GetPlace("SupportAgent");
					if (place.GetConnectionCount() != 0)
					{
						SupportDepartment supportDepartment = null;
						SupportDepartment[] departments = chatPortal.DataManager.GetDepartments();
						int i = 0;
						while (i < (int)departments.Length)
						{
							SupportDepartment supportDepartment1 = departments[i];
							if (string.Compare(supportDepartment1.Name, department, true) != 0)
							{
								i++;
							}
							else
							{
								supportDepartment = supportDepartment1;
								break;
							}
						}
						if (supportDepartment != null)
						{
							ChatPlaceUser[] allUsers = place.GetAllUsers();
							for (i = 0; i < (int)allUsers.Length; i++)
							{
								ChatPlaceUser chatPlaceUser = allUsers[i];
								if (!chatPlaceUser.AppearOffline)
								{
									SupportAgent[] agents = supportDepartment.Agents;
									int num = 0;
									while (num < (int)agents.Length)
									{
										if (string.Compare(agents[num].UserId, chatPlaceUser.Identity.UniqueId, true) != 0)
										{
											num++;
										}
										else
										{
											flag = true;
											return flag;
										}
									}
								}
							}
							return false;
						}
						else
						{
							flag = false;
						}
					}
					else
					{
						flag = false;
					}
				}
				else
				{
					flag = false;
				}
			}
			return flag;
		}

		public static bool HasReadyAgents()
		{
			ChatPortal chatPortal = null;
			bool flag;
			if (ClusterSupport.IsClusterClient)
			{
				return (bool)ClusterSupport.ExecuteCurrentMethod(new object[0]);
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				if (chatPortal.IsPlaceStarted("SupportAgent"))
				{
					ChatPlace place = chatPortal.GetPlace("SupportAgent");
					if (place.GetConnectionCount() != 0)
					{
						ChatPlaceUser[] allUsers = place.GetAllUsers();
						int num = 0;
						while (num < (int)allUsers.Length)
						{
							ChatPlaceUser chatPlaceUser = allUsers[num];
							if (chatPlaceUser.AppearOffline || !(chatPlaceUser.OnlineStatus == "ONLINE"))
							{
								num++;
							}
							else
							{
								flag = true;
								return flag;
							}
						}
						return false;
					}
					else
					{
						flag = false;
					}
				}
				else
				{
					flag = false;
				}
			}
			return flag;
		}

		public static bool HasReadyDepartmentAgents(string department)
		{
			ChatPortal chatPortal = null;
			bool flag;
			if (ClusterSupport.IsClusterClient)
			{
				return (bool)ClusterSupport.ExecuteCurrentMethod(new object[] { department });
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				if (chatPortal.IsPlaceStarted("SupportAgent"))
				{
					ChatPlace place = chatPortal.GetPlace("SupportAgent");
					if (place.GetConnectionCount() != 0)
					{
						SupportDepartment supportDepartment = null;
						SupportDepartment[] departments = chatPortal.DataManager.GetDepartments();
						int i = 0;
						while (i < (int)departments.Length)
						{
							SupportDepartment supportDepartment1 = departments[i];
							if (string.Compare(supportDepartment1.Name, department, true) != 0)
							{
								i++;
							}
							else
							{
								supportDepartment = supportDepartment1;
								break;
							}
						}
						if (supportDepartment != null)
						{
							ChatPlaceUser[] allUsers = place.GetAllUsers();
							for (i = 0; i < (int)allUsers.Length; i++)
							{
								ChatPlaceUser chatPlaceUser = allUsers[i];
								if (chatPlaceUser.OnlineStatus == "ONLINE")
								{
									SupportAgent[] agents = supportDepartment.Agents;
									int num = 0;
									while (num < (int)agents.Length)
									{
										if (string.Compare(agents[num].UserId, chatPlaceUser.Identity.UniqueId, true) != 0)
										{
											num++;
										}
										else
										{
											flag = true;
											return flag;
										}
									}
								}
							}
							return false;
						}
						else
						{
							flag = false;
						}
					}
					else
					{
						flag = false;
					}
				}
				else
				{
					flag = false;
				}
			}
			return flag;
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static Hashtable LoadConfigs()
		{
			ChatPortal chatPortal = null;
			Hashtable hashtables;
			if (ClusterSupport.IsClusterClient)
			{
				return (Hashtable)ClusterSupport.ExecuteCurrentMethod(new object[0]);
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				hashtables = chatPortal.LoadProperties();
			}
			return hashtables;
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static bool LSIsSessionActive(string placename)
		{
			ChatPortal chatPortal = null;
			bool flag;
			if (ClusterSupport.IsClusterClient)
			{
				return (bool)ClusterSupport.ExecuteCurrentMethod(new object[] { placename });
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				if (chatPortal.IsPlaceStarted(placename))
				{
					ChatPlaceUser[] allUsers = (chatPortal.GetPlace(placename) as SupportSessionChannel).GetAllUsers();
					int num = 0;
					while (num < (int)allUsers.Length)
					{
						ChatPlaceUser chatPlaceUser = allUsers[num];
						if (!chatPortal.DataManager.IsAgent(chatPlaceUser.Identity.UniqueId))
						{
							num++;
						}
						else
						{
							flag = true;
							return flag;
						}
					}
					return false;
				}
				else
				{
					flag = false;
				}
			}
			return flag;
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static byte[] NLBTestData(byte[] data)
		{
			if (!ClusterSupport.IsClusterClient)
			{
				return data;
			}
			return (byte[])ClusterSupport.ExecuteCurrentMethod(new object[] { data });
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static void RemoveBadWord(int ruleid)
		{
			ChatPortal chatPortal = null;
			if (ClusterSupport.IsClusterClient)
			{
				ClusterSupport.ExecuteCurrentMethod(new object[] { ruleid });
				return;
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				IChatRule rule = chatPortal.DataManager.GetRule(ruleid);
				if (rule != null)
				{
					chatPortal.DataManager.DeleteRule(rule);
				}
			}
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static void RemoveIPRule(int ruleid)
		{
			ChatPortal chatPortal = null;
			if (ClusterSupport.IsClusterClient)
			{
				ClusterSupport.ExecuteCurrentMethod(new object[] { ruleid });
				return;
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				IChatRule rule = chatPortal.DataManager.GetRule(ruleid);
				if (rule != null)
				{
					chatPortal.DataManager.DeleteRule(rule);
				}
			}
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static void RemoveLobby(int lobbyid)
		{
			ChatPortal chatPortal = null;
			if (ClusterSupport.IsClusterClient)
			{
				ClusterSupport.ExecuteCurrentMethod(new object[] { lobbyid });
				return;
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				IChatLobby lobby = chatPortal.DataManager.GetLobby(lobbyid);
				if (lobby != null)
				{
					chatPortal.DataManager.DeleteLobby(lobby);
				}
			}
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static void SetConfig(string name, string value)
		{
			ChatPortal chatPortal = null;
			if (ClusterSupport.IsClusterClient)
			{
				ClusterSupport.ExecuteCurrentMethod(new object[] { name, value });
				return;
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				chatPortal.SetProperty(name, value);
			}
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static void UpdateBadWord(IChatRule rule)
		{
			ChatPortal chatPortal = null;
			if (ClusterSupport.IsClusterClient)
			{
				ClusterSupport.ExecuteCurrentMethod(new object[] { rule });
				return;
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				chatPortal.DataManager.UpdateRule(rule);
			}
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static void UpdateIPRule(IChatRule rule)
		{
			ChatPortal chatPortal = null;
			if (ClusterSupport.IsClusterClient)
			{
				ClusterSupport.ExecuteCurrentMethod(new object[] { rule });
				return;
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				chatPortal.DataManager.UpdateRule(rule);
			}
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		[Obsolete("This method is not compatible with NLB mode", false)]
		public static void UpdateLobby(IChatLobby lobby)
		{
			ChatPortal chatPortal = null;
			if (ClusterSupport.IsClusterClient)
			{
				ClusterSupport.ExecuteCurrentMethod(new object[] { lobby });
				return;
			}
			lock (ChatSystem.Instance.GetCurrentPortal())
			{
				chatPortal.DataManager.UpdateLobby(lobby);
			}
		}
	}
}