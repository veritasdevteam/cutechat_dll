using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Globalization;
using System.Runtime.Serialization;

namespace CuteChat
{
	[Serializable]
	public class StringsContainer : Strings, ISerializable
	{
		private string _culture;

		private NameValueCollection _nvc;

		public StringsContainer(string culture, DataTable table)
		{
			this._culture = culture;
			this._nvc = new NameValueCollection();
			CultureInfo cultureInfo = new CultureInfo(culture);
			CultureInfo parent = cultureInfo.Parent;
			DataColumn item = table.Columns["culture"];
			DataColumn dataColumn = table.Columns["name"];
			DataColumn item1 = table.Columns["value"];
			foreach (DataRow row in table.Rows)
			{
				if ((string)row[item] != "")
				{
					continue;
				}
				this._nvc[(string)row[dataColumn]] = (string)row["value"];
			}
			string str = cultureInfo.ToString();
			string str1 = parent.ToString();
			if (string.Compare(str, "en-us", true) != 0)
			{
				foreach (DataRow dataRow in table.Rows)
				{
					string item2 = (string)dataRow[item];
					if (string.Compare(item2, str, true) != 0)
					{
						if (string.Compare(item2, str1, true) != 0)
						{
							continue;
						}
						this._nvc[(string)dataRow[dataColumn]] = (string)dataRow["value"];
					}
					else
					{
						this._nvc[(string)dataRow[dataColumn]] = (string)dataRow["value"];
					}
				}
			}
			base.InitializeProperties();
		}

		protected StringsContainer(SerializationInfo info, StreamingContext context)
		{
			this._culture = info.GetString("_culture");
			this._nvc = new NameValueCollection();
			int num = info.GetInt32("_count");
			for (int i = 0; i < num; i++)
			{
				string str = info.GetString(string.Concat("k", i));
				string str1 = info.GetString(string.Concat("v", i));
				this._nvc[str] = str1;
			}
			base.InitializeProperties();
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_culture", this._culture);
			info.AddValue("_count", this._nvc.Count);
			int num = 0;
			foreach (string key in this._nvc.Keys)
			{
				info.AddValue(string.Concat("k", num), key);
				info.AddValue(string.Concat("v", num), this._nvc[key]);
				num++;
			}
		}

		public override string GetString(string name)
		{
			string stringOrNull = this.GetStringOrNull(name);
			if (stringOrNull != null)
			{
				return stringOrNull;
			}
			return string.Concat("[", name, "]");
		}

		public override string GetStringOrNull(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			return this._nvc[name];
		}

		public override NameValueCollection GetValues()
		{
			return this._nvc;
		}
	}
}