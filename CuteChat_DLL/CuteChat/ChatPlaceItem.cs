using System;
using System.Collections;
using System.Collections.Specialized;
using System.Reflection;
using System.Xml;

namespace CuteChat
{
	public abstract class ChatPlaceItem : ChatPortalDataItem
	{
		private ChatPlace @ӓ;

		private NameValueCollection @Ӕ;

		protected NameValueCollection infoProps;

		private Hashtable @ӕ = new Hashtable();

		public ChatPlace Place
		{
			get
			{
				return this.@ӓ;
			}
		}

		public ChatPlaceItem(ChatPlace place) : base(place.Portal)
		{
			this.@ӓ = place;
		}

		private void @Ӗ(ChatConnection u0040ӓ, bool u0040Ӕ)
		{
			if (this.IsVisibleConnection(u0040ӓ))
			{
				if (!this.@ӕ.Contains(u0040ӓ.ObjectGuid))
				{
					this.@ӕ.Add(u0040ӓ.ObjectGuid, u0040ӓ);
					this.OnShowForConnection(u0040ӓ);
					return;
				}
				if (u0040Ӕ)
				{
					this.OnUpdateForConnection(u0040ӓ);
					return;
				}
			}
			else if (this.@ӕ.Contains(u0040ӓ.ObjectGuid))
			{
				this.@ӕ.Remove(u0040ӓ.ObjectGuid);
				this.OnHideForConnection(u0040ӓ);
			}
		}

		public virtual void Dump(XmlWriter writer)
		{
		}

		public string GetInfoProperty(string name)
		{
			if (this.infoProps == null)
			{
				return null;
			}
			return this.infoProps[name];
		}

		protected virtual bool GetItemInfo(ChatConnection conn, string formsg, out string type, out string[] args)
		{
			type = null;
			args = null;
			return false;
		}

		public ChatConnection[] GetRelativeConnections()
		{
			ChatConnection[] chatConnectionArray = new ChatConnection[this.@ӕ.Count];
			this.@ӕ.Values.CopyTo(chatConnectionArray, 0);
			return chatConnectionArray;
		}

		public string GetRuntimeProperty(string name)
		{
			if (this.@Ӕ == null)
			{
				return null;
			}
			return this.@Ӕ[name];
		}

		public abstract bool IsVisibleConnection(ChatConnection conn);

		protected internal virtual void Maintain()
		{
		}

		protected internal virtual void OnAdded()
		{
			ArrayList arrayLists = new ArrayList();
			ChatConnection[] allConnections = this.Place.GetAllConnections();
			for (int i = 0; i < (int)allConnections.Length; i++)
			{
				ChatConnection chatConnection = allConnections[i];
				if (this.IsVisibleConnection(chatConnection))
				{
					arrayLists.Add(chatConnection);
				}
			}
			foreach (ChatConnection arrayList in arrayLists)
			{
				this.@ӕ.Add(arrayList.ObjectGuid, arrayList);
				this.OnShowForConnection(arrayList);
			}
		}

		protected internal virtual void OnConnectionAdded(ChatConnection conn)
		{
			if (!this.IsVisibleConnection(conn))
			{
				return;
			}
			this.@ӕ.Add(conn.ObjectGuid, conn);
			this.OnShowForConnection(conn);
		}

		protected internal virtual void OnConnectionRemoved(ChatConnection conn)
		{
			this.@ӕ.Remove(conn.ObjectGuid);
		}

		protected internal virtual void OnConnectionUpdated(ChatConnection conn)
		{
			this.@Ӗ(conn, false);
		}

		protected virtual void OnHideForConnection(ChatConnection conn)
		{
			this.RenderItemInfo(conn, "ITEM_REMOVED");
		}

		protected internal virtual void OnRemoved(string reason)
		{
			foreach (ChatConnection value in this.@ӕ.Values)
			{
				this.OnRemoveForConnection(value, reason);
			}
			this.@ӕ.Clear();
		}

		protected virtual void OnRemoveForConnection(ChatConnection conn, string reason)
		{
			this.RenderItemInfo(conn, "ITEM_REMOVED");
		}

		protected virtual void OnShowForConnection(ChatConnection conn)
		{
			this.RenderItemInfo(conn, "ITEM_ADDED");
		}

		protected internal virtual void OnUpdated()
		{
			ChatConnection[] allConnections = this.Place.GetAllConnections();
			for (int i = 0; i < (int)allConnections.Length; i++)
			{
				this.@Ӗ(allConnections[i], true);
			}
		}

		protected virtual void OnUpdateForConnection(ChatConnection conn)
		{
			this.RenderItemInfo(conn, "ITEM_UPDATED");
		}

		public void PushSTCMessageToRelativeConnections(string msgid, NameValueCollection nvc, params string[] args)
		{
			this.PushSTCMessageToRelativeConnections(ChatUtility.JoinToMsg(msgid, nvc, args));
		}

		public virtual void PushSTCMessageToRelativeConnections(string message)
		{
			foreach (ChatConnection value in this.@ӕ.Values)
			{
				this.Place.Manager.PushSTCMessage(value, message);
			}
		}

		protected virtual void RenderItemInfo(ChatConnection conn, string msgid)
		{
			string str;
			string[] strArrays;
			string[] objectGuid;
			NameValueCollection nameValueCollection = this.infoProps;
			if (!this.GetItemInfo(conn, msgid, out str, out strArrays))
			{
				return;
			}
			if (strArrays != null)
			{
				objectGuid = new string[2 + (int)strArrays.Length];
				strArrays.CopyTo(objectGuid, 2);
			}
			else
			{
				objectGuid = new string[2];
			}
			objectGuid[0] = str;
			objectGuid[1] = base.ObjectGuid;
			this.Place.Manager.PushSTCMessage(conn, msgid, nameValueCollection, objectGuid);
		}

		public void SetInfoProperty(string name, string value)
		{
			if (this.infoProps == null)
			{
				this.infoProps = new NameValueCollection();
			}
			this.infoProps[name] = value;
		}

		public void SetRuntimeProperty(string name, string val)
		{
			if (this.@Ӕ == null)
			{
				this.@Ӕ = new NameValueCollection();
			}
			this.@Ӕ[name] = val;
		}

		public override string ToString()
		{
			string str;
			string[] strArrays;
			string str1 = string.Concat(base.GetType().Name, ":", base.ObjectGuid);
			if (this.GetItemInfo(null, "ToString", out str, out strArrays))
			{
				str1 = string.Concat(str1, ":", ChatUtility.JoinToMsg(str, this.infoProps, strArrays));
			}
			return str1;
		}
	}
}