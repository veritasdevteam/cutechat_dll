using System;
using System.Collections;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Xml;

namespace CuteChat.Configuration
{
	public class Config
	{
		private bool @ӓ = true;

		private Regex[] @Ӕ = new Regex[0];

		public static string ConfigFileName
		{
			get
			{
				return "EasyHttpZipModule.config";
			}
		}

		public static string ConfigSectionName
		{
			get
			{
				return "EasyHttpZipModule";
			}
		}

		public bool Enable
		{
			get
			{
				return this.@ӓ;
			}
		}

		public Regex[] UrlPatterns
		{
			get
			{
				return this.@Ӕ;
			}
		}

		public Config(Config parent, XmlNode section)
		{
			if (parent != null)
			{
				this.@ӕ(parent);
			}
			if (section != null)
			{
				this.@Ӗ((XmlElement)section);
			}
		}

		private void @Ӗ(XmlElement u0040ӓ)
		{
			u0040ӓ.GetAttribute("enable");
			XmlNodeList xmlNodeLists = u0040ӓ.SelectNodes("Url");
			if (xmlNodeLists.Count != 0)
			{
				ArrayList arrayLists = new ArrayList(this.@Ӕ);
				foreach (XmlElement xmlElement in xmlNodeLists)
				{
					string attribute = xmlElement.GetAttribute("pattern");
					if (attribute == "")
					{
						throw new ConfigurationException("Missing attribute @pattern", xmlElement);
					}
					arrayLists.Add(new Regex(attribute, RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Singleline));
				}
				this.@Ӕ = (Regex[])arrayLists.ToArray(typeof(Regex));
			}
		}

		private void @ӕ(Config u0040ӓ)
		{
			this.@ӓ = u0040ӓ.@ӓ;
			this.@Ӕ = (Regex[])u0040ӓ.@Ӕ.Clone();
		}

		public static Config GetConfig()
		{
			return (Config)ConfigurationSettings.GetConfig("EasyHttpZipModule");
		}
	}
}