using System;
using System.Collections;

namespace CuteChat
{
	public interface IChatPortalInfo
	{
		string PortalName
		{
			get;
		}

		string GetProperty(string name);

		Hashtable LoadProperties();

		void SetProperty(string name, string value);
	}
}