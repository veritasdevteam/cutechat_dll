using System;
using System.Runtime.Serialization;

namespace CuteChat
{
	[Serializable]
	public class ChatLicenseException : Exception, ISerializable
	{
		private EnumChatLicense _lt;

		public EnumChatLicense LicenseType
		{
			get
			{
				return this._lt;
			}
		}

		public ChatLicenseException(EnumChatLicense licenseType, Exception innerException)
		{
			object[] objArray = new object[] { "Check ", licenseType, " License Failed", null };
			objArray[3] = (innerException == null ? "" : string.Concat(" : ", innerException.Message));
			//base(string.Concat(objArray));
			this._lt = licenseType;
		}

		protected ChatLicenseException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this._lt = (EnumChatLicense)info.GetValue("LicenseType", typeof(EnumChatLicense));
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("LicenseType", this._lt);
		}
	}
}