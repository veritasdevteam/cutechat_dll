using System;

namespace CuteChat
{
	public interface IChatDataProvider : IDisposable
	{
		void AddAgent(int departmentid, string userid);

		void AddDepartment(string name);

		void AddFeedback(SupportFeedback feedback);

		void CreateLobby(IChatLobby lobby);

		void CreateRule(IChatRule rule);

		void CreateSession(SupportSession session);

		void DeleteAgentSessions(DateTime start, DateTime endtime, string agentid, string departmentid, string customer, string email, string ipaddr, string culture);

		void DeleteFeedback(int feedbackid);

		void DeleteInstantMessages(string ownerid, string targetid);

		void DeleteLobby(IChatLobby lobby);

		void DeleteMessagesBeforeDays(int days);

		void DeleteRule(IChatRule rule);

		string GetCustomerData(string userid);

		int GetCustomerSessionCount(string userid);

		SupportSession GetLastSession(string customerid);

		IChatLobby[] GetLobbies();

		IChatLobby GetLobby(int lobbyid);

		ChatMsgData[] GetMessages(string location, string placename, int pagesize, int pageindex, out int totalcount);

		IChatPortalInfo GetPortalInfo(string portalname);

		IChatRule GetRule(int ruleid);

		IChatRule[] GetRules();

		IChatUserInfo GetUserInfo(string userid);

		SupportSession[] LoadAgentSessions(string agentid);

		ChatMsgData[] LoadChannelHistoryMessages(string channelname, int maxcount, DateTime afterdate);

		SupportSession[] LoadCustomerSessions(string customerid);

		SupportDepartment[] LoadDepartments();

		SupportFeedback LoadFeedback(int feedbackid);

		SupportFeedback[] LoadFeedbacks(bool history);

		ChatMsgData[] LoadInstantMessages(string ownerid, string targetid, bool offlineOnly, int maxid, int pagesize);

		ChatMsgData[] LoadInstantOfflineMessages(string targetid, DateTime time);

		ChatMsgData[] LoadSessionMessages(int sessionid);

		string LoadSetting(string name);

		void LogEvent(ChatPlace place, ChatIdentity user, string category, string message);

		void LogInstantMessage(ChatIdentity sender, string targetid, string targetname, bool offline, string text, string html);

		void LogMessage(ChatPlace place, ChatIdentity sender, ChatIdentity target, bool whisper, string text, string html);

		void LogSupportMessage(ChatPlace place, SupportSession session, string type, ChatIdentity sender, ChatIdentity target, string text, string html);

		void RemoveAgent(int departmentid, string userid);

		void RemoveDepartment(int departmentid);

		void RenameDepartment(int departmentid, string newname);

		void SaveSetting(string name, string data);

		SupportSession[] SearchAgentSessions(DateTime start, DateTime endtime, string agentid, string departmentid, string customer, string email, string ipaddr, string culture);

		SupportSession SelectSession(string sessionid);

		void SetCustomerData(string userid, string value);

		void UpdateFeedback(SupportFeedback feedback);

		void UpdateLobby(IChatLobby lobby);

		void UpdatePortalInfo(IChatPortalInfo info);

		void UpdateRule(IChatRule rule);

		void UpdateSession(SupportSession session);

		void UpdateUserInfo(IChatUserInfo info);
	}
}