using System;

namespace CuteChat
{
	public class SupportGoSessionItem : ChatPlaceItem
	{
		private ChatIdentity @ӓ;

		private ChatPlaceUser @Ӕ;

		private ChatPlaceUser @ӕ;

		private bool @Ӗ;

		public ChatPlaceUser Agent
		{
			get
			{
				return this.@ӕ;
			}
		}

		public ChatIdentity CustomerId
		{
			get
			{
				return this.@ӓ;
			}
		}

		public ChatPlaceUser Sender
		{
			get
			{
				return this.@Ӕ;
			}
		}

		public bool Transfer
		{
			get
			{
				return this.@Ӗ;
			}
		}

		public SupportGoSessionItem(ChatPlace place, ChatPlaceUser sender, ChatIdentity customerid, ChatPlaceUser agent, bool transfer) : base(place)
		{
			this.@ӓ = customerid;
			this.@Ӕ = sender;
			this.@ӕ = agent;
			this.@Ӗ = transfer;
		}

		protected override bool GetItemInfo(ChatConnection conn, string formsg, out string type, out string[] args)
		{
			type = "GOSESSION";
			string[] uniqueId = new string[] { this.@ӓ.UniqueId, this.@ӓ.DisplayName, this.@ӕ.Identity.UniqueId, this.@ӕ.DisplayName, null, null };
			uniqueId[4] = (this.@Ӗ ? "1" : "0");
			uniqueId[5] = this.@Ӕ.DisplayName;
			args = uniqueId;
			return true;
		}

		public override bool IsVisibleConnection(ChatConnection conn)
		{
			if (conn.Identity == this.@Ӕ.Identity)
			{
				return true;
			}
			if (conn.Identity == this.@ӕ.Identity)
			{
				return true;
			}
			return false;
		}

		protected internal override void OnConnectionRemoved(ChatConnection conn)
		{
			base.OnConnectionRemoved(conn);
			if (conn.Identity == this.@ӕ.Identity)
			{
				base.Place.Manager.RemovePlaceItem(this, "EXPIRED");
			}
		}
	}
}