using System;
using System.Text;

namespace CuteChat
{
	public class IPChecker
	{
		private static @Ժ @ӓ;

		static IPChecker()
		{
			IPChecker.@ӓ = new @Ժ(TimeSpan.FromMinutes(20), true);
		}

		public IPChecker()
		{
		}

		private static int @ӗ(string u0040ӓ)
		{
			int num = int.Parse(u0040ӓ);
			if (num < 0)
			{
				throw new Exception(string.Concat("v<0? =", num));
			}
			if (num > 255)
			{
				throw new ArgumentException(string.Concat("value of IP part is too large:", num));
			}
			return num;
		}

		private static string @Ӗ(string u0040ӓ, out bool u0040Ӕ)
		{
			string[] strArrays = u0040ӓ.Split(".".ToCharArray());
			if ((int)strArrays.Length > 4)
			{
				throw new ArgumentException("to many '.'");
			}
			if (u0040ӓ.EndsWith("."))
			{
				u0040ӓ = u0040ӓ.Substring(0, u0040ӓ.Length - 1);
			}
			if (u0040ӓ.EndsWith(".*"))
			{
				u0040ӓ = u0040ӓ.Substring(0, u0040ӓ.Length - 2);
			}
			if (u0040ӓ.IndexOf('*') != -1)
			{
				throw new ArgumentException("invalid usage for '*'");
			}
			strArrays = u0040ӓ.Split(".".ToCharArray());
			u0040Ӕ = (int)strArrays.Length == 4;
			string[] strArrays1 = strArrays;
			for (int i = 0; i < (int)strArrays1.Length; i++)
			{
				IPChecker.@ӗ(strArrays1[i]);
			}
			return u0040ӓ;
		}

		private static long @ә(string u0040ӓ)
		{
			long num;
			try
			{
				string[] strArrays = u0040ӓ.Split(".".ToCharArray());
				if ((int)strArrays.Length != 4)
				{
					throw new Exception(string.Concat("ip is not 4 parts : ", u0040ӓ));
				}
				num = (long)(IPChecker.@ӗ(strArrays[0]) * 16777216 + IPChecker.@ӗ(strArrays[1]) * 65536 + IPChecker.@ӗ(strArrays[2]) * 256 + IPChecker.@ӗ(strArrays[3]));
			}
			catch (Exception exception)
			{
				throw new ArgumentException(string.Concat("ip?", u0040ӓ), "ipaddr", exception);
			}
			return num;
		}

		private static long @Ә(string u0040ӓ, bool u0040Ӕ)
		{
			if (u0040ӓ.Length == 0)
			{
				if (u0040Ӕ)
				{
					return 4294967296L;
				}
				return (long)0;
			}
			string[] strArrays = u0040ӓ.Split(".".ToCharArray());
			long num = (long)(IPChecker.@ӗ(strArrays[0]) * 16777216);
			if ((int)strArrays.Length > 1)
			{
				num += (long)(IPChecker.@ӗ(strArrays[1]) * 65536);
			}
			else if (u0040Ӕ)
			{
				num += (long)16777216;
			}
			if ((int)strArrays.Length > 2)
			{
				num += (long)(IPChecker.@ӗ(strArrays[2]) * 256);
			}
			else if (u0040Ӕ)
			{
				num += (long)65536;
			}
			if ((int)strArrays.Length > 3)
			{
				num += (long)IPChecker.@ӗ(strArrays[3]);
			}
			else if (u0040Ӕ)
			{
				num += (long)256;
			}
			return num;
		}

		private static string @ӕ(string u0040ӓ)
		{
			bool flag;
			return IPChecker.@Ӗ(u0040ӓ, out flag);
		}

		private static bool @Ӕ(string u0040ӓ, string u0040Ӕ, bool u0040ӕ)
		{
			long num;
			int i;
			bool flag = false;
			bool flag1;
			if (u0040Ӕ == null)
			{
				throw new ArgumentNullException("ip");
			}
			if (u0040ӓ == null)
			{
				throw new ArgumentNullException("iplist");
			}
			if (u0040ӓ == "*")
			{
				return true;
			}
			if (u0040ӓ == "")
			{
				return false;
			}
			StringBuilder stringBuilder = new StringBuilder(u0040ӓ);
			stringBuilder.Replace(" ", "").Replace("\r", "").Replace("\n", "").Replace("\t", "");
			u0040ӓ = stringBuilder.ToString();
			if (u0040ӓ.Length == 0)
			{
				return false;
			}
			if (u0040ӓ == "*")
			{
				return true;
			}
			string str = u0040ӓ;
			for (i = 0; i < str.Length; i++)
			{
				char chr = str[i];
				if ("*1234567890|,.-!".IndexOf(chr) == -1)
				{
					throw new ArgumentException(string.Concat("unexprected char '", chr.ToString(), "'"), "iplist");
				}
			}
			string[] strArrays = u0040ӓ.Split("|,".ToCharArray());
			try
			{
				num = IPChecker.@ә(u0040Ӕ);
			}
			catch
			{
				throw new Exception("this IP is not invalid");
			}
			string[] strArrays1 = strArrays;
			for (i = 0; i < (int)strArrays1.Length; i++)
			{
				string str1 = strArrays1[i];
				if (str1.Length != 0)
				{
					bool flag2 = false;
					if (str1.StartsWith("!"))
					{
						flag2 = true;
						str1 = str1.Remove(0, 1);
						if (str1.Length == 0)
						{
							throw new ArgumentException("nothing after '!'", "iplist");
						}
					}
					if (str1.IndexOf('!') != -1)
					{
						throw new ArgumentException("invalid usage for '!'", "iplist");
					}
					string[] strArrays2 = str1.Split("-".ToCharArray());
					if ((int)strArrays2.Length > 2)
					{
						throw new ArgumentException(string.Concat("the pattern ", str1, " contains one more '-'"), "iplist");
					}
					if ((int)strArrays2.Length != 1)
					{
						long num1 = IPChecker.@Ә(IPChecker.@ӕ(strArrays2[0]), false);
						long num2 = IPChecker.@Ә(IPChecker.@ӕ(strArrays2[1]), true);
						if (num1 >= num2)
						{
							throw new ArgumentException(string.Concat("in pattern ", str1, " the front must less than the after"), "iplist");
						}
						if (u0040ӕ)
						{
							//goto Label0;
						}
						flag = (num1 > num ? false : num <= num2);
					}
					else
					{
						string str2 = strArrays2[0];
						if (str2 != "*")
						{
							str2 = IPChecker.@Ӗ(str2, out flag1);
							if (u0040ӕ)
							{
							//	goto Label0;
							}
							flag = (!flag1 ? u0040Ӕ.IndexOf(string.Concat(str2, ".")) == 0 : str2 == u0040Ӕ);
						}
						else if (!u0040ӕ)
						{
							return !flag2;
						}
						else
						{
							//goto Label0;
						}
					}
					if (flag)
					{
						if (!flag2)
						{
							return true;
						}
					}
					else if (flag2)
					{
						return true;
					}
				}
			//Label0:
			}
			return false;
		}

		public static bool TestIP(string iplist, string ipaddr)
		{
			bool value;
			string str = string.Concat(ipaddr, ":", iplist);
			using (@Ժ.Iterator iterator = new @Ժ.Iterator(IPChecker.@ӓ, str))
			{
				if (iterator.IsNotSet)
				{
					iterator.Value = IPChecker.@Ӕ(iplist, ipaddr, false);
				}
				value = (bool)iterator.Value;
			}
			return value;
		}

		public static void TestIPList(string iplist)
		{
			IPChecker.@Ӕ(iplist, "0.0.0.0", true);
		}
	}
}