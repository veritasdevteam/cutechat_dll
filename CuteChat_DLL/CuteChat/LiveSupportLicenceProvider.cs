using System;
using System.ComponentModel;

namespace CuteChat
{
	public class LiveSupportLicenceProvider : LicenseProvider
	{
		public LiveSupportLicenceProvider()
		{
		}

		public override License GetLicense(LicenseContext context, Type type, object instance, bool allowExceptions)
		{
			return (new @Ԑ()
			{
				ProductType = EnumChatLicense.CuteLiveSupport
			}).GetLicense(context, type, instance, allowExceptions);
		}
	}
}