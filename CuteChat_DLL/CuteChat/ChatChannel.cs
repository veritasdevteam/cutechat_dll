using System;
using System.Collections.Specialized;

namespace CuteChat
{
	public abstract class ChatChannel : ChatPlace
	{
		public override int MaxOnlineCount
		{
			get
			{
				return 400;
			}
		}

		public ChatChannel(ChatPortal portal, string name) : base(portal, name)
		{
		}

		protected internal override void DispatchMessage(ChatConnection conn, NameValueCollection nvc, string[] args)
		{
			string str;
			string str1 = args[0];
			string str2 = args[1];
			string str3 = args[2];
			string str4 = args[3];
			bool flag = args[4] == "1";
			if (!base.CheckMessage(conn, str1, ref str2, out str))
			{
				base.Manager.PushSTCMessage(conn, "SYS_ERROR_MESSAGE", null, new string[] { str });
				return;
			}
			ChatPlaceUser chatPlaceUser = null;
			if (str3 != null && str3.Length != 0)
			{
				chatPlaceUser = base.FindUser(str3);
				if (chatPlaceUser == null || !chatPlaceUser.IsVisibleConnection(conn))
				{
					base.Manager.PushSTCMessage(conn, "SYS_ERROR_MESSAGE", null, new string[] { "INVALIDTARGET" });
					return;
				}
			}
			if (flag && chatPlaceUser == null)
			{
				base.Manager.PushSTCMessage(conn, "SYS_ERROR_MESSAGE", null, new string[] { "INVALIDTARGET" });
				return;
			}
			if (conn.PlaceUser.AppearOffline)
			{
				base.Manager.PushSTCMessage(conn, "SYS_ERROR_MESSAGE", null, new string[] { "You cannot send a message when your status is offline." });
				return;
			}
			ChatIdentity identity = null;
			if (chatPlaceUser != null)
			{
				identity = chatPlaceUser.Identity;
			}
			string str5 = "USER_MESSAGE";
			if (nvc != null && nvc["Emotion"] != null)
			{
				str5 = "USER_DOEMOTE";
			}
			string str6 = str5;
			NameValueCollection nameValueCollection = nvc;
			string[] objectGuid = new string[] { conn.PlaceUser.ObjectGuid, conn.PlaceUser.Identity.UniqueId, conn.PlaceUser.DisplayName, str1, str2, str3, str4, null, null };
			objectGuid[7] = (flag ? "1" : "0");
			DateTime universalTime = DateTime.Now.ToUniversalTime();
			objectGuid[8] = universalTime.Ticks.ToString();
			string msg = ChatUtility.JoinToMsg(str6, nameValueCollection, objectGuid);
			if (!flag)
			{
				conn.PlaceUser.PushSTCMessageToRelativeConnections(msg);
			}
			else
			{
				base.Manager.PushSTCMessage(chatPlaceUser.Connection, msg);
				base.Manager.PushSTCMessage(conn, msg);
			}
			this.LogMessage(conn, identity, flag, str1, str2);
		}

		protected virtual void LogMessage(ChatConnection conn, ChatIdentity targetidentity, bool whisper, string text, string html)
		{
			base.Portal.DataManager.AsyncLogMessage(this, conn.Identity, targetidentity, whisper, text, html);
		}
	}
}