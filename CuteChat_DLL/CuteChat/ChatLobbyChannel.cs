using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;

namespace CuteChat
{
	public class ChatLobbyChannel : ChatChannel
	{
		private IChatLobby @ӓ;

		private ArrayList @Ӕ;

		public override bool AllowAnonymous
		{
			get
			{
				return this.@ӓ.AllowAnonymous;
			}
			set
			{
				this.@ӓ.AllowAnonymous = value;
				base.Portal.DataManager.UpdateLobby(this.Lobby);
			}
		}

		public override int AutoAwayMinute
		{
			get
			{
				int autoAwayMinute = this.@ӓ.AutoAwayMinute;
				if (autoAwayMinute <= 0)
				{
					return 2147483647;
				}
				return autoAwayMinute;
			}
			set
			{
				this.@ӓ.AutoAwayMinute = value;
				base.Portal.DataManager.UpdateLobby(this.@ӓ);
			}
		}

		public override int HistoryCount
		{
			get
			{
				int historyCount = this.@ӓ.HistoryCount;
				if (historyCount <= 0)
				{
					return 0;
				}
				return historyCount;
			}
			set
			{
				this.@ӓ.HistoryCount = value;
				base.Portal.DataManager.UpdateLobby(this.@ӓ);
			}
		}

		public override int HistoryDay
		{
			get
			{
				int historyDay = this.@ӓ.HistoryDay;
				if (historyDay <= 0)
				{
					return 1000;
				}
				return historyDay;
			}
			set
			{
				this.@ӓ.HistoryDay = value;
				base.Portal.DataManager.UpdateLobby(this.@ӓ);
			}
		}

		public IChatLobby Lobby
		{
			get
			{
				return this.@ӓ;
			}
		}

		public override string Location
		{
			get
			{
				return "Lobby";
			}
		}

		public override bool Locked
		{
			get
			{
				return this.@ӓ.Locked;
			}
			set
			{
				this.@ӓ.Locked = value;
				base.Portal.DataManager.UpdateLobby(this.Lobby);
			}
		}

		public override int MaxIdleMinute
		{
			get
			{
				int maxIdleMinute = this.@ӓ.MaxIdleMinute;
				if (maxIdleMinute <= 0)
				{
					return 2147483647;
				}
				return maxIdleMinute;
			}
			set
			{
				this.@ӓ.MaxIdleMinute = value;
				base.Portal.DataManager.UpdateLobby(this.@ӓ);
			}
		}

		public override int MaxOnlineCount
		{
			get
			{
				int maxOnlineCount = this.@ӓ.MaxOnlineCount;
				if (maxOnlineCount <= 0)
				{
					return 2147483647;
				}
				return maxOnlineCount;
			}
			set
			{
				this.@ӓ.MaxOnlineCount = value;
				base.Portal.DataManager.UpdateLobby(this.@ӓ);
			}
		}

		public override string Password
		{
			get
			{
				return this.@ӓ.Password;
			}
			set
			{
				this.@ӓ.Password = value;
				base.Portal.DataManager.UpdateLobby(this.Lobby);
			}
		}

		public override string PlaceTitle
		{
			get
			{
				return this.@ӓ.Title;
			}
		}

		public ChatLobbyChannel(ChatPortal portal, string name, IChatLobby lobby) : base(portal, name)
		{
			if (lobby == null)
			{
				throw new ArgumentNullException("lobby");
			}
			this.@ӓ = lobby;
		}

		private void @ӕ()
		{
			if (this.@Ӕ != null)
			{
				return;
			}
			ArrayList arrayLists = new ArrayList();
			string[] strArrays = this.@ӓ.ManagerList.Split(new char[] { ',' });
			for (int i = 0; i < (int)strArrays.Length; i++)
			{
				string str = strArrays[i];
				if (str.Length != 0)
				{
					arrayLists.Add(str.ToLower());
				}
			}
			this.@Ӕ = arrayLists;
		}

		public override void AddModerator(string userid)
		{
			this.@ӕ();
			if (this.@Ӕ.Contains(userid.ToLower()))
			{
				return;
			}
			if (this.@ӓ.ManagerList.Length != 0)
			{
				IChatLobby chatLobby = this.@ӓ;
				chatLobby.ManagerList = string.Concat(chatLobby.ManagerList, ",", userid);
			}
			else
			{
				this.@ӓ.ManagerList = userid;
			}
			base.Portal.DataManager.UpdateLobby(this.@ӓ);
		}

		public override bool IsModerator(ChatIdentity identity)
		{
			this.@ӕ();
			if (this.@Ӕ.Contains(identity.UniqueId.ToLower()))
			{
				return true;
			}
			if (base.Portal.DataManager.IsLobbyAdminId(this.@ӓ, identity.UniqueId))
			{
				return true;
			}
			return base.Portal.DataManager.IsAdministratorId(identity.UniqueId);
		}

		protected internal override void LoadHistoryMessages(ChatConnection conn)
		{
			base.Manager.PushSTCMessage(conn, "HISTORY_BEGIN", null, new string[0]);
			ChatDataManager dataManager = base.Portal.DataManager;
			string placeName = base.PlaceName;
			int historyCount = this.HistoryCount;
			DateTime now = DateTime.Now;
			ChatMsgData[] chatMsgDataArray = dataManager.LoadChannelHistoryMessages(placeName, historyCount, now.Subtract(TimeSpan.FromDays((double)this.HistoryDay)));
			for (int i = 0; i < (int)chatMsgDataArray.Length; i++)
			{
				ChatMsgData chatMsgDatum = chatMsgDataArray[i];
				if (chatMsgDatum.Whisper)
				{
					string uniqueId = conn.Identity.UniqueId;
					if (string.Compare(uniqueId, chatMsgDatum.SenderId, true) != 0 && string.Compare(uniqueId, chatMsgDatum.TargetId, true) != 0)
					{
						//goto Label0;
					}
				}
				ChatManager manager = base.Manager;
				ChatConnection chatConnection = conn;
				string[] senderId = new string[] { "", chatMsgDatum.SenderId, chatMsgDatum.Sender, chatMsgDatum.Text, chatMsgDatum.Html, chatMsgDatum.TargetId, chatMsgDatum.Target, null, null };
				senderId[7] = (chatMsgDatum.Whisper ? "1" : "0");
				now = chatMsgDatum.Time.ToUniversalTime();
				senderId[8] = now.Ticks.ToString();
				manager.PushSTCMessage(chatConnection, "USER_MESSAGE", null, senderId);
			//Label0:
			}
			base.Manager.PushSTCMessage(conn, "HISTORY_END", null, new string[0]);
		}

		public void OnLobbyDeleted()
		{
			base.Portal.@Ӛ(this);
		}

		public void OnLobbyUpdated(IChatLobby newlobby)
		{
			this.@ӓ = newlobby;
			this.@Ӕ = null;
		}

		public override bool PreProcessCTSMessage(ChatConnection conn, string msgid, string[] args, NameValueCollection nvc)
		{
			if (msgid == "MODERATOR_COMMAND")
			{
				if (!conn.PlaceUser.IsModerator)
				{
					return false;
				}
				string str = args[0];
				if (str == "SETMODERATORMODE")
				{
					bool flag = args[1] == "1";
					if (base.ModerateMode == flag)
					{
						return true;
					}
					if (!flag)
					{
						ChatPlaceItem[] allItems = base.GetAllItems();
						for (int i = 0; i < (int)allItems.Length; i++)
						{
							ChatPlaceItem chatPlaceItem = allItems[i];
							if (chatPlaceItem is ChatModerateItem)
							{
								base.Manager.RemovePlaceItem(chatPlaceItem, "REJECT");
							}
						}
					}
					base.ModerateMode = flag;
					base.SendUpdatedMessage();
					return true;
				}
				if (str == "ACCEPTMODERATEITEM")
				{
					ChatModerateItem chatModerateItem = (ChatModerateItem)base.FindItem(new ChatGuid(args[1]));
					if (chatModerateItem == null)
					{
						return true;
					}
					base.Manager.RemovePlaceItem(chatModerateItem, "ACCEPT");
					if (base.FindConnection(chatModerateItem.User.Connection.ObjectGuid) != null)
					{
						base.Manager.HandleCTSMessage(chatModerateItem.User.Connection, chatModerateItem.MsgId, chatModerateItem.Nvc, chatModerateItem.Args);
						base.Manager.PushSTCMessage(chatModerateItem.User.Connection, "SYS_INFO_MESSAGE", null, new string[] { "MODERATEMESSAGEACCEPTED" });
					}
					return true;
				}
				if (str == "REJECTMODERATEITEM")
				{
					ChatModerateItem chatModerateItem1 = (ChatModerateItem)base.FindItem(new ChatGuid(args[1]));
					if (chatModerateItem1 == null)
					{
						return true;
					}
					base.Manager.RemovePlaceItem(chatModerateItem1, "REJECT");
					if (base.FindConnection(chatModerateItem1.User.Connection.ObjectGuid) != null)
					{
						base.Manager.PushSTCMessage(chatModerateItem1.User.Connection, "SYS_INFO_MESSAGE", null, new string[] { "MODERATEMESSAGEREJECTED" });
					}
					return true;
				}
				if (str == "POSTQUESTION")
				{
					conn.PlaceUser.PushSTCMessageToRelativeConnections("POSTQUESTION", null, new string[] { args[1] });
					return true;
				}
			}
			if (!(msgid == "USER_COMMAND") || !(args[0] == "ANSWERQUESTION"))
			{
				if (!(msgid == "USER_MESSAGE") || !base.ModerateMode || this.IsModerator(conn.Identity) || !(conn.PlaceUser.Level == "Normal"))
				{
					return base.PreProcessCTSMessage(conn, msgid, args, nvc);
				}
				ChatModerateItem chatModerateItem2 = new ChatModerateItem(this)
				{
					User = conn.PlaceUser,
					MsgId = msgid,
					Nvc = nvc,
					Args = args
				};
				base.Manager.AddPlaceItem(chatModerateItem2);
				return true;
			}
			if (!conn.PlaceUser.IsModerator && conn.PlaceUser.Level != "Speaker")
			{
				base.Manager.PushSTCMessage(conn, "SYS_ERROR_MESSAGE", null, new string[] { "INVALIDIDENTITY" });
				return true;
			}
			string str1 = string.Concat("Q : ", args[1], " \r\nA : ", args[2]);
			base.Manager.HandleCTSUserMessage(conn, null, new string[] { str1, null, null, null, null });
			return true;
		}

		public override void RemoveModerator(string userid)
		{
			this.@ӕ();
			if (this.@Ӕ.Contains(userid.ToLower()))
			{
				StringBuilder stringBuilder = new StringBuilder();
				string[] strArrays = this.Lobby.ManagerList.Split(new char[] { ',' });
				for (int i = 0; i < (int)strArrays.Length; i++)
				{
					string str = strArrays[i];
					if (str.Length != 0 && string.Compare(userid, str, true) != 0)
					{
						if (stringBuilder.Length != 0)
						{
							stringBuilder.Append(",");
						}
						stringBuilder.Append(str);
					}
				}
				this.@ӓ.ManagerList = stringBuilder.ToString();
				base.Portal.DataManager.UpdateLobby(this.@ӓ);
			}
		}

		protected internal override void SendInitializeMessages(ChatConnection conn)
		{
			base.SendInitializeMessages(conn);
			string announcement = this.@ӓ.Announcement;
			if (announcement != null && announcement.Length != 0)
			{
				base.Manager.PushSTCMessage(conn, "SYS_INFO_MESSAGE", null, new string[] { announcement });
			}
		}
	}
}