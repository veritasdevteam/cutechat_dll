using System;

namespace CuteChat
{
	public interface IAsyncDataHandler
	{
		void Process(IChatDataProvider provider);
	}
}