using System;

namespace CuteChat
{
	[Serializable]
	public class ChatCookie
	{
		public int ResponseId;

		public ChatGuid ConnectionId;

		public Guid ConnectionKey = Guid.Empty;

		public string GuestName;

		public string Password;

		public ChatCookie()
		{
		}
	}
}