using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace CuteChat
{
	public class ChatUtility
	{
		private static string[] @ӓ;

		private static char[] @Ӕ;

		private static string @ӕ;

		static ChatUtility()
		{
			ChatUtility.@ӓ = new string[0];
			ChatUtility.@Ӕ = new char[] { '\u005E', ';', ':', '|', '<', '>', '#', '/', '%', '?', '!' };
			ChatUtility.@ӕ = "0123456789ABCDEF";
		}

		public ChatUtility()
		{
		}

		private static string @ӗ(string u0040ӓ)
		{
			if (u0040ӓ.IndexOfAny(ChatUtility.@Ӕ) != -1)
			{
				u0040ӓ = u0040ӓ.Replace("^", "^0");
				u0040ӓ = u0040ӓ.Replace(";", "^1");
				u0040ӓ = u0040ӓ.Replace(":", "^2");
				u0040ӓ = u0040ӓ.Replace("|", "^3");
				u0040ӓ = u0040ӓ.Replace("<", "^4");
				u0040ӓ = u0040ӓ.Replace(">", "^5");
				u0040ӓ = u0040ӓ.Replace("#", "^6");
				u0040ӓ = u0040ӓ.Replace("/", "^7");
				u0040ӓ = u0040ӓ.Replace("%", "^8");
				u0040ӓ = u0040ӓ.Replace("?", "^9");
				u0040ӓ = u0040ӓ.Replace("!", "^a");
			}
			return u0040ӓ;
		}

		//private static void @Ӗ(HttpContext u0040ӓ, string u0040Ӕ)
		//{
		//	if (string.IsNullOrEmpty(u0040Ӕ))
		//	{
		//		return;
		//	}
		//	if (u0040Ӕ.IndexOfAny("<>'\"".ToCharArray()) != -1)
		//	{
		//		u0040ӓ.Response.Clear();
		//		u0040ӓ.Response.StatusCode = 404;
		//		u0040ӓ.Response.ContentType = "text/html";
		//		u0040ӓ.Response.Write("404-not-found");
		//		u0040ӓ.Response.Flush();
		//		u0040ӓ.Response.Close();
		//		u0040ӓ.Response.End();
		//		throw new Exception("end");
		//	}
		//}

		private static string @ә(string u0040ӓ, Random u0040Ӕ, int u0040ӕ)
		{
			int num = u0040Ӕ.Next(10) + u0040ӕ;
			if (num < 0)
			{
				return null;
			}
			if (num == 0)
			{
				return "";
			}
			char[] chrArray = new char[num];
			for (int i = 0; i < num; i++)
			{
				chrArray[i] = u0040ӓ[u0040Ӕ.Next(u0040ӓ.Length - 1)];
			}
			return new string(chrArray);
		}

		private static string @Ә(string u0040ӓ)
		{
			if (u0040ӓ.IndexOf('\u005E') != -1)
			{
				u0040ӓ = u0040ӓ.Replace("^a", "!");
				u0040ӓ = u0040ӓ.Replace("^9", "?");
				u0040ӓ = u0040ӓ.Replace("^8", "%");
				u0040ӓ = u0040ӓ.Replace("^7", "/");
				u0040ӓ = u0040ӓ.Replace("^6", "#");
				u0040ӓ = u0040ӓ.Replace("^5", ">");
				u0040ӓ = u0040ӓ.Replace("^4", "<");
				u0040ӓ = u0040ӓ.Replace("^3", "|");
				u0040ӓ = u0040ӓ.Replace("^2", ":");
				u0040ӓ = u0040ӓ.Replace("^1", ";");
				u0040ӓ = u0040ӓ.Replace("^0", "^");
			}
			return u0040ӓ;
		}

		public static void CheckQueryString()
		{
			//NameValueCollection queryString = context.Request.QueryString;
			//string[] allKeys = queryString.AllKeys;
			//for (int i = 0; i < (int)allKeys.Length; i++)
			//{
			//	string item = queryString[allKeys[i]];
			//	if (!string.IsNullOrEmpty(item))
			//	{
			//		ChatUtility.@Ӗ(context, item);
			//	}
			//}
			//string query = context.Request.Url.Query;
			//if (query != null)
			//{
			//	query = query.TrimStart(new char[] { '?' });
			//}
			//ChatUtility.@Ӗ(context, query);
		}

		public static string EncodeJScript(string str)
		{
			StringBuilder stringBuilder = null;
			int length = str.Length;
			for (int i = 0; i < str.Length; i++)
			{
				char chr = str[i];
				if (chr == '\\' || chr == '\"' || chr == '\'' || chr == '>' || chr == '<' || chr == '&' || chr == '\r' || chr == '\n')
				{
					if (stringBuilder == null)
					{
						stringBuilder = new StringBuilder();
						if (i > 0)
						{
							stringBuilder.Append(str, 0, i);
						}
					}
					if (chr == '\\')
					{
						stringBuilder.Append("\\x5C");
					}
					else if (chr == '\"')
					{
						stringBuilder.Append("\\x22");
					}
					else if (chr == '\'')
					{
						stringBuilder.Append("\\x27");
					}
					else if (chr == '\r')
					{
						stringBuilder.Append("\\x0D");
					}
					else if (chr == '\n')
					{
						stringBuilder.Append("\\x0A");
					}
					else if (chr == '<')
					{
						stringBuilder.Append("\\x3C");
					}
					else if (chr == '>')
					{
						stringBuilder.Append("\\x3E");
					}
					else if (chr != '&')
					{
						char chr1 = chr;
						int num = chr1 & '\u000F';
						int num1 = (chr1 & 'ð') / 16;
						stringBuilder.Append("\\x");
						stringBuilder.Append(ChatUtility.@ӕ[num1]);
						stringBuilder.Append(ChatUtility.@ӕ[num]);
					}
					else
					{
						stringBuilder.Append("\\x26");
					}
				}
				else if (stringBuilder != null)
				{
					stringBuilder.Append(chr);
				}
			}
			if (stringBuilder == null)
			{
				return str;
			}
			return stringBuilder.ToString();
		}

		public static string EncodeJScriptString(string str, char quote)
		{
			if (str == null)
			{
				return "null";
			}
			return string.Concat(quote.ToString(), ChatUtility.EncodeJScript(str), quote.ToString());
		}

		public static string EncodeJScriptString(string str)
		{
			return ChatUtility.EncodeJScriptString(str, '\"');
		}

		public static string EncodeJScriptString(string str, bool usesinglequote)
		{
			return ChatUtility.EncodeJScriptString(str, (usesinglequote ? '\'' : '\"'));
		}

		public static string EncodeJScriptString(string[] strs)
		{
			if (strs == null)
			{
				return "null";
			}
			ArrayList arrayLists = new ArrayList();
			string[] strArrays = strs;
			for (int i = 0; i < (int)strArrays.Length; i++)
			{
				string str = strArrays[i];
				arrayLists.Add(ChatUtility.EncodeJScriptString(str));
			}
			return string.Concat("[", string.Join(",", (string[])arrayLists.ToArray(typeof(string))), "]");
		}

		public static string GetProperties(NameValueCollection props)
		{
			StringBuilder stringBuilder = new StringBuilder();
			foreach (string key in props.Keys)
			{
				string item = props[key];
				if (item == null)
				{
					continue;
				}
				if (stringBuilder.Length != 0)
				{
					stringBuilder.Append("|");
				}
				//key = ChatUtility.@ӗ(key);
				//item = ChatUtility.@ӗ(item);
				//stringBuilder.Append(key).Append(":").Append(item);
			}
			if (stringBuilder.Length == 0)
			{
				return null;
			}
			return stringBuilder.ToString();
		}

		public static NameValueCollection GetProperties(string str)
		{
			if (str == null)
			{
				return null;
			}
			if (str.Length == 0)
			{
				return null;
			}
			NameValueCollection nameValueCollection = new NameValueCollection();
			string[] strArrays = str.Split(new char[] { '|' });
			for (int i = 0; i < (int)strArrays.Length; i++)
			{
				string[] strArrays1 = strArrays[i].Split(new char[] { ':' });
				string str1 = ChatUtility.@Ә(strArrays1[0]);
				nameValueCollection.Add(str1, ChatUtility.@Ә(strArrays1[1]));
			}
			return nameValueCollection;
		}

		public static string JoinToMsg(string msgid, NameValueCollection nvc, params string[] args)
		{
			string properties = null;
			if (nvc != null && nvc.Count != 0)
			{
				properties = ChatUtility.GetProperties(nvc);
			}
			if (args == null || args.Length == 0)
			{
				if (properties == null)
				{
					return msgid;
				}
				return string.Concat(msgid, ";", properties);
			}
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(msgid);
			stringBuilder.Append(";");
			stringBuilder.Append(properties);
			for (int i = 0; i < (int)args.Length; i++)
			{
				stringBuilder.Append(';');
				string str = args[i];
				if (str != null && str.Length != 0)
				{
					str = ChatUtility.@ӗ(str);
					stringBuilder.Append(str);
				}
			}
			return stringBuilder.ToString();
		}

		public static void SplitMsg(string msg, out string msgid, out NameValueCollection nvc, out string[] args)
		{
			nvc = null;
			args = ChatUtility.@ӓ;
			int num = msg.IndexOf(';');
			if (num == -1)
			{
				msgid = msg;
				return;
			}
			msgid = msg.Substring(0, num);
			msg = msg.Substring(num + 1);
			num = msg.IndexOf(';');
			if (num == -1)
			{
				nvc = ChatUtility.GetProperties(msg);
				return;
			}
			nvc = ChatUtility.GetProperties(msg.Substring(0, num));
			string[] strArrays = msg.Substring(num + 1).Split(new char[] { ';' });
			for (int i = 0; i < (int)strArrays.Length; i++)
			{
				string str = strArrays[i];
				if (str.IndexOf('\u005E') != -1)
				{
					str = ChatUtility.@Ә(str);
					strArrays[i] = str;
				}
			}
			args = strArrays;
		}

		public static string StripScript(string html)
		{
			if (html == null || html == "")
			{
				return html;
			}
			return Regex.Replace(html, "<script((.|\n)*?)(</script>)?", "", RegexOptions.IgnoreCase | RegexOptions.Multiline);
		}

		public static void TestSplitJoinToMsg()
		{
			string str;
			NameValueCollection nameValueCollection;
			string[] strArrays;
			string str1 = "\r\n\t ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890`-=\\[];',./~!@#$%^&*()_+|{}:\"<>?";
			int length = str1.Length;
			Random random = new Random(Guid.NewGuid().GetHashCode());
			string str2 = ChatUtility.@ә(str1, random, 1000);
			string str3 = ChatUtility.@Ә(ChatUtility.@ӗ(str2));
			if (str2 != str3)
			{
				throw new Exception(string.Concat("error on encode. ", str2, " ======== ", str3));
			}
			string str4 = random.Next().ToString();
			int num = random.Next(15) - 5;
			NameValueCollection nameValueCollection1 = null;
			if (num >= 0)
			{
				nameValueCollection1 = new NameValueCollection();
				for (int i = 0; i < num; i++)
				{
					string str5 = ChatUtility.@ә(str1, random, 5);
					nameValueCollection1.Add(str5, ChatUtility.@ә(str1, random, -5));
				}
			}
			int num1 = random.Next(15) - 5;
			string[] strArrays1 = null;
			if (num1 >= 0)
			{
				strArrays1 = new string[num1];
				for (int j = 0; j < num1; j++)
				{
					strArrays1[j] = ChatUtility.@ә(str1, random, -5);
				}
			}
			if (nameValueCollection1 != null)
			{
				string properties = ChatUtility.GetProperties(nameValueCollection1);
				if (properties != null)
				{
					string properties1 = ChatUtility.GetProperties(ChatUtility.GetProperties(properties));
					if (properties != properties1)
					{
						throw new Exception(string.Concat("error on props. ", properties, " ======== ", properties1));
					}
				}
			}
			string msg = ChatUtility.JoinToMsg(str4, nameValueCollection1, strArrays1);
			ChatUtility.SplitMsg(msg, out str, out nameValueCollection, out strArrays);
			string msg1 = ChatUtility.JoinToMsg(str, nameValueCollection, strArrays);
			if (msg != msg1)
			{
				throw new Exception(string.Concat("error on join. ", msg, " ======== ", msg1));
			}
			if (str4 != str)
			{
				throw new Exception(string.Concat("error on msgid. ", str4, " ======== ", str));
			}
			if (nameValueCollection1 != null)
			{
				if (nameValueCollection != null)
				{
					foreach (string key in nameValueCollection1.Keys)
					{
						if (nameValueCollection1[key] == nameValueCollection[key])
						{
							continue;
						}
						if (nameValueCollection1[key] != null)
						{
							throw new Exception("error on nvc.");
						}
						if (nameValueCollection[key] == "")
						{
							continue;
						}
						throw new Exception("error on nvc.");
					}
				}
				else
				{
					foreach (string key1 in nameValueCollection1.Keys)
					{
						if (nameValueCollection1[key1] == null)
						{
							continue;
						}
						throw new Exception("error on nvc.");
					}
				}
			}
			else if (nameValueCollection != null)
			{
				throw new Exception("error on nvc.");
			}
			if (strArrays1 == null)
			{
				if (strArrays != null && strArrays.Length != 0)
				{
					throw new Exception(string.Concat("error on args ", (int)strArrays.Length));
				}
				return;
			}
			if (strArrays == null)
			{
				throw new Exception("error on args");
			}
			if ((int)strArrays1.Length != (int)strArrays.Length)
			{
				throw new Exception("error on args");
			}
			for (int k = 0; k < (int)strArrays1.Length; k++)
			{
				if (strArrays1[k] != strArrays[k])
				{
					if (strArrays1[k] != null)
					{
						throw new Exception("error on args");
					}
					if (strArrays[k] != "")
					{
						throw new Exception("error on args");
					}
				}
			}
		}
	}
}