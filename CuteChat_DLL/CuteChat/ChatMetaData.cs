using System;

namespace CuteChat
{
	public class ChatMetaData
	{
		public const string DisconnectReason_EXPIRED = "EXPIRED";

		public const string DisconnectReason_IDLE = "IDLE";

		public const string DisconnectReason_BYCLIENT = "BYCLIENT";

		public const string DisconnectReason_OVERRIDE = "OVERRIDE";

		public const string DisconnectReason_KICK = "KICK";

		public const string DisconnectReason_REJECTED = "REJECTED";

		public const string ReturnCode_NOPLACE = "NOPLACE";

		public const string ReturnCode_READY = "READY";

		public const string ReturnCode_ERROR = "ERROR";

		public const string ReturnCode_INQUEUE = "INQUEUE";

		public const string ReturnCode_KICK = "KICK";

		public const string ReturnCode_REMOVED = "REMOVED";

		public const string ReturnCode_REJECTED = "REJECTED";

		public const string ReturnCode_NEEDLOGIN = "NEEDLOGIN";

		public const string ReturnCode_NEEDNAME = "NEEDNAME";

		public const string ReturnCode_LOCKED = "LOCKED";

		public const string ReturnCode_NOTENABLE = "NOTENABLE";

		public const string ReturnCode_NEEDPASSWORD = "NEEDPASSWORD";

		public const string ReturnCode_LIMITED = "LIMITED";

		public const string ReturnCode_IDEXISTS = "IDEXISTS";

		public const string ReturnCode_NOCONNECTION = "NOCONNECTION";

		public const string ServerMsg_CUSTOM = "CUSTOM";

		public const string ServerMsg_ADDON = "ADDON";

		public const string ServerMsg_PLACE_UPDATED = "PLACE_UPDATED";

		public const string ServerMsg_MYINFO_UPDATED = "MYINFO_UPDATED";

		public const string ServerMsg_USER_UPDATED = "USER_UPDATED";

		public const string ServerMsg_USER_REMOVED = "USER_REMOVED";

		public const string ServerMsg_USER_TYPING = "USER_TYPING";

		public const string ServerMsg_USER_DOEMOTE = "USER_DOEMOTE";

		public const string ServerMsg_USER_MESSAGE = "USER_MESSAGE";

		public const string ServerMsg_SYS_INFO_MESSAGE = "SYS_INFO_MESSAGE";

		public const string ServerMsg_SYS_ANNOUNCEMENT = "SYS_ANNOUNCEMENT";

		public const string ServerMsg_SYS_ERROR_MESSAGE = "SYS_ERROR_MESSAGE";

		public const string ServerMsg_SYS_ALERT_MESSAGE = "SYS_ALERT_MESSAGE";

		public const string ServerMsg_SYS_DEBUG_MESSAGE = "SYS_DEBUG_MESSAGE";

		public const string ServerMsg_USER_RENAME = "USER_RENAME";

		public const string ClientMsg_CUSTOM = "CUSTOM";

		public const string ClientMsg_ADDON = "ADDON";

		public const string ClientMsg_USER_TYPING = "USER_TYPING";

		public const string ClientMsg_USER_MESSAGE = "USER_MESSAGE";

		public const string ClientMsg_USER_COMMAND = "USER_COMMAND";

		public const string ClientMsg_USER_BROADCAST = "USER_BROADCAST";

		public const string ClientMsg_MODERATOR_COMMAND = "MODERATOR_COMMAND";

		public ChatMetaData()
		{
		}
	}
}