using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Xml;

namespace CuteChat
{
	public class SupportCustomerItem : ChatPlaceItem
	{
		private ChatIdentity @ӓ;

		private static bool @Ӕ;

		private DateTime @ӕ = DateTime.Now;

		public string Status = "VISIT";

		public ChatIdentity Agent;

		public string DisplayName;

		public string Email;

		public string Department;

		public string Question;

		public string CustomData;

		public string Culture;

		public string Browser;

		public string Platform;

		public string IPAddress;

		public string Url;

		private string @Ӗ;

		public DateTime BeginTime = DateTime.Now;

		private int @ӗ = -1;

		public DateTime ActiveTime
		{
			get
			{
				return this.@ӕ;
			}
		}

		public ChatIdentity Identity
		{
			get
			{
				return this.@ӓ;
			}
		}

		public string Referrer
		{
			get
			{
				return this.@Ӗ;
			}
			set
			{
				if (value == null || value == "")
				{
					return;
				}
				if (this.Url == null)
				{
					this.@Ӗ = value;
					return;
				}
				if (this.@Ӗ == null)
				{
					this.@Ӗ = value;
					return;
				}
				if (this.@Ә(this.Url, value))
				{
					return;
				}
				this.@Ӗ = value;
			}
		}

		static SupportCustomerItem()
		{
			SupportCustomerItem.@Ӕ = "True".Equals(ConfigurationSettings.AppSettings["CuteChat.HideVisitCustomer"]);
		}

		public SupportCustomerItem(ChatPlace place, ChatIdentity customerid) : base(place)
		{
			this.@ӓ = customerid;
			SupportSession lastSession = place.Portal.DataManager.GetLastSession(customerid.UniqueId);
			if (lastSession != null)
			{
				ChatPlaceUser chatPlaceUser = place.FindUser(lastSession.AgentUserId);
				if (chatPlaceUser != null)
				{
					this.Agent = chatPlaceUser.Identity;
				}
			}
		}

		private bool @Ә(string u0040ӓ, string u0040Ӕ)
		{
			int num = u0040ӓ.IndexOf("://");
			if (num == -1)
			{
				return false;
			}
			int num1 = u0040ӓ.IndexOf("/", num + 3);
			string str = (num1 == -1 ? u0040ӓ : u0040ӓ.Substring(0, num1));
			num = u0040Ӕ.IndexOf("://");
			if (num == -1)
			{
				return false;
			}
			num1 = u0040Ӕ.IndexOf("/", num + 3);
			return string.Compare(str, (num1 == -1 ? u0040Ӕ : u0040Ӕ.Substring(0, num1)), true) == 0;
		}

		public override void Dump(XmlWriter writer)
		{
			base.Dump(writer);
			writer.WriteAttributeString("status", this.Status);
			writer.WriteAttributeString("userid", this.Identity.UniqueId);
			writer.WriteAttributeString("username", this.Identity.DisplayName);
			writer.WriteAttributeString("displayname", this.DisplayName);
			writer.WriteAttributeString("begintime", this.BeginTime.ToString("yyyy-MM-dd HH:mm:ss"));
			writer.WriteAttributeString("activetime", this.ActiveTime.ToString("yyyy-MM-dd HH:mm:ss"));
			writer.WriteAttributeString("email", this.Email);
			writer.WriteAttributeString("department", this.Department);
			writer.WriteAttributeString("question", this.Question);
			writer.WriteAttributeString("culture", this.Culture);
			writer.WriteAttributeString("browser", this.Browser);
			writer.WriteAttributeString("platform", this.Platform);
			writer.WriteAttributeString("ipaddress", this.IPAddress);
			writer.WriteAttributeString("url", this.Url);
			writer.WriteAttributeString("referrer", this.Referrer);
			if (this.Agent != null)
			{
				writer.WriteAttributeString("agentid", this.Agent.UniqueId);
				writer.WriteAttributeString("agentname", this.Agent.DisplayName);
			}
		}

		protected override bool GetItemInfo(ChatConnection conn, string formsg, out string type, out string[] args)
		{
			string uniqueId = null;
			string displayName = null;
			if (this.Agent != null)
			{
				uniqueId = this.Agent.UniqueId;
				displayName = this.Agent.DisplayName;
			}
			string[] status = new string[20];
			status[0] = this.Identity.UniqueId;
			status[1] = this.Identity.DisplayName;
			status[2] = (this.Identity.IsAnonymous ? "1" : "0");
			status[3] = this.DisplayName;
			status[4] = uniqueId;
			status[5] = displayName;
			status[6] = this.Status;
			DateTime universalTime = this.BeginTime.ToUniversalTime();
			status[7] = universalTime.ToString("yyyy-MM-dd HH:mm:ss");
			universalTime = this.ActiveTime.ToUniversalTime();
			status[8] = universalTime.ToString("yyyy-MM-dd HH:mm:ss");
			status[9] = this.Email;
			status[10] = this.Department;
			status[11] = this.Question;
			status[12] = this.Culture;
			status[13] = this.Browser;
			status[14] = this.Platform;
			status[15] = this.IPAddress;
			status[16] = this.Url;
			status[17] = this.Referrer;
			status[18] = this.GetSessionCount().ToString();
			status[19] = this.CustomData;
			args = status;
			type = "CUSTOMER";
			return true;
		}

		public int GetSessionCount()
		{
			if (this.@ӗ == -1)
			{
				this.@ӗ = base.Portal.DataManager.GetCustomerSessionCount(this.Identity.UniqueId);
			}
			return this.@ӗ;
		}

		public override bool IsVisibleConnection(ChatConnection conn)
		{
			if (SupportCustomerItem.@Ӕ && this.Status == "VISIT" && !base.Portal.DataManager.IsAdministratorId(conn.Identity.UniqueId))
			{
				return false;
			}
			return true;
		}

		protected internal override void Maintain()
		{
			base.Maintain();
			TimeSpan now = DateTime.Now - this.ActiveTime;
			if (!(this.Status == "WAIT") || (DateTime.Now - this.ActiveTime).TotalSeconds <= 15 || (this.ActiveTime - this.BeginTime).TotalSeconds <= 5)
			{
				if (now.TotalSeconds > 30)
				{
					base.Place.Manager.RemovePlaceItem(this, "EXPIRED");
				}
				return;
			}
			base.Place.Manager.RemovePlaceItem(this, "EXPIRED");
			string str = string.Format("Customer - {0}\r\nEmail - {1}\r\nDepartment - {2}\r\nIP - {3}\r\nCulture - {4}\r\nWaited - {5}\r\nQuestion - {6}\r\nCustomData -{7}\r\n", new object[] { this.DisplayName, this.Email, this.Department, this.IPAddress, this.Culture, this.ActiveTime - this.BeginTime, this.Question, this.CustomData });
			((SupportAgentChannel)base.Portal.GetPlace("SupportAgent")).AddFeedback(new GeneralChatIdentity("SYSTEM", false, "SYSTEM:SYSTEM", "127.0.0.1"), "SYSTEM", this.Email, string.Concat("MISSED CONVERSATION : ", this.DisplayName, "/", this.Department), str);
		}

		public void SetActive()
		{
			this.@ӕ = DateTime.Now;
		}
	}
}