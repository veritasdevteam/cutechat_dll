using System;

namespace CuteChat
{
	public interface IChatLobby
	{
		bool AllowAnonymous
		{
			get;
			set;
		}

		string Announcement
		{
			get;
			set;
		}

		int AutoAwayMinute
		{
			get;
			set;
		}

		string AvatarChatURL
		{
			get;
			set;
		}

		string Description
		{
			get;
			set;
		}

		int HistoryCount
		{
			get;
			set;
		}

		int HistoryDay
		{
			get;
			set;
		}

		string Integration
		{
			get;
			set;
		}

		bool IsAvatarChat
		{
			get;
			set;
		}

		int LobbyId
		{
			get;
		}

		bool Locked
		{
			get;
			set;
		}

		string ManagerList
		{
			get;
			set;
		}

		int MaxIdleMinute
		{
			get;
			set;
		}

		int MaxOnlineCount
		{
			get;
			set;
		}

		string Password
		{
			get;
			set;
		}

		int SortIndex
		{
			get;
			set;
		}

		string Title
		{
			get;
			set;
		}

		string Topic
		{
			get;
			set;
		}
	}
}