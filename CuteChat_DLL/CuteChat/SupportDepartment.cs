using System;

namespace CuteChat
{
	[Serializable]
	public class SupportDepartment
	{
		private int _id;

		private string _name;

		private SupportAgent[] _agents;

		public SupportAgent[] Agents
		{
			get
			{
				if (this._agents == null)
				{
					this._agents = new SupportAgent[0];
				}
				return this._agents;
			}
			set
			{
				this._agents = value;
			}
		}

		public int DepartmentId
		{
			get
			{
				return this._id;
			}
			set
			{
				this._id = value;
			}
		}

		public string Name
		{
			get
			{
				return this._name;
			}
			set
			{
				this._name = value;
			}
		}

		public SupportDepartment()
		{
		}
	}
}