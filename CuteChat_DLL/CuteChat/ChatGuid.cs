using System;
using System.Threading;

namespace CuteChat
{
	[Serializable]
	public sealed class ChatGuid
	{
		private static object synclock;

		private static long _nextindex;

		private string _str;

		static ChatGuid()
		{
			ChatGuid.synclock = new object();
			ChatGuid._nextindex = (long)256;
		}

		public ChatGuid()
		{
			this._str = ChatGuid.AllocNewString();
		}

		public ChatGuid(string str)
		{
			if (str == null)
			{
				throw new ArgumentNullException("str");
			}
			this._str = str;
		}

		private static string AllocNewString()
		{
			long num = Interlocked.Increment(ref ChatGuid._nextindex);
			return string.Concat("_", num.ToString("X"));
		}

		public override bool Equals(object obj)
		{
			ChatGuid chatGuid = obj as ChatGuid;
			if (chatGuid == null)
			{
				return false;
			}
			return this._str == chatGuid._str;
		}

		public override int GetHashCode()
		{
			return this._str.GetHashCode();
		}

		public static bool operator ==(ChatGuid guid1, ChatGuid guid2)
		{
			if ((object)guid1 == (object)guid2)
			{
				return true;
			}
			if (guid1 == null)
			{
				return false;
			}
			if (guid2 == null)
			{
				return false;
			}
			return guid1._str == guid2._str;
		}

		public static implicit operator String(ChatGuid guid1)
		{
			if (guid1 == null)
			{
				return null;
			}
			return guid1._str;
		}

		public static bool operator !=(ChatGuid guid1, ChatGuid guid2)
		{
			return !(guid1 == guid2);
		}

		public override string ToString()
		{
			return this._str;
		}
	}
}