using System;
using System.IO;

namespace CuteChat
{
	public class GZip
	{
		public GZip()
		{
		}

		internal static byte[] @ӓ(byte[] u0040ӓ)
		{
			byte[] array;
			try
			{
				MemoryStream memoryStream = new MemoryStream();
				using (@Ւ _u0040Ւ = new @Ւ(memoryStream, false))
				{
					_u0040Ւ.Write(u0040ӓ, 0, (int)u0040ӓ.Length);
				}
				array = memoryStream.ToArray();
			}
			catch
			{
				array = u0040ӓ;
			}
			return array;
		}

		internal static byte[] @Ӕ(byte[] u0040ӓ)
		{
			byte[] numArray;
			int num;
			byte[] array;
			try
			{
				using (@Ց _u0040Ց = new @Ց(new MemoryStream(u0040ӓ)))
				{
					MemoryStream memoryStream = new MemoryStream();
					do
					{
						numArray = new byte[(int)u0040ӓ.Length * 2];
						num = _u0040Ց.Read(numArray, 0, (int)numArray.Length);
						if (num == 0)
						{
							break;
						}
						memoryStream.Write(numArray, 0, num);
					}
					while (num >= (int)numArray.Length);
					array = memoryStream.ToArray();
				}
			}
			catch
			{
				array = u0040ӓ;
			}
			return array;
		}

		public static Stream CreateGZipStream(Stream innerstream)
		{
			return GZip.CreateGZipStream(innerstream, false);
		}

		public static Stream CreateGZipStream(Stream innerstream, bool checkHeader)
		{
			Stream _u0040Ւ;
			try
			{
				if (innerstream == null)
				{
					throw new ArgumentNullException("innerstream");
				}
				_u0040Ւ = new @Ւ(innerstream, checkHeader);
			}
			catch
			{
				_u0040Ւ = innerstream;
			}
			return _u0040Ւ;
		}
	}
}