using System;

namespace CuteChat
{
	public delegate void EachInstantMessage(DateTime date, string senderid, string sendername, string targetid, string targetname, bool offline, string text, string html);
}