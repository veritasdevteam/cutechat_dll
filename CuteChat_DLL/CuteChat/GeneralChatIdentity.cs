using System;

namespace CuteChat
{
	public class GeneralChatIdentity : ChatIdentity
	{
		private string @ӓ;

		private bool @Ӕ;

		private string @ӕ;

		private string @Ӗ;

		public override string DisplayName
		{
			get
			{
				return this.@ӓ;
			}
		}

		public override string IPAddress
		{
			get
			{
				return this.@Ӗ;
			}
		}

		public override bool IsAnonymous
		{
			get
			{
				return this.@Ӕ;
			}
		}

		public override string UniqueId
		{
			get
			{
				return this.@ӕ;
			}
		}

		public GeneralChatIdentity(string name, bool isanonymous, string uniqueid, string ipaddress)
		{
			if (name == null || name.Length == 0)
			{
				throw new ArgumentNullException("name");
			}
			if (uniqueid == null || uniqueid.Length == 0)
			{
				throw new ArgumentNullException("uniqueid");
			}
			this.@ӓ = name;
			this.@Ӕ = isanonymous;
			this.@ӕ = uniqueid;
			this.@Ӗ = ipaddress;
			if (this.@Ӗ == null)
			{
				this.@Ӗ = "";
			}
		}
	}
}