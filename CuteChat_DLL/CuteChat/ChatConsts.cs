using System;

namespace CuteChat
{
	public class ChatConsts
	{
		public const bool DefaultDisplayLobbyList = true;

		public const bool DefaultDisplayHeaderPanel = true;

		public const bool DefaultDisplayLogo = true;

		public const bool DefaultDisplayTopBanner = true;

		public const bool DefaultDisplayBottomBanner = false;

		public const bool DefaultDisableWhisper = false;

		public const bool DefaultGlobalShowBoldButton = true;

		public const bool DefaultGlobalShowItalicButton = true;

		public const bool DefaultGlobalShowUnderlineButton = true;

		public const bool DefaultGlobalShowColorButton = true;

		public const bool DefaultGlobalShowFontName = true;

		public const bool DefaultGlobalShowFontSize = true;

		public const bool DefaultGlobalShowEmotion = true;

		public const bool DefaultGlobalShowControlPanel = true;

		public const bool DefaultGlobalShowSkinButton = true;

		public const bool DefaultGlobalShowSignoutButton = true;

		public const bool DefaultGlobalShowSystemMessages = true;

		public const bool DefaultGlobalShowSystemErrors = true;

		public const bool DefaultGlobalShowTypingIndicator = true;

		public const bool DefaultGlobalTextWrittenfromRighttoLeft = true;

		public const bool DefaultGlobalShowAvatarBeforeMessage = true;

		public const bool DefaultGlobalShowTimeStampWebMessenger = true;

		public const bool DefaultGlobalShowTimeStampBeforeMessage = false;

		public const bool DefaultGlobalEnableHtmlBox = true;

		public const bool DefaultGlobalAllowSendFile = true;

		public const string DefaultGlobalSendFileType = "png,gif,jpg";

		public const int DefaultGlobalSendFileSize = 1024000;

		public const bool DefaultSupportAllowSendFile = true;

		public const bool DefaultSupportAllowSendMail = true;

		public const string DefaultSupportSendFileType = "txt,chm,doc,xls,psd,pdf,ppt,zip,rar,gz,bmp,png,gif,jpg,swf,mp3,wmv,rm,rmvb";

		public const int DefaultSupportSendFileSize = 1024000;

		public const bool DefaultMessengerAllowSendFile = true;

		public const string DefaultMessengerSendFileType = "txt,chm,doc,xls,psd,pdf,ppt,zip,rar,gz,bmp,png,gif,jpg,swf,mp3,wmv,rm,rmvb";

		public const int DefaultMessengerSendFileSize = 1024000;

		public const bool DefaultGlobalAllowPrivateMessage = true;

		public const bool DefaultGlobalDenyAnonymous = false;

		public const string DefaultColorsArray = "#000000,#993300,#333300,#003300,#003366,#000080,#333399,#333333,#800000,#FF6600,#808000,#008000,#008080,#0000FF,#666699,#808080,#FF0000,#FF9900,#99CC00,#339966,#33CCCC,#3366FF,#800080,#999999,#FF00FF,#FFCC00,#FFFF00,#00FF00,#00FFFF,#00CCFF,#993366,#C0C0C0,#FF99CC,#FFCC99,#FFFF99,#CCFFCC,#CCFFFF,#99CCFF,#CC99FF,#FFFFFF";

		public const string DefaultEmotionsArray = "emsmile.gif,emteeth.gif,emwink.gif,emsmileo.gif,emsmilep.gif,emsmiled.gif,emangry.gif,emembarrassed.gif,emcrook.gif,emsad.gif,emcry.gif,emdgust.gif,emangel.gif,emlove.gif,emunlove.gif,emmessag.gif,emcat.gif,emdog.gif,emmoon.gif,emstar.gif,emfilm.gif,emnote.gif,emrose.gif,emrosesad.gif,emclock.gif,emlips.gif,emgift.gif,emcake.gif,emphoto.gif,emidea.gif,emtea.gif,emphone.gif,emhug.gif,emhug2.gif,embeer.gif,emcocktl.gif,emmale.gif,emfemale.gif,emthup.gif,emthdown.gif";

		public const string DefaultClientPath = "~/CuteSoft_Client/CuteChat/";

		public const string DefaultLoginUrl = "~/Login.aspx";

		public const string DefaultLogoutUrl = "~/";

		public const string DefaultLogoUrl = "images/logo.gif";

		public const string DefaultChannelSkins = "Normal,Royale,MacWhite,Indigo";

		public const int DefaultFloodControlCount = 5;

		public const int DefaultFloodControlDelay = 10;

		public const int DefaultMaxMSGLength = 10000;

		public const int DefaultMaxWordLength = 100;

		public static Guid OperatorChannelId
		{
			get
			{
				return new Guid(100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
			}
		}

		public ChatConsts()
		{
		}
	}
}