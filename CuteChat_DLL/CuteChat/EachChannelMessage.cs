using System;

namespace CuteChat
{
	public delegate void EachChannelMessage(DateTime date, string placename, string senderid, string sendername, string targetid, string targetname, bool whisper, string text, string html);
}