﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<script runat="server">

	protected void Page_Load(object sender, EventArgs e)
	{

	}
</script>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Demo Home</title>
	<style>
		form {
			margin:32px auto;
			width: 1100px;
			border: solid 2px #666;
			padding: 32px;
			font-size: 20px;
			font-family: -apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif;
		}
		code {
			color:darkgreen;
			font-weight:bold;
		}
		.auto-style1 {
			width: 1099px;
			height: 225px;
		}
		.auto-style2 {
			width: 971px;
			height: 268px;
		}
	</style>
</head>
<body>
	<form id="form1" runat="server">
		CuteLiveSupport Guest Integration Guide<br />
		<br />
		This guide introduces how to send the logon user information to CuteLiveSupport with a custom encryption way.<br />
		<br />
		After integration, the logon user don&#39;t need to type their name or email. And every session are keeped as a same identity , instead of random anonymous.<br />
		<br />
		In this demo website , there&#39;s 2 sample pages , which help developer to understand how to replace their logic easily.<br />
		<br />
		1 - <a href="Demo_Simple.aspx" target="_blank">Demo_Simple.aspx</a>
		<br />
		&nbsp;&nbsp;&nbsp; Show how to send the key/name/email in a very simple way.<br />
		<br />
		2 - <a href="Demo_WinAuth.aspx">Demo_WinAuth.aspx</a><br />
		&nbsp;&nbsp;&nbsp; This sample is more hard than the Demo_Simple<br />
		&nbsp;&nbsp;&nbsp; It use IIS+ASP.NET Windows Authentication to get the end-user identity.<br />
		&nbsp;&nbsp;&nbsp; The web.config specified &lt;authentication mode=&quot;Windows&quot; /&gt;
		<br />
		&nbsp;&nbsp;&nbsp; This website must deployed in IIS (not in IIS Expess) , and turn on the Windows Authentication feature to let it work.<br />
		&nbsp;&nbsp;&nbsp; Please use IE/Chrome for testing.<br />
		<br />
		<br />
		Please make sure you have downloaded and installed the last version of the CuteLiveSupport.<br />
		<br />
		Developers shall learn and understand how the 2 samples work , and replace the key/name by their own way.<br />
		<br />
		<br />
		<hr />

		<br />

		How it works :
		<br />
		<br />
		<strong>How the data be sent : </strong>
		<br />
		In the sample page , the script tag reference the Support-Visitor-monitor-crossdomain.js.aspx , which provide a JavaScript function&nbsp; <code>Chat_SetUserAuth(userkey,username,useremail)</code> <br />
		<br />
		The 3 parameters are for :<br />
		1 - userkey , MUST specified , the uniqueid of the user , and must be encrypted<br />
		2 - username , MUST specified , the display name of the user <br />
		3 - useremail , OPTIONAL, the email address of the user<br />
		<br />
		In the sample page , the userkey is encrypted by a very simple way , so that the developer can port the code to Java/PHP/etc.. easily.
		<br />

		The simple code use this key
		<br />
		<code>static string UserKeyEncryptBasicKey = &quot;mytestkey&quot;;</code><br />
		Which pair to the code in Global.asax of CuteLiveSupport website<br />
		<br />
		Please replace the key to your owner string in production server .<br />
		<br />
		<br />
		<strong>How the data be recieved:</strong><br />
		In the Global.asax of CuteLiveSupport website<br />
		There&#39;s a function :
		<br />
		<code>public override AppChatIdentity GetLogonIdentity()</code>
		<br />
		It use this way to get whether the userkey be passed or not:<br />
		<code>string userkey = GetQueryString(context.Request, &quot;UserKey&quot;);</code>
		<br />
		And decrypt the key like this :
		<br />
		<code>&nbsp;&nbsp;&nbsp;
		userkey = BasicDecrypt(userkey); 
		<br />
&nbs&nbsp;&nbsp;&nbsp; string username = GetQueryString(context.Request, &quot;UserName&quot;); 
		<br />
&nbsp;&nbsp;&nbsp; return new AppChatIdentity(username, true, &quot;Guest:@&quot; + userkey,<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; context.Request.UserHostAddress);<br />
			</code>
		<br />
		The devloper can replace the BasicEncrypt and BasicDescrypt for higher secure level.<br />
		The Global.asax also contains the same key :&nbsp;
		<br />
		<code>static string UserKeyEncryptBasicKey = &quot;mytestkey&quot;;</code><br />
		Don&#39;t forgot modify it also.<br />
		<br />
		<hr />
		<br />
		Integration Effect :
		<br />
		<br />
		1 - The users don&#39;t need to type their name any more. And also no need to type email if developer specified it.<br />
		<br />
		2 - In the Agent console , the user are identified as a known user , when mouse over it , the agent can get the key of the user.<br />
		<br />
		<img alt="" class="auto-style1" src="doc_images/graph01.png" /><br />
		<br />

		3 - In the database , the CustomerId will be saved as prefix &quot;Guest:@&quot; , so developer can integrate the chat logs with their membership system .<br />
		<br />
		<img alt="" class="auto-style2" src="doc_images/graph02.png" /><br />
		<br />
	</form>

</body>

</html>
