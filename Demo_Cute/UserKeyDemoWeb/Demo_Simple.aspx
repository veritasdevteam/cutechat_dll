﻿<%@ Page Language="C#" %>

<!DOCTYPE html>

<script runat="server">

	// general auth demo
	// now you get the logon user form the browser cookie and your database
	string thelogonuserkey = "user001";
	string thelogonusername = "Andy";
	string thelogonuseremail = "andy@email.com";

	// Please change the key , which match the same key in ChatProvider code
	static string UserKeyEncryptBasicKey = "mytestkey";

	protected void Page_Load(object sender, EventArgs e)
	{

	}
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Demo_Simple</title>
</head>
<body>
	<form id="form1" runat="server">

		<%--<h1><%=System.Security.Principal.WindowsIdentity.GetCurrent().Name %></h1>--%>
		<h1>Your Windows Name : <%=Context.User.Identity.Name %></h1>

		This is the Demo_Simple page
		<br />
		It demostrates how to pass userkey/username/useremail to CuteLiveSupport website<br />
		<br />
		<br />
		<span style="color:red">Please modify the code and correct the script link url host : http://localhost:56026/</span>
		<br />
		<br />
		<br />
		<div>
			Button Script:
			<br />
			<script src="http://localhost:56026/CuteSoft_Client/CuteChat/Support-Image-Button.js.aspx"></script>

		</div>

		<br />
		<br />
		<br />
		<div><a href="Default.aspx" target="_blank">Integration Documentation</a></div>
		<br />
		<br />
		<br />
	</form>

</body>


<script id="LiveSupportVisitorMonitorScript" src="http://localhost:56026/CuteSoft_Client/CuteChat/Support-Visitor-monitor-crossdomain.js.aspx"></script>
<script>
		<%


	Response.Write("var userkey='" + BasicEncrypt(thelogonuserkey) + "';\r\n");
	Response.Write("var username='" + thelogonusername + "';\r\n");
	Response.Write("var useremail='" + thelogonuseremail + "';\r\n");

		%>

	Chat_SetUserAuth(userkey, username, useremail);
</script>

</html>


<script runat="server">




	#region Encrypt Functions

	int EncryptLoopCount = 4;
	public int[] MakeArray(string str, bool random)
	{
		int len = (int)Math.Pow(2, Math.Floor(Math.Log(str.Length, 2)) + 1) + 8;
		if (len < 32) len = 32;

		int[] arr = new int[len];

		if (random)
		{
			Random r = new Random();
			for (int i = 0; i < arr.Length; i++)
				arr[i] = str[r.Next() % str.Length];

			int start = 1 + r.Next() % (len - str.Length - 2);

			for (int i = 0; i < str.Length; i++)
				arr[start + i] = str[i];

			arr[start - 1] = 0;
			arr[start + str.Length] = 0;
		}
		else
		{
			for (int i = 0; i < arr.Length; i++)
				arr[i] = str[i % str.Length];
		}

		return arr;
	}

	static public string BasicFormat(int[] vals)
	{
		StringBuilder sb = new StringBuilder();
		foreach (int i in vals)
		{
			if (sb.Length > 0)
				sb.Append("-");
			sb.Append(i);
		}
		return sb.ToString();
	}

	public string BasicEncrypt(string data)
	{
		return BasicEncrypt(data, UserKeyEncryptBasicKey);
	}

	public string BasicEncrypt(string data, string encryptkey)
	{
		if (string.IsNullOrEmpty(data))
			throw (new ArgumentNullException("data"));
		if (string.IsNullOrEmpty(encryptkey))
			throw (new ArgumentNullException("encryptkey"));

		int[] vals = MakeArray(data, true);
		int[] keys = MakeArray(encryptkey, false);

		for (int t = 0; t < EncryptLoopCount; t++)
		{
			for (int i = 0; i < vals.Length; i++)
			{
				int v = vals[i];
				int im = (v + i) % 5;

				for (int x = 0; x < vals.Length; x++)
				{
					if (x == i)
						continue;
					if (x % 5 != im)
						continue;

					for (int y = 0; y < keys.Length; y++)
					{
						int k = keys[y];
						if (k == 0)
							continue;

						vals[x] += v % k;
					}
				}
			}
		}
		return BasicFormat(vals);
	}

	#endregion


</script>
