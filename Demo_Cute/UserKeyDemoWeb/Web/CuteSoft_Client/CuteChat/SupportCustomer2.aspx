<%@ Page Language="c#" %>

<%@ Import Namespace="System.Web.Mail" %>
<%@ Import Namespace="CuteChat" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head runat="server">
	<title>Live Support</title>
	<link rel="icon" href="Icons/Support.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="Icons/Support.ico" type="image/x-icon" />
	<style type="text/css">
		body {
			background-color: #edf1fa;
		}

		body, td {
			font: 12px Arial, Helvetica, sans-serif;
		}
	</style>
	<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
</head>
<body bottommargin="0" topmargin="0" marginwidth="0" marginheight="0">
	Sorry, the application is not supported for your browser. 
	Please upgrade your browser and try again .
</body>
</html>
